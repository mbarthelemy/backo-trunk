using System;
using System.Runtime.InteropServices;
using System.Text;
using VDDKWrappers;
using System.IO;
using System.Configuration;
using P2PBackup.Common;

namespace VDDKWrappers {
	public class Main_test {

		static VixDiskLibConnectParams connParams;
		static IntPtr connPtr;

		public static void Main(string[] args){
			MountDisk();
			//RawDisk();
		}

		public static void MountDisk(){
			VixDiskLibGenericLogFunc logI = new VixDiskLibGenericLogFunc(LogI);
			VixDiskLibGenericLogFunc logW = new VixDiskLibGenericLogFunc(LogW);
			VixDiskLibGenericLogFunc logC = new VixDiskLibGenericLogFunc(LogC);

			VixDiskLibGenericLogFunc mntlogI = new VixDiskLibGenericLogFunc(LogI);
			VixDiskLibGenericLogFunc mntlogW = new VixDiskLibGenericLogFunc(LogW);
			VixDiskLibGenericLogFunc mntlogC = new VixDiskLibGenericLogFunc(LogC);

			string libdir = ConfigurationManager.AppSettings ["libdir"];
			string configfile = ConfigurationManager.AppSettings["configfile"];
			Console.WriteLine ("libdir="+libdir+", configfile="+configfile);

			VixError  error = VixDiskLib.InitEx(1, 2, logI, logW, logC, libdir, configfile);
			Console.WriteLine ("  ##### Result 2 (VixDiskLib_InitEx) : "+error.ToString());

			connParams = new VixDiskLibConnectParams();
			connParams.CredType = (uint)VixDiskLibCredType.VIXDISKLIB_CRED_UID;
			connParams.VixCredentials = new VixDiskLibCreds();
			connParams.VixCredentials.Uid = new VixDiskLibUidPasswdCreds();


			// ux
			connParams.ServerName = "192.168.0.27";
			connParams.VixCredentials.Uid.UserName = "root";
			connParams.VixCredentials.Uid.Password = "totototo";
			uint nbDisks = 1;
			// try to put the boot disk as first element, else inGuestMountPoints won't work,
			// ref : http://communities.vmware.com/thread/223740?start=0&tstart=0
			string[] diskNames = new string[nbDisks];
			string snapDisk1 = "[datastore2] vm2/vm2.vmdk";
			diskNames[0] = snapDisk1; 
			connParams.VmxSpec = "moref=2";
			string snapName = "2-snapshot-10";




			/*connParams.VmxSpec = "moref=3";
			string snapName = "3-snapshot-14";
			string snapDisk1 = "[datastore2] 2008R2/2008R2.vmdk";
			string snapDisk2 = "[datastore2] 2008R2/2008R2_1.vmdk";
			uint nbDisks = 2;
			// try to put the boot disk as first element, else inGuestMountPoints won't work,
			// ref : http://communities.vmware.com/thread/223740?start=0&tstart=0
			string[] diskNames = new string[nbDisks];
			diskNames[0] = snapDisk1; 
			diskNames[1] = snapDisk2; 
			 */



			// win
			/*connParams.ServerName = "172.29.9.50";
			connParams.VixCredentials.Uid.UserName = "admin";
			connParams.VixCredentials.Uid.Password = "PSn1+Elx";
			connParams.VmxSpec = "moref=vm-374";
			uint nbDisks = 1;
			string snapName = "snapshot-1129";
			string snapDisk1 = "[SAN_MSA2012FC_L19 (BackUp-Monitoring ENG)] WH0719v2/WH0719v2.vmdk";
			string[] diskNames = new string[nbDisks];
			diskNames[0] = snapDisk1; */

			
			connParams.Port = 902;
			connPtr = Marshal.AllocHGlobal(8);



			// if !utilities is uniclient
			//error =  VixDiskLib.VixDiskLib_ConnectEx(ref connParams, true, snapName, "nbd", out connPtr);
			// else 
			error = VixDiskLib.Connect(ref connParams, out connPtr);


			Console.WriteLine ("  ##### Result 3 (Connect to '"+snapName+"') : "+error.ToString()+", intPtr null : "+(connPtr == IntPtr.Zero));
			if(error != VixError.VIX_OK) return;

			/*uint nbCleanedUp = 0;
			uint nbRemaining = 0;
			error = VixDiskLib.Cleanup(ref connParams, ref nbCleanedUp, ref nbRemaining);
			Console.WriteLine ("  ##### Result 2 (VixDiskLib_Cleanup) : "+error.ToString()+", cleaned up "+nbCleanedUp+", remaining="+nbRemaining);
*/

			error = VixMntApi.Init(1,0, mntlogI, mntlogW, mntlogC, libdir, configfile);
			Console.WriteLine ("  ##### Result 5 (VixMntapi_Init) : "+error.ToString()+", intPtr null : "+(connPtr == IntPtr.Zero));
			if(error != VixError.VIX_OK) return;




			IntPtr diskSetPtr = Marshal.AllocHGlobal(8);
			error = VixMntApi.OpenDisks(connPtr, diskNames, nbDisks, (uint)VixDiskLib.VIXDISKLIB_FLAG_OPEN_READ_ONLY, ref diskSetPtr);
			//error = VixMntApi.VixMntapi_OpenDiskSet(
			Console.WriteLine ("  ##### Result 6 (VixMntapi_OpenDisks) : "+error.ToString()+", intPtr null : "+(diskSetPtr == IntPtr.Zero));
			if(error != VixError.VIX_OK){
				//Console.WriteLine (" ### ERROR : "+VixDiskLib.VixDiskLib_GetErrorText((ulong)error, null));
				CleanupAndDisconnect();
				return;
			}

			IntPtr  diskSetInfoPtr =  Marshal.AllocHGlobal(Marshal.SizeOf(typeof(VixDiskSetInfo)));
			error = VixMntApi.GetDiskSetInfo(diskSetPtr, ref diskSetInfoPtr);
			Console.WriteLine ("  ##### Result 6 (GetDiskSetInfo) : "+error.ToString()+", intPtr null : "+(diskSetPtr == IntPtr.Zero));
			if(error != VixError.VIX_OK){
				CleanupAndDisconnect();
				return;
			}
			VixDiskSetInfo diskSetInfo = (VixDiskSetInfo)Marshal.PtrToStructure(diskSetInfoPtr, typeof(VixDiskSetInfo));
			Console.WriteLine ("Mount Path  :"+diskSetInfo.MountPath);


			IntPtr vixOsInfoPtr = Marshal.AllocHGlobal(Marshal.SizeOf(typeof(VixOsInfo)));
			error = VixMntApi.GetOsInfo(diskSetPtr, ref vixOsInfoPtr);
			Console.WriteLine ("  ##### Result 6 (GetOsInfo) : "+error.ToString()+", intPtr null : "+(diskSetPtr == IntPtr.Zero));
			if(error == VixError.VIX_OK){
			VixOsInfo osInfo = (VixOsInfo)Marshal.PtrToStructure(vixOsInfoPtr, typeof(VixOsInfo));
				Console.WriteLine ("OS info : family="+osInfo.Family+ ", Version = "+osInfo.MajorVersion+"."+osInfo.MinorVersion);
			}
			int nbVol = 16;
			IntPtr[] volumeHandles = new IntPtr[nbVol];
			int arraySize = volumeHandles.Length;
			IntPtr buffer = Marshal.AllocHGlobal(Marshal.SizeOf(arraySize) * arraySize);
			Marshal.Copy(volumeHandles, 0, buffer, arraySize);
			long longNbVol = 0;
			error = VixMntApi.GetVolumeHandles(diskSetPtr, ref longNbVol, ref buffer); 
			Console.WriteLine ("  ##### Result 7 (VixMntapi_GetVolumeHandles) : "+error.ToString()+", intPtr null : "+(connPtr == IntPtr.Zero));
			//if(error != VixError.VIX_OK) return;
			Console.WriteLine ("  ##### Result 7 (VixMntapi_GetVolumeHandles) : Got "+longNbVol+" Volumes inside disk");

			IntPtr[] volHandles = new IntPtr[(int)longNbVol];
			for(int i=0; i< (int)longNbVol; i++)
				volHandles[i] = IntPtr.Zero;
			Marshal.Copy(buffer, volHandles, 0, (int)longNbVol);
			foreach(IntPtr volHandle in volHandles){
				if( volHandle != IntPtr.Zero){
					error = VixMntApi.MountVolume(volHandle, false);
					Console.WriteLine ("  ##### Result 8 (VixMntapi_MountVolume) : "+error.ToString()+", intPtr null : "+(connPtr == IntPtr.Zero));

					IntPtr volMountInfoPtr =  Marshal.AllocHGlobal(Marshal.SizeOf(typeof(VixVolumeInfo)));
					error = VixMntApi.GetVolumeInfo(volHandle, ref volMountInfoPtr);
					Console.WriteLine ("  ##### Result 8 (GetVolumeInfo) : "+error.ToString());
					if(error == VixError.VIX_OK){
						VixVolumeInfo volInfo = (VixVolumeInfo)Marshal.PtrToStructure(volMountInfoPtr, typeof(VixVolumeInfo));
						Console.WriteLine ("\t Mounted : "+volInfo.IsMounted);
						Console.WriteLine ("\t Symlink : "+volInfo.SymbolicLink);
						Console.WriteLine ("\t Type : "+volInfo.Type.ToString());
						Console.WriteLine ("\t NB Mountpoints : "+volInfo.NumGuestMountPoints);
						for (int i = 0; i < volInfo.NumGuestMountPoints; i++){
							IntPtr p = new IntPtr(volInfo.InGuestMountPoints.ToInt32() + sizeof(uint)*i);
							string s = Marshal.PtrToStringAnsi(p);
							Console.WriteLine ("\t Mounted as "+s);

						}

					}

				}

			}
			Console.ReadKey();

			foreach(IntPtr volHandle in volHandles){
				if(volHandle != IntPtr.Zero){

					error = VixMntApi.VixMntapi_DismountVolume(volHandle, true);
					Console.WriteLine ("  ##### Result 9 (VixMntapi_DismountVolume) : "+error.ToString()+", intPtr null : "+(connPtr == IntPtr.Zero));
				}

			}

			error = VixMntApi.CloseDiskSet(diskSetPtr);
			VixMntApi.Exit();
			Console.WriteLine ("  ##### Result 10 (VixMntapi_CloseDiskSet) : "+error.ToString()+", intPtr null : "+(connPtr == IntPtr.Zero));
			CleanupAndDisconnect();

			//VixDiskLib.VixDiskLib_Exit();
		}

		private static void CleanupAndDisconnect(){
			uint nbCleanedUp = 0;
			uint nbRemaining = 0;
			VixError error = VixDiskLib.Cleanup(ref connParams, ref nbCleanedUp, ref nbRemaining);
			Console.WriteLine ("  ##### Result 11 (VixDiskLib_Cleanup) : "+error.ToString()+", cleaned up "+nbCleanedUp+", remaining="+nbRemaining);

			error = VixDiskLib.Disconnect(connPtr);
			Console.WriteLine ("  ##### Result 12 (VixDiskLib_Disconnect) : "+error.ToString()+", intPtr null : "+(connPtr == IntPtr.Zero));

		}
	
		public static void RawDisk(){
			string libdir = ConfigurationManager.AppSettings ["libdir"];
			string configfile = ConfigurationManager.AppSettings["configfile"];
			Console.WriteLine ("libdir="+libdir+", configfile="+configfile);
			//1-snapshot-3
			VixError  error = VixDiskLib.InitEx(1, 2, null, null, null, libdir, configfile);
			Console.WriteLine ("  ##### Result 2 (VixDiskLib_InitEx) : "+error.ToString());

			VixDiskLibConnectParams connParams = new VixDiskLibConnectParams();
			connParams.CredType = (uint)VixDiskLibCredType.VIXDISKLIB_CRED_UID;
			connParams.VixCredentials = new VixDiskLibCreds();
			connParams.VixCredentials.Uid = new VixDiskLibUidPasswdCreds();

			connParams.ServerName = "192.168.0.27";
			connParams.VixCredentials.Uid.UserName = "root";
			connParams.VixCredentials.Uid.Password = "totototo";
			//connParams.VmxSpec = "vm2/vm2.vmx?dcPath=ha-datacenter&dsName=datastore2";
			connParams.VmxSpec = "moref=2";

			connParams.Port = 902;
			IntPtr connPtr = Marshal.AllocHGlobal(8);
			//connPtr =  IntPtr.Zero;
			//VixError error =  VixDiskLib.VixDiskLib_Connect(ref connParams, out connPtr);
			error =  VixDiskLib.VixDiskLib_ConnectEx(ref connParams, true, "2-snapshot-6", "san:hotadd:nbd", out connPtr);
			Console.WriteLine ("  ##### Result 3 (Connect) : "+error.ToString()+", intPtr null : "+(connPtr == IntPtr.Zero));


			error = VixDiskLib.PrepareForAccess(ref connParams, "backup_task");
			Console.WriteLine ("  ##### Result 3 (VixDiskLib_PrepareForAccess) : "+error.ToString());


			IntPtr diskPtr = Marshal.AllocHGlobal(8); //IntPtr.Zero;
			error = VixDiskLib.VixDiskLib_Open(connPtr, 
				@"[datastore2] vm2/vm2.vmdk", 
			    (uint)VixDiskLib.VIXDISKLIB_FLAG_OPEN_READ_ONLY, out diskPtr);


			Console.WriteLine ("  ##### Result 4 (Open) : "+error.ToString()+", intPtr null : "+(connPtr == IntPtr.Zero));
			if(error != VixError.VIX_OK){
				Console.WriteLine (" ### ERROR : "+VixDiskLib.GetErrorText((ulong)error, null));
				return;
			}
			/*string transport = VixDiskLib.VixDiskLib_GetTransportMode(diskPtr);
			Console.WriteLine ("transport :"+transport);*/

			//IntPtr keysBuf = Marshal.AllocHGlobal(4096);

			uint length = 129;
			IntPtr buf = IntPtr.Zero;
			//error = VixDiskLib.VixDiskLib_GetMetadataKeys(diskPtr, ref buf, 1, ref length);
			//Console.WriteLine ("  ##### Result 5 (VixDiskLib_GetMetadataKeys) : "+error.ToString()+", keys length="+length);
			//char[] realBuf = new char[length];
			uint dummy = 0;
			IntPtr buf2 =  Marshal.AllocHGlobal((int)length);
			//string keys = Marshal.PtrToStringAuto(keysBuf);
			error = VixDiskLib.GetMetadataKeys(diskPtr, ref buf2, length*8, ref dummy);
			Console.WriteLine ("  ##### Result 5 (VixDiskLib_GetMetadataKeys) : "+error.ToString()+", keys length="+length);
			Console.WriteLine ("lulute");
			Console.WriteLine ("Keys="+Marshal.PtrToStringAnsi(buf2));
			//char[] test = new char[length*2]
			//Marshal.PtrToStringAuto
			/*Console.WriteLine ("---- dumping metadata keys : -------");
			for(int i=0; i<length; i++)
				Console.Write*/

			/*FileStream vmdkfs  = new FileStream("test-backup-vmdk.vmdk", FileMode.CreateNew);
			ulong readSectors = 0;
			while (readSectors < 2097152){

				ulong nbSectToRead = 1000;
				if((2097152-readSectors) < 1000)
					nbSectToRead = 2097152-readSectors;
				byte[] dataBuf = new byte[512*nbSectToRead];
				error = VixDiskLib.VixDiskLib_Read(diskPtr, readSectors, nbSectToRead, dataBuf);
				vmdkfs.Write(dataBuf, 0, (int)nbSectToRead*512);
				readSectors += 1000;
			}*/









			//VixDiskLib.VixDiskLib_Disconnect(connPtr);

		}









		public static void Main_old(string[] args){

			Console.WriteLine ("Init 1");
			//Native.VixDiskLibGenericLogFunc infoNull = new Native.VixDiskLibGenericLogFunc();
			//Native.VixDiskLibGenericLogFunc warnNull = new Native.VixDiskLibGenericLogFunc();
			//Native.VixDiskLibGenericLogFunc critNul = new Native.VixDiskLibGenericLogFunc();
			/*VixDiskLibGenericLogFunc logI = new VixDiskLib.VixDiskLibGenericLogFunc(LogI);
			VixDiskLibGenericLogFunc logW = new VixDiskLib.VixDiskLibGenericLogFunc(LogW);
			VixDiskLibGenericLogFunc logC = new VixDiskLib.VixDiskLibGenericLogFunc(LogC);*/
			VixDiskLib.InitEx(1, 2, null, null, null, @"/usr/lib/vmware", @"/home/matt/dev/SharpBackup/VDDK-Wrappers/vddk.conf");

			VixDiskLibConnectParams connParams = new VixDiskLibConnectParams();
			connParams.CredType = (uint)VixDiskLibCredType.VIXDISKLIB_CRED_UID;
			connParams.VixCredentials = new VixDiskLibCreds();
			connParams.VixCredentials.Uid = new VixDiskLibUidPasswdCreds();


			string diskUrl = @"WH0719v2/WH0719v2.vmx?dcPath=Datacenter&dsName=SAN_MSA2012FC_L19 (BackUp-Monitoring ENG)";
			byte[] utf8Bytes = UTF8Encoding.UTF8.GetBytes(diskUrl);
			Encoding ANSI = Encoding.GetEncoding(1252);
			byte[] ansiBytes = Encoding.Convert(Encoding.UTF8, ANSI, utf8Bytes);
			String ansiString = ANSI.GetString(ansiBytes);


			//connParams.ServerName = "172.29.9.50";
			//connParams.VixCredentials.Uid.UserName = "admin";
			//connParams.VixCredentials.Uid.Password = "PSn1+Elx";
			//connParams.VmxSpec = diskUrl;
			//connParams.VmxSpec =  ansiString;


			connParams.ServerName = "192.168.0.27";
			connParams.VixCredentials.Uid.UserName = "root";
			connParams.VixCredentials.Uid.Password = "totototo";
			//connParams.VmxSpec = "vm1/vm1.vmx?dcPath=&dsName=[local_1]";

			connParams.Port = 902;
			IntPtr connPtr;
			connPtr =  IntPtr.Zero;
			VixError error =  VixDiskLib.Connect(ref connParams, out connPtr);
			//VixError error =  VixDiskLib.VixDiskLib_ConnectEx(ref connParams, true, "", "nbd:file:san:hotadd:nbdssl", out connPtr);
			string transport = VixDiskLib.GetTransportMode(connPtr);
			Console.WriteLine ("transport :"+transport);
			Console.WriteLine ("  ##### Result 3 (Connect) : "+error.ToString()+", intPtr null : "+(connPtr == IntPtr.Zero));

			IntPtr diskPtr = IntPtr.Zero;

			//win
			/*error = VixDiskLib.VixDiskLib_Open(connPtr, 
				@"[SAN_MSA2012FC_L19 (BackUp-Monitoring ENG)] WH0719v2/WH0719v2.vmdk", 
			    (uint)VixDiskLib.VIXDISKLIB_FLAG_OPEN_READ_ONLY, out diskPtr);*/


			//ux
			error = VixDiskLib.VixDiskLib_Open(connPtr, 
				@"[local_1] vm1/vm1.vmdk", 
			    (uint)VixDiskLib.VIXDISKLIB_FLAG_OPEN_READ_ONLY, out diskPtr);





			/*error = Native.VixDiskLib_Open(connPtr, 
				"lute.vmdk", 
			    (uint)Native.VIXDISKLIB_FLAG_OPEN_READ_ONLY, out diskPtr);
			Console.WriteLine ("Result 4 : "+error.ToString()+", disk inptr null : "+(diskPtr == IntPtr.Zero));*/
			/*Console.WriteLine (infoNull.Fmt+" , "+infoNull.Va_list);
			Console.WriteLine (warnNull.Fmt+" , "+warnNull.Va_list);
			Console.WriteLine (critNul.Fmt+" , "+critNul.Va_list);*/

			Console.WriteLine ("  ##### Result 4 (Open) : "+error.ToString()+", intPtr null : "+(connPtr == IntPtr.Zero));



			/// vmdk copy !works!
			/*FileStream vmdkfs  = new FileStream("test-backup-vldk.vmdk", FileMode.CreateNew);
			ulong readSectors = 0;
			while (readSectors < 2097152){

				ulong nbSectToRead = 1000;
				if((2097152-readSectors) < 1000)
					nbSectToRead = 2097152-readSectors;
				byte[] dataBuf = new byte[512*nbSectToRead];
				error = VixDiskLib.VixDiskLib_Read(diskPtr, readSectors, nbSectToRead, dataBuf);
				vmdkfs.Write(dataBuf, 0, (int)nbSectToRead*512);
				readSectors += 1000;
			}*/
			//Console.WriteLine ("Result 4 (Read) : "+error.ToString()+", inptr null : "+(connPtr == IntPtr.Zero));


			IntPtr diskInfoPtr = IntPtr.Zero;
			error = VixDiskLib.GetInfo(diskPtr, ref diskInfoPtr);
			VixDiskLibInfo di = new VixDiskLibInfo();
			Marshal.PtrToStructure(diskInfoPtr, di);
			//di = (VixDiskLibInfo)diskInfoPtr;
			Console.WriteLine ("*****"+di.BiosGeo.Cylinders+" CYL, "+di.BiosGeo.Sectors+" sec, bios cyl="+di.PhysGeo.Cylinders+", bios sec="+di.PhysGeo.Sectors+", nb sect="+di.Capacity);
			Console.WriteLine("physical disk size = "+di.Capacity*di.PhysGeo.Cylinders/1024/1024+" MB");
			       //VixDiskLib.VixDiskLib_ReadMetadata(
			Console.WriteLine ("Result  (VixDiskLib_GetInfo) : "+error.ToString()+", inptr null : "+(connPtr == IntPtr.Zero));


			error = VixMntApi.Init(1,0, null, null, null,
			    @"C:\Program Files\VMware\VMware Virtual Disk Development Kit", 
				@"E:\QHMP5573\Data\p\shb2\Vddk-wrappers\VDDK-Wrappers\bin\Debug\");

			Console.WriteLine ("  ##### Result 5 (VixMntapi_Init) : "+error.ToString()+", intPtr null : "+(connPtr == IntPtr.Zero));
			string[] diskNames = new string[1];
			diskNames[0] = "test"; //"WH0719v2.vmdk"; //transport; 
			uint numVolumes = 1;
			IntPtr diskSetHandle = IntPtr.Zero;
			//VixMntApi.
			//error = VixMntApi.VixMntapi_OpenDisks(connPtr, diskNames, numVolumes, 0, out diskSetHandle);
			error = VixMntApi.MountVolume(diskPtr, true);
			Console.WriteLine ("  ##### Result 6 (VixMntapi_MountVolume) : "+error.ToString()+", intPtr null : "+(connPtr == IntPtr.Zero));
			if(error == VixError.VIX_E_NOT_SUPPORTED) // disk is not readable : not formatted?
				Console.WriteLine ("  ##### Disk not mountable : "+error.ToString());






			VixDiskLib.Disconnect(connPtr);


		}

		private static void LogM(Severity severity, IntPtr messagePtr, IntPtr args){
			string message = Marshal.PtrToStringAnsi(messagePtr);
			int size = message.Length;
			//debug
			//Console.WriteLine ("RAW : "+message);

			/*IntPtr[] array = new IntPtr[size];
			if(args!= null && args != IntPtr.Zero && array.Length > 0){
				Marshal.Copy(args, array, 0, size);
				string[] argsArray = new string[size];

				for (int i = 0 ; i< size; i++){
					int placeHolderPos = message.IndexOf("%");
					if(placeHolderPos <0 || array[i] == IntPtr.Zero) continue;
					string placeHolder = message.Substring(placeHolderPos, 2);
					int phLength = 2;
					string currentArg = "-";
					//try{
					switch(placeHolder){
					case "%s":
						currentArg = Marshal.PtrToStringAnsi(array[i]);
						break;
					case "%d":
						currentArg = array[i].ToString();
						break;
					case "%#":
						// don't know how to read that, ignore
						currentArg = "<U:"+placeHolderPos+">";
						break;
					case "%I":
						if(message.Substring(placeHolderPos, 5) == "%I64u")
							currentArg = ((ulong)array[i]).ToString();
						else if(message.Substring(placeHolderPos, 5) == "%I64d")
							currentArg = ((long)array[i]).ToString();
						else
							currentArg = "<U:"+message.Substring(placeHolderPos, 4)+">"; 
						phLength = 4;
						break;
					case "%u":
						currentArg = ((uint)array[i]).ToString();
						break;
					case "%x":
						currentArg = Marshal.PtrToStringAnsi(array[i]);
						break;
					default:
						//currentArg = array[i].ToString();
						currentArg = "< ===== U:"+placeHolder+" ==== >";
						break;
					}
					//}
					//catch(Exception e){// mono won't like va_args, so unlikely to be able to parse them

					//}
					if(message != null && placeHolderPos >=0 && currentArg != null)
						message = message.Substring(0, placeHolderPos)
							+currentArg.ToString()
							+message.Substring(placeHolderPos+phLength);

				}
			}*/
		  
			Console.WriteLine (severity.ToString()+" <VDDK> : "+message.Replace(Environment.NewLine, string.Empty));

		}

		private static void LogI(IntPtr message, IntPtr args){
			LogM(Severity.INFO, message, args);
		}

		private static void LogW(IntPtr message, IntPtr args){
			LogM(Severity.WARNING, message, args);
		}

		private static void LogC(IntPtr message, IntPtr args){
			LogM(Severity.ERROR, message, args);
		}

		/*private static void mntLogI(string message, IntPtr args){
			LogM(Severity.INFO, message, args);
		}

		private static void mntLogW(string message, IntPtr args){
			LogM(Severity.WARNING, message, args);
		}

		private static void mntLogC(string message, IntPtr args){
			LogM(Severity.ERROR, message,  args);
		}*/

	}
}

