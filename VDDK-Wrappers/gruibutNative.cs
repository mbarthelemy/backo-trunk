using System;
using System.Runtime.InteropServices;

namespace VDDKWrappers {





    //*****The error codes are returned by all public VIX routines.
    public enum VixError:ulong{
        VIX_OK = 0,

        /* General errors */
        VIX_E_FAIL = 1,
        VIX_E_OUT_OF_MEMORY = 2,
        VIX_E_INVALID_ARG = 3,
        VIX_E_FILE_NOT_FOUND = 4,
        VIX_E_OBJECT_IS_BUSY = 5,
        VIX_E_NOT_SUPPORTED = 6,
        VIX_E_FILE_ERROR = 7,
        VIX_E_DISK_FULL = 8,
        VIX_E_INCORRECT_FILE_TYPE = 9,
        VIX_E_CANCELLED = 10,
        VIX_E_FILE_READ_ONLY = 11,
        VIX_E_FILE_ALREADY_EXISTS = 12,
        VIX_E_FILE_ACCESS_ERROR = 13,
        VIX_E_REQUIRES_LARGE_FILES = 14,
        VIX_E_FILE_ALREADY_LOCKED = 15,
        VIX_E_NOT_SUPPORTED_ON_REMOTE_OBJECT = 20,
        VIX_E_FILE_TOO_BIG = 21,
        VIX_E_FILE_NAME_INVALID = 22,
        VIX_E_ALREADY_EXISTS = 23,
        VIX_E_BUFFER_TOOSMALL = 24,

        /* Handle Errors */
        VIX_E_INVALID_HANDLE = 1000,
        VIX_E_NOT_SUPPORTED_ON_HANDLE_TYPE = 1001,
        VIX_E_TOO_MANY_HANDLES = 1002,

        /* XML errors */
        VIX_E_NOT_FOUND = 2000,
        VIX_E_TYPE_MISMATCH = 2001,
        VIX_E_INVALID_XML = 2002,

        /* VM Control Errors */
        VIX_E_TIMEOUT_WAITING_FOR_TOOLS = 3000,
        VIX_E_UNRECOGNIZED_COMMAND = 3001,
        VIX_E_OP_NOT_SUPPORTED_ON_GUEST = 3003,
        VIX_E_PROGRAM_NOT_STARTED = 3004,
        VIX_E_CANNOT_START_READ_ONLY_VM = 3005,
        VIX_E_VM_NOT_RUNNING = 3006,
        VIX_E_VM_IS_RUNNING = 3007,
        VIX_E_CANNOT_CONNECT_TO_VM = 3008,
        VIX_E_POWEROP_SCRIPTS_NOT_AVAILABLE = 3009,
        VIX_E_NO_GUEST_OS_INSTALLED = 3010,
        VIX_E_VM_INSUFFICIENT_HOST_MEMORY = 3011,
        VIX_E_SUSPEND_ERROR = 3012,
        VIX_E_VM_NOT_ENOUGH_CPUS = 3013,
        VIX_E_HOST_USER_PERMISSIONS = 3014,
        VIX_E_GUEST_USER_PERMISSIONS = 3015,
        VIX_E_TOOLS_NOT_RUNNING = 3016,
        VIX_E_GUEST_OPERATIONS_PROHIBITED = 3017,
        VIX_E_ANON_GUEST_OPERATIONS_PROHIBITED = 3018,
        VIX_E_ROOT_GUEST_OPERATIONS_PROHIBITED = 3019,
        VIX_E_MISSING_ANON_GUEST_ACCOUNT = 3023,
        VIX_E_CANNOT_AUTHENTICATE_WITH_GUEST = 3024,
        VIX_E_UNRECOGNIZED_COMMAND_IN_GUEST = 3025,
        VIX_E_CONSOLE_GUEST_OPERATIONS_PROHIBITED = 3026,
        VIX_E_MUST_BE_CONSOLE_USER = 3027,
        VIX_E_NOT_ALLOWED_DURING_VM_RECORDING = 3028,
        VIX_E_NOT_ALLOWED_DURING_VM_REPLAY = 3029,

        /* VM Errors */ 
        VIX_E_VM_NOT_FOUND = 4000,
        VIX_E_NOT_SUPPORTED_FOR_VM_VERSION = 4001,
        VIX_E_CANNOT_READ_VM_CONFIG = 4002,
        VIX_E_TEMPLATE_VM = 4003,
        VIX_E_VM_ALREADY_LOADED = 4004,
        VIX_E_VM_ALREADY_UP_TO_DATE = 4006,

        /* Property Errors */
        VIX_E_UNRECOGNIZED_PROPERTY = 6000,
        VIX_E_INVALID_PROPERTY_VALUE = 6001,
        VIX_E_READ_ONLY_PROPERTY = 6002,
        VIX_E_MISSING_REQUIRED_PROPERTY = 6003,

        /* Completion Errors */
        VIX_E_BAD_VM_INDEX = 8000,

        /* Snapshot errors */
        VIX_E_SNAPSHOT_INVAL = 13000,
        VIX_E_SNAPSHOT_DUMPER = 13001,
        VIX_E_SNAPSHOT_DISKLIB = 13002,
        VIX_E_SNAPSHOT_NOTFOUND = 13003,
        VIX_E_SNAPSHOT_EXISTS = 13004,
        VIX_E_SNAPSHOT_VERSION = 13005,
        VIX_E_SNAPSHOT_NOPERM = 13006,
        VIX_E_SNAPSHOT_CONFIG = 13007,
        VIX_E_SNAPSHOT_NOCHANGE = 13008,
        VIX_E_SNAPSHOT_CHECKPOINT = 13009,
        VIX_E_SNAPSHOT_LOCKED = 13010,
        VIX_E_SNAPSHOT_INCONSISTENT = 13011,
        VIX_E_SNAPSHOT_NAMETOOLONG = 13012,
        VIX_E_SNAPSHOT_VIXFILE = 13013,
        VIX_E_SNAPSHOT_DISKLOCKED = 13014,
        VIX_E_SNAPSHOT_DUPLICATEDDISK = 13015,
        VIX_E_SNAPSHOT_INDEPENDENTDISK = 13016,
        VIX_E_SNAPSHOT_NONUNIQUE_NAME = 13017,

        /* Host Errors */
        VIX_E_HOST_DISK_INVALID_VALUE = 14003,
        VIX_E_HOST_DISK_SECTORSIZE = 14004,
        VIX_E_HOST_FILE_ERROR_EOF = 14005,
        VIX_E_HOST_NETBLKDEV_HANDSHAKE = 14006,
        VIX_E_HOST_SOCKET_CREATION_ERROR = 14007,
        VIX_E_HOST_SERVER_NOT_FOUND = 14008,
        VIX_E_HOST_NETWORK_CONN_REFUSED = 14009,
        VIX_E_HOST_TCP_SOCKET_ERROR = 14010,
        VIX_E_HOST_TCP_CONN_LOST = 14011,
        VIX_E_HOST_NBD_HASHFILE_VOLUME = 14012,
        VIX_E_HOST_NBD_HASHFILE_INIT = 14013,

        /* Disklib errors */
        VIX_E_DISK_INVAL = 16000,
        VIX_E_DISK_NOINIT = 16001,
        VIX_E_DISK_NOIO = 16002,
        VIX_E_DISK_PARTIALCHAIN = 16003,
        VIX_E_DISK_NEEDSREPAIR = 16006,
        VIX_E_DISK_OUTOFRANGE = 16007,
        VIX_E_DISK_CID_MISMATCH = 16008,
        VIX_E_DISK_CANTSHRINK = 16009,
        VIX_E_DISK_PARTMISMATCH = 16010,
        VIX_E_DISK_UNSUPPORTEDDISKVERSION = 16011,
        VIX_E_DISK_OPENPARENT = 16012,
        VIX_E_DISK_NOTSUPPORTED = 16013,
        VIX_E_DISK_NEEDKEY = 16014,
        VIX_E_DISK_NOKEYOVERRIDE = 16015,
        VIX_E_DISK_NOTENCRYPTED = 16016,
        VIX_E_DISK_NOKEY = 16017,
        VIX_E_DISK_INVALIDPARTITIONTABLE = 16018,
        VIX_E_DISK_NOTNORMAL = 16019,
        VIX_E_DISK_NOTENCDESC = 16020,
        VIX_E_DISK_NEEDVMFS = 16022,
        VIX_E_DISK_RAWTOOBIG = 16024,
        VIX_E_DISK_TOOMANYOPENFILES = 16027,
        VIX_E_DISK_TOOMANYREDO = 16028,
        VIX_E_DISK_RAWTOOSMALL = 16029,
        VIX_E_DISK_INVALIDCHAIN = 16030,
        VIX_E_DISK_KEY_NOTFOUND = 16052, // metadata key is not found
        VIX_E_DISK_SUBSYSTEM_INIT_FAIL = 16053,
        VIX_E_DISK_INVALID_CONNECTION = 16054,
        VIX_E_DISK_NOLICENSE = 16064,

        /* Remoting Errors. */
        VIX_E_CANNOT_CONNECT_TO_HOST = 18000,
        VIX_E_NOT_FOR_REMOTE_HOST = 18001,

        /* Guest Errors */
        VIX_E_NOT_A_FILE = 20001,
        VIX_E_NOT_A_DIRECTORY = 20002,
        VIX_E_NO_SUCH_PROCESS = 20003,
        VIX_E_FILE_NAME_TOO_LONG = 20004

        
	}

    //*****Connection Structs
    public enum VixDiskLibCredType:uint{
        [MarshalAsAttribute(UnmanagedType.U4)] VIXDISKLIB_CRED_UID = 1 , // use userid password
        [MarshalAsAttribute(UnmanagedType.U4)] VIXDISKLIB_CRED_SESSIONID = 2,  // http session id
        [MarshalAsAttribute(UnmanagedType.U4)] VIXDISKLIB_CRED_TICKETID = 3 , // vim ticket id
        [MarshalAsAttribute(UnmanagedType.U4)] VIXDISKLIB_CRED_UNKNOWN = 256
	}

    [StructLayout(LayoutKind.Sequential)]//, CharSet=CharSet.Auto)]
    public struct VixDiskLibConnectParams{
			//[MarshalAsAttribute(UnmanagedType.LPStr)]
			public string MmxSpec;//'"MyVm/MyVm.vmx?dcPath=Path/to/MyDatacenter&dsName=storage1"
			//[MarshalAsAttribute(UnmanagedType.LPStr)]
			public string ServerName;
			[MarshalAsAttribute(UnmanagedType.U4)]	
			public uint CredType;                                         //As VixDiskLibCredType 'todo'
		public VixDiskLibCreds VixCredentials;// = new VixDiskLibCreds();
			public uint Port;
	}

    [StructLayout(LayoutKind.Sequential, CharSet=CharSet.Auto)]
    public struct VixDiskLibCreds{
		public VixDiskLibUidPasswdCreds Uid; // = new VixDiskLibUidPasswdCreds();
		public IntPtr ticketId;
        //'<FieldOffset(0)> Dim sessionId As VixDiskLibSessionIdCreds     //Not Yet Supported
        //'<FieldOffset(0)> Dim ticketId As IntPtr                        //VMware Internal Use Only
	}

    [StructLayout(LayoutKind.Sequential, CharSet=CharSet.Auto)]
    public struct VixDiskLibUidPasswdCreds{
			[MarshalAsAttribute(UnmanagedType.LPStr)] public string UserName;
			[MarshalAsAttribute(UnmanagedType.LPStr)] public string Password;
    }

    [StructLayout(LayoutKind.Sequential, CharSet=CharSet.Auto)]
    public struct VixDiskLibSessionIdCreds{
			[MarshalAsAttribute(UnmanagedType.LPStr)] public string cookie ;
			[MarshalAsAttribute(UnmanagedType.LPStr)] public string username; 
			[MarshalAsAttribute(UnmanagedType.LPStr)] public string key ;
    }

    

    //Disk info
    [StructLayout(LayoutKind.Sequential, CharSet=CharSet.Auto)]
    public struct VixDiskLibInfo{
		VixDiskLibGeometry biosGeo;    // BIOS geometry for booting and partitioning
		VixDiskLibGeometry physGeo;   // physical geometry
		UInt64 capacity;             // total capacity in sectors
		VixDiskLibAdapterType adapterType;  // adapter type
		Int16 numLinks;               // number of links (i.e. base disk + redo logs)
					IntPtr parentFileNameHint;     // parent file for a redo log                 '<<<todo: string or ptr?
    }

    //Disk types
    public enum VixDiskLibDiskType{
        VIXDISKLIB_DISK_MONOLITHIC_SPARSE = 1,    // monolithic file, sparse
        VIXDISKLIB_DISK_MONOLITHIC_FLAT = 2,    // monolithic file,  all space pre-allocated
        VIXDISKLIB_DISK_SPLIT_SPARSE = 3,    // disk split into 2GB extents, sparse
        VIXDISKLIB_DISK_SPLIT_FLAT = 4,    // disk split into 2GB extents, pre-allocated
        VIXDISKLIB_DISK_VMFS_FLAT = 5,    // ESX 3.0 and above flat disks
        VIXDISKLIB_DISK_STREAM_OPTIMIZED = 6,    // compressed monolithic sparse
        VIXDISKLIB_DISK_UNKNOWN = 256  // unknown type
	}

    //Disk adapter types
    public enum VixDiskLibAdapterType{
        VIXDISKLIB_ADAPTER_IDE = 1,
        VIXDISKLIB_ADAPTER_SCSI_BUSLOGIC = 2,
        VIXDISKLIB_ADAPTER_SCSI_LSILOGIC = 3,
        VIXDISKLIB_ADAPTER_UNKNOWN = 256
	}

    //Geometry
    [StructLayout(LayoutKind.Sequential, CharSet=CharSet.Auto)]
    public struct VixDiskLibGeometry{
		uint cylinders;
		uint heads;
		uint sectors;
    }

    //Disk Create
    [StructLayout(LayoutKind.Sequential, CharSet=CharSet.Auto)]
    public struct VixDiskLibCreateParams{
			VixDiskLibDiskType diskType;
			VixDiskLibAdapterType adapterType ;
			UInt16 hwVersion ;
			UInt32 capacity ;
    }

	public class Native {
    
		public int  VIXDISKLIB_VERSION_MAJOR  = 1;
		public int  VIXDISKLIB_VERSION_MINOR = 0;
			
		public int VIXDISKLIB_SECTOR_SIZE = 512;

	    //Disk Open Flags
	    //VIXDISKLIB_FLAG_OPEN_UNBUFFERED -> (1 << 0)
		public static int VIXDISKLIB_FLAG_OPEN_UNBUFFERED  = (1) << (0);
	    //'VIXDISKLIB_FLAG_OPEN_SINGLE_LINK -> (1 << 1)
		public static int VIXDISKLIB_FLAG_OPEN_SINGLE_LINK  = (1) << (1);
	    //'VIXDISKLIB_FLAG_OPEN_READ_ONLY -> (1 << 2)
		public static int VIXDISKLIB_FLAG_OPEN_READ_ONLY = (1) << (2);
	    //*****Virtual hardware version
		public static int  VIXDISKLIB_HWVERSION_WORKSTATION_4 = 3; // VMware Workstation 4.x and GSX Server 3.x
		public static int  VIXDISKLIB_HWVERSION_WORKSTATION_5 = 4; // VMware Workstation 5.x and Server 1.x
		public static int  VIXDISKLIB_HWVERSION_ESX30 = VIXDISKLIB_HWVERSION_WORKSTATION_5; // VMware ESX Server 3.0
		public static int  VIXDISKLIB_HWVERSION_WORKSTATION_6 = 6; // VMware Workstation 6.x
		public static int  VIXDISKLIB_HWVERSION_CURRENT = VIXDISKLIB_HWVERSION_WORKSTATION_6; //Defines the state of the art hardware version.  Be careful using this as it will change from time to time.

	    //*****Delegates
		public delegate bool VixDiskLibProgressFunc(IntPtr progressData, int percentCompleted);
		public delegate void VixDiskLibGenericLogFunc(
			[System.Runtime.InteropServices.InAttribute(), System.Runtime.InteropServices.MarshalAsAttribute(System.Runtime.InteropServices.UnmanagedType.LPStr)] 
			string fmt, IntPtr args);

	    //*****Clone Progress Callback Func
	    public int CloneProgressFunc(IntPtr param0, int percentCompleted){
				return percentCompleted;
			}

	    //*****Log struct
	   /* [StructLayout(LayoutKind.Sequential)]
	    public struct VixDiskLibGenericLogFunc{
			public string Fmt;
			public string Va_list;
	    }*/

	    //*****Function Definitions
	    [DllImport("libvixDiskLib.dll", SetLastError=true)]
		public static extern VixError VixDiskLib_Attach();
	        //n/a
	    

	    [DllImport("libvixDiskLib.dll", SetLastError=true)]
		public static extern VixError VixDiskLib_Clone(IntPtr dstConnection, string dstPath, IntPtr srcConnection, string srcPath, ref VixDiskLibCreateParams vixCreateParams, VixDiskLibProgressFunc progressFunc, IntPtr progressCallbackData, bool overWrite);
	    

	    [DllImport("libvixDiskLib.dll", SetLastError=true)]
		public static extern VixError VixDiskLib_Close(IntPtr diskHandle);

	    [DllImport("libvixDiskLib.dll", SetLastError=true,ExactSpelling = true, EntryPoint = "VixDiskLib_Connect")]
		public static extern VixError VixDiskLib_Connect([In][Out]ref VixDiskLibConnectParams connectParams, out IntPtr connection);

	    [DllImport("libvixDiskLib.dll", SetLastError=true)]
	    public static extern VixError VixDiskLib_Create(IntPtr connection, 
		        [InAttribute(), MarshalAsAttribute(UnmanagedType.LPStr)] string path, ref VixDiskLibCreateParams createParams, VixDiskLibProgressFunc progressFunc, IntPtr progressCallbackData);

	    [DllImport("libvixDiskLib.dll", SetLastError=true)]
		public static extern VixError VixDiskLib_CreateChild(IntPtr diskHandle, 
		        [InAttribute(), MarshalAsAttribute(UnmanagedType.LPStr)] string childPath, VixDiskLibDiskType diskType, VixDiskLibProgressFunc progressFunc, IntPtr progressCallbackData);

	    [DllImport("libvixDiskLib.dll", SetLastError=true)]
		public static extern VixError VixDiskLib_Defragment(IntPtr diskHandle, VixDiskLibProgressFunc progressFunc, IntPtr progressCallbackData);

	    [DllImport("libvixDiskLib.dll", SetLastError=true)]
		public static extern VixError VixDiskLib_Disconnect(IntPtr connection);

	    [DllImport("libvixDiskLib.dll", SetLastError=true)]
		public static extern void VixDiskLib_Exit();

	    [DllImport("libvixDiskLib.dll", SetLastError=true)]
		public static extern void VixDiskLib_FreeErrorText(IntPtr vixErrorMsgPtr);

	    [DllImport("libvixDiskLib.dll", SetLastError=true)]
		public static extern void VixDiskLib_FreeInfo(IntPtr info);

	    [DllImport("libvixDiskLib.dll", SetLastError=true)]
		public static extern IntPtr VixDiskLib_GetErrorText(UInt64 vixErrorCode, IntPtr locale);

	    [DllImport("libvixDiskLib.dll", SetLastError=true)]
		public static extern VixError VixDiskLib_GetInfo(IntPtr diskHandle, ref IntPtr info);

	    [DllImport("libvixDiskLib.dll", SetLastError=true)]
		public static extern VixError VixDiskLib_GetMetadataKeys(IntPtr diskHandle, IntPtr keysBuffer, uint bufLen, ref uint requiredLen);

	    [DllImport("libvixDiskLib.dll", SetLastError=true)]
		public static extern VixError VixDiskLib_Grow();

	    [DllImport("libvixDiskLib.dll", SetLastError=true)]
		public static extern VixError VixDiskLib_Init(uint majorVersion, uint minorVersion, VixDiskLibGenericLogFunc logInfo, VixDiskLibGenericLogFunc logWarn, VixDiskLibGenericLogFunc logPanic, string libDir);
	        //'Logs: C:\Documents and Settings\user\Local Settings\Temp\vmware-user

	    [DllImport("libvixDiskLib.dll",  ExactSpelling = true, EntryPoint = "VixDiskLib_Open", SetLastError=true)]
		public static extern VixError VixDiskLib_Open(IntPtr connection, 
			/*[InAttribute(), MarshalAsAttribute(UnmanagedType.LPStr)] */
		    System.String path,
		    uint flags, out IntPtr diskHandle);

	    [DllImport("libvixDiskLib.dll", SetLastError=true)]
		public static extern VixError VixDiskLib_Read(IntPtr diskHandle, UInt64 startSector, UInt64 numSectors , byte[] readBuffer);

	    [DllImport("libvixDiskLib.dll", SetLastError=true)]
		public static extern VixError VixDiskLib_ReadMetadata(IntPtr diskHandle, string key, IntPtr buf, uint bufLen, ref uint requiredLen);

	    [DllImport("libvixDiskLib.dll", SetLastError=true)]
		public static extern VixError VixDiskLib_Rename();

	    [DllImport("libvixDiskLib.dll", SetLastError=true)]
		public static extern VixError VixDiskLib_Shrink();

	    [DllImport("libvixDiskLib.dll", SetLastError=true)]
		public static extern VixError VixDiskLib_SpaceNeededForClone(); 

	    [DllImport("libvixDiskLib.dll", SetLastError=true)]
		public static extern VixError VixDiskLib_Unlink();

	    [DllImport("libvixDiskLib.dll", SetLastError=true)]
		public static extern VixError VixDiskLib_Write(IntPtr diskHandler, UInt64 startSector, UInt64 numSectors, byte[] writeBuffer);

	    [DllImport("libvixDiskLib.dll", SetLastError=true)]
		public static extern VixError VixDiskLib_WriteMetadata();

	    [DllImport("libvixDiskLib.dll", SetLastError=true)]
		public static extern VixError VixDiskLib_Rename([InAttribute(), MarshalAsAttribute(UnmanagedType.LPStr)] string srcFileName, [InAttribute(), MarshalAsAttribute(UnmanagedType.LPStr)] string dstFileName) ;

	    [DllImport("libvixDiskLib.dll", SetLastError=true)]
		public static extern VixError VixDiskLib_SpaceNeededForClone(IntPtr diskHandle, VixDiskLibDiskType diskType, ref UInt64 spaceNeeded);

	    [DllImport("libvixDiskLib.dll", SetLastError=true)]
		public static extern VixError VixDiskLib_Unlink(IntPtr connection, [InAttribute(), MarshalAsAttribute(UnmanagedType.LPStr)] string path) ;





		public Native ()
		{
		}
	}
}

