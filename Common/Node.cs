using System;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Xml.Serialization;


namespace P2PBackup.Common{
	/// <summary>
	/// Holds all information about an online node
	/// </summary>
	//public enum NodeStatus{Idle, Backuping, Restoring, Duplicating, Removing, Error}
	//public enum ActionType{Default, Backup, Restore}

	public enum KindEnum{Physical, Virtual}

	[Serializable]
	[KnownType(typeof(NodeStatus))]
	[DataContract]
	public class Node : IEquatable<P2PBackup.Common.Node>{

		private NodeCertificate certificate;
		private bool isUnixClient;
		private string pubKey;

		//[field: NonSerialized] private string verificationHash;
				
		
		/*[field: NonSerialized] private bool verified = false;
		[field: NonSerialized] private string browseData ="";	
		[field: NonSerialized] private string specialObjects = "";
		[field: NonSerialized] private string drivesData = "";
		[field: NonSerialized] private string vms = "";
		[field: NonSerialized]	private ActionType action = ActionType.Default;	*/	// 0 = default	1 = backup	2 = recovery
		
		/*[DataMember(Order = 25)]
		public string UserName{
			set { userName = value; }
			get { return userName; }
		}*/



		[DataMember]
		[DisplayFormatOption(Size=3)]
		public int Uid {get;set;}
			
		[DataMember]
		[DisplayFormatOption(Size=25)]
		public string NodeName{get;set;}
			
		[DataMember]
		[DisplayFormatOption(Size=6)]
		public KindEnum Kind{get;set;}

		//[DataMember]
		//[DisplayFormatOption(Size=6)]
		/// If node is virtual, id of the proxy/host node in charge of backups
		//public int HandledBy{get;set;}

		/*[DataMember]
		[DisplayFormatOption(Display=false)]*/
		[XmlIgnore]
		[DisplayFormatOption(Display=false)]
		public NodeCertificate Certificate{
			get{
				return certificate;
			}
			set{
				certificate = value;
			}
		}
		/*public string CertCN{
			get{ return certCN;}	
			set{certCN = value;}
		}*/
		
		[DataMember]
		[DisplayFormatOption(Display=false)]
		public string PubKey{
			get{ return pubKey;}	
			set{pubKey = value;}
		}

		[DataMember]
		[DisplayFormatOption(Size=7)]
		public string OS{get;set;}
			
		[DataMember]
		[DisplayFormatOption(Size=15)]
		public string IP{get;set;}

		[DataMember]
		[DisplayFormatOption(Size=15)]
		public string ListenIp{get;set;}

		[DataMember]
		[DisplayFormatOption(Size=5)]
		public int Port{get;set;}

		[DataMember]
		[DisplayFormatOption(Size=8,FormatAs=DisplayFormat.Size)]
		public long Quota{get;set;}

		[DataMember]
		[DisplayFormatOption(Size=8,DisplayAs="Used",FormatAs=DisplayFormat.Size)]
		public long UsedQuota{get;set;}

		[DataMember]
		[DisplayFormatOption(Size=8,DisplayAs="Stor.Size",FormatAs=DisplayFormat.Size)]
		public long StorageSize{get;set;}
			
		[DataMember]
		[DisplayFormatOption(Size=2,DisplayAs="Stor.Prio")]
		public int StoragePriority{get;set;}

		[DataMember]
		[DisplayFormatOption(Display=false)]
		public float CurrentLoad{get;set;}

		[DataMember]
		[DisplayFormatOption(Size=8,FormatAs=DisplayFormat.Size)]
		public long Available{get;set;}
		
		/*public double Filesize{
			get { return filesize; }
		}*/
		[DataMember]
		[DisplayFormatOption(Size=3,DisplayAs="Stor.Group")]
		public int StorageGroup{get;set;}

		[DataMember]
		[DisplayFormatOption(Size=3)]
		public int Group{get;set;}
			

		[DataMember]
		[DisplayFormatOption(Display=false)]
		public int NbBackupSets{get;set;}
			

		/*[DataMember]
		[DisplayFormatOption(Size=5)]
		public bool Locked{get;set;}*/
		
		/*internal bool Verified{
			get{ return verified;}
			set{ verified = value;}
		}*/

		[DataMember]
		[DisplayFormatOption(Display=false)]
		public string ShareRoot{get;set;}
			

		[DataMember]
		[DisplayFormatOption(Size=10)]
		public NodeStatus Status{get;set;}
			
		[DataMember]
		[DisplayFormatOption(Size=6)]
		public string Version{get;set;}
			
		[DataMember]
		[DisplayFormatOption(Display=false)]
		public bool IsUnixClient{
			get{return isUnixClient;}
			set{ isUnixClient = value;}
		}
		
		[DataMember]
		[DisplayFormatOption(Display=false)]
		public string InternalId {get;set;}
		
		[DataMember]
		[DisplayFormatOption(Size=19,FormatAs=DisplayFormat.Time)]
		public DateTime LastConnection{get;set;}

		[DataMember]
		[DisplayFormatOption(Display=false)]
		public int Hypervisor {get;set;}

		/// <summary>
		/// Gets or sets the storage space reserved (and not yet consumed) by backup operations.
		/// </summary>
		/// <value>
		/// The reserved space.
		/// </value>
		internal long ReservedSpace{get;set;}


		[DisplayFormatOption(Display=false)]
		public	bool HasAgent{get{return this.Version != null;}}

		/*public delegate void AddUserHandler(Node node);
		[field: NonSerialized]
		public event AddUserHandler AddUserEvent;
		public delegate void RemoveUserHandler(Node node);
		[field: NonSerialized]		
		public event RemoveUserHandler RemoveUserEvent;
		public delegate void LoggHandler (string user, bool received, string message);
		[field: NonSerialized]
		public event LoggHandler LoggEvent;
		public delegate Node CalculateDestinationHandler(Node node, string nodeToExclude);
		[field: NonSerialized]
		public event CalculateDestinationHandler CalculateDestinationEvent;
		
		public delegate Node GetClientHandler(string nodeName);
		[field: NonSerialized]
		public event GetClientHandler GetClientEvent;
		
		private static Queue<string> MessageQueue;*/
		
		//public Socket Connection{
		//	get { return connection; }
		//}
		
		
		/*[NonSerialized]
		internal Socket Connection;
		
		public string BrowseData {
			get {return this.browseData;}
		}
		
		public string DrivesData {
			get {return this.drivesData;}
		}
		
		public string VMS {
			get {return this.vms;}
		}
		
		public string SpecialObjects{ // NT VSS writers
			get {return this.specialObjects;}	
		}*/
		
		
		public Node (){

		}

		public Node (string name, string ip, string listenIp, int listenPort, long share, /*long access,*/ long avail, NodeStatus status, 
		             int uid, int sg, int groupId, string version, string os, long quota, long usedQuota, int backupsets, DateTime lastConnection){
			this.NodeName = name;
			//this.password = pass;
			this.IP = ip;
			this.ListenIp = listenIp;
			this.Port = listenPort;
			//this.PubKey = key;
			//this.certCN = GetCertCN();
			this.StorageSize = share;
			//this.Accessible = access;
			this.Available = avail;
			//if(locked)
			this.Status = status;
			//this.Locked = locked;
			this.Uid = uid;
			this.StorageGroup = sg;
			this.Group = groupId;
			//this.shareRoot = root;
			this.Version = version;
			this.OS = os;
			this.Quota = quota;
			this.UsedQuota = usedQuota;
			this.NbBackupSets = backupsets;
			this.LastConnection = lastConnection;
			//this.Status = NodeStatus.Idle;
			//Logger.Append("HUBRN", Severity.DEBUG2, "Created new node : Uid="+this.Uid+", NodeName="+this.NodeName
			//+",IP="+this.IP+", listenIp="+this.ListenIp+", listenPort="+this.ListenPort);
		}


		public bool Equals(P2PBackup.Common.Node other){
		      if (this.Uid == other.Uid)
		         return true;
		      else
		         return false;
		}


		public override string ToString(){
			return ""+this.Uid+" ("+this.NodeName+")";
		}
	}
}
