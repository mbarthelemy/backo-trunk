using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters;

using System.ServiceModel;


namespace P2PBackup.Common{
	public enum NodeStatus{Offline, Idle, Backuping, Restoring, Duplicating, Removing, Error, Locked, New}
	public enum ActionType{Default, Backup, Restore}
	
	
	public interface IClientNode_OLD: IEquatable<IClientNode_OLD>{
		
		
		
		//string UserName{get;set;}
		
		bool IsUnixClient{get;set;}
		
		int Uid{get;set;}
		string NodeName{get;set;}
		
		string CertCN{get;set;}
		
		string PubKey{get;set;}
		
		string OS{get;set;}
		
		string IP{get;set;}
		
		string ListenIp{get;set;}
		
		int ListenPort{get;set;}
		//public string Password{
		//	get { return password; }
		//}
		long Available{get;set;}
		/*long Accessible{get;set;}*/
		long Quota{get;set;}
		long UsedQuota{get;set;}
		long StorageSize{get;set;}
		/*public double Filesize{
			get { return filesize; }
		}*/
		int StorageGroup{get;set;}
		int Group{get;set;}
		int NbBackupSets{get;set;}
		// bool Locked{get;set;}
		/*internal bool Verified{
			get{ return verified;}
			set{ verified = value;}
		}*/
		
		string ShareRoot{get;set;}
		NodeStatus Status{get;set;}
		
		string Version{get;set;}



		KindEnum Kind{get;}

		int Hypervisor{get;}

		int StoragePriority{get;}
	
		bool HasAgent{get;}
	}
}

