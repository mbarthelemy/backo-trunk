using System;
using System.Runtime.Serialization;

namespace P2PBackup.Common {

	public enum SessionType{Unknown=0, Backup=1, Recover=2, Store=3, RecoverData=4, Clean=5, CleanData=6}; 

	public class PeerSession {

		[DataMember]
		[DisplayFormatOption(Size=6)]
		public int Id{get;set;}

		[DataMember]
		[DisplayFormatOption(Size=25)]
		public string Name{get;set;}

		[DataMember]
		[DisplayFormatOption(Size=15)]
		public SessionType Kind{get;set;}

		[DataMember]
		[DisplayFormatOption(Size=7)]
		public long TaskId{get; set;}

		[DataMember]
		[DisplayFormatOption(Size=3)]
		public int Budget{get;set;}

		/*[DataMember]
		[DisplayFormatOption(Size=20)]
		public Node FromNode{get;set;}

		[DataMember]
		[DisplayFormatOption(Size=20)]
		public Node ToNode{get;set;}*/

		[DataMember]
		[DisplayFormatOption(Size=3)]
		public int FromNode{get;set;}

		[DataMember]
		[DisplayFormatOption(Size=3)]
		public int ToNode{get;set;}

		[DataMember]
		[DisplayFormatOption(Size=20)]
		public DateTime Started{get;private set;}

		public PeerSession(){
			this.Started = DateTime.UtcNow;
		}

		public PeerSession(int id, SessionType sessType,  Node fromNode, Node toNode, long taskId, int budget){
			this.Id = id;
			this.Kind = sessType;
			this.FromNode = fromNode.Uid;
			this.ToNode  = toNode.Uid;
			this.TaskId = taskId;
			this.Budget = budget;
			this.Started = DateTime.UtcNow;
		}
	}
}

