using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace P2PBackup.Common{
	
	[DataContract]public enum TaskOperation{
		[EnumMember]Backup, 
		[EnumMember]Restore, 
		[EnumMember]Check, 
		[EnumMember]HouseKeeping};
	/// <summary>
	///  User-requested actions on queued tasks
	/// </summary>
	[DataContract]public enum TaskAction{
		[EnumMember]Start, 
		[EnumMember]Pause, 
		[EnumMember]Restart, 
		[EnumMember]Cancel}; 
	/// <summary>
	/// Task type : scheduled by hub or manually (user) requested
	/// </summary>
	[DataContract]
	public enum TaskStartupType{
		[EnumMember]Scheduled,
		[EnumMember]Manual};
	
	/// <summary>
	/// Task running status once it is in scheduler queue
	/// </summary>
	[DataContract(Name = "RunStatus")]public enum TaskRunningStatus{
		[EnumMember]Unknown, 
		[EnumMember]PendingStart,
		[EnumMember]PreProcessing, 
		[EnumMember]Started, 
		[EnumMember]Paused, 
		[EnumMember]PostProcessing, 
		[EnumMember]Stopped, 
		[EnumMember]Error, 
		[EnumMember]Cancelling, 
		[EnumMember]Cancelled, 
		[EnumMember]Done, 
		[EnumMember]Expiring, 
		[EnumMember]Expired}
	
	[DataContract]public enum TaskStatus{
		[EnumMember]Ok,
		[EnumMember]Warning,
		[EnumMember]Error,
		[EnumMember]Null}
	
	/// <summary>
	/// Task priority.
	/// High is the default and will maximize system resources usage to complete as quickly as possible.
	/// Low will make small pauses (eg. after each file write() operation in case of a backup tasdk) to be more fair 
	/// with the system	and other running applications
	/// </summary>
	[DataContract]public enum TaskPriority{
		[EnumMember]High, 
		[EnumMember]Low}
	
	/*[Serializable]*/
	[DataContract]
	[KnownType(typeof(TaskRunningStatus))]
	[KnownType(typeof(TaskStatus))]
	[KnownType(typeof(TaskStartupType))]
	[KnownType(typeof(TaskAction))]
	[KnownType(typeof(TaskOperation))]
	[KnownType(typeof(TaskPriority))]
	[KnownType(typeof(Node))]
	[KnownType(typeof(BackupSet))]
	public class Task : IEquatable<Task>{
		

		private List<Tuple<DateTime, int, string, string>> logEntries; //warning or errors during task execution
		private List<int> indexStorageNodes;//backup and restore
		
		[DataMember(Order = 0)] 
		[DisplayFormatOption(Size=6)]
		public long Id {get;set;}

		[DataMember]
		[DisplayFormatOption(Size=8)]
		public TaskOperation Operation{get;set;}
		
		[DataMember]
		[DisplayFormatOption(Size=9)]
		public TaskStartupType Type{get;set;}

		[DataMember]
		[DisplayFormatOption(Size=6)]
		public BackupLevel Level{get;set;} 

		[DataMember]
		[DisplayFormatOption(Size=3, DisplayAs="Node")]
		public int NodeId{get;set;}

		[DataMember]
		[DisplayFormatOption(Size=25)]
		public BackupSet BackupSet {get;set;}

		[DataMember]
		[DisplayFormatOption(Size=9)]
		public TaskRunningStatus RunStatus {get;set;}
			
		[DataMember]
		[DisplayFormatOption(Size=3, DisplayAs="%")]
		public int Percent{get; set;}
		
		[DataMember]
		[DisplayFormatOption(Size=8)]
		public TaskStatus Status {get;set;}
			
		[DataMember]
		[DisplayFormatOption(Size=7,DisplayAs="Items")]
		public int TotalItems{get;set;}

		[DataMember]
		[DisplayFormatOption(Size=8,DisplayAs="Size",FormatAs=DisplayFormat.Size)]
		public long OriginalSize {get;set;}

		[DataMember]
		[DisplayFormatOption(Size=8,FormatAs=DisplayFormat.Size,DisplayAs="F.Size")]
		public long FinalSize {get;set;}

		[DataMember]	
		[DisplayFormatOption(Display=false)]
		public int StorageBudget{get;set;}
		

		[DisplayFormatOption(Display=false)]
		public  List<P2PBackup.Common.Node> StorageNodes{
			get{ return storageNodes;}
			protected set{ storageNodes = value;}
		}

		private List<P2PBackup.Common.Node> storageNodes;
		
		[DataMember]
		[DisplayFormatOption(Display=false)]
		public List<int> IndexStorageNodes {
			get {
				return this.indexStorageNodes;
			}
			set{indexStorageNodes = value;}
		}
		
		[DataMember]
		[DisplayFormatOption(Display=false)]
		public string IndexName {get;set;}
			
		[DataMember]
		[DisplayFormatOption(Display=false)]
		public long IndexSize {get;set;}
			
		[DataMember]
		[DisplayFormatOption(Display=false)]
		public string IndexSum {get;set;}

		[DataMember]
		[DisplayFormatOption(Display=false)]
		public string SyntheticIndexSum {get;set;}

		[DataMember]
		[DisplayFormatOption(Display=false)]
		public long ParentTrackingId {get;set;}
			
		[DataMember]
		[DisplayFormatOption(Size=12,FormatAs=DisplayFormat.Time)]
		public DateTime StartDate {get;set;}

		[DataMember]
		[DisplayFormatOption(Display=false)]
		public DateTime EndDate {get;set;}
			
		[DataMember]
		[DisplayFormatOption(Display=false)]
		public List<Tuple<DateTime, int, string, string>> LogEntries {
			get {
				return this.logEntries;
			}
			set{logEntries = value;}
		}

		// For non-scheduled operations, returns the user who started them.
		[DataMember]
		[DisplayFormatOption(Display=false)]
		public int UserId {get;set;}
			
		[DataMember]
		[DisplayFormatOption(Display=false)]
		public TaskPriority Priority{get;set;}

		[DataMember]
		[DisplayFormatOption(Size=50)]
		public String CurrentAction{get;set;}
			
		//date of the last stats update received for the task
		[DataMember]
		[DisplayFormatOption(Display=false)]
		public long LastUpdated{get;set;}

		/// <summary>
		/// Creates a task to be run
		/// </summary>
		/// <param name="bs">
		/// A <see cref="BackupSet"/>
		/// </param>
		/// <param name="operation">
		/// A <see cref="TaskOperation"/>
		/// </param>
		/// <param name="type">
		/// A <see cref="TaskType"/>
		/// </param>
		public Task(BackupSet bs, TaskOperation operation, TaskStartupType type){
			this.BackupSet = bs;
			this.Operation = operation;
			this.Type = type;
			logEntries = new List<Tuple<DateTime, int, string, string>>();
			indexStorageNodes = new List<int>();
			this.Percent = 0;
			this.IndexSize = 0;
			this.IndexName = String.Empty;
			this.IndexSum = String.Empty;
			this.Priority = TaskPriority.High;
			this.Status = TaskStatus.Ok;
			this.NodeId = bs.NodeId;
			this.OriginalSize = 0;
			this.FinalSize = 0;
			this.TotalItems = 0;
			StorageBudget = 0;
			storageNodes = new List<P2PBackup.Common.Node>();
			//this.StorageNodes = new List<P2PBackup.Common.Node>();
		}
		
		/// <summary>
		/// Constructor for data processing
		/// </summary>
		public Task(){
			this.Id = -1;
			storageNodes = new List<P2PBackup.Common.Node>();
			this.Status = TaskStatus.Ok;
			indexStorageNodes = new List<int>();
			logEntries = new List<Tuple<DateTime, int, string, string>>();
		}
		
		public void AddLogEntry(int code, string message1, string message2){
			logEntries.Add(new Tuple<DateTime, int, string, string>(DateTime.Now, code, message1, message2));	
		}
		
		public void AddStorageNode(P2PBackup.Common.Node node){
			if(!storageNodes.Contains(node)){
				storageNodes.Add(node);
			}
		}
		
		public void RemoveStorageNode(P2PBackup.Common.Node node){
			lock(storageNodes)
				for(int i = storageNodes.Count; i==0; i--)
					if(storageNodes[i].Uid == node.Uid)
						storageNodes.RemoveAt(i);
		}

		public bool Equals(Task other){
	        if (this.Id == other.Id)   
	            return true;
	        else
	            return false;
    	}

	}
}

