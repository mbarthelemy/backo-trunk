using System;
using System.Runtime.Serialization;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;

namespace P2PBackup.Common{

	[DataContract]
	public class NodeCertificate{

		[DataMember]
		[DisplayFormatOption(Size=4)]
		public int Id{get; set;}

		[DataMember]
		[DisplayFormatOption(Size=4)]
		public int NodeId{get; set;}

		[DataMember]
		[DisplayFormatOption(Size=25)]
		public string CN{get; set;}

		[DataMember]
		[DisplayFormatOption(Size=25)]
		public string Issuer{get; set;}

		[DataMember]
		[DisplayFormatOption(Size=20)]
		public DateTime ValidFrom{get; set;}

		[DataMember]
		[DisplayFormatOption(Size=20)]
		public DateTime ValidUntil{get; set;}

		[DataMember]
		[DisplayFormatOption(Size=32)]
		public string Serial{get; set;}

		[DataMember]
		[DisplayFormatOption(Size=6)]
		public bool IsActive{get;set;}


		public NodeCertificate (X509Certificate2 cert){
			this.CN = cert.Subject;
			this.ValidFrom = cert.NotBefore;
			this.ValidUntil = cert.NotAfter;
			this.Issuer = cert.Issuer;
			this.Serial = cert.SerialNumber;
		}

	}
}

