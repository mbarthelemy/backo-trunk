using System;
namespace P2PBackup.Common{
	
	//public enum BackupType{Default=0, Full=1, Differential=2, Incremental=3, TransactionLog=4, SyntheticFull=5};
	public enum BackupLevel{Default=0, Refresh=1, SyntheticFull=2, Full=3, SnapshotOnly=10, TransactionLog=11}; //, Incremental=3, TransactionLog=4, SyntheticFull=5};



	[Serializable]
	public class ScheduleTime{

		
		public string Begin {get; set;}
		public string End {get; set;}
		public BackupLevel Level{get;set;}
		public DayOfWeek Day{get; set;}
			

		public ScheduleTime (){
		
		}
		
		public ScheduleTime(BackupLevel level, DayOfWeek day, string begin, string end){
			this.Level = level;
			this.Day = day;
			this.Begin = begin;
			this.End = end;
		}

		public override string ToString(){
			return string.Format ("{0}-{1}, {2}]", Begin, End, Level);
		}
		
	}
}

