using System;

namespace P2PBackup.Common {
	
	public class OverQuotaException:Exception {
		public new string Message{get;private set;}
		
		
		public OverQuotaException (long used, long quota){
			this.Message = "Node is overquota";
		}
	}
	
	public class UnreachableNodeException:Exception {
		//public new string Message{get;private set;}
		
		
		public UnreachableNodeException (){
			
		}
	}
}

