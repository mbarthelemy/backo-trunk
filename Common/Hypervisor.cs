using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace P2PBackup.Common {

	[Serializable]
	[DataContract]
	[KnownType(typeof(Password))]
	public class Hypervisor {

		[DataMember]
		[DisplayFormatOption(Size=3)]
		public int Id{get;set;}

		[DataMember]
		[DisplayFormatOption(Size=15)]
		public string Name{get;set;}

		[DataMember]
		[DisplayFormatOption(Size=10)]
		public string Kind{get;set;}

		[DataMember]
		[DisplayFormatOption(Size=30)]
		public string Url{get;set;}

		[DataMember]
		[DisplayFormatOption(Size=15)]
		public string UserName{get;set;}

		[DataMember]
		[DisplayFormatOption(Size=12)]
		public Password Password{get;set;}

		[DataMember]
		[DisplayFormatOption(Size=20)]
		public DateTime LastDiscover{get;set;}

		public Hypervisor(){

		}

		public override string ToString () {
			return string.Format ("[Hypervisor: Id={0}, Name={1}, Kind={2}, Url={3}, UserName={4}, Password={5}, LastDiscover={6}]", Id, Name, Kind, Url, UserName, Password.Id, LastDiscover);
		}
	}
}

