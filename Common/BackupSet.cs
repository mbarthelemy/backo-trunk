using System;
using System.Collections;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Runtime.Serialization.Formatters;
using System.Collections.Generic;

namespace P2PBackup.Common{
	

	public enum BackupStatus{Ok, Warning, Error, Null}

	[Flags]
	public enum DataProcessingFlags{None=0,CCompress=1,CEncrypt=2,CDedup=4,CReplicate=8,CChecksum=16,SCompress=512,SEncrypt=1024,SDedup=2048,SReplicate=4096,HybridDedup=16384}

	public enum ParallelismLevel{Absolute,Disk,FS};

	public class Parallelism{

		public ParallelismLevel Kind{get;set;}
		public uint Value{get;set;}

		public Parallelism(){}

		public Parallelism(string rawValue){
			string[] split = rawValue.ToLower().Split(new char[]{':'});
			int pValue = 0;
			if(split[0] == "a")
				this.Kind = ParallelismLevel.Absolute;
			else if(split[0] == "d")
				this.Kind = ParallelismLevel.Disk;
			else if (split[0] == "f")
				this.Kind = ParallelismLevel.FS;
			else
				throw new InvalidCastException("Cannot map raw value to ParallelismType");
			if(int.TryParse(split[1], out pValue))
			   this.Value = (uint)pValue;
			else
			   throw new InvalidCastException("Cannot cast raw value'"+split[1]+"' to parallism value(integer)");
		}

		public override string ToString () {
			return string.Format ("[{0}, {1}]", Kind, Value);
		}
	}

	[Serializable]
	public class BackupSet: Object,IEquatable<BackupSet>{ // implement iequatable to allow comparison in scheduler
		
		private List<BasePath> basePaths;
		private List<ScheduleTime> backupTimes;
		private List<int> storageGroups;
		private long maxChunkSize;
		private long maxPackSize;
		private int maxChunkFiles;


		[DisplayFormatOption(Size=4)]
		public int Id {get;set;}

		[DisplayFormatOption(Display=false)]
		public int Generation{get; set;}
		
		[DisplayFormatOption(Size=20)]
		public string Name{get;set;}
			
		[DisplayFormatOption(Size=3, DisplayAs="Node")]
		public int NodeId {get;set;}

		/// <summary>
		/// Indicated if this backupset is processed by an alternate, 'proxy' node
		/// will be mainly used if client Node is a VM(backuped by a VADP host)
		/// </summary>
		/// <value>
		/// The handled by.
		/// </value>
		[DisplayFormatOption(Size=3, DisplayAs="H.By")]
		public int HandledBy {get;set;}

		[DisplayFormatOption(Size=8, DisplayAs="Stor.Grps")]
		public List<int> StorageGroups{
			get{return storageGroups;}	
			set{storageGroups = value;}
		}

		[DisplayFormatOption(Size=30)]
		public List<ScheduleTime> ScheduleTimes {
			get {return this.backupTimes;}
			set {backupTimes = value;}
		}

		[DisplayFormatOption(Size=30)]
		public List<BasePath> BasePaths {
			get {return this.basePaths;}
			set {basePaths = value;}
		}

		/*[DisplayFormatOption(Size=2,DisplayAs="//")]
		public int Parallelism{get;set;}*/
		[DisplayFormatOption(Size=10)]
		public Parallelism Parallelism{get;set;}


		[DisplayFormatOption(Size=25)]
		public DataProcessingFlags DataFlags{get;set;}

		[DisplayFormatOption(Display=false)]
		public string Preop {get;set;}

		[DisplayFormatOption(Display=false)]
		public string Postop {get;set;}

		[DisplayFormatOption(Size=3,DisplayAs="Copies")]
		public int Redundancy{get;set;}

		[DisplayFormatOption(Size=4,DisplayAs="Ret.")]
		public int RetentionDays {get;set;}

		[DisplayFormatOption(Size=4,DisplayAs="Snap Ret.")]
		public int SnapshotRetention{get;set;}

		[DisplayFormatOption(Size=8)]
		public TaskOperation Operation{get;set;}
		//public BackupSet(List<BasePath> basePaths, List<BackupTime backupTimes, bool encrypt, bool compress, string preop, string postop, int redundancy, int retentiondays){

		[DisplayFormatOption(Size=7,Display=false,FormatAs=DisplayFormat.Size,DisplayAs="PSize")]
		public long MaxPackSize {
			get {return this.maxPackSize;}
			set {maxPackSize = value;}
		}

		[DisplayFormatOption(Size=7,Display=false,FormatAs=DisplayFormat.Size,DisplayAs="CSize")]
		public long MaxChunkSize {
			get {return this.maxChunkSize;}
			set {maxChunkSize = value;}
		}
	
		[DisplayFormatOption(Size=7,Display=false,DisplayAs="CFiles")]
		public int MaxChunkFiles {
			get {return this.maxChunkFiles;}
			set {maxChunkFiles = value;}
		}

		/// <summary>
		/// For a proxied backup or a backup that needs help from hypervisor,
		/// define a tuple with clientInternalId (for hyperisor to find it) and hypervisor information (url, username, password)
		/// </summary>
		/// <value>
		/// The proxy info.
		/// </value>
		[DisplayFormatOption(Size=7,Display=false)]
		public ProxyTaskInfo ProxyingInfo{get;set;}

		[DisplayFormatOption(Display=false)]
		public List<TaskNotification> Notifications{get;set;}
		
		public BackupSet(){
			//Status = BackupStatus.Null;
			storageGroups = new List<int>();
			this.DataFlags = DataProcessingFlags.None;
			//this.ProxyingInfo = null;
		}

	/*	public BackupSet(List<BasePath> basePaths, List<BackupTime backupTimes, bool encrypt, bool compress, string preop, string postop, int redundancy, int retentiondays){
			this.basePaths = basePaths;
			this.backupTimes = backupTimes;
			this.encrypt = encrypt;
			this.compress = compress;
			this.preop = preop;
			this.postop = postop;
			this.redundancy = redundancy;
			this.retentionDays = retentionDays;
		}*/
		
		public static BackupSet GetFromXml(string xml){
			XmlSerializer serializer =  new XmlSerializer(typeof(BackupSet));
			XmlReader bsReader = XmlReader.Create(new StringReader(xml));
			BackupSet bs = (BackupSet)serializer.Deserialize(bsReader);
			//bs.Status = BackupStatus.Null;
			return bs;
		}
		
		public String DumpToXml(){
			XmlSerializer serializer =  new XmlSerializer(typeof(BackupSet));
			StringBuilder sb = new StringBuilder();
			XmlWriterSettings xs = new XmlWriterSettings();
			xs.Indent = true;
			xs.NewLineHandling = NewLineHandling.Entitize;
			XmlWriter bsWriter = XmlWriter.Create(sb);
			serializer.Serialize(bsWriter, this);
			return sb.ToString();
		}
		
		public bool Equals(BackupSet other){
	        if (this.Id == other.Id)  
	            return true;
	        else
	            return false;
    	}
		
		public override string ToString (){
				return this.Id+" ("+this.Name+")";
		}

	}
}

