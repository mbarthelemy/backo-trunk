using System;

namespace P2PBackup.Common {

	public class LogEventArgs : EventArgs{
	    public LogEventArgs(int code, Severity severity, string message)  {
			Code = code;
			Severity = severity;
			Message = message;
	    }
	    public int Code { get; private set; }
		public Severity Severity{get; private set;}
		public string Message{get;set;}
	}



}

