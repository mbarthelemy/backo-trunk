using System;
using System.Collections.Generic;

namespace Node.Snapshots{

	[Serializable]
	public class SPOMetadata{
		//public Hashtable Metadata{get;set;}
		public Dictionary<string, object> Metadata{get;set;}
		
		public SPOMetadata(){
			//Metadata = new Hashtable(); //<object>();
			Metadata = new Dictionary<string, object>();
		}
	}
}

