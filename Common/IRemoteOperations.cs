using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ServiceModel;
//using 

namespace  P2PBackup.Common{
	
	
	//[ServiceBehavior(IncludeExceptionDetailInFaults=true)]
	[ServiceContract(SessionMode = SessionMode.Required)]
	[ServiceKnownType(typeof(NodeGroup))]
	[ServiceKnownType(typeof(StorageGroup))]
	[ServiceKnownType(typeof(P2PBackup.Common.Node))]
	[ServiceKnownType(typeof(BackupSet))]
	[ServiceKnownType(typeof(P2PBackup.Common.Task))]
	[ServiceKnownType(typeof(TaskRunningStatus))]
	[ServiceKnownType(typeof(TaskStatus))]
	[ServiceKnownType(typeof(TaskStartupType))]
	[ServiceKnownType(typeof(TaskPriority))]
	[ServiceKnownType(typeof(TaskAction))]
	[ServiceKnownType(typeof(TaskOperation))]
	[ServiceKnownType(typeof(Severity))]
	[ServiceKnownType(typeof(NodeCertificate))]
	[ServiceKnownType(typeof(PeerSession))]
	[ServiceKnownType(typeof(DataProcessingFlags))]
	[ServiceKnownType(typeof(RoleEnum))]
	[ServiceKnownType(typeof(Hypervisor))]
	[ServiceKnownType(typeof(Password))]
	public interface  IRemoteOperations{
		
		//[OperationContract]
		//ArrayList GetClients();
		
		[OperationContract]
		List<P2PBackup.Common.Node> GetNodes();

		[OperationContract]
		Node GetNode (int id);

		[OperationContract]
		List<P2PBackup.Common.Node> GetStorageNodes();
		
		[OperationContract]
		List<P2PBackup.Common.StorageGroup> GetStorageGroups();
		
		//[OperationContract]
		//P2PBackup.Common.Node GetNode(string hostName, string pubKey);
		
		/*[OperationContract]
		P2PBackup.Common.Node GetNode(int nodeId);*/

		[OperationContract]
		Dictionary<string, string> GetNodeConf(int nodeId);

		[OperationContract]
		List<NodeCertificate> GetCertificates();

		[OperationContract]
		List<PeerSession> GetSessions();

		[OperationContract]
		List<NodeGroup> GetNodeGroups();
		
		[OperationContract]
		void ApproveNode(int nodeId, bool lockStatus);
		
		[OperationContract]
		int Ping();
		
		/*[OperationContract]
		User BeginSession(string username, string password);*/
		
		[OperationContract]
		User GetCurrentUser();
		
		[OperationContract]
		bool Login(string userName, string userPassword); 
		
		/*[OperationContract]
		List<Node> GetOnlineClients();*/
		
		[OperationContract]
		Dictionary<int, NodeStatus> GetOnlineClients();
		
		[OperationContract]
		List<BackupSet> GetBackupPlan(int interval);

		[OperationContract]
		List<BackupSet> GetTaskSets();

		[OperationContract]
		BackupSet GetTaskSet(int bsid);

		[OperationContract]
		Dictionary<string, string> GetConfigurationParameters();
		
		[OperationContract]
		List<P2PBackup.Common.Task> GetRunningTasks();
		
		[OperationContract]
		List<P2PBackup.Common.Task> GetTasksHistory(string[] bs, DateTime from, DateTime to, List<TaskRunningStatus> status, string sizeOperator, long size, int limit, int offset, out int totalCount);

		[OperationContract]
		Task GetTaskHistory(long taskId);

		[OperationContract]
		List<Tuple<DateTime, int, string, string>> GetArchivedTaskLogEntries(int taskId);
			
		[OperationContract]
		List<BackupSet> GetNodeBackupSets(int nodeId);
		
		[OperationContract]
		Hashtable GetBackupHistory(int bsId, DateTime startDate, DateTime endDate);
		
		[OperationContract]
		string Browse(int nodeId, string path);
		
		[OperationContract]
		string GetSpecialObjects(int nodeId);
			
		[OperationContract]
		string GetDrives(int nodeId);
		
		[OperationContract]
		string GetVMs(int nodeId);

		[OperationContract]
		List<Hypervisor> GetHypervisors();

		[OperationContract]
		List<Node> Discover (int hypervisorId);

		[OperationContract]
		string CreateBackupSet(BackupSet bs);
		
		[OperationContract]
		void SetNodeConfig(int nodeId, Hashtable confTable);
		
		[OperationContract]
		void UpdateNodeGeneralConf(int nodeId, long? storageSize, long? quota, int? storageGroup, int? nodeGroup);
		
		[OperationContract]
		LogEntry[] GetLogBuffer();
		//used to manage users session time zones and formats, languages
		[OperationContract]
		Hashtable GetCultures();
		
		[OperationContract]
		List<User> GetUsers();
		
		[OperationContract]
		long StartTask(int bsid, BackupLevel? level);
		
		[OperationContract]
		void StopTask(long taskId);
		
		[OperationContract]
		string ChangeTasks(List<long> tasks, TaskAction action);
		
		[OperationContract]
		string WhoAmI();
		
		[OperationContract]
		void ShutdownHub();
	}
//}
	
}
