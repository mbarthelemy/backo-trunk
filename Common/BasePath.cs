using System;
using System.Collections.Generic;

namespace P2PBackup.Common{
	
	
	/// <summary>
	/// A BackupPath can be one of : Filesystem (directory/file), Volume (raw block device), Object (VSS writer and components), 
	/// or VM (a virtual machine file drive).
	/// Include and exclude policies only applies to Filesystem entries or "ALL" wildcard results.
	/// </summary>
	[Serializable]
	public class BasePath{

		//public enum PathType{FS,VOLUME,OBJECT,VM,FILE}

		
		public string SnapshotType {get;set;}

		public string Path{get;set;}

		//public string ProxiedPath{get;set;}
		
		public List<string> IncludePolicy{get;set;}
		
		public List<string> ExcludePolicy{get;set;}

		//public BasePathConfig Config{get;set;}

		public List<string> ExcludedPaths{get;set;}

		// if a basepath has been added since last backup, override to perform a full backup
		// of this path (the previously existing paths can keep the default level)
		public BackupLevel OverridenLevel{get;set;}
		
		public bool Recursive{get;set;}

		/// <summary>
		/// Gets or sets the type. Syntax TYPE:PROVIDER
		/// Example: FS:LOCAL, FS:VMWARE, OBJECT:VSS, OBJECT:StorageLayout, STREAM 
		/// </summary>
		/// <value>
		/// The type.
		/// </value>
		public string Type{get;set;}

		/*public PathType Type{
			get{return type;}
			set{type = value;}
		}*/
		public BasePath (){
			this.ExcludedPaths = new List<string>();
			this.IncludePolicy = new List<string>();
			this.ExcludePolicy = new List<string>();
			this.OverridenLevel = BackupLevel.Default;
			//this.Config = new BasePathConfig();
			// TODO : move this somewhere else and don't set it on unix
			this.ExcludePolicy.Add("pagefile.sys");
			this.ExcludePolicy.Add("hiberfil.sys");
			this.ExcludePolicy.Add("PAGEFILE.SYS");
			this.ExcludePolicy.Add("HIBERFIL.SYS");
			this.ExcludePolicy.Add("$*");
		}
		
		internal BasePath(string basePath, List<string> includepolicy, List<string> excludepolicy){
			this.Path = basePath;	
			this.IncludePolicy = includepolicy;
			this.ExcludePolicy = excludepolicy;
			this.ExcludedPaths = new List<string>();
			this.OverridenLevel = BackupLevel.Default;
			this.ExcludePolicy.Add("pagefile.sys");
			this.ExcludePolicy.Add("hiberfil.sys");
			this.ExcludePolicy.Add("PAGEFILE.SYS");
			this.ExcludePolicy.Add("HIBERFIL.SYS");
			this.ExcludePolicy.Add("$*");

		}

		public override string ToString(){
			return this.Type+":"+ Path;
		}
		
		
	}

	/*      Table ½ shbackup.specialobjects ╗
 Colonne  |       Type        | Modificateurs
----------+-------------------+---------------
 bsid     | integer           |
 spopath  | character varying |
 config   | character varying |
 password | integer           |*/
	[Serializable]
	public struct BasePathConfig{
		public Password Password{get;set;}
		// error : Tuple is not serializable
		public List<Tuple<string, string>> ConfigPairs{get;set;}

	}
}

