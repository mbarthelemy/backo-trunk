using System;

namespace P2PBackup.Common{
	
	/// <summary>
	/// Permisssion exception.
	/// Raised when a remote method is called on an item for which the logged user has insufficient rights.
	/// Mostly throwed by dbhandle
	/// </summary>
	public class PermisssionException:Exception{
		
		public PermisssionException(string message){
			
		}
	}
}
