using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace P2PBackup.Common{
	
	[DataContract]
	public class UserRole{
		
		public RoleEnum Role{get;set;}
		public List<int> GroupsInRole{get;set;}
		
		public UserRole(){
			//Role = RoleEnum.None;
			this.GroupsInRole = new List<int>();
		}

		public override string ToString (){
			return string.Format ("[Role={0}, GroupsInRole={1}]", Role, ( (GroupsInRole==null)?"<none>":string.Join(",", GroupsInRole)));
		}
	}
	
}
