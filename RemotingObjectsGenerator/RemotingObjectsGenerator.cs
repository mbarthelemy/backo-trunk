using System;
using System.Runtime.CompilerServices;
using System.Runtime.Remoting;
using System.Reflection;
using System.Collections;
using System.CodeDom;
using System.CodeDom.Compiler;
using Microsoft.CSharp;
using System.IO;
using System.Linq;

namespace RemotingObjectsGenerator{
	// TODO:
	// derived types (UserList:Arraylist)
	// assembly references
	// public properties
	// getters and setters
	// option to add/ignore delegates and events
	// [done]find why a method that returns an arraylist is not correctly written
	// test with "weird things" : generics...
	
	
	
	public class RemotingObjectsGenerator{
		
		private static Assembly a;
		
		public RemotingObjectsGenerator (){
		}
		
		private  static void LoadAssembly(string inAssembly){
		
			a = Assembly.LoadFile(inAssembly);
		}
		
		public void GenerateRemotingStubs(string outAssembly){
			
		}
		
		public static void Main(string[] args){
			if(args.Length < 3){
				Console.WriteLine("Error, expecting at least 3 parameters :");
				Console.WriteLine("inAssembly, outAssembly, outNameSpace [, -o:option1,option2]");
				Console.WriteLine("-o can be : keepdelegates (by deault we omit them)");
				Console.WriteLine("\t");
				return;
			}
			string inAssembly = args[0];
			string outAssembly = args[1];
			string outNamespace = args[2];
			LoadAssembly(inAssembly);
			ArrayList exportableTypes = GetExportableTypes();
			ArrayList exportableMethods = new ArrayList();
			ArrayList exportableProperties = new ArrayList();
			ArrayList exportableAccessors = new ArrayList();
			
			ICodeGenerator codeGenerator = new CSharpCodeProvider().CreateGenerator();			
			CSharpCodeProvider cdp = new CSharpCodeProvider();
			codeGenerator = cdp.CreateGenerator();
			
			CodeNamespace Samples = new CodeNamespace(outNamespace);
			foreach (AssemblyName an in a.GetReferencedAssemblies()){
				Console.WriteLine("Name={0}, Version={1}, Culture={2}, PublicKey token={3}", an.Name, an.Version, an.CultureInfo.Name, (BitConverter.ToString (an.GetPublicKeyToken())));
			}
			Samples.Imports.Add(new CodeNamespaceImport("System"));
			ArrayList namespaces = new ArrayList();
			foreach(Type t in exportableTypes){
				CodeTypeDeclaration Class1 = new CodeTypeDeclaration(t.Name);
				Class1.TypeAttributes = TypeAttributes.Abstract| TypeAttributes.Public;
				Class1.BaseTypes.Add(new CodeTypeReference(typeof(MarshalByRefObject)));
				
				// TODO : handle constructors with parameters!!!
				foreach(ConstructorInfo ci in t.GetConstructors()){
					Console.WriteLine("constructor found");
					if(ci.IsPublic){
						Console.WriteLine("...public constructor");
						CodeConstructor cc = new CodeConstructor();
						cc.Attributes = MemberAttributes.Public;
						foreach (ParameterInfo pi in ci.GetParameters()){
							Console.WriteLine("...public constructor parameter="+pi.Name);
							cc.Parameters.Add(new CodeParameterDeclarationExpression(pi.ParameterType, pi.Name));
						}
						Class1.Members.Add(cc);
					}
				}
				
				
				exportableAccessors = GetExportableAccessors(t);
				exportableProperties = GetExportableProperties(t);
				exportableMethods = GetExportableMethods(t);
				
				// Getters/Setters
				foreach(PropertyInfo pi in GetExportableProperties(t)){
					CodeMemberProperty prop = new CodeMemberProperty();
					prop.Name = pi.Name;
					prop.Attributes = MemberAttributes.Public | MemberAttributes.Final;
					prop.HasGet = pi.CanRead;
					prop.HasSet = pi.CanWrite;
					prop.Type = new CodeTypeReference(pi.PropertyType);
					if(prop.HasGet){
						//prop.GetStatements.Add( 
							//new CodeCastExpression(pi.PropertyType.Name, new CodePrimitiveExpression( ))); 
							//new CodeThrowExceptionStatement(new CodeObjectCreateExpression(   
    						//new CodeTypeReference(typeof(System.NotImplementedException)), new CodeExpression[]{} )));
							
							prop.GetStatements.Add( new CodeMethodReturnStatement(     new CodeCastExpression(pi.PropertyType.Name, new CodePrimitiveExpression() )    ));
					}
					if(prop.HasSet)
						prop.SetStatements.Add( 
							//new CodeCastExpression("System.Nullable<"+pi.PropertyType.Name+">", new CodePrimitiveExpression() ))); 
							//new CodeThrowExceptionStatement(new CodeObjectCreateExpression(   
    						//new CodeTypeReference(typeof(System.NotImplementedException)), new CodeExpression[]{} )));
						                       
						    new CodeAssignStatement(new CodeFieldReferenceExpression(new CodeThisReferenceExpression(), String.Format("{0}", prop.Name)), new CodePropertySetValueReferenceExpression()));
					Class1.Members.Add(prop);
				}
				
				// Methods
				foreach(MethodInfo mi in exportableMethods){
					CodeMemberMethod method = new CodeMemberMethod();
             		method.Name = mi.Name;
					if (mi.IsStatic)
						method.Attributes = MemberAttributes.Public | MemberAttributes.Static;
					else
						method.Attributes = MemberAttributes.Public | MemberAttributes.Final;
            		
					if(!(namespaces.Contains(mi.ReturnType.Namespace)))
						namespaces.Add(mi.ReturnType.Namespace);
					
					foreach(ParameterInfo pi in mi.GetParameters() ){
						method.Parameters.Add(new CodeParameterDeclarationExpression(pi.ParameterType.Name, pi.Name));
						
						
					}
					// dummy return
					if(mi.ReturnType != typeof(void)){
						method.ReturnType = new CodeTypeReference(mi.ReturnType.Name);
						//method.Statements.Add( new CodeMethodReturnStatement( 
						//	new CodeCastExpression(mi.ReturnType.Name, new CodePrimitiveExpression() ))); 
						method.Statements.Add(new CodeThrowExceptionStatement(new CodeObjectCreateExpression(   
    					new CodeTypeReference(typeof(System.NotImplementedException)), new CodeExpression[]{} )));
					}
					Class1.Members.Add(method);
            		
				}
				Class1.IsClass = true;
				Samples.Types.Add(Class1);
				foreach(string ns in namespaces)
					Samples.Imports.Add(new CodeNamespaceImport(ns));
				Console.WriteLine("samples="+Samples.ToString());
				 
				//Response.Write();
			}
			TextWriter tw = new StreamWriter(new FileStream("generated.cs", FileMode.Create));
			codeGenerator.GenerateCodeFromNamespace(Samples, tw, null);
			tw.Close();
		}
		
		
		private static ArrayList GetExportableTypes(){
			// we get classes and interfaces
			ArrayList exp = new ArrayList();
			foreach (Type t in a.GetTypes ()){
					if(t.IsMarshalByRef || t.IsSerializable && !(t.IsSubclassOf(typeof(Delegate)) || t==typeof(Delegate))){
						Console.WriteLine("type MarshalByRefObject in assembly :"+t.ToString());
						exp.Add(t);
					}
			}
			return exp;
		}
		
		private static ArrayList GetExportableMethods(Type t){
			ArrayList met = new ArrayList();
			// we get all methods that return a serializable type, else throw an error 
			// (all return types from methods of a MarshalByRefObject must be serializable)
			// and it makes no sense to get private members.
			foreach(MethodInfo mi in t.GetMethods( BindingFlags.DeclaredOnly |BindingFlags.Instance |BindingFlags.Public)){
				if(mi.IsPublic && mi.ReturnType.IsSerializable && !(mi.IsSpecialName)){
					met.Add(mi);
					Console.WriteLine("adding method "+mi.Name);
				}
				else if(mi.IsPublic && !(mi.ReturnType.IsSerializable))
					Console.WriteLine("WARNING : method "+mi.Name+" returns a "+mi.ReturnType.ToString()+" which is not serializable, omitting...");
				
			}
			return met;
		}
		
		private static ArrayList GetExportableAccessors(Type t){
			ArrayList acc = new ArrayList();
			foreach(FieldInfo fi in t.GetFields(BindingFlags.NonPublic | BindingFlags.Instance))
				Console.WriteLine("private member="+fi.Name);
			return acc;
		}
		
		private static ArrayList GetExportableProperties(Type t){
			ArrayList props = new ArrayList();
			foreach(PropertyInfo pi in t.GetProperties(System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.Public |BindingFlags.DeclaredOnly)){
				if(pi.PropertyType.IsSerializable)
					props.Add(pi);
				else
					Console.WriteLine("WARNING : public property "+pi.Name+" returns a "+pi.PropertyType.ToString()+" which is not serializable, omitting...");
			}
			return props;
		}
		
	}
}

