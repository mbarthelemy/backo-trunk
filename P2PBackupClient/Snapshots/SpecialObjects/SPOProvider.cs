using System;
using System.Collections.Generic;
using P2PBackup.Common;

//  create table specialobjects(nodeid integer, spopath varchar, password integer);

namespace Node.Snapshots{
	public class SPOProvider{
		/*public SPOProvider (){
		}*/
		
		public static ISpecialObject GetByCategory(string objectName, Backup backup, P2PBackup.Common.BackupLevel level, ProxyTaskInfo pti){
			switch(objectName){
			case "VSS":
				return new VSS(level);
			case "VMWare VM configuration":
				return new VMWareVmConfig(pti);
			case "StorageLayout":
				return new StorageLayoutSPO(backup.StorageLayout);
			/*case "VADP":
				return new VMWareVADP(level);*/
			/*case "MYSQL":
				return new Mysql(level);*/
			default:
				throw new Exception("No Special object matching category '"+objectName+"'");
				
				
			}
		}
		
		public static List<string> ListAvailableProviders(){
			List<string> providers = new List<string>();
			if(!Utilities.PlatForm.IsUnixClient())
				providers.Add("VSS");
			providers.Add("VMWARE");
			providers.Add ("VMWare VM configuration");
			providers.Add("MYSQL");
			providers.Add("LIBVIRT");
			return providers;
		}
	}
}

