using System;
//using System.Collections;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Collections.Generic;
//using System.Runtime.Serialization.Formatters.Binary;
//using System.Runtime.Serialization.Formatters;
//using System.Xml.Serialization;
using System.Linq;
using System.Diagnostics;
using P2PBackup.Common;
using P2PBackup.Common.Volumes;
using Node.Utilities;
using Node.Snapshots;
using Node.DataProcessing;
using Node.StorageLayer;
//using Mono.Unix;
//using Mono.Unix.Native;

namespace Node{
	/// <summary>
	/// Summary description for BFileList.
	/// </summary>
	[Serializable]
	public class Backup:BackupSet{
		
		private string indexFileName = "";
		private static string pathTemp;
		/*private long maxChunkSize;
		private long maxPackSize;
		private int maxChunkFiles;*/
		
		//private long currentSize;
		/*private bool compress;
		private bool encrypt;
		private bool clientDedup;
		private int redundancy;*/
		private static long taskId;
		private BackupSet bs;
		internal BackupSet Bs{get{return bs;}}
		//private int nbChunks;
		// incr and diffs:
		
		private List<ISnapshot> backupSnapshots;
		private List<BackupRootDrive> backupRootDrives;
		private List<ISpecialObject> specialObjects;
		private StorageLayoutManager slManager;
		//private Dictionary<string, byte[]> driveIncrMetadata;

		//internal BackupIndex Index{get;private set;}
		internal Index Index{get;private set;}
		
		internal long OriginalSize{get;set;}
		internal long FinalSize{get; set;}
		internal long RefTaskId{get;private set;}
		internal long RefStartDate;
		internal long RefEndDate;
		internal StorageLayout StorageLayout{get; private set;}
		public int TotalChunks{get;set;}
		//number of entries backuped (files, directories, links...)
		public int TotalItems{get;set;}
		public int[] ItemsByType{get;set;}
		private int completionBase;
		public int SubCompletion{get;set;}

		public string CurrentAction{
			get{return "";}
			set{
				HubNotificationEvent(taskId, 700 /*snapshotting*/, value, "");
			}
		}
		//private int completionPercent;
		
		/// <summary>
		/// passes back messages about backup processing to user, in order to send it to Hub for tracing and statistics purpose
		/// </summary> 
		public delegate void HubNotificationHandler(long taskId, int code, string data, string additionalMessage);
		public event HubNotificationHandler HubNotificationEvent;//User.HubSendTaskEvent()
		
		public int CompletionBase{
			get{
				if(completionBase == 0)
					completionBase = GetCompletionBase();
				return completionBase;
			}
		}
		
		/*public bool IsCompressed{
			get{ return compress;}	
			set{ compress = value;}
		}
		
		public bool IsClientDeduped{
			get{ return clientDedup;}	
			set{ clientDedup = value;}
		}
		
		public bool IsEncrypted{
			get{ return encrypt;}	
			set{ encrypt = value;}
		}
		
		public long MaxChunkSize{
			get{return maxChunkSize;}
		}
		
		public int MaxChunkFiles{
			get{return maxChunkFiles;}
		}
		
		public long MaxPackSize{
			get{return maxPackSize;}
		}
		
		public int Redundancy{
			get{return redundancy;}	
		}
		
		
		
		public int ClientId{ //only for serialization as index (we need to maintain nodeid inside index for redundancy and crash recovery purposes)
			get{return int.Parse(ConfigManager.GetValue("Node.Id"));}
			set{;}
		}*/
		
		public BackupLevel Level{
			get{return this.bs.ScheduleTimes[0].Level;}	
		}
		
		public string Version{
			get{ return Utilities.PlatForm.Instance().NodeVersion;}	
			set{ ;}	
		}
		
		
		/*public int BsId{
			get{return bsId;}
			set{bsId = value;}
		}*/
		
		internal long TaskId{
			get{return taskId;}	
		}
		

		public string IndexFileName{
			get {return indexFileName; }
			set {indexFileName = value; }	
		}
		
		
		public List<BackupRootDrive> RootDrives{
			get{return backupRootDrives;}
		}
		
		public delegate void UpdateGUIHandler (string command, string param);
		public delegate void BackupDoneHandler();
		//public event BackupDoneHandler BackupDoneEvent;//User.FileSent()
		
		internal Backup(){
			
		}

		~Backup(){
			Logger.Append(Severity.DEBUG2, "<TRACE> Backup destroyed.");	
		}
		/// <summary>
		/// Constructor for FULL backups. 
		/// </summary>
		/// <param name="bs">
		/// A <see cref="BackupSet"/>
		/// </param>
		/// <param name="ctaskId">
		/// A <see cref="System.Int32"/>
		/// </param>
		public Backup(BackupSet bs, long ctaskId){
			this.bs = bs;
			taskId = ctaskId;
			base.MaxChunkSize = bs.MaxChunkSize;
			base.MaxPackSize = bs.MaxPackSize;
			base.MaxChunkFiles = bs.MaxChunkFiles;

			base.DataFlags = bs.DataFlags;
			base.Redundancy = bs.Redundancy;
			base.Parallelism = bs.Parallelism;
			pathTemp = ConfigManager.GetValue("Backups.TempFolder");
			//bChunks = new List<BChunk>();
			//this.currentSize = 0;
			TotalChunks = 0;
			TotalItems = 0;
			completionBase = 0;
			SubCompletion = 0;
			ItemsByType = new int[15];
			//driveIncrMetadata
			//Index = new BackupIndex(this);
			
			
		}
		/// <summary>
		///Constructor for incremental or differential backup.
		/// </summary>
		/// <param name="bs">
		/// A <see cref="BackupSet"/>
		/// </param>
		/// <param name="ctaskId">
		/// A <see cref="System.Int32"/>
		/// </param>
		/// <param name="refStart">
		/// A <see cref="DateTime"/> (UTC)
		/// </param>
		/// <param name="refEnd">
		/// A <see cref="DateTime"/> (UTC)
		/// </param>
		public Backup(BackupSet bs, long ctaskId, long refTaskId, long refStart, long refEnd):this(bs, ctaskId){
			/*this.bs = bs;
			taskId = ctaskId;
			this.maxChunkSize = bs.MaxChunkSize;
			this.maxPackSize = bs.MaxPackSize;
			this.maxChunkFiles = bs.MaxChunkFiles;
			this.compress = bs.Compress;
			this.encrypt = bs.Encrypt;
			this.clientDedup = bs.ClientDedup;
			this.redundancy = bs.Redundancy;*/
			/*
			pathTemp = ConfigManager.GetValue("Backups.TempFolder");
			bChunks = new List<BChunk>();
			this.currentSize = 0;
			this.nbChunks = 0;*/
			this.RefTaskId = refTaskId;
			this.RefStartDate = refStart;
			this.RefEndDate = refEnd;
		}


		internal void PrepareAll(){

			// lauch pre-backup commands/scripts
			string [] cmdOuts = ExecuteCommand(this.Preop);
			HubNotificationEvent(taskId, 710, "STDOUT", cmdOuts[0]);
			HubNotificationEvent(taskId, 710, "STDERR", cmdOuts[1]);

			// Gather the FSs paths used by special objects (if there are any)
			//Dictionary<string, SPOMetadata> spoMetadatas = PrepareSpecialObjects();
			List< Tuple<string, SPOMetadata, List<string>>> spoMetadatas  = PrepareSpecialObjects();
			// Telling Special objects to tell the app they manage to put themselves into backup mode (if possble)
			// and/or freeze their IOs
			Logger.Append(Severity.INFO, "Freezing "+this.specialObjects.Count+" special objects...");
			foreach(ISpecialObject spo in this.specialObjects)
				spo.Freeze();

			// Now snapshot (if requested). 
			PrepareDrivesAndSnapshots();

			Index = new Index();
			Index.Header = new IndexHeader{TaskId=taskId, BackupType=bs.ScheduleTimes[0].Level};
			//Index.Header.RootDrives = backupRootDrives;
			Index.Create(taskId, (this.Bs.ScheduleTimes[0].Level != BackupLevel.Full), this.RootDrives);
			Index.WriteHeaders();

			// Collect incremental providers metadata (for subsequent backups)
			foreach(BackupRootDrive brd in backupRootDrives){
				Dictionary<string, byte[]> provMetadata = IncrementalPluginProvider.SignalFullBackup(this.TaskId, brd);
				if(provMetadata == null) continue;
				foreach(KeyValuePair<string, byte[]> kp in provMetadata){
					Logger.Append(Severity.DEBUG2, "Signaled backup to Incremental providers, got metadata from "+kp.Key);
					Index.AddProviderMetadata(kp.Key, brd, kp.Value);
				}
			}
			if(spoMetadatas != null)
				//foreach(KeyValuePair<string,SPOMetadata> spoMetadata in spoMetadatas){
				foreach(Tuple<string, SPOMetadata, List<string> > tuple in spoMetadatas){
					if(tuple.Item2 != null)
						using(MemoryStream mStr = new MemoryStream()){
							(new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter()).Serialize(mStr, tuple.Item2);
							Index.AddProviderMetadata(tuple.Item1, backupRootDrives[0], mStr.GetBuffer());
							Index.AddSpecialObject(tuple.Item1, tuple.Item3);
						}
					else
						Logger.Append(Severity.WARNING, "Could'nt save metadata from provider '"+tuple.Item1+"' : metadata is null");
				
				}



		}
		
		/// <summary>
		/// This method first looks if backup needs snapshotting drives. If so, then we do snapshot
		/// and use it to call BuildBackup method
		/// </summary>
		private void PrepareDrivesAndSnapshots(){

			SnapshotSupportedLevel snapLevel = ConvertBackupLevelToSnLevel(this.Level);
			Logger.Append(Severity.INFO, "Preparing backup for Task "+taskId);
			HubNotificationEvent(taskId, 701 /*snapshotting*/, "", "");
			//SystemDrive[] devices = GetBackupInvolvedSystemDrives();
			
			backupRootDrives = GetBackupInvolvedRootDrives();
			Console.WriteLine ("backupRootDrives count="+backupRootDrives.Count);
			string[] snapshotProvNames = new string[backupRootDrives.Count];
			for(int i=0; i<backupRootDrives.Count; i++){
				if(backupRootDrives[i].RequiresSnapshot)
					snapshotProvNames[i] = SnapshotProvider.GetDriveSnapshotProviderName(backupRootDrives[i].SystemDrive.MountPoint);
				else
					snapshotProvNames[i] = "NONE";
			}
			// the final list of all snapshot needed to perform the backup set
			backupSnapshots = new List<ISnapshot>();
			// We try to make all snapshot in the shortest amount of time we can, 
			// for data to be as consistent as possible
			foreach(string snapProv in snapshotProvNames.Distinct()){
				Console.WriteLine ("snapshotProvNames="+snapProv);
				List<FileSystem> snapShotMemberDrives = new List<FileSystem>();
				for(int i=0; i< backupRootDrives.Count; i++){
					if(snapshotProvNames[i] == snapProv)
						snapShotMemberDrives.Add(backupRootDrives[i].SystemDrive);
				}
				//Gather SpecialObjects (VSS writers) if any
				var spoList = from BasePath bp in bs.BasePaths
						where bp.Type != null && bp.Type.ToLower().StartsWith("object:")//P2PBackup.Common.BasePath.PathType.OBJECT
						select bp.Path;
				
				ISnapshotProvider snapProvider = SnapshotProvider.GetProvider(snapProv);
				snapProvider.LogEvent += LogReceivedEvent; 
				ISnapshot[] volSnaps;
				try{

					volSnaps =  snapProvider.CreateVolumeSnapShot(snapShotMemberDrives, spoList.ToArray(), snapLevel);
					//if(snapProvider.Metadata != null)
					//	Index.Header.ProviderMetadata.Add(new Tuple<string, Hashtable>(snapProvider.Name, snapProvider.Metadata.Metadata));
					string volList = "";
					foreach(FileSystem vol in snapShotMemberDrives) volList += vol.MountPoint+",";
					Logger.Append(Severity.INFO, "Took snapshots (type "+snapProvider.Type+") of drives "+volList);
				}
				catch(Exception e){
					// we return a fake snapshot (snapshot path is the volume itself)
					string volList = "";
					foreach(FileSystem vol in snapShotMemberDrives)
						volList += vol.MountPoint+",";
					Logger.Append(Severity.WARNING, "Unable to take snapshot of drives "+volList+", falling back to fake provider. Error was : "+e.Message+" --- "+e.StackTrace);
					HubNotificationEvent(taskId, 805 /*cant snapshot*/, volList, e.Message);
					ISnapshotProvider nullProv = SnapshotProvider.GetProvider("NONE");
					volSnaps = nullProv.CreateVolumeSnapShot(snapShotMemberDrives, spoList.ToArray(), snapLevel);
				}
				backupSnapshots.AddRange(volSnaps);
				snapProvider.LogEvent -= LogReceivedEvent; 
			}
			
				//finally, add snapshot to corresponding BackupRootDrive
				Console.WriteLine ("##### dumping rootdrives : ");
				foreach(BackupRootDrive rd in backupRootDrives){
					
					foreach(ISnapshot snap in backupSnapshots){
						if(/*snap.MountPoint*/snap.Path == rd.SystemDrive.MountPoint){
							rd.Snapshot = snap;
							Console.WriteLine("matched snapshot : "+snap.Path+", snap mount path="+snap.MountPoint);
							// let's change paths and excluded paths to their snapshotted values
							foreach(BasePath bp in rd.Paths){
								//bp.Path = snap.Path + bp.Path;
								/*if(snap.Name == "/")
									bp.Path = snap.Path;
								else
									bp.Path = bp.Path.Replace(snap.Name, snap.Path);*/
								for(int i=0; i< bp.ExcludedPaths.Count; i++){
									//bp.ExcludedPaths[i] = snap.Path +  bp.ExcludedPaths[i];
									if(snap.Path == "/")
										bp.ExcludedPaths[i] = snap.MountPoint + bp.ExcludedPaths[i];
									else{
										bp.ExcludedPaths[i] = bp.ExcludedPaths[i].Replace(snap.Path, snap.MountPoint);
										//bp.ExcludedPaths[i] = bp.ExcludedPaths[i].Replace(snap.Name, "");
										//bp.ExcludedPaths[i] = Path.Combine(snap.Path, bp.ExcludedPaths[i]);
										//bp.ExcludedPaths[i] = Path.GetFullPath(bp.ExcludedPaths[i]);
									}
								}
							}
							break;
						}
					}
					Console.WriteLine("BackupRootDrive id="+rd.ID+", mount="+rd.SystemDrive.MountPoint/*+", snapshot= "+rd.snapshot.Path*/);
					foreach(BasePath p in rd.Paths) {
						Console.WriteLine("\t p="+p.Path+", excludes : ");
						foreach(string s in p.ExcludedPaths)
							Console.WriteLine("\t excluded "+s);
						Console.WriteLine("\t\t, include patterns : ");
						foreach(string s in p.IncludePolicy)
							Console.WriteLine("\t match "+s);
					}	
				}
				Console.WriteLine ("#####   #####");
		}	
		
		SnapshotSupportedLevel  ConvertBackupLevelToSnLevel(BackupLevel backupLevel){
			if(backupLevel == BackupLevel.Full || backupLevel == BackupLevel.SyntheticFull)
				return SnapshotSupportedLevel.Full;
			/*else if (backupLevel == BackupLevel.Differential)
				return SnapshotSupportedLevel.Differential;*/
			else if (backupLevel == BackupLevel.Refresh)
				return SnapshotSupportedLevel.Incremental;
			else
				return SnapshotSupportedLevel.Full;
		}
		
		/// <summary>
		/// Group basepaths by SPO type, call each SPO to inject relevant basepaths and
		/// expand/traduce it as additional basepaths
		/// </summary>
		private List< Tuple<string, SPOMetadata, List<string> >> PrepareSpecialObjects(){

			List<string> spoProviders = SPOProvider.ListAvailableProviders();
			//Dictionary<string, SPOMetadata> spoMetadatas = new Dictionary<string, SPOMetadata>();
			List< Tuple<string, SPOMetadata, List<string> >> spoMetadatas = new List<Tuple<string, SPOMetadata, List<string>>>();
			this.specialObjects = new List<ISpecialObject>();
			Console.WriteLine ("PrepareSpecialObjects() : 1");
			foreach(string provName in spoProviders){
				if(string.IsNullOrEmpty(provName)) continue;
				var provItems = (from BasePath b in bs.BasePaths 
						where b!= null && !(string.IsNullOrEmpty(b.Type)) && b.Type.ToLower().StartsWith("object:") //BasePath.PathType.OBJECT 
				        && b.Path != null && b.Type.ToLower().Substring(b.Type.IndexOf(":")+1) == provName.ToLower()
						select b.Path).ToList<string>();
				Console.WriteLine ("PrepareSpecialObjects() : 2");
				if(provItems == null || provItems.Count == 0) continue;
				Console.WriteLine ("PrepareSpecialObjects() : 3 : "+string.Join(",", provItems));
				ISpecialObject spo = SPOProvider.GetByCategory(provName, this, this.bs.ScheduleTimes[0].Level, this.bs.ProxyingInfo);

				Console.WriteLine ("PrepareSpecialObjects() : 4");
				spo.SetItems(provItems);
				specialObjects.Add(spo);
				//spoMetadatas.Add(spo.Name, spo.Metadata);
				spoMetadatas.Add(new Tuple<string, SPOMetadata, List<string>>(spo.Name, spo.Metadata, spo.ExplodedComponents));
				bs.BasePaths.AddRange(spo.BasePaths);
				/*foreach(BasePath bp in spo.BasePaths){
					Console.WriteLine ("*** basepath="+bp.Path+", includepol="+bp.IncludePolicy+", recursive="+bp.Recursive);	
				}*/

			}
			int beforeFatorize = bs.BasePaths.Count;
			Console.WriteLine ("bs count1="+beforeFatorize);
			FactorizePaths();
			FactorizePaths();
			return spoMetadatas;
		}
		
		/// <summary>
		/// Factorizes all BasePaths by merging them when possible.
		/// </summary>
		private void FactorizePaths(){
			Console.WriteLine ("bs null? : "+(bs == null));
			Console.WriteLine ("bs basep null?: "+(bs.BasePaths == null));
			int beforeFatorize = bs.BasePaths.Count;
			Console.WriteLine ("bs count2="+beforeFatorize);
			bs.BasePaths = (from BasePath b in bs.BasePaths 
			                where b!=null && b.Path!=null
			                orderby b.Path descending 
			                select b).ToList();//bs.BasePaths.OrderByDescending(path => path.Path).ToList();
			for(int i = bs.BasePaths.Count-1; i >= 1; i--){
				if(bs.BasePaths[i].Path == bs.BasePaths[i-1].Path){
					bs.BasePaths[i-1] = MergeRules(bs.BasePaths[i-1], bs.BasePaths[i]);
					bs.BasePaths[i-1].Path = ExpandFilesystemPath(bs.BasePaths[i-1].Path);
					bs.BasePaths.RemoveAt(i);
				}
				else if(bs.BasePaths[i].Path.IndexOf(bs.BasePaths[i-1].Path) == 0 
						&&  bs.BasePaths[i-1].Recursive
						&& (bs.BasePaths[i-1].IncludePolicy == null ||  bs.BasePaths[i-1].IncludePolicy.Count == 0)
						&& !bs.BasePaths[i-1].ExcludedPaths.Contains(bs.BasePaths[i].Path)){
					bs.BasePaths[i-1] = MergeRules(bs.BasePaths[i-1], bs.BasePaths[i]);
					bs.BasePaths.RemoveAt(i);
				}
			}
			Logger.Append(Severity.DEBUG2, "Factorized and reduced paths from "+beforeFatorize+" to "+bs.BasePaths.Count);
		}
		
		private BasePath MergeRules(BasePath first, BasePath second){
			first.ExcludedPaths.AddRange(second.ExcludedPaths);
			first.ExcludePolicy.AddRange(second.ExcludePolicy);
			first.IncludePolicy.AddRange(second.IncludePolicy);
			if(first.Recursive || second.Recursive)
				first.Recursive = true;
			return first;
		}
					
		private void FactorizeBasePathRules(){
			
		}
		
		/// <summary>
		/// Replaces environment variables (if found) by their values
		/// </summary>
		/// <param name='path'>
		/// Path.
		/// </param>
		private string ExpandFilesystemPath(string path){
			//string[] pathItems = path.Split(new char[]{Path.DirectorySeparatorChar}, StringSplitOptions.RemoveEmptyEntries);
			//foreach(string item in pathItems){
				return Environment.ExpandEnvironmentVariables(path);
			//}
			
		}
		/// <summary>
		/// Gets the completion base as number of depth-1 subdirectories.
		/// Used to calculate backup completion %
		/// </summary>
		/// <returns>
		/// The completion base.
		/// </returns>
		private int GetCompletionBase(){
			int cBase = 0;
			if(backupRootDrives == null) return 0; // storage layout not yet initialized
			try{
				foreach(BackupRootDrive brd in backupRootDrives){
					foreach(BasePath bp in brd.Paths){
						foreach(string f in Directory.EnumerateDirectories(bp.Path, "*", SearchOption.TopDirectoryOnly))
							cBase++;
						}
						
				}
			}catch{} // will throw exception on proxied backups 
			return cBase+1;
		}
		
		
		public void Terminate(bool isSuccessful){
			Logger.Append(Severity.DEBUG, "Calling Terminate");
			//if( isSuccessful && bs.BackupTimes[0].Type == BackupType.Full)
			//		IncrementalProvider.SignalFullBackup();	
			// delete snapshots

			try{
				// Execute post-backup commands
				string[] postCmdOut = ExecuteCommand(this.Postop);
				HubNotificationEvent(taskId, 711, "STDOUT", postCmdOut[0]);
				HubNotificationEvent(taskId, 711, "STDERR", postCmdOut[1]);
				Logger.Append(Severity.DEBUG, "Disposing storage layout builders...");
				if(slManager != null)
					slManager.Dispose();
			}
			catch(Exception e){
				Logger.Append(Severity.ERROR, "Error cleaning task resources : "+e.ToString ());
			}
			if(bs.SnapshotRetention == 0)
				foreach(BackupRootDrive rootDrive in this.backupRootDrives){
					try{
						//string prov = SnapshotProvider.GetDriveSnapshotProviderName(rootDrive.SystemDrive.MountPoint);
						SnapshotProvider.GetProvider(rootDrive.Snapshot.Type).Delete(rootDrive.Snapshot);
						//SnapshotProvider.GetProvider(prov)

					}
					catch(Exception e){
						AddHubNotificationEvent(906, rootDrive.Snapshot.Path, e.Message);	
					}
				}
			Logger.Append(Severity.DEBUG, "Terminated");
		}
		
		/// <summary>
		/// Get all system volumes (Drives under Windows, Mount points under Unix) involved 
		/// in this particular backupset.
		/// We my use this list to take snapshots (if requested) before backuping.
		/// </summary>
		/// <returns>
		/// A <see cref="Hashtable"/>
		/// </returns>
		/*public string[] GetBackupInvolvedDevices(){
			IPathBrowser pb = PathBrowser.GetPathBrowser();
			string[] systemDrives = pb.GetDrivesNames();
			List<string> drives = new List<string>();
			foreach(BasePath path in bs.BasePaths){
				//Console.WriteLine("GetBackupInvolvedDevices : path="+path.Path);
				foreach(string sysDrive in systemDrives){
					if(path.Path.IndexOf(sysDrive) >= 0){
						drives.Add(sysDrive);
						//Console.WriteLine("match : path "+path.Path+", drive "+sysDrive);
					}
				}
			}
			return drives.Distinct().ToArray();
		}*/
		
		/// <summary>
		/// Get all system volumes (Drives under Windows, Mount points under Unix) involved 
		/// in this particular backupset.
		/// We my use this list to take snapshots (if requested) before backuping.
		/// </summary>
		/// <returns>
		/// A <see cref="SpecialDrive"/> array.
		/// </returns>
		/*internal SystemDrive[] GetBackupInvolvedSystemDrives(){
			// we sort drives by reverse mountpoint length : on unix this allows us to find, for example,
			// /home before / when asked to backup '/home/user'
			var drivesByNameLength = from sd in VolumeManager.Instance().GetAllDrives() 
				orderby sd.MountPoint.Length descending 
				select sd;
			List<SystemDrive> drives = new List<SystemDrive>();
			// first pass : 
			foreach(BasePath path in bs.BasePaths){
				// avoid to try snapshotting a false drive for a path that doesn't exist
				if(!Directory.Exists(path.Path))
				   continue;
				foreach(SystemDrive sysDrive in drivesByNameLength){
					// fallback, for solaris. Buggy. <TODO> Correct that
					if(path.Path.IndexOf(sysDrive.MountPoint) == 0 && !drives.Contains(sysDrive)){
						drives.Add(sysDrive);
						Console.WriteLine("match : path "+path.Path+", drive "+sysDrive.MountPoint);
						break;
					}
				}
			}
			return drives.ToArray();
		}*/
		
		private bool IsDriveExcluded(BasePath bp, FileSystem sd){
			foreach(string exclude in bp.ExcludedPaths){
				//Console.WriteLine ("Searching if "+sd.MountPoint+" is excluded from rule "+exclude);
				if(sd.MountPoint.IndexOf(exclude) == 0){
					//Console.WriteLine ("(yes)");
					return true;
				}
			}
			return false;
		}
		
		private List<BackupRootDrive> GetBackupInvolvedRootDrives(){
			// we sort drives by reverse mountpoint length : on unix this allows us to find, for example,
			// '/home' before '/' when asked to backup '/home/user'
			Int16 rootDriveId = 0;
			char[] pathSeparators = new char[]{Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar, '/', '\\'};
			// old way of doping things, with only flat filesystems
			/*var drivesByNameLength = from sd in FilesystemManager.Instance().GetAllDrives() 
											orderby sd.MountPoint.Length descending 
											select sd; */



			// check that all backup base paths refer to the same provider
			var storageProvs = (from bp in bs.BasePaths where 
			                    	bp != null 
			                    	&& (!string.IsNullOrEmpty(bp.Type)) 
			                    	&& bp.Type.ToLower().StartsWith("fs") 
			                    select bp.Type).Distinct().ToList();
			if(storageProvs.Count > 1)
				throw new Exception ("Only 1 storage provider type supported by task, but here found "+string.Join(",", storageProvs));
			if(storageProvs.Count == 0)
				throw new Exception ("No storage provider was found. storage provider must be in 'FS, FS:,OBJECT, BLOCK'");
			// gather all paths to backup and send them to the storagelayout (allows it to build a partial layout including only the layout necessary for this task)
			var taskPathsToBackup = (from bp in bs.BasePaths 
			                        where bp != null && !string.IsNullOrEmpty(bp.Type) && bp.Type.ToLower().StartsWith("fs") 
			                    	select bp.Path).ToList();
			slManager = new StorageLayoutManager(taskPathsToBackup);
			slManager.LogEvent += this.LogReceivedEvent;
			//List<FileSystem> allFSes = new List<FileSystem>();
			//string storageP = bs.BasePaths[0].
			//Logger.Append (Severity.DEBUG2, "Building storage layout using provider '"+storageProvs[0]+"'...");
			if (storageProvs[0].IndexOf(":") >=0)
				storageProvs[0] = storageProvs[0].Split(new char[]{':'}, StringSplitOptions.None)[1];
			this.StorageLayout = slManager.BuildStorageLayout(storageProvs[0], bs.ProxyingInfo);
			//sl.GetAllFileSystems(sl.Entries, ref allFSes);

			Logger.Append(Severity.DEBUG, "Got "+this.StorageLayout.GetAllFileSystems(null).Count()+" raw FSes");

			var fsMountsByNameLength = from sd in this.StorageLayout.GetAllFileSystems(null) 
											where (! string.IsNullOrEmpty(sd.MountPoint))
											orderby sd.MountPoint.Length descending 
											select sd;

			Logger.Append(Severity.DEBUG, "Got "+fsMountsByNameLength.Count()+" FSes");
			List<BackupRootDrive> drives = new List<BackupRootDrive>();
			// We first expand the basepaths defined by backupset configuration
			//  in order to manage nested mountpoints (eg : backups tells to save /usr, but this path contains /usr/local 
			//  which is a mountpoint to another drive
			
			for(int i= bs.BasePaths.Count-1; i>=0; i--){

				/*Console.WriteLine ("basepath :i="+i);
				Console.WriteLine ("basepath :path="+bs.BasePaths[i].Path);
				Console.WriteLine ("basepath :type="+bs.BasePaths[i].Type);*/
				//Console.WriteLine ("basepath :i="+i);
				if(bs.BasePaths[i].Type != null && bs.BasePaths[i].Type.ToLower().StartsWith("object:"))//BasePath.PathType.OBJECT)
					continue;
				//Console.WriteLine ("basepath :i="+i+", lulute 0");
				foreach(FileSystem filesystem in fsMountsByNameLength){
					if(filesystem.DriveFormat == "proc" || filesystem.DriveFormat == "sysfs" || filesystem.DriveFormat == "debugfs"
					   		|| filesystem.DriveFormat == "devpts" || filesystem.DriveFormat == "procfs"){
							Logger.Append (Severity.DEBUG2, "GetBackupInvolvedRootDrives() : excluded non-backupable fs  "+filesystem.MountPoint);
							Logger.Append(Severity.INFO, "Excluded fs "+filesystem.MountPoint+" from "+bs.BasePaths[i].Path+" (non-backupable fs)");
							bs.BasePaths[i].ExcludedPaths.Add(filesystem.MountPoint);
							continue;
					}
					//Console.WriteLine ("basepath :i="+i+", lulute 1");
					//if(IsDriveExcluded(bs.BasePaths[i], filesystem)) continue;

					if(string.IsNullOrEmpty(filesystem.OriginalMountPoint)){
						Logger.Append(Severity.NOTICE, "Proxied Filesystem '"+filesystem.MountPoint+"' has unknown original mountpoint, will be backuped with current mountpoint as root"); 
						filesystem.OriginalMountPoint = filesystem.MountPoint;
					}
					//Console.WriteLine ("basepath :i="+i+", lulute 2");
					if(bs.BasePaths[i].Path == "*" || filesystem.OriginalMountPoint.IndexOf(bs.BasePaths[i].Path) == 0 && bs.BasePaths[i].Path !=filesystem.Path ){
						BasePath bp = new BasePath();
						bp.Path = filesystem.MountPoint;
						bp.Type = "FS"; //BasePath.PathType.FS;
						// inherit include/exclude rules
						bp.IncludePolicy = bs.BasePaths[i].IncludePolicy;
						bp.ExcludePolicy = bs.BasePaths[i].ExcludePolicy;
						bs.BasePaths.Add(bp);
						Logger.Append (Severity.DEBUG2, "GetBackupInvolvedRootDrives() : added path "+bp.Path);
					}
					//Console.WriteLine ("basepath :i="+i+", lulute 3");
				}
			}
			//Console.WriteLine ("basepath : step 2");
			foreach(BasePath path in bs.BasePaths){

				foreach(FileSystem fsMount in fsMountsByNameLength){
					//Console.WriteLine ("basepath.Path="+path.Path+", current fs path="+fsMount.Path+", mntpt="+fsMount.MountPoint+", origmntpt="+fsMount.OriginalMountPoint);
					// if drive is explicitely deselected (excluded), don't add it to list
					//if(IsDriveExcluded(path, sysDrive))
					//		continue;

					// special case of "*" has been treated before and splitted into 1 BasePath per filesystem, so ignore it
					if(	path.Path == "*") continue;
					if(	/*path.Path == "*"
						||*/ path.Path.IndexOf(fsMount.MountPoint) == 0 
					   	|| (path.Path.EndsWith(Path.PathSeparator+"*") && path.Path.Substring(0, path.Path.LastIndexOfAny(pathSeparators)) == 
					 		fsMount.Path.Substring(0, fsMount.Path.LastIndexOfAny(pathSeparators)))
					   ){
						Console.WriteLine ("basepath.Path="+path.Path+" MATCHES");
						// 1/2 exclude weird/pseudo fs
						if(fsMount.DriveFormat == "proc" || fsMount.DriveFormat == "sysfs" || fsMount.DriveFormat == "debugfs"
					   		|| fsMount.DriveFormat == "devpts" || fsMount.DriveFormat == "procfs"){
							Console.WriteLine ("GetBackupInvolvedRootDrives() : excluded mountpoint  "+path.Path+" (non-backupable fs)");
							continue;
						}
						
						/*if(udi.DriveFormat */
						
						// first pass : add basepaths as defined in backupset configuration
						// if drive doesn't exist yet in rootdrives, add it. Else, add path to existing rootdrive
						bool found = false;
						foreach(BackupRootDrive rd in drives){
							Console.WriteLine (" @@@@ GetBInvoldedRd :cur rd="+rd.SystemDrive.OriginalMountPoint+", fsmountpath="+fsMount.Path);
							if(rd.SystemDrive.OriginalMountPoint == fsMount.OriginalMountPoint){
								rd.Paths.Add(path);
								found = true;
							}
						}
						if(found == false/* && !IsDriveExcluded(path, sysDrive)*/){
							Console.WriteLine (" @@@@ GetBInvoldedRd : added new rootdrives to list : "+fsMount.OriginalMountPoint+" for path "+path.Path);
							BackupRootDrive rootDrive = new BackupRootDrive();
							rootDrive.SystemDrive = fsMount;
							rootDrive.Paths.Add(path);
							rootDrive.ID = rootDriveId;
							drives.Add(rootDrive);
							rootDriveId++;
						}
						//Console.WriteLine("match : path "+path.Path+", drive "+sysDrive.MountPoint);
						break; //avoid continuing scanning  until '/' (would be false positive)
					}
				}
			}
			
			return drives;
		}
				

		/// <summary>
		/// Executes the command. Used for backup tasks pre- ans post- commands
		/// </summary>
		/// <returns>
		/// The command outputs, as : [0] = stdout, [1] = stderr
		/// </returns>
		/// <param name='cmd'>
		/// The command(s) to be executed
		/// </param>
		private string[] ExecuteCommand(string cmd){
			string[] outputs = new string[2]; // stdout and stderr
			if(cmd == null || cmd == string.Empty) return outputs;
			ProcessStartInfo pi;
			if(Utilities.PlatForm.IsUnixClient())
				pi = new ProcessStartInfo("sh", "-c \""+@cmd+"\"");
			else
				pi = new ProcessStartInfo("cmd.exe", "/c \""+@cmd+"\"");
			pi.RedirectStandardOutput = true;
			pi.RedirectStandardError = true;
			pi.UseShellExecute = false;
			Process p = Process.Start(pi);
			p.WaitForExit();
			//p.ExitCode;
			outputs[0] = p.StandardOutput.ReadToEnd();
			outputs[1] = p.StandardError.ReadToEnd();
			return outputs;
		}
		
		public int CompletionPercent{
			get{
				if(CompletionBase == 0) return 0;
				return (int)(Math.Round((double)SubCompletion/CompletionBase,2)*100);
			}
		}
		
		internal void AddHubNotificationEvent(int code, string message, string additionalData){
			HubNotificationEvent(taskId, code, message, additionalData);	
		}

		private void LogReceivedEvent(object sender, LogEventArgs args){
			Logger.Append(args.Severity, args.Message);
			if(args.Code >0){
				HubNotificationEvent(this.TaskId, args.Code, "", args.Message);
			}
			
		}

		/*public void Dispose(){
			slManager.Dispose();
		}*/

	}
}
