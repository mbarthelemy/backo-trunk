using System;
using System.IO;
using System.Text;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Threading;
using Node.DataProcessing.Utils;
using Node.DataProcessing;
using Node.Utilities;

namespace Node{
	
	// A Bchunk is an unit ready to get stored by a node.
	// it has a maximum fixed size, and can consist of:
	// - several files, smaller than Bchunk naxsize, packed together
	// -or-
	// - one piece of file, if file is bigger than Bchunk maxsize.
	// AND always an index file
	// It represent the minimal stored unit on nodes.
	// this way we achieve good performances, avoiding to transmit, calculate, index on hub, and keep track 
	// of, many small files. This choice was made because the majority o files on an average system are very small (<1M)
	// and because, on storage nodes, it saves disk blocks and inodes (if storage is on *nix).
	// It is always created on the client node. It can be compressed and/or encrypted (both locally before sending))
	// A Bchunk index file is a binary file which contains :
	// -A version field
	// -A clientName file
	[Serializable]
	public class BChunk{
		
		private List<IFSEntry> filesList;
		private long originalSize; // raw size
		private long size; // final size, after post-processing (compression a/o encryption)
		
		private string name; // file name of the chunk
		private bool sent = false;
		private bool fetched = false;
		// chunk redundancy is, in normal situations, equals to backupset's (wanted, theorical) redundancy.
		// However, if the Hub couldn't find enough storage nodes to satisfy it for this chunk, we will update this property to 
		// the effective redundancy level provided by Hub.
		private int effectiveRedundancy; 
		//private string clientName;	// the username of the destination client
		//private bool isEncrypted = true;
		//private bool isCompressed = true;
		private int bsId;
		private List<int> storageDestinations;
		private string bPath;
		private string snapPath;
		
		[NonSerialized] private ManualResetEventSlim sentEvent;
		
		internal ManualResetEventSlim SentEvent{
			get{return sentEvent;}
			set{sentEvent = value;}
		}


		public string Name{
			get{ return name;}
		}
			
		/*public int BsId{
			get{return bsId;}
		}*/
		
		public long OriginalSize{
			get{return originalSize;}	
			set{originalSize = value;}
		}
		
		public long Size {
			get {return size;}
			set{size = value;}
		}
		public long TaskId{get;private set;}
		public int Order{get;set;}
		public Int16 RootDriveId{get; set;}
		
		public bool Sent{
			get{ return sent;}
			set{sent = value;}
		}
		
		public int Redundancy{
			get{return effectiveRedundancy;}
			set{effectiveRedundancy = value;}
		}
		
		public string Sum{get; set;}
		
		public bool Fetched{
			set{fetched = value;}
			get{return fetched;}
		}
		
		public List<int> StorageDestinations{
			get{ return storageDestinations;}
		}
		
		/*internal MemoryStream DataStream{
			get{return dataStream;}
		}*/
		/*public bool IsEncrypted{
			set{isEncrypted = value;}	
			get{return isEncrypted;}
		}
		
		public bool IsCompressed{
			set{isCompressed = value;}
			get{return isCompressed;}
		}*/
		
		/*public string ClientName{
			get {return clientName; }
			set {clientName = value; }
		}*/
		
		//add nodeid to the list and return destinations count (to check if redundancy level is satisfied)
		public bool AddDestination(int nodeId){
				storageDestinations.Add(nodeId);
			return (storageDestinations.Count == this.Redundancy);
		}
		
		/*internal void MarkAsSent(){
			sentEvent.Set();	
		}*/
		
		public List<IFSEntry> Files{
			get{ return filesList;}
		}
		
	/*	public BChunk(){
			this.name = CreateChunkName(bPath);
			this.sentEvent = new ManualResetEvent(false);
		}*/
		
		public BChunk (/*int bsid,*/ string bPath, string snapPath, long taskId){
			this.bPath = bPath;
			this.snapPath = snapPath;
			this.TaskId = taskId;
			filesList = new List<IFSEntry>();
			storageDestinations = new List<int>();
			originalSize = 0;
			size = 0;
			Order = 0;
			Sum = String.Empty;
			//currentPos = 0;
			this.name = CreateChunkName(bPath);
			/*this.bsId = bsid;*/
			this.sentEvent = new ManualResetEventSlim(false);
		}
		
		// posForSplittedFiles is a counter that increments while we go forward inside big file
		// it only purpose is to get really unique chunk name on platforms that don't provide
		// an accurate Ticks value (virtualbox....)
		/*public BChunk (string bPath, string snapPath, long taskId, long posForSplittedFiles):this(bPath, snapPath, taskId){
	
			this.name = CreateChunkName(bPath+posForSplittedFiles);

		}*/
		
		public void Add(IFSEntry bf){
			filesList.Add(bf);	
			//originalSize += bf.FileSize;
		}
		
		private string CreateChunkName(string path){
			
				return Guid.NewGuid().ToString().ToUpper();
		}
		
		private static string ByteArrayToString(byte [] toConvert){
			StringBuilder sb = new StringBuilder(toConvert.Length);
			for (int i = 0; i < toConvert.Length - 1; i++){
				sb.Append(toConvert[i].ToString("X"));
			}
			return sb.ToString();
		}
		
		/*public long Encrypt(){	
			string cPath = ConfigManager.GetValue("Backups.TempFolder") + Path.DirectorySeparatorChar + name;
			File.Move(cPath, cPath + ".wrk");
			
			RSACryptoServiceProvider keyPair = null;
			GetKeyPair(out keyPair);
			FileStream fsCrypt = null;
			FileStream fsKey = null;
			FileStream fsInput = null;
			CryptoStream cs = null;
			byte [] key;
			byte [] IV;
			long writtenBytes = 0;
			try{
				//Console.WriteLine("DEBUG: 1-BChunk.EncryptFile source="+cPath+".wrk  dest="+cPath);
				fsKey = new FileStream(cPath, FileMode.Create);
				//Console.WriteLine("DEBUG: 2-BChunk.EncryptFile created fskey "+cPath);
				// generate the session key
				RijndaelManaged AES = new RijndaelManaged();
				AES.GenerateIV();
				AES.GenerateKey();
				key = AES.Key;
				IV = AES.IV;

				using (fsKey){
					// encrypt iv and key with the public key
					byte [] encryptedIV = keyPair.Encrypt(AES.IV, false);
					byte [] encryptedKey = keyPair.Encrypt(AES.Key, false);
					// write the encrypted iv and key
					fsKey.Write(encryptedIV, 0, encryptedIV.Length);
					fsKey.Flush();
					fsKey.Write(encryptedKey, 0, encryptedKey.Length);
					fsKey.Flush();
				}
				//Console.WriteLine("DEBUG: 3-BChunk.EncryptFile creating fsCrypt"+cPath);
				fsCrypt = new FileStream(cPath, FileMode.Append);
				//Console.WriteLine("DEBUG: 4-BChunk.EncryptFile, initialized encrypted file "+cPath);
				ICryptoTransform aesencrypt = AES.CreateEncryptor(key, IV);
				cs = new CryptoStream(fsCrypt, aesencrypt, CryptoStreamMode.Write);
				fsInput = new FileStream(cPath+".wrk", FileMode.Open);
				//Console.WriteLine("DEBUG: 5-BChunk.EncryptFile, opened source file "+cPath+".wrk");
				//int data;
				
				// write all data to the crypto stream and flush it
				
				byte[] buffed = new byte[8192] ;
				int rdbyte = 0;
				while(rdbyte<fsInput.Length){
					fsInput.Position = rdbyte;
					int i = fsInput.Read(buffed,0,buffed.Length) ;
					
					cs.Write(buffed, 0, i);
					rdbyte=rdbyte+i ;
					}
				
				
				cs.FlushFinalBlock();
				writtenBytes = fsCrypt.Length;
				Logger.Append(Severity.DEBUG, "BChunk.Encrypt", "Encrypted chunk "+name);
			}
			catch (Exception ex){
				Logger.Append(Severity.ERROR, "BChunk.Encrypt", "Cannot encrypt Bchunk : "+ex.Message);
				// TODO : rethrow to abort backup in case of impossible encryption? or keep doing backup partly unencrypted is better? 
			}
			finally{	
				fsInput.Close();
				cs.Close();
				fsCrypt.Close();
				File.Delete(cPath + ".wrk");
			}
			return writtenBytes;
		}*/
		
		/* TO USE EXISTING CERTIFICATE
		 byte[] l_EncryptedData = null;
        byte[] l_Data = null;
        X509Certificate2 l_X509Certificate2 = FindCertificateBySerialNoAndName(a_CertificateName, a_SerialNo);
        if (l_X509Certificate2 != null)
        {
          RSACryptoServiceProvider l_RSA = (RSACryptoServiceProvider)l_X509Certificate2.PublicKey.Key;
          using (FileStream l_FileStream = new FileStream(a_FilePath, FileMode.Open))
          {
            l_Data = new byte[l_FileStream.Length];
            l_FileStream.Read(l_Data, 0, (int)l_FileStream.Length);
          }
          l_EncryptedData = l_RSA.Encrypt(l_Data, true); 
        }

        using (FileStream l_FileStream = new FileStream(a_FilePath + ".enc", FileMode.Create, FileAccess.Write))
        {
          l_FileStream.Write(l_EncryptedData, 0, l_EncryptedData.Length);
        }
        
        */
		

		
		/*internal void RemoveItem(IFile item){
			for(int i=filesList.Count-1;i==0;i--){
				if(filesList[i].SnapshottedFullPath == item.SnapshottedFullPath)
					filesList.RemoveAt(i);
			}
		}*/
		
		
	}
}

