using System;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
//using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using Node.Utilities;
using P2PBackup.Common;

namespace Node{

	/// <summary>
	/// A session is created when the first request for a transfer (send or receive) between 2 nodes is received. 
	/// The session stays open until a task ends (backup or restore done) to allow reuse.
	/// </summary>
	internal class Session : PeerSession{
		private Socket clientSocket;
		private Socket dataSocket;
		private string clientName;
		private string clientKey;
		private string clientIp;
		private int clientId;
		private int clientPort;
		private RSACryptoServiceProvider myKeyPairCrypto;
		private string recoveryPath;
		private Verification ver;
		//private int sessionType;//1:SendBack, 2:GetRecov, 3:ReceiveBack, 4:SendRecov}
		private SessionType sessionType;
		internal delegate void RemoveSessionHandler (Session s);
		internal event RemoveSessionHandler RemoveSessionEvent;//User.RemoveSession()
		//public delegate void FileSentHandler (bool sent, BChunk bFSent);
		public delegate void TransfertDoneHandler (bool sent, long taskId, string chunkName, int destination, int finalSize);
		public event TransfertDoneHandler TransfertDoneEvent;//User.FileSent()
		public delegate void FileReceivedHandler (bool received, BChunk bFReceived);
		public event FileReceivedHandler FileReceivedEvent;//User.
		public delegate void UpdateStorageHandler (long taskid, long receivedData, int initialBudget);
		public event UpdateStorageHandler UpdateStorageEvent;//User.
		public delegate void MessageHandler (long taskid, int code, string data);
		public event MessageHandler MessageEvent;//User.
		//public delegate void AuthenticatedAndReadyHandler();
		internal ManualResetEvent AuthenticatedAndReadyEvent;//User.
		//private   AsyncCallback asyncCallback = new AsyncCallback(EndReadClient);
		private SessionLogger logger;
		private long currentChunkSize;
		private int initialBudget;
		//internal int Budget{get;private set;}
		//internal long TaskId{get; private set;}
		private long storedDataSize; // total size of data stored during the session (after processing, so real storage used)
		private CancellationTokenSource cancellationTokenSource;
		private bool disconnecting = false;
		private DataProcessing.DataPipeline pipeline; // data processing pipeline

		// used as symmetric encryption key (if tasks requires pipeline to encrypt data
		// IV will be the task ID.
		internal byte[] GuidKey{get;private set;}
		internal byte[] CryptoKey{get;private set;} // used if data encryption is required

		internal Socket ClientSocket{
			get{ return clientSocket;} // temp, for debug
			set { clientSocket = value; }
		}
		
		internal Socket DataSocket{
			get{ return dataSocket;} // temp, for debug
			set { dataSocket = value; }
		}

		internal string ClientIp{
			get { return clientIp; }
		}
		
		internal int ClientPort{
			get{ return clientPort;}	
		}
		
		internal int ClientId{
			get{ return clientId;}
		}

		internal SessionType Type{
			set { sessionType = value; }
			get{ return sessionType;}
		}
		
		internal SessionLogger LoggerInstance{
			get{return logger;}
		}

		public Session(SessionType sT, int sessionId, int nodeId, string cIp, int port, string cN, string cK, RSACryptoServiceProvider kpc){
			logger = new SessionLogger(this);
			logger.Log(Severity.DEBUG, "Creating client session #"+sessionId+" with storage node #"+nodeId+" ("+cIp+":"+port+")");
			sessionType = (SessionType)sT;
			this.Id = sessionId;
			clientId = nodeId;
			clientIp = cIp;
			clientPort = port;
			clientName = cN;
			clientKey = cK;
			myKeyPairCrypto = kpc;
			recoveryPath = ConfigManager.GetValue("pathTempRestore");
			ver = new Verification(myKeyPairCrypto, clientKey);
			AuthenticatedAndReadyEvent = new ManualResetEvent(false);
			Budget=0;
			this.GuidKey = System.Guid.NewGuid().ToByteArray();
			//this.CryptoKey = this.GuidKey;
			// Only used if Client-side encryption is required ; else guid is used for storage-side encryption.
			this.CryptoKey = System.Guid.NewGuid().ToByteArray();

		}
		
		// Storage (data receive) session
		public Session(SessionType sT, int nodeId, string cIp, string cN, string cK, RSACryptoServiceProvider kpc, string sP, int budget, long taskId, DataProcessingFlags pipelineFlags){
			logger = new SessionLogger(this);
			sessionType = (SessionType)sT;
			logger.Log(Severity.DEBUG, "Creating storage session ("+sessionType.ToString()+") with client node "+cN+" ("+cIp+":<UNAVAILABLE>)");
			clientId = nodeId;
			clientIp = cIp;
			clientName = cN;
			clientKey = cK;
			this.Budget = budget;
			this.initialBudget = budget;
			this.TaskId = taskId;
			this.GuidKey = System.Guid.NewGuid().ToByteArray();
			myKeyPairCrypto = kpc;
			ver = new Verification(myKeyPairCrypto, clientKey);
			AuthenticatedAndReadyEvent = new ManualResetEvent(false);
			Budget=0;
			DataProcessingFlags storageFlags = DataProcessingFlags.None;
			foreach (DataProcessingFlags flag in Enum.GetValues(typeof(DataProcessingFlags))){
				if((int)flag >= 512 && pipelineFlags.HasFlag(flag))
					storageFlags |= flag;
			}
			pipeline = new Node.DataProcessing.DataPipeline(Node.DataProcessing.PipelineMode.Read, storageFlags);
		}
		
		// for debug purpose
		~Session(){
			Logger.Append(Severity.DEBUG2," <TRACE> Session destroyed");	
		}
		
		/// <summary>
		/// Connects to client2
		/// </summary>
		public void ConnectToStorageNode(Object o){
			Logger.Append(Severity.DEBUG, "Trying to start connection to node "+clientIp+", port "+clientPort);
			IPAddress addr = IPAddress.Parse(clientIp);
			IPEndPoint dest = new IPEndPoint(addr, clientPort);
			// Opening control socket
			clientSocket = new Socket(AddressFamily.InterNetwork,SocketType.Stream,ProtocolType.Tcp);
			if(ConfigManager.GetValue("STDSOCKBUF") == null)
				try{
					clientSocket.SendBufferSize = 0;
					clientSocket.NoDelay = true;
				}
				catch{} // not supported on some platforms (freebsd)
			//clientSocket.Connect(dest);
			IAsyncResult result = clientSocket.BeginConnect(dest, null, null);
			result.AsyncWaitHandle.WaitOne(30000, true);
			if(!clientSocket.Connected)
				throw new Exception("Could not connect message socket");
			// Opening data socket
			dataSocket = new Socket(AddressFamily.InterNetwork,SocketType.Stream,ProtocolType.Tcp);
			try{
				if(ConfigManager.GetValue("STDSOCKBUF") == null) dataSocket.SendBufferSize = 512*1024+1; //512k
				dataSocket.NoDelay = false;
			}
			catch{} 
			dataSocket.SendTimeout = 120000;
			//dataSocket.Connect(dest);
			result = dataSocket.BeginConnect(dest, null, null);
			result.AsyncWaitHandle.WaitOne(30000, true);
			if(!dataSocket.Connected)
				throw new Exception("Could not connect data socket");
			/*if( !clientSocket.Connected || !dataSocket.Connected){
            	clientSocket.Close();
				Logger.Append(Severity.ERROR, "Could not connect to storage node "+dest.ToString());
            	throw new Exception("Failed to connect to storage node.");
			}*/
			//clientSocket.Connect(dest);
			StartListening();
			SendDigitalSignature("DS1", this.GuidKey);
			Thread.Sleep(200);
			logger.Log(Severity.INFO, "Opened 1 session with storage node #"+clientId);
			//}
			//catch(SocketException sex){
			//	logger.Log(Severity.ERROR,"Session.ConnectToStorageNode","Could not connect to node "+this.clientName+" on IP "+clientIp
			//	              +" ("+sex.Message+"). Asking alternate destination to Hub...");	
			//	throw new Exception();
			//}
			//catch(Exception ex){
			//	logger.Log(Severity.ERROR,"Session.ConnectToStorageNode",ex.Message); 
			//}
		}
		
		public class StateObject {
		    // Client socket.
		    public Socket workSocket = null;
		    // Size of receive buffer.
		    public const int BufferSize = 2048;
		    // Receive buffer.
		    public byte[] buffer = new byte[BufferSize];
		}
		
		/// <summary>
		/// Sets the streams to the socket, creates a new thread that listens
		/// </summary>
		public void StartListening(){
			//try	{
				clientSocket.ReceiveTimeout = -1;
				try{
					clientSocket.ReceiveBufferSize = 0;
					clientSocket.NoDelay = true;
				}catch{}
				//clientSocket.ReceiveBufferSize = 131072;
				StateObject state = new StateObject();
        		state.workSocket = clientSocket;
       			//clientSocket.BeginReceive( state.buffer, 0, StateObject.BufferSize, 0, this.EndReadClient, state);
				clientSocket.BeginReceive( state.buffer, 0, 4, 0, this.HeaderReceived, state);
				logger.Log(Severity.DEBUG, "Listening for messages from "+clientSocket.RemoteEndPoint.ToString());
			//}
			//catch(Exception ex)	{
			//	logger.Log(Severity.ERROR, "SESSION.StartListening",ex.Message); 
			//}
		}
		
		private void HeaderReceived (IAsyncResult ar){
			StateObject so = (StateObject) ar.AsyncState;
			
			int read = 0;
			try{	
				clientSocket = so.workSocket;
				read = clientSocket.EndReceive(ar);
			}
			catch{
				Disconnect();	
			}
			if(read == 0 && ! disconnecting){
				logger.Log(Severity.ERROR, "Peer node #"+clientId+" has disconnected. (read 0)");
				Disconnect();
				return;
				//throw new SocketException();
			}
			
			//logger.Log(Severity.DEBUG2, "Received message header, msg size will be "+msg_length);
			try{
				int msg_length = BitConverter.ToInt32(so.buffer, 0);
				if(!disconnecting)
					clientSocket.BeginReceive( so.buffer, 0, msg_length/*so.buffer.Length*/, 0, this.MessageReceived, so);
			}
			catch(Exception e){
				logger.Log(Severity.ERROR, "Peer node has disconnected ("+e.Message+")");
				//throw(e);
			}
		}
		
		private void MessageReceived(IAsyncResult ar){
			try{
				StateObject so = (StateObject) ar.AsyncState;
				clientSocket = so.workSocket;
				int read = clientSocket.EndReceive(ar);
				Decode(Encoding.UTF8.GetString(so.buffer, 0, read));
				if(!disconnecting)
					clientSocket.BeginReceive( so.buffer, 0, 4, 0, this.HeaderReceived, so);
			}
			catch(Exception ioe){
				logger.Log(Severity.ERROR, "Error reading data, Disconnecting session. (" + ioe.Message+")"+" ---- "+ioe.StackTrace);
				Disconnect();
			}
		}	
		
		/// <summary>
		/// Decodes the incoming messages
		/// </summary>
		/// <param name="message">the incoming message</param>
		private void Decode(string message){
			message = message.Replace(System.Environment.NewLine, "");
			string[] decoded = message.Split(' ');
			string msgExcerpt = null;
			if(message.Length > 45)
				msgExcerpt = message.Substring(0,45);
			else
				msgExcerpt = message;
			logger.Log(Severity.DEBUG2, "Received message '"+message+"'");
			string type = decoded[0].Trim();
			switch(type){
				case "CRC"://Checksum
					if(decoded.Length == 2){
					}
					break;
				case "END":
					UpdateStorageEvent(this.TaskId, storedDataSize, this.Budget);
					Disconnect();
					break;
				case "ERR":
					UpdateStorageEvent(this.TaskId, storedDataSize, this.Budget);
					Disconnect();
					break;
				case "CHD": // error while storing chunk
					if(decoded.Length == 3){
						currentChunkSize = long.Parse(decoded[2]);
						cancellationTokenSource.Cancel();
					}
					else
						ProtocolViolationException("CHD operation requires 2 parameters");
					break;
				case "DS1"://Digital Signature from client node
					if(decoded.Length == 3){
						//logger.Log(Severity.DEBUG, "received DS1, NOT called clientVerification yet.");
						bool verifPeer = ver.CheckDigitalSignature(decoded[1],decoded[2]);
						//if(hash != null){
						if(verifPeer){
							// Use Original string as cryptokey if encryption is done at the storagenode level
							this.CryptoKey = Convert.FromBase64String(decoded[1]);
							EndInit();
							logger.Log(Severity.DEBUG2, "received DS1, ver.ClientVerification is ok, successfully checked signature of client.");
							SendDigitalSignature("DS2", this.GuidKey);
						}
						else{
							logger.Log(Severity.ERROR, "failed to check signature from client, check keys.");
							SendMessageToNode("506");
						}
					}
					else
						ProtocolViolationException("DS1 operation expects 3 parameters, but got "+decoded.Length+". Raw message :"+message);
					
					break;
				case "DS2"://Digital Signature from storage node 
					if(decoded.Length == 3){
						logger.Log(Severity.DEBUG, "Received DS2, digital signature from storage node "+this.clientIp + "("+this.clientName+")");
						
						if(ver.CheckDigitalSignature(decoded[1],decoded[2])){   // 20110331&& (ver.MyVerification)){
							//SendMessageToNode("213");
							logger.Log(Severity.INFO, "Storage node verification Ok, connection and authentication successful");
							AuthenticatedAndReadyEvent.Set();
						}
						else{
							logger.Log(Severity.ERROR, "Problem with ClientVerification or MyVerification TODO : ASK ALTERNATE DESTINATION");
						}	
					}
					else
						ProtocolViolationException("DS2 operation expects 3 parameters, but got "+decoded.Length+". Raw message :"+message);
					break;
				case "FIL":
					if(decoded.Length == 3){ 
						cancellationTokenSource = new CancellationTokenSource();
        				var cancellationToken = cancellationTokenSource.Token;
						System.Threading.Tasks.Task.Factory.StartNew(() =>{
							ReceiveChunk(decoded[1], int.Parse(decoded[2]), cancellationToken);
						}, cancellationTokenSource.Token, TaskCreationOptions.LongRunning, TaskScheduler.Default);
					
					}
						
					break;
				case "GET":
					if(decoded.Length == 2){
						if(sessionType == SessionType.RecoverData){
							/*if(File.Exists(this.sharePath + Path.DirectorySeparatorChar + decoded[1])){
								CheckForFileSize(decoded[1]);
							}
							else{
								SendMessageToNode("507");
								Console.WriteLine("INFO : Session.Decode : Asked file not found "+decoded[1]);
							}*/
						}
					}
					break;
				case "204"://Verification of client1 succeded
					if(decoded.Length == 1){
						ver.LocalVerification = true;
					}
					else
						ProtocolViolationException("204 operation expects 1 parameter, but got "+decoded.Length+". Raw message :"+message);
					
					break;
				case "205": //File received
					if(decoded.Length == 3 /*&& RemoveSessionEvent != null*/){
						if(sessionType == SessionType.Backup && TransfertDoneEvent != null){
							TransfertDoneEvent(true, 0, /*bf.BsId*/ decoded[1], this.ClientId, int.Parse(decoded[2]));
						}
						if(sessionType == SessionType.RecoverData){
							//Disconnect();
							//RemoveSessionEvent(this,sessionType);
						}
					}
					else
						ProtocolViolationException("205 operation expects 2 parameter and removesessionevent, but got "+decoded.Length+". Raw message :"+message);
					break;
				case "213"://Verification of client2 succeded
					if(decoded.Length == 1){
						ver.LocalVerification = true;
						//ContinueWork();
					}
					else
						ProtocolViolationException("213 operation expects 1 parameter, but got "+decoded.Length+". Raw message :"+message);
					break;
				case "406"://No storage space for backup requested by me
					logger.Log(Severity.ERROR, "No storage space available for backup.");
					break;
				case "506"://Client sent unrecognized key.
					logger.Log(Severity.DEBUG, "RECEIVED 506 (wrong key)!!!!!!!!!!!!!!!!!!!!!!");
					break;
				case "507" :
					logger.Log(Severity.ERROR, "received 507  <TODO> request other destination having another copy of this file");
					break;
				case "OOS":
					logger.Log(Severity.ERROR, "Received OOS (Peer claims to be out of space)");
					break;
				case "RSD" : // RSD, ReSenD missing part of chunk
					logger.Log(Severity.WARNING, "received RSD (resend petition) for chunk "+decoded[1]);
					break;
				default : 
					logger.Log(Severity.WARNING, "received message with unknown type : "+msgExcerpt+"...");
					//throw new Exception(); // DEBUG hack : on weird message, choose to crash
					break;
			}
		}

		/// <summary>
		/// Closes the socket and streams for this session
		/// if the connected client has disconnected
		/// Sets the bool attributes to false
		/// </summary>
		internal  void Disconnect(){
			if(disconnecting) return;
			disconnecting = true;
			//Thread.MemoryBarrier();
			logger.Log(Severity.DEBUG, "Ending session #"+this.Id+" with node #"+ClientId+" (" + ClientIp+":"+ClientPort+")...");
			try{
				clientSocket.Shutdown(SocketShutdown.Both);
				clientSocket.Close();
				dataSocket.Shutdown(SocketShutdown.Both);
				dataSocket.Close();
			}
			catch(Exception e){
				logger.Log(Severity.INFO, "Socket was probably already closed by sudden client disconnection : "+e.Message);
			}
			finally{
				if(RemoveSessionEvent != null)	RemoveSessionEvent(this);
				logger.Log(Severity.DEBUG, "Session #"+this.Id+" with node " + ClientIp+":"+ClientPort+" ended.");

				
				//MessageEvent(taskId, 799, clientIp);
			}
		}

		private void EndInit(){
			if(this.Type != SessionType.Store)
				return;
			logger.Log(Severity.DEBUG2, "Finishing storage session niit.");
			// TODO: IMPORTANT  find a much better place to put all this
			pipeline.CryptoKey = this.CryptoKey;
			byte[] iv = new byte[16];
			Array.Copy (System.BitConverter.GetBytes(this.TaskId), iv, 8);
			Array.Copy (System.BitConverter.GetBytes(this.TaskId), 0, iv, 8, 8);
			pipeline.IV = iv; //new byte[]{Conver

			pipeline.Init();
		}

		/// <summary>
		/// Sends original message and digital signature from client1 with command DS1.
		/// Sends original message and digital signature from client2 with command "DS2"
		/// </summary>
		/// <param name="command">DS1 or DS2</param>
		public void SendDigitalSignature(string command, byte[] original/*string original*/){
			//Console.WriteLine("DEBUG: 1-Session.SendDigitalSignature : command "+command);
			//string original = "";
			string digSig = "";
			//ver = new Verification(
			digSig = ver.CreateDigitalSignature(original);
			//Console.WriteLine("DEBUG: 1-Session.SendDigitalSignature : called, writing to socket :"+command + " " + original ); //+ " " + digSig);
			Logger.Append(Severity.DEBUG, "Created and sent digital signature (step "+command+") to peer");
			SendMessageToNode(command + " " + Convert.ToBase64String(original) + " " + digSig);
			//Console.WriteLine("DEBUG: 2-Session.SendDigitalSignature : sent digital signature");
		}


		/// <summary>
		/// Sends filname and filesize to client2
		/// </summary>
		/// <param name="fileName">filename</param>
		/// <param name="fileSize">filesize</param>
		internal void AnnounceChunkBeginTransfer(string fileName, long fileSize){
			SendMessageToNode("FIL " + fileName + " " + fileSize);
		}
		
		internal void AnnounceChunkEndTransfer(string fileName, long fileSize){
			SendMessageToNode("CHD " + fileName + " " + fileSize);
			Budget--;
		}

		/// <summary>
		/// Tells the other client that a recoveryfile should be fetched
		/// </summary>
		/// <param name="fileName"></param>
		private void GetFile(string fileName){
			SendMessageToNode("GET " + fileName);
		}
		
		/// <summary>
		/// Renews the budget.To be called when hub asks us to receive more chunks than previously budgeted
		/// </summary>
		/// <param name='additionalBudget'>
		/// Additional budget.
		/// </param>
		internal void RenewBudget(int additionalBudget){
			Budget += additionalBudget;	
			logger.Log(Severity.INFO, "Renewed budget to "+Budget);
		}
		
		private void ReceiveChunk(string chunkName, int headerSize, CancellationToken token){
			if(Budget == 0){
				logger.Log(Severity.ERROR, "Can't receive chunk, authorized budget is 0 (zero)");
				SendMessageToNode("OOB "+chunkName); //out-of-budget
				Disconnect();
				return;
			}
			string storePath = null;
			currentChunkSize = 0;
			if(sessionType == SessionType.RecoverData)
				storePath = this.recoveryPath + Path.DirectorySeparatorChar + chunkName;	
			else if(sessionType == SessionType.Store)
				storePath = Utilities.Storage.GetStoragePath(chunkName);
			DateTime start = DateTime.Now;
			long rdbyte = 0;
			int received = 0;
			try	{
				//dataSocket.ReceiveTimeout = 30000;
				logger.Log(Severity.DEBUG, "1/2 Receiving chunk from client node "+clientName+", header size "+headerSize+", destination "+ storePath);
				using(Stream file = PlatformStreamProvider.Instance().GetPlatformStream(true, storePath, FileMode.CreateNew)){
					pipeline.OutputStream = file;
					pipeline.Init();
					byte[] buffer = new byte[512*1024]; //512k
					long bytesToReceive = long.MaxValue;
					while(rdbyte < bytesToReceive){
						Console.WriteLine ("@1/4 after preparing to receive()");
						if(dataSocket.Available == 0 && !cancellationTokenSource.IsCancellationRequested){
							Console.WriteLine ("ReceiveChunk() : cancellationTokenSource.IsCancellationRequested : "+cancellationTokenSource.IsCancellationRequested);
							Thread.Sleep(10);
							continue;
						}
						if(cancellationTokenSource.IsCancellationRequested){// transfert done
							bytesToReceive = currentChunkSize;
							Console.WriteLine("received cancellation request, final size="+bytesToReceive+", received until now="+rdbyte);
							if(rdbyte >= bytesToReceive)
								break;
						}
						if(dataSocket.Available<=0) continue;
						Console.WriteLine ("@2/4before Receive()");
						received = dataSocket.Receive(buffer, buffer.Length, SocketFlags.None);
						Console.WriteLine ("@3/4after Receive()");
						//Console.WriteLine("received="+received+", total="+rdbyte);
						rdbyte += received;
						//file.Write(buffer,0,received) ;
						pipeline.Stream.Write(buffer,0,received) ;
						Console.WriteLine ("@4/4after pipeline Write()");
					}
					pipeline.Stream.Flush();
					logger.Log(Severity.DEBUG2, "Received "+rdbyte+"/"+bytesToReceive+", final size after processing="+/*pipeline.Stream.Length*/pipeline.FinalSize);
					SendMessageToNode("205 "+chunkName+" "+pipeline.FinalSize);

				}// end using. Output file is automatically closed.
				// on *Nix, set sticky bit to prevent accidntal deletion of the chunk (it's backuped data!!)

				double transferTime = (DateTime.Now - start).TotalMilliseconds;
				if(rdbyte == headerSize || rdbyte == 0) // don't store empty chunks
					DeleteChunk(chunkName, false);
				storedDataSize += pipeline.FinalSize;
				pipeline.Reset();
				Budget--;
				logger.Log(Severity.DEBUG, "2/2 Successfully received chunk "+chunkName+", size="+Math.Round((double)rdbyte/1024)+"KB,"
					+"speed="+Math.Round((double)(rdbyte/1024)/(transferTime/1000),1)+" KB/s), remaining budget="+Budget);
				if(Budget == 0){
					UpdateStorageEvent(this.TaskId, storedDataSize, initialBudget); // TODO : replace receivedData with pipeline final size
					storedDataSize = 0;
				}
				/*if(sessionType == 2 && FileReceivedEvent != null){
					FileReceivedEvent(true,bf);
				}*/
			}
			catch(Exception e){
				Logger.Append(Severity.ERROR, "Error while receiving data chunk : "+e.ToString());
				if(sessionType == SessionType.Store)
					DeleteChunk(chunkName, false);
				SendMessageToNode("ERR "+chunkName);
				/*if(soe.ErrorCode == 10054){
					// something weird happened at network level. Ask for retransmission of missing part of the chunk
					SendMessageToNode("RSD "+chunkName+" "+rdbyte);
				}
				else
					Disconnect();
			}
			catch(IOException ioe){
				Logger.Append(Severity.ERROR, "Error reading data, Disconnecting session. " + ioe.Message+" ---- "+ioe.StackTrace);
				if(sessionType == SessionType.Store){
					File.Delete(storePath);
					SendMessageToNode("OOS "+chunkName);	//out-of-space on disk
				}
				Disconnect();
			}
			catch(Exception ex)	{
				Console.WriteLine("ERROR : Session.ReceiveFile : fileName "+storePath+", "+ex.Message+"   "+ex.StackTrace);
				SendMessageToNode("ERR "+chunkName);*/
			}

		}	

		/// <summary>
		/// Deletes a chunk.
		/// </summary>
		/// <returns>
		/// The deleted chunk size (== recovered space)
		/// </returns>
		/// <param name='chunkName'>
		/// Chunk name.
		/// </param>
		/// <param name='confirm'>
		/// if true, confirm to peer node that chunk has been deleted
		/// </param>
		private long DeleteChunk(string chunkName, bool confirm){
			string chunkFullPath = Utilities.Storage.GetStoragePath(chunkName);
			try{
				long size = (new FileInfo(chunkFullPath)).Length;
				File.Delete(chunkFullPath);
				if(confirm)
					SendMessageToNode("206 "+chunkName+" "+size);
				return size;
			}
			catch(Exception e){
				this.LoggerInstance.Log(Severity.ERROR, "Could not delete chunk "+chunkName+" : "+e.Message);
				if(confirm)
					SendMessageToNode("806 "+chunkName);//error : cant delete chunk
			}
			return 0;
		}
		
		internal void SendDisconnect(){
			SendMessageToNode("END");
			Disconnect();
		}
		
		private void SendMessageToNode(string message){
			if(disconnecting) return; // avoid racy situations
			byte[] byteMsg = Encoding.UTF8.GetBytes(message);
			int msgSize = byteMsg.Length;
			byte[] header = BitConverter.GetBytes(msgSize); // header always has 'int' size (4 bytes)
			try{
				clientSocket.Send(header);
				clientSocket.Send(byteMsg);
				logger.Log(Severity.DEBUG2, "Sent "+message);
			}
			catch(Exception e){
				logger.Log(Severity.ERROR, "Can't send message '"+message+"' to peer node : "+e.Message);
				throw(e);
			}
			
		}
		
		private void ProtocolViolationException(string msg){
			Console.WriteLine("ERROR : Session.Decode : protocol violation exception : "+msg);
		}
	}
}
