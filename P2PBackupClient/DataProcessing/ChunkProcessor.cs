using System;
using System.IO;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using P2PBackup.Common;
using Node.Utilities;

namespace Node.DataProcessing{

	// to replace DataPipeline Process() method. DataPipeline must be reduced to only a mean of building a serie/pipeline of data procvessing streams.
	internal class ChunkProcessor	{

		DataPipeline pipeline;
		Session storageSession;
		Backup backup;
		//IDataProcessorStream firstStream;
		Stream sessionDataStream;
		//NullSinkStream finalStream;
		private byte[] headerData;
		private bool cancelRequested = false;

		internal int HeaderLength{
			get{return headerData.Length;}
		}

		internal ChunkProcessor(Session session, DataPipeline p, Backup b){

			backup = b;
			pipeline = p;
			storageSession = session;
			BinaryFormatter formatter = new BinaryFormatter();
		 	BChunkHeader header = new BChunkHeader();
			/*header.Compressed = compress;
			header.Encrypted = encrypt;*/
			header.DataFlags = pipeline.Flags;
			header.Version = Utilities.PlatForm.Instance().NodeVersion;
			//header.TaskId = taskId;
			header.TaskId = session.TaskId;
			//header.TaskId = b.TaskId;
			MemoryStream headerStream = new MemoryStream();
			formatter.Serialize(headerStream, header);
			headerData = headerStream.ToArray();



			// end-of-chain stream
			sessionDataStream = new NetworkStream(storageSession.DataSocket);
			p.OutputStream = sessionDataStream;
			p.Init();
		}


		internal void Process(BChunk chunk, long maxChunkSize){

			IDataProcessorStream pipelineStream = pipeline.Stream;
			//pipeline.Stream.SetLength(0);
			pipeline.Reset();
			try{
				storageSession.AnnounceChunkBeginTransfer(chunk.Name, headerData.Length);
				sessionDataStream.Write(headerData, 0, headerData.Length);
			}
			catch(Exception ioe){
				storageSession.LoggerInstance.Log(Severity.ERROR, "Network I/O error : "+ioe.Message+" ---- "+ioe.StackTrace);
				backup.AddHubNotificationEvent(904, "", ioe.Message);	
				if(ioe.InnerException != null)
					throw(ioe.InnerException);
			}
			chunk.Size = headerData.Length;
			DateTime startChunkBuild = DateTime.Now;
			Stream fs = null;
			byte[] content = new byte[1024*512]; // read 512k at once
			long /*sent = 0,*/ offset, remaining; // to know chunk final size (after pipeling processing streams)
			int read, itemReallyRead, partialRead;

			foreach(IFSEntry file in chunk.Files){

				//if(file == null || file.Kind != FileType.File)
				//	Console.WriteLine ("    DEBUG : item is not a file, name="+file.Name+", size="+file.FileSize);
				if(file.FileSize == 0)
					continue; 


				// TODO!! is that correct?? now that DataLayoutInfos is a flag
				if(file.ChangeStatus == DataLayoutInfos.NoChange || file.ChangeStatus == DataLayoutInfos.Deleted) // no data change
					continue;



				//offset = file.FileStartPos; // if a file is split into multiple chunks, start reading at required filepart pos
			    remaining = file.FileSize;
				read = 0;
				itemReallyRead = 0;
				partialRead = 0;
				try{	
					fs = file.OpenStream(FileMode.Open);
					long seeked = fs.Seek(file.FileStartPos, SeekOrigin.Begin);
					if(seeked != file.FileStartPos){
						storageSession.LoggerInstance.Log(Severity.ERROR, "Unable to seek to required position ( reached "+seeked+" instead of "+file.FileStartPos+") in file "+file.SnapFullPath);
						backup.AddHubNotificationEvent(912, file.SnapFullPath, "Seek error : wanted to go to"+file.FileStartPos+" but went to "+seeked);
					}
					// we read from snapshot but we need to set original file path:
					// TODO !!! change back filename to original path (remove snapshotteds path)
					//file.FileName = file.FileName.Replace(snapPath, bPath);
				}
				catch(Exception e){
					file.ChangeStatus = DataLayoutInfos.Invalid;
					storageSession.LoggerInstance.Log (Severity.ERROR, "Unable to open file "+file.SnapFullPath+": "+e.Message);
					backup.AddHubNotificationEvent(912, file.SnapFullPath, e.Message);
					try{
						fs.Close ();
					}catch{}
					//chunk.RemoveItem(file); // we don't want a failed item to be part of the backup index
					continue;
				}
				try{
					//Console.WriteLine ("reading item '"+file.Name+"'");
					while( (read = fs.Read(content, partialRead, content.Length-partialRead)) > 0 && itemReallyRead <= maxChunkSize && !cancelRequested){
						// if file has to be splitted, take care to read no more than maxchunksize
						if(itemReallyRead+read > maxChunkSize){
								read = (int)maxChunkSize - itemReallyRead;
							if(read == 0) break;
						}
						remaining -= read;
						partialRead += read;
						//read until reaching buffer size (else data may be corrupted by processingstreams that expect full buffer sizes unless we reach file end)
						/*if(partialRead < content.Length && remaining >0 && read >0){
							//int partiallyRead = read;
							while(read < content.Length){
								read += fs.Read(content, read, content.Length- read);
							}
							Console.WriteLine (" $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$ chunkprocessor read : read="+read+", remaining="+remaining);
						}*/


				        //offset += read;
						itemReallyRead += read;
						if(partialRead == content.Length || remaining == 0){
							pipelineStream.Write(content, 0, partialRead);
							partialRead = 0;
							//Console.WriteLine (" $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$ chunk , sent data "+partialRead); 
						}
						//else
							//Console.WriteLine (" $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$ chunkprocessor read : read="+read+", remaining="+remaining);

					}
					//Console.WriteLine ("Done reading item '"+file.Name+"', estimated size="+file.FileSize+", really read="+itemReallyRead);

					// now we correct FileSize with REAL size (which includes Alternate Streams on NT)
					// TODO 2: if total file size is < than expected, file has changed too.
					if(itemReallyRead > file.FileSize && Utilities.PlatForm.IsUnixClient()) {
						Logger.Append(Severity.WARNING, "Item '"+file.SnapFullPath+"' : size has changed during backup : expected "+file.FileSize+", got "+itemReallyRead);
						backup.AddHubNotificationEvent(903, file.SnapFullPath, itemReallyRead.ToString());	
					}
					file.FileSize = itemReallyRead;

					// Now that file has been processed, gather its metadata from processing streams, and add it to index 
					//Console.WriteLine ("Process(1/3) : about to call pipelineStream.FlushMetadata()");
					pipelineStream.FlushMetadata();

					//file.BlockMetadata.BlockMetadata.AddRange(pipelineStream.BlockMetadata);

					// UNCOMMENT!!!!
					foreach(IFileBlockMetadata mtd in pipeline.FinalStream.BlockMetadata){
						if(mtd is ClientDedupedBlocks){
							//Console.WriteLine ("chunkprocessor : item has deduped blocks ("+((ClientDedupedBlocks)mtd).Ids.Count+")");
							file.BlockMetadata.DedupedBlocks = ((ClientDedupedBlocks)mtd).Ids;
						}
						else
							file.BlockMetadata.BlockMetadata.Add(mtd);
					}
					/*if(Logger.MinSeverity >= Severity.DEBUG2)
						Logger.Append(Severity.DEBUG2, "Item "+file.Name+" has "+file.BlockMetadata.BlockMetadata.Count+" metadata blocks");
					*/


					/*pipelineStream.BlockMetadata = null; //.Clear();
					pipelineStream.BlockMetadata = new System.Collections.Generic.List<IFileBlockMetadata>();*/
					pipeline.FinalStream.BlockMetadata = new System.Collections.Generic.List<IFileBlockMetadata>();


					fs.Close();
					
					chunk.OriginalSize += itemReallyRead;
				}
				catch(Exception ioe){
					fs.Close();
					if(ioe.InnerException is SocketException){
						storageSession.LoggerInstance.Log(Severity.ERROR, "I/O error, could not process file "+file.SnapFullPath+" of chunk "+chunk.Name+": "+ioe.Message/*+"---"+ioe.StackTrace*/);
						backup.AddHubNotificationEvent(904, file.SnapFullPath, ioe.Message);	
						throw(ioe.InnerException);
					}
					else{
						storageSession.LoggerInstance.Log(Severity.ERROR, "Could not process file "+file.SnapFullPath+" of chunk "+chunk.Name+": "+ioe.Message+"---"+ioe.StackTrace);
						backup.AddHubNotificationEvent(912, file.SnapFullPath, ioe.Message);	
						continue;	
					}
				}
		  	}	// end foreach ifile
			
			
			//pipeline.Stream.Flush();
			DateTime endChunkBuild = DateTime.Now;
			TimeSpan duration = endChunkBuild - startChunkBuild;
			//Console.WriteLine ("Process() : Chunk Done 0");
			
			// !!!! TODO  TODO TODO BUG. Length SHOULD work, not throw stackoverflow with clientdedup
			//chunk.Size += pipelineStream.Length;
			
			
			//Console.WriteLine ("Process() : Chunk Done 1");
			pipeline.Stream.Flush();
			//Console.WriteLine ("Process() : flushed pipeline");
			//chunk.Size += sessionStream.Length;
			//Console.WriteLine ("Process() : Chunk Done 2");
#if DEBUG
			if(ConfigManager.GetValue("BENCHMARK") != null)
				storageSession.AnnounceChunkEndTransfer(chunk.Name, 0);
			else
				storageSession.AnnounceChunkEndTransfer(chunk.Name, pipeline.FinalSize + headerData.Length );
				//storageSession.AnnounceChunkEndTransfer(chunk.Name, chunk.Size);
#else
			storageSession.AnnounceChunkEndTransfer(chunk.Name, chunk.Size);
#endif
			//Console.WriteLine ("Process() : Chunk Done 3");
			//privilegesManager.Revoke();
			storageSession.LoggerInstance.Log(Severity.DEBUG, "Processed and transferred "+chunk.Name+", original size="+chunk.OriginalSize/1024+"k, final size="+chunk.Size/1024+"k, "+chunk.Files.Count+" files in "+duration.Seconds+"."+duration.Milliseconds+" s, "+Math.Round((chunk.OriginalSize/1024)/duration.TotalSeconds,0)+"Kb/s");
		}
		
		internal void Cancel(){
			cancelRequested = true;	
		}
	}
}

