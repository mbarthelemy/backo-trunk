using System;
using System.IO;
using System.IO.Compression; // DEBUG ONLY
using System.Diagnostics;
using Node.Utilities;
using System.Threading;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization.Formatters;
using P2PBackup.Common;

namespace Node.DataProcessing{

	//TODO write version tag in the index db file
	internal class DedupIndex	{

		private static readonly DedupIndex _instance = new DedupIndex();
		//private ConcurrentBag<DedupedBlock> index;
		private List<LightDedupedBlock> index;
		private ConcurrentBag<FullDedupedBlock> addPending;
		private BinaryFormatter formatter;
		private LightDedupedBlock lastDedupedBlock;
		private string dedupDB;
		private int currentPos;
		private int corruptedDDBCount;
		private int pendingAddCount;
		private long maxId;

		private bool initializing = false;
		internal string IndexDBName{
			get{return dedupDB;}
		}
		
		internal LightDedupedBlock LastDedupedBlock {
			get {
				return this.lastDedupedBlock;
			}
		}
		
		private DedupIndex (){

			
		}
		
		~DedupIndex(){
			Logger.Append(Severity.DEBUG2, "DESTROYED dedup instance.");
		}
		
		internal void Initialize(int bsId){
			if(initializing){
				Logger.Append(Severity.INFO, "Deduplication indexes initializing...");
				while(initializing == true)
					Thread.Sleep(250);
				return;
			}
			if(index != null){
				Logger.Append(Severity.INFO, "Deduplication indexes DB ready ("+index.Count+" items)");
				return;
			}
			initializing = true;
			addPending = new ConcurrentBag<FullDedupedBlock>();
			dedupDB = Path.Combine(Utilities.ConfigManager.GetValue("Backups.IndexFolder"),"dedup_"+bsId+".idx");
			currentPos = 0;
			pendingAddCount = 0;
			/*if(!File.Exists(dedupDB))
				File.Create(dedupDB);*/
			if(File.Exists(dedupDB+".new"))
				File.Delete(dedupDB+".new");


			index = new List<LightDedupedBlock>();
			lock(index){

				Logger.Append(Severity.INFO, "Reading deduplication indexes from DB "+dedupDB+"...");
				Stopwatch stopWatch = new Stopwatch();
	        	stopWatch.Start();


				foreach(FullDedupedBlock fddb in this.GetFullDbEnumerator())
					index.Add((LightDedupedBlock)fddb);


				stopWatch.Stop();
	        	TimeSpan ts = stopWatch.Elapsed;
				Logger.Append(Severity.INFO, "Got "+index.Count+" deduplication indexes" 
					+(corruptedDDBCount > 0 ? " ( + "+corruptedDDBCount+" corrupted) " : "")+" from DB in "+ts.TotalMilliseconds+"ms.");	
			}// end lock
			maxId = (long)index.Count;// why can't List<t> contain more that int32.max elements?
			initializing = false;
		}
		
		internal static DedupIndex Instance(){
			return _instance;
		}
		
		internal bool DeReference(byte[] checksum){
			for(int j=0; j< index.Count; j++){
				if(UnsafeCompare(index[currentPos].Checksum, checksum)){
					index[currentPos].RefCounts--;
					return true;
				}
			}
			return false;
		}
		
		/*internal int ChunkReferences(string chunkName){
			int references = 0;
			for(int j=0; j< index.Count; j++){
				if(index[j].DataChunkName == chunkName)
					references++;
			}
			return references;
		}*/

		internal int ChunkReferences(long id){
			int references = 0;
			for(int j=0; j< index.Count; j++){
				if(index[j].ID == id)
					references++;
			}
			return references;
		}


		internal bool Contains(byte[] checksum, string chunkname, int posInChunk, uint bufSize, int storageNode, ref long dedupId){
#if DEBUG
			BenchmarkStats.Instance().DedupLookups++;
#endif
			//if(index.Count > 0) {// TODO : remove this check. If list is empty, add a new element with checksum of '0'
				for(int j=0; j< index.Count; j++){
					if(UnsafeCompare(index[currentPos].Checksum, checksum)){
						lastDedupedBlock = index[currentPos];
						//lock(index[currentPos]){ // TODO!!! increment recounts after backup is done, by reading index. this way we avoid locking
								index[currentPos].RefCounts++;
						//}
#if DEBUG
						if(j<currentPos+2)
							BenchmarkStats.Instance().DedupHotFound ++;
						else
							BenchmarkStats.Instance().DedupColdFound ++;
#endif
						//currentPos = j; //test
						//return lastDedupedBlock.ID;
						dedupId = lastDedupedBlock.ID;
						return true;
					}
					if(currentPos == index.Count -1)
							currentPos = 0;
					else
							currentPos = j;
					currentPos++;
				}
			//}
			// key not found, add it
			FullDedupedBlock newDdb = new FullDedupedBlock();
			newDdb.Checksum = checksum;
			newDdb.DataChunkName = chunkname;
			newDdb.Length = bufSize;
			newDdb.StartPos = posInChunk;
			newDdb.StorageNodes[0] = storageNode;
			newDdb.RefCounts = 1;
			Interlocked.Increment(ref maxId);
			//maxId++;
			newDdb.ID = maxId;
			//index.Add(newDdb);
			addPending.Add(newDdb);
			//pendingAddCount++;
			Interlocked.Increment(ref pendingAddCount);
#if DEBUG
			BenchmarkStats.Instance().DedupAdd++;
#endif			
			if(pendingAddCount > 500){
				MergePending();
			}
			dedupId = maxId;
			return false;
		}
		
		private void MergePending(){
			if(addPending.Count == 0) return;
			Logger.Append(Severity.DEBUG, "Merging new deduped blocks into main list");
				lock(index){
					index.AddRange(addPending);
					addPending = new ConcurrentBag<FullDedupedBlock>();
					pendingAddCount = 0;
				}	
		}
		
		/// <summary>
		/// Renames previous indexes file and serialize current indexes list.
		/// To be called after a  backup operation.
		/// Also empties the index list to allow content to be GCed.
		/// </summary> 
		internal void Persist(){
			MergePending();
			Logger.Append(Severity.DEBUG, "Saving deduplication indexes database...");
			if(initializing){
				Logger.Append(Severity.NOTICE, "Won't save deduplication DB, not initialized yet");
				return;
			}

			//FileStream indexWriter = new FileStream(indexDB, FileMode.CreateNew, FileAccess.Write);
			FileStream fs = new FileStream(dedupDB+".new", FileMode.CreateNew, FileAccess.Write);
			IDataProcessorStream indexWriter = new NullSinkStream(fs, PipelineMode.Write);
			//indexWriter = new CompressorStream(indexWriter, CompressorAlgorithm.Lzo, (int)1024*512);
			indexWriter = new GZCompressorStream(indexWriter, System.IO.Compression.CompressionMode.Compress);

			using (IEnumerator<FullDedupedBlock> fullBlocksRef = GetFullDbEnumerator().GetEnumerator()){
				lock(index){
					int oldCount = index.Count;
					int count = 0; // DEBUG
					foreach(LightDedupedBlock ddb in index){
						if(fullBlocksRef.MoveNext()){
							FullDedupedBlock fdb = fullBlocksRef.Current;
							fdb.RefCounts = ddb.RefCounts;
							if(ddb.RefCounts == 0)
								continue;
							formatter.Serialize(indexWriter, fdb);
						}
						else
							if(ddb is FullDedupedBlock)
								formatter.Serialize(indexWriter, (FullDedupedBlock)ddb);
							else
								Logger.Append(Severity.CRITICAL, "Expected FULL deduplication entry, but got light one. Something werid must have occured");

						//formatter.Serialize(indexWriter, ddb);
						count++;
					}
					index = null;
					Logger.Append(Severity.INFO, "Saved "+count+" deduplication records (previously "+oldCount+") into database");
				}
				//indexWriter.Flush();
				indexWriter.Close();
				fs.Close();
			}
			try{
				//if(File.Exists(dedupDB))
				File.Move(dedupDB, dedupDB.Replace(".idx", ".idx.bak."+Utilities.Utils.GetUnixTime()));

			}
			catch(FileNotFoundException e){/*nothing wrong  :first use of the dedup db*/}
			catch(Exception _e){
				Logger.Append (Severity.ERROR, "Could not move old dedup DB to backup location: "+_e.Message);
				throw(_e);
			}
			try{
				File.Move(dedupDB+".new", dedupDB);
			}
			catch(Exception f){
				Logger.Append (Severity.ERROR, "Could not move new dedup DB to definitive location: "+f.Message);
				throw(f);
			}
		}

		private IEnumerable<FullDedupedBlock> GetFullDbEnumerator(){

			IDataProcessorStream indexReader;
			using(FileStream fs = new FileStream(dedupDB, FileMode.OpenOrCreate, FileAccess.Read, FileShare.Read)){
				try{
					indexReader = new NullSinkStream(fs, PipelineMode.Read);
					indexReader = new GZCompressorStream(indexReader, System.IO.Compression.CompressionMode.Decompress);
					//indexReader = new FileStream(indexDB, FileMode.Open, FileAccess.Read);
				}
				/*catch(FileNotFoundException fnf){
					Logger.Append(Severity.ERROR, "Couldn't read dedup index: "+fnf.Message);
					return;	
				}*/
				catch(Exception e){
					Logger.Append(Severity.ERROR, "Couldn't read dedup index: "+e.Message);
					yield break;	
				}
				formatter = new BinaryFormatter();
				//index = new ConcurrentBag<DedupedBlock>();
				MemoryStream ms = new MemoryStream();
				
				int readDDBCount = 0;
				int read = 0;
				byte[] buffer = new byte[1024*512];
				//GZipStream gzs = new GZipStream(new FileStream(indexDB, FileMode.Open), CompressionMode.Decompress);
				while((read = indexReader.Read(buffer, 0, buffer.Length)) > 0){
					ms.Write(buffer, 0, read);
				}
				ms.Flush();
				//Console.WriteLine ("dedup db read :ms length="+ms.Length);
				ms.Seek(0, SeekOrigin.Begin);
				//try{
				//gzs.Seek(0, SeekOrigin.Begin);
				while(ms.Position < ms.Length){
				//while( (read = indexReader.Read(buffer, 0, buffer.Length)) >0){
					//ms.Write(buffer, 0, read);
	                // We read the DedupedBlocks one by one to be able to handle corruption gracefully
						//Console.WriteLine ("reading dedup idx "+ms.Position +"/"+ ms.Length);
					FullDedupedBlock ddb = null;
					try{ 
						ddb = (FullDedupedBlock)formatter.Deserialize(ms);

						readDDBCount++;
					}
					//catch(IndexOutOfRangeException){break;}
					catch(Exception ex){
						Logger.Append(Severity.ERROR, "Couldn't read dedup block from index at position "+readDDBCount+": "+ex.Message+"---"+ex.StackTrace);
						corruptedDDBCount++;
					}
					if(ddb != null) yield return ddb;
	            }
				//}
				//catch{} // reached end of file
				ms.Dispose();
				indexReader.Close();
			}
		}

		private void DumpDedupDb(){
			foreach(FullDedupedBlock fdb in index)
				Console.WriteLine(fdb.ToString());
		}


		// FIXME !!! find method to compare arrays WITHOUT /4 divisible limitation
		static unsafe bool UnsafeCompare_old(byte[] a1, byte[] a2) {
		  if(a1==null || a2==null || a1.Length!=a2.Length)
		    return false;
		  fixed (byte* p1=a1, p2=a2) {
		    byte* x1=p1, x2=p2;
		    int l = a1.Length;
		    for (int i=0; i < l/8; i++, x1+=8, x2+=8)
		      if (*((long*)x1) != *((long*)x2)) return false;
		    if ((l & 4)!=0) { if (*((int*)x1)!=*((int*)x2)) return false; x1+=4; x2+=4; }
		    if ((l & 2)!=0) { if (*((short*)x1)!=*((short*)x2)) return false; x1+=2; x2+=2; }
		    if ((l & 1)!=0) if (*((byte*)x1) != *((byte*)x2)) return false;
		    return true;
		  }
		}
		
		static unsafe bool UnsafeCompare(byte[] sourceA, byte[] destA) {
		  if (sourceA.Length != destA.Length) return false;
		  int len = sourceA.Length;
		  unsafe {
		    fixed (byte* ap = sourceA, bp = destA) {
		      long* alp = (long*)ap, blp = (long*)bp;
		      for (; len >= 8; len -= 8) {
		        if (*alp != *blp) return false;
		        alp++;
		        blp++;
		      }
		      byte* ap2 = (byte*)alp, bp2 = (byte*)blp;
		      for (; len > 0; len--) {
		        if (*ap2 != *bp2) return false;
		        ap2++;
		        bp2++;
		      }
		    }
		  }
		  return true;
		}
				
		/*private void Serialize(DedupedBlock ddb){
			//using (indexReader){
        	
            //}
		}*/
	}
}

