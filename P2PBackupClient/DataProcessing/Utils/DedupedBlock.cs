using System;

namespace Node.DataProcessing{
	[Serializable]
	// TODO : use Struct instead
	public class FullDedupedBlock:LightDedupedBlock{

		public int StartPos;//{get;set;}
		public int[] StorageNodes;//{get;set;}
		public uint Length;//{get;set;}
		public string DataChunkName;//{get;set;}
		//{get;set;}
		//{get;set;}
		
		public FullDedupedBlock(){
			RefCounts = 0;
			Length = 0;
			this.StorageNodes = new int[2];
		}

		public override string ToString () {
			return string.Format ("[Id={0}, DataChunkName={1}, StorageNodes=[{2}], StartPos={3}, Size={4}]",
				this.ID, this.DataChunkName, this.StorageNodes.ToString(), this.StartPos, this.Length);
		}
		// TODO : try to reduce size:
		// - make RefCounts
	}
}

