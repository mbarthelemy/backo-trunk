using System;


namespace Node {

	[Serializable]
	public class LightDedupedBlock {

		public long ID;
		public byte[] Checksum;
		public short RefCounts;

	}
}

