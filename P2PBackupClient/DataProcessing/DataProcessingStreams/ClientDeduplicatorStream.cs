//# define DEBUG
using System;
using System.IO;
using System.Collections.Generic;
using System.Diagnostics;

namespace Node.DataProcessing{
	
	/// <summary>
	/// This class takes the previously calculated checksum and, if not in the global client dedupe list, adds it and returns
	/// the date. If checksum already exists, it enters the heavy/slow path : calculate a second checksum of data, more robust
	/// and collsion-free than the previous quick calculation. If 
	/// </summary>
	public class ClientDeduplicatorStream:IDataProcessorStream {
		private const int minDedupBlockSize = 16*1024; // don't dedup blocks < 16k
		private IDataProcessorStream inputStream;
		private long length;
		private long currentPos;
		//private bool lastReadIsDeduped;
		private string currentChunkName;
		private int currentChunkPos;
		private long dedupedCount;
		private int storageNodeId;
 // total number of deduped blocks since stream creation
		//private LightDedupedBlock lastDedupedBlock;
		private byte[] checksumToMatch;
		private List<long> dedupedBlocks;

		long dedupedBlockId; // latest deduped data block id

		//benchmarking
#if DEBUG
		Stopwatch sw = new Stopwatch();
#endif	
		public override List<IFileBlockMetadata> BlockMetadata{get;set;}
		
		public byte[] ChecksumToMatch {
			get {
				return this.checksumToMatch;
			}
			set {
				checksumToMatch = value;
			}
		}

		/*public LightDedupedBlock LastDedupedBlock {
			get {
				return this.lastDedupedBlock;
			}
		}*/
		
		public string CurrentChunkName {
			get {
				return this.currentChunkName;
			}
			set {
				currentChunkName = value;
			}
		}

		public int CurrentChunkPos {
			get {
				return this.currentChunkPos;
			}
			set {
				currentChunkPos = value;
			}
		}		

		public override bool CanRead{
			get{ return true;}
		}
		
		public override bool CanWrite{
			get{ 
				return false;
			}
		}
		
		/*public bool IsLastReadDeduped{
			get{return lastReadIsDeduped;}
		}*/
		
		public long DedupedCount {
			get {
				return this.dedupedCount;
			}
		}
		
		public override bool CanSeek{
			get{return false;}
		}
		
		public override long Position{
			get{ return currentPos;}
			set{ Seek(value, SeekOrigin.Begin);}
		}
		
		public override long Length{
			get{ return length;}	
		}
		
		public override void SetLength(long value){
			length = value;
			inputStream.SetLength(value);
		}
		
		public override void Flush(){
			inputStream.Flush();
		}
		
		public override void FlushMetadata(){
			if(dedupedBlocks.Count>0){
				inputStream.BlockMetadata.Add(new ClientDedupedBlocks(dedupedBlocks));
				this.BlockMetadata = new List<IFileBlockMetadata>();//.Clear();
				this.dedupedBlocks = new List<long>();
			}
			inputStream.FlushMetadata();
		}
		/// <summary>
		/// Reads checksummed data and sees if it already exists.
		/// If yes, returns 0 bytes
		/// If no, checksum for current data is created and the method returns full data into destBuffer.
		/// For this first reason, read can return 0 although previous streams contains data ; this only means that data 
		/// 	has been deduplicated.
		/// </summary>
		/// <param name="destBuffer">
		/// A <see cref="System.Byte[]"/>
		/// </param>
		/// <param name="offset">
		/// A <see cref="System.Int32"/>
		/// </param>
		/// <param name="count">
		/// A <see cref="System.Int32"/>
		/// </param>
		/// <returns>
		/// A <see cref="System.Int32"/>
		/// </returns>
		public override int Read(byte[] destBuffer, int offset, int count){
			//lastChecksum = md5.ComputeHash(destBuffer);
			throw new NotImplementedException("Restore direction not implemented.");
		}
		
		
		public override void Write(byte[] fromBuffer, int offset, int count){
			if(count < minDedupBlockSize){ 
				/*lastReadIsDeduped = false;
				lastDedupedBlock = null;*/
				inputStream.Write(fromBuffer, offset, count);
				length += count;
				return;
			}
#if DEBUG
			sw.Start();
#endif	
			//byte[] tempData;
			//if(count != fromBuffer.Length)//{
			//	Console.WriteLine ("ClientDeduplicatorstream : asked to work on data ("+(count-offset)+") < buffer size ("+fromBuffer.Length+"), suboptimal!!");
				/*tempData = new byte[count];
				for(int i=0; i<count; i++)
					tempData[i] = fromBuffer[offset+i];
			}
			else
				tempData = fromBuffer;*/
			 
				//lastDedupedBlock = DedupIndex.Instance().LastDedupedBlock;
			if(DedupIndex.Instance().Contains(checksumToMatch, this.currentChunkName, this.currentChunkPos, (uint)count/*tempData.Length*/, storageNodeId, ref dedupedBlockId)){

				dedupedCount++;
				
#if DEBUG
				sw.Stop();
				BenchmarkStats.Instance().DedupTime += sw.ElapsedMilliseconds;
				sw.Reset();
#endif
				//return;
			}
			//Console.WriteLine("ClientDeduplicatorStream: NON-deduped block(size="+tempData.Length+",sum="+Convert.ToBase64String(checksumToMatch)+")");
			// not duplicated, let's write to underlying stream
			else{
				inputStream.Write(fromBuffer, offset, count/*tempData.Length*/);
				length += count;
			}
			dedupedBlocks.Add(dedupedBlockId);
		}
		
		public override long Seek(long offset, SeekOrigin origin){
			inputStream.Seek(offset, origin);
			return offset;
		}
		
		public ClientDeduplicatorStream(IDataProcessorStream inputStream, int storageNode){
			this.inputStream = inputStream;
			this.currentPos = 0;
			this.length = 0;
			this.dedupedCount = 0;
			storageNodeId = storageNode;
			this.BlockMetadata = new List<IFileBlockMetadata>();
			dedupedBlocks = new List<long>();
			
		}
	}
}

