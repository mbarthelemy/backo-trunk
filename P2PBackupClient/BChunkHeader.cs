using System;
using P2PBackup.Common;

namespace Node{
	
	[Serializable]
	public struct BChunkHeader{
		
		public string Version{get;set;}
		/*public bool Compressed{get;set;}
		public bool Encrypted{get;set;}*/
		public DataProcessingFlags DataFlags{get;set;}
		public long TaskId{get;set;}
		public byte[] EncryptionMetaData{get;set;}
		
		/*public BChunkHeader(){
		}*/
	}
}

