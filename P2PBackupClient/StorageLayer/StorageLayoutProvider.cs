using System;
using Node.Utilities;
using P2PBackup.Common;
using P2PBackup.Common.Volumes;

namespace Node.StorageLayer  {

	public class StorageLayoutProvider {

		private StorageLayoutProvider ()
		{
		}

		internal static IStorageDiscoverer GetStorageLayoutDiscoverer(string providerName){
			switch (providerName.ToLower()){
				case "vmware":
				return new VMWareDisksDiscoverer();

				case "local":case "fs":case null:
				if(Utilities.PlatForm.Instance().OS.ToLower() == "linux")
					return new LinuxStorageDiscoverer();
				else if(Utilities.PlatForm.Instance().OS.ToLower().StartsWith("nt"))
					return new NTStorageDiscoverer();
				return null;
				break;

				default:
				Logger.Append(Severity.ERROR, "Cannot select storage  type'"+providerName+"' (doesn't exist or plugin not loaded)");
				return null;

			}
		}
	}
}

