namespace Node.StorageLayer {

	using System;
	using System.Collections.Generic;
	using P2PBackup.Common;
	using P2PBackup.Common.Volumes;

	internal interface IStorageDiscoverer: IDisposable {

		event EventHandler<LogEventArgs> LogEvent;

		string  Name{get;}

		bool Initialize(ProxyTaskInfo proxyingInfo);


		//string[] GetPhysicalDisksPaths();
		StorageLayout BuildStorageLayout();

	}


}


