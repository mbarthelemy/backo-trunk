namespace Node.StorageLayer {

	using System;
	using System.Collections.Generic;
	using P2PBackup.Common;
	using P2PBackup.Common.Volumes;

	public class LocalStorageDiscoverer:IStorageDiscoverer{

		public delegate void LogHandler(int code, Severity severity, string message);
		public event EventHandler<LogEventArgs> LogEvent;

		public string Name{get{ return "local";}}


		public bool Initialize(ProxyTaskInfo ptI){

			return true;
		}

		/*public string[] GetPhysicalDisksPaths(){

			return default (string[]);
		}*/

		public StorageLayout BuildStorageLayout(){
			if(Utilities.PlatForm.IsUnixClient())
				return (new LinuxStorageDiscoverer()).BuildStorageLayout();

			return null;
		}


		public void Dispose(){

		}

		public LocalStorageDiscoverer ()
		{
		}
	}
}

