
using System;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Net.Security;
using System.Security.Authentication;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
//using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Node.Utilities;
using P2PBackup.Common;

namespace Node{
	/// <summary>
	/// Class handling communication with Hub, sessions with peer nodes, tasks processing
	/// This is the "main loop" so NO BLOCKING/LONG RUNNING TASK ALLOWED HERE
	/// </summary>
	internal partial class User: P2PBackup.Common.Node{
		//private string userName;
		//private string password;
		//--private String pubKey;
		private Socket hubSocket;
		private Socket storageSocket;
		private static SslStream hubStream;
		private static NetworkStream underlyingHubStream;
		private List<Session> sessions = new List<Session>();
		public delegate void StorageSessionReceivedHandler (long taskId, Session storageSession/*, int budget*/);
		public static event StorageSessionReceivedHandler StorageSessionReceivedEvent;//BackupManager consume
		private static RSACryptoServiceProvider keyPairCrypto = null;
		//--private string accessible;
		//private bool isUnixClient;
		//private static HubWaiter hubWaiter;
		private static Queue<string> MessageQueue;
		//private int NumberOfJobs; // current number of running tasks (backups, restores)
		//private Dictionary<int,int> tasksListDone;
		//private Dictionary<int,int> tasksListTodo;
		private static List<Backup> backupsList;
		private List<P2PBackup.Common.Task> tasksList;
		BackupManager bManager;
		private static Queue<string> pendingHubMessages; // messages that couldn't be sent due to hub failure/disconnect
		X509Certificate cert ;
		/*
		
		private bool isUnixClient;
		private long shareSize;
		private string sharePath;
		private long accessible;
		private long available;
		//private long filesize;
		private string ip;
		private string listenIp;
		private int listenPort; 
		private bool locked;
		private string pubKey;
		private string certCN;

		private NodeStatus status;
		private long share;
		private long quota;
		private long usedQuota;
		private string shareRoot;
		private int uid;
		private int storageGroup;
		private int groupId;
		private int backupSets;
		private string nodeName;
		private string os;*/
		private bool run = false;
		//[field: NonSerialized] private string verificationHash;
		
		
		/*public bool IsUnixClient{
			get{return isUnixClient;}
			set{ isUnixClient = value;}
		}
		
		public int Uid {
			get {return this.uid;}
			set{uid = value;}
		}

		public string NodeName{
			get { return nodeName; }
			set { nodeName = value; }
		}
		
		public string CertCN{
			get{ return certCN;}	
			set{certCN = value;}
		}
		
		public string PubKey{
			get{ return pubKey;}	
			set{pubKey = value;}
		}
		
		public string OS{
			get{ return os;}	
			set{os = value;}
		}
		
		public string IP{
			get { return ip; }
			set{ip = value;}
		}
		
		public string ListenIp{
			get{return listenIp;}
			set{listenIp = value;}
		}
		
		public int ListenPort{
			get{return listenPort;}
			set{listenPort = value;}
		}
		//public string Password{
		//	get { return password; }
		//}
		public long Available{
			get { return available; }
			set{available = (long)value;}
		}
		
		public long Quota{
			get { return quota; }
			set{ quota = (long)value;}
		}
		public long UsedQuota{
			get { return usedQuota; }
			set{usedQuota = (long)value;}
		}
		public long StorageSize{
			get{return share;}
			set{share = value;}
		}

		public int StorageGroup{
			get{ return storageGroup;}
			set{storageGroup = value;}
		}
		public int Group{
			get{ return groupId;}
			set{groupId = value;}
		}
		public int NbBackupSets{
			get{ return backupSets;}
			set{backupSets = value;}
		}
		public bool Locked{
			get{return locked;}	
			set{locked = value;}
		}
		

		
		public string ShareRoot{
			get{return shareRoot;}	
			set{shareRoot = value;}
		}
		public NodeStatus Status{
			get{return status;}	
			set{status = value;}
		}
		
		public string Version{
			get{return Utilities.PlatForm.Instance().NodeVersion;}
			set{}
		}
		*/

		internal ManualResetEvent CertificateGeneratedEvent{get;set;}
		internal static RSACryptoServiceProvider KeyPair{get{return keyPairCrypto;}}
		
		public delegate void UpdateGUIHandler (string command, string param);
		public event UpdateGUIHandler UpdateGUIEvent;//Client.UpdateGUI()

		public User(){
			GetKeyPair(out keyPairCrypto);
			backupsList = new List<Backup>();
			tasksList = new List<P2PBackup.Common.Task>();
			pendingHubMessages = new Queue<string>();
		}


		/*public bool Connected{
			get{ return hubSocket.Connected; }
		}*/
		
		/*public  RSACryptoServiceProvider KeyPairCrypto{
			get { return keyPairCrypto; }
		}*/

		public bool Run{
			get {return run;}
		}
		
		
		static bool CheckHubCertificate(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors){
			  if (sslPolicyErrors != SslPolicyErrors.None) {
			    Logger.Append(Severity.WARNING, "Hub certificate can't be fully trusted : "+sslPolicyErrors.ToString()+@" /!\ IGNORING");
			    //return false;
			  }
			  //else
			    return true;
		}
		
		private X509Certificate LocalSelectionCallback (object sender, string targetHost, X509CertificateCollection localCertificates,
                                                        X509Certificate remoteCertificate, string[] acceptableIssuers){
            //Console.WriteLine ("Selected certificate");
            return cert;
        }
		
		public bool  ConnectToHub(bool useCertificate){
				return ConnectToHub(0, useCertificate);
		}
		
		/// <summary>
		/// Connects the user to the hub and creates Reader and Writer for communication.
		/// </summary>
		/// <param name="ip">Hub IP</param>
		/// <param name="port">Hub Port</param>
		/// 
		private bool ConnectToHub(short tries, bool useCertificate){
			run = false;
			Logger.Append(Severity.INFO, "Connecting to Hub (attempt "+tries+")...");
			try{
				hubSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
				try{
					hubSocket.SendBufferSize = 0; //4*1024; //4k
					hubSocket.ReceiveBufferSize = 0; //4*1024; //4k
					//hubSocket.NoDelay = true;
				}catch{
					Logger.Append(Severity.DEBUG, "Unable to set TCP nodelay");
				} // not supported (freebsd...)	
				//Utils.SetProcInfo("Node (Connect)");
				tries  ++ ;
				//IPAddress addr = IPAddress.Parse(ConfigManager.GetValue("Hub.IP"));
				//IPEndPoint dest = new IPEndPoint(addr, int.Parse(ConfigManager.GetValue("Hub.Port")));
				hubSocket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, (int)1);
				//hubSocket.Connect(dest);
				hubSocket.Connect(ConfigManager.GetValue("Hub.IP"), int.Parse (ConfigManager.GetValue("Hub.Port")));
				underlyingHubStream = new NetworkStream(hubSocket);
				
				HubNoSslWrite("lilute"); // Why the hell do we need to send initial dummy data????
				if(!useCertificate){
					//cert = new X509Certificate(
					cert= new X509Certificate2(Convert.FromBase64String(Client.DefaultPubKey), "");
				   	/*Logger.Append(Severity.ERROR, "Could not load certificate '"+(string)ConfigManager.GetValue("Security.CertificateFile")+"'");	
					//HubNoSslWrite("CERTREQ");
					//Thread.Sleep(1000);
					

					//certs.Add(rootCert); // TODO : see if we really need to add root cert
					try{
						
						//hubStream = new SslStream(underlyingHubStream);
						hubStream = new SslStream(underlyingHubStream, false, new RemoteCertificateValidationCallback(CheckHubCertificate), LocalSelectionCallback );
						hubStream.AuthenticateAsClient(ConfigManager.GetValue("Hub.IP"));
						//hubStream.ReadTimeout = 30000; // Timeout after 30s : hub is probably disconnected.
						byte[] certificate = new byte[4096];
						Logger.Append(Severity.INFO, "Receiving certificate from Hub...");
						HubWrite("CERT");
						try{
							int read = hubStream.Read(certificate, 0, certificate.Length);
							if(read == 0) throw new Exception("received data is zero length");
							System.IO.FileStream certStream = new System.IO.FileStream(ConfigManager.GetValue("Security.CertificateFile"), System.IO.FileMode.CreateNew);
							certStream.Write(certificate, 0, read);
							certStream.Close();
							Logger.Append(Severity.INFO, "Received certificate from Hub, saving...");
							// re-connect, this time using the regular way (providing the certificate)
							hubStream.Close();
							ConnectToHub();
						}
						catch(Exception e){
							Console.WriteLine("Cannot save certificate to '"+ConfigManager.GetValue("Security.CertificateFile")+"': "+e.Message);
							Environment.Exit(6);
						}
					}
					catch(Exception ex){ // probably SSL error
						Logger.Append(Severity.ERROR, ex.Message+" : "+ex.StackTrace); 
					}
					*/
					
				}
				else{ // dummy cert
					cert = new X509Certificate2(ConfigManager.GetValue("Security.CertificateFile"), "");
					//HubNoSslWrite("SSL");
					//Thread.Sleep(500);

				}
				hubStream = new SslStream(underlyingHubStream, true, new RemoteCertificateValidationCallback(CheckHubCertificate), LocalSelectionCallback );
				hubStream.WriteTimeout = 10000; // Timeout after 30s : hub is probably disconnected.
				//X509Certificate2 rootCert = new X509Certificate2("root.cer");

				X509CertificateCollection certs = new X509CertificateCollection();
				certs.Add(cert);			
				//certs.Add(rootCert); // TODO : see if we really need to add root cert
				hubStream.AuthenticateAsClient(ConfigManager.GetValue("Hub.IP"), certs, SslProtocols.Default, false);
				
				Logger.Append(Severity.DEBUG2, "SSL authentication done");
				//HubWrite("test ssl");

				//Utils.SetProcInfo("Node (Sleeping)");
				
			}
			catch(SocketException e){
				Logger.Append(Severity.ERROR, "Can't connect to hub ("+tries+" attempts)... will retry indefinitely every 30s (error : "+e.Message+")");
				Thread.Sleep(30000);
				ConnectToHub(tries, useCertificate);
			}
			catch(ObjectDisposedException ode){
				// if the user, for some reason, has been logged out and the user is logging in again
				// create a new socket aund call ConnectToHub again
				Console.WriteLine("WARN: User.ConnectToHub : re-connecting because of : "+ode.Message);
				hubSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
				ConnectToHub(tries, useCertificate);
			}
			catch(Exception ex){ // probably SSL error
				Logger.Append(Severity.ERROR, ex.Message+" : "+ex.StackTrace); 
				Logger.Append(Severity.ERROR, ex.InnerException.Message+" : "+ex.InnerException.StackTrace); 
				return false;
			}
			StartListening ();
			return true;
		}

		internal void RequestCertificate(){
			CertificateGeneratedEvent = new ManualResetEvent(false);
			HubWrite("CERTREQ ");
		}

		private  static void HubWrite(string message){
			byte[] byteMsg = Encoding.UTF8.GetBytes(message);
			int msgSize = byteMsg.Length;
			byte[] header = BitConverter.GetBytes(msgSize); // header always has 'int' size (4 bytes)
			lock(hubStream){
				try{
					hubStream.Write(header);
					hubStream.Write(byteMsg);
					hubStream.Flush();
				}
				catch(Exception e){
					Logger.Append(Severity.ERROR, "Can't contact Hub. ("+e.Message+")");
					lock(pendingHubMessages){
						pendingHubMessages.Enqueue(message);
					}
				}
			}
			Logger.Append(Severity.DEBUG2, "Sent message to hub : "+message); 
		}
		
		private  static void HubNoSslWrite(string message){
			byte[] byteMsg = Encoding.UTF8.GetBytes(message);
			//int msgSize = byteMsg.Length;
			//byte[] header = BitConverter.GetBytes(msgSize); // header always has 'int' size (4 bytes)
			lock(underlyingHubStream){
				try{
					/*underlyingHubStream.Write(header, 0, header.Length);*/
					underlyingHubStream.Write(byteMsg, 0, byteMsg.Length);
					underlyingHubStream.Flush();
				}
				catch(Exception e){
					Logger.Append(Severity.ERROR, "Can't contact Hub. ("+e.Message+")");
					
				}
			}
			Logger.Append(Severity.DEBUG2, "Sent message to hub : "+message); 
		}
		
		/// <summary>
		/// When a critical, unrecoverable error occurs, try to inform Hub.
		/// </summary>
		/// <param name='data'>
		/// Data.
		/// </param>
		internal static void SendEmergency(string data){
			HubWrite("EMERGENCY "+data);
		}
		
		private void HeaderReceived (IAsyncResult ar){
			StateObject so = (StateObject) ar.AsyncState;
			//hubStream = so.stream;
			try{
				int read = so.stream.EndRead(ar);
				if(read == 0){
					Logger.Append(Severity.INFO, "Hub has disconnected.");
					Disconnect(true);
					ConnectToHub(true);
				}
			}
			catch(ArgumentOutOfRangeException){
				Logger.Append(Severity.INFO, "Hub has disconnected.");
				Disconnect(true);
				//ConnectToHub(true);
			}
			catch(Exception e){
				Logger.Append(Severity.ERROR, "HeaderReceived error : "+e.Message);
				Disconnect(true);
				//ConnectToHub(true);
			}
			int msg_length = BitConverter.ToInt32(so.buffer, 0);
			//Logger.Append(Severity.DEBUG2, "Received message header");
			//Logger.Append(Severity.DEBUG2, "Received message header, msg size will be "+msg_length);
			try{
				so.stream.BeginRead(so.buffer, 0, msg_length, new AsyncCallback(MessageReceived), so);
			}
			catch(Exception e){
				Logger.Append(Severity.ERROR, "HeaderReceived(2) error : "+e.Message);
				//Disconnect(true);
			}
		}
		
		private void MessageReceived(IAsyncResult ar){
			if(!run) return;
			try{
				StateObject so = (StateObject) ar.AsyncState;
				//hubStream = so.stream;
			so.stream = hubStream;
				int read = so.stream.EndRead(ar);
				Decode(Encoding.UTF8.GetString(so.buffer, 0, read));
				so.stream.BeginRead(so.buffer, 0, 4, new AsyncCallback(HeaderReceived), so);
				//hubStream.BeginRead(so.buffer, 0, 4, new AsyncCallback(HeaderReceived), so);
			}
			catch(Exception ioe){
				Logger.Append(Severity.ERROR, "Error reading data " + ioe.Message);
				Disconnect(true);
			}
		}	
		
		/// <summary>
		/// Sets the client in listening mode.
		/// </summary>
		private void StartListening(){
			Logger.Append (Severity.DEBUG, "Starting to listen to Hub");
			try{
				run = true;
				StateObject state = new StateObject();
        		//state.workSocket = hubSocket;
				state.stream = hubStream;
				byte[] header = new byte[4];
				//state.buffer = header;
				state.stream.BeginRead(state.buffer, 0, header.Length, this.HeaderReceived, state);
				Logger.Append (Severity.DEBUG2, "Startlistening : done");
			}
			catch(Exception ex)	{
				run = false;
				Logger.Append(Severity.ERROR, ex.Message+" ---- "+ex.StackTrace);
			}
		}

		/// <summary>
		/// Breaks the listening mode and disconnects the client from the hub.
		/// </summary>
		internal void Disconnect(bool closing){
			try{
				run = false;
				try{

					/*this.hubSocket.Shutdown(SocketShutdown.Both);
					this.hubSocket.Close();*/
					hubStream.Close();
					underlyingHubStream.Close();
					hubStream = null;
					underlyingHubStream = null;
					/*if(hubSocket.Connected){
						this.hubSocket.Shutdown(SocketShutdown.Both);
						this.hubSocket.Close();
					}*/

					/*if(closing){
						hubStream.Dispose();
						underlyingHubStream.Dispose();
						this.hubSocket.Dispose();
						hubStream = null;
						hubSocket = null;
					}*/
				}
				catch(Exception e){
					Logger.Append (Severity.DEBUG, "Error while disconnecting from Hub : "+e.Message);
				}
				if(UpdateGUIEvent != null)
					UpdateGUIEvent("OUT", "");
				Thread.Sleep(1000);
				Logger.Append(Severity.INFO, "Closed connection to hub.");
			}
			catch(Exception ex){
				Logger.Append(Severity.ERROR, ex.Message); 
			}
		}

	
		/// <summary>
		/// Sends the username to the hub for login
		/// </summary>
		public void Login(){
			HubWrite("LOG " + this.NodeName); 	
		}
		
		public void AskConfig(){
			HubWrite("CNF "+this.NodeName);	
		}
		
		/// <summary>
		///  Prepare for backup. To be called upon "BKS" message reception
		/// </summary>
		/// <TODO> : Process includelists and excludelists.
		public void RunBackup(int taskId, long? refTaskId, string refIndexSum, string synthIndexSum, long refStartDate, long refEndDate,  BackupSet bs){
			if(backupsList.Count > 0){ 
				foreach(Backup runningTask in backupsList)
					if(runningTask.Bs.Id == bs.Id){
						HubWrite("TSK "+taskId+" 800");
						Logger.Append(Severity.WARNING, "Taskset "+bs.ToString()+" si already running, refusing to process it twice...");
						return;
					}
			}
			Backup backup;
			if(bs.ScheduleTimes[0].Level == BackupLevel.Full)
				backup = new Backup(bs, taskId);
			else{
				// check, if not a full backup, that we have the needed reference index
				if(Node.DataProcessing.IndexManager.IsIndexPresentAndValid((long)refTaskId, synthIndexSum, false))
					backup = new Backup(bs, taskId, refTaskId.Value, refStartDate, refEndDate);
				else if(Node.DataProcessing.IndexManager.IsIndexPresentAndValid((long)refTaskId, refIndexSum, true)){
					Node.DataProcessing.IndexManager im = new Node.DataProcessing.IndexManager();
					//im.CreateSyntheticFullIndex((long)refTaskId,
					Console.WriteLine (" @@@@@@@@@@ TODO : recreate synth");
					backup = new Backup(bs, taskId, refTaskId.Value, refStartDate, refEndDate);
				}
				else{
					Logger.Append(Severity.INFO, "Asking Hub for a storage node from which download ref index copy");
					return; // TEMPORARY!!!
				}
			}
			backupsList.Add(backup);
			Logger.Append(Severity.INFO, "Starting task +"+taskId+", operation Backup, type "+bs.ScheduleTimes[0].Level.ToString()+".");
			backup.HubNotificationEvent += this.HubSendTaskEvent;

			SendBackupToHub(taskId);//sends BCK, ok to start backup task
			
			// applications freeze and snapshotting happens here
			try{
				System.Threading.Tasks.Task prepareTask = new System.Threading.Tasks.Task(
					()=>{
						backup.PrepareAll();
						bManager = new BackupManager(backup, taskId);
						bManager.BackupDoneEvent += new BackupManager.BackupDoneHandler(this.BackupDone);
						bManager.Run();

					}, 
					System.Threading.Tasks.TaskCreationOptions.LongRunning);


				/*System.Threading.Tasks.Task doBackupTask = new System.Threading.Tasks.Task(
					()=>{
						Console.WriteLine ("doBackupTask : starting bManager");
						bManager = new BackupManager(backup, taskId);
						bManager.BackupDoneEvent += new BackupManager.BackupDoneHandler(this.BackupDone);
						Console.WriteLine ("doBackupTask : calling bManager Run()");
						bManager.Run();
					}, System.Threading.Tasks.TaskCreationOptions.LongRunning);

				prepareTask.ContinueWith(o=>doBackupTask,  TaskContinuationOptions.OnlyOnRanToCompletion);
				*/
				//System.Threading.Tasks.Task.Factory.StartNew(prepareTask);
				prepareTask.Start(TaskScheduler.Default);

				// handle exceptions...
				prepareTask.ContinueWith(
					o=>{UnexpectedError(prepareTask, taskId);},
					System.Threading.Tasks.TaskContinuationOptions.OnlyOnFaulted
				);

				/*doBackupTask.ContinueWith( 
			        o=>{UnexpectedError(doBackupTask, taskId);},
					System.Threading.Tasks.TaskContinuationOptions.OnlyOnFaulted
				);*/


			}
			catch(Exception e){
				Logger.Append(Severity.ERROR, "<--> Interrupting task "+taskId+" after unexpected error : "+e.ToString());
				HubWrite("TSK "+taskId+" 999 "+e.Message);
				BackupDone(taskId);
			}
		}
		
		private void UnexpectedError(System.Threading.Tasks.Task task, long taskId){

			var aggException = task.Exception.Flatten();
         	foreach(var exception in aggException.InnerExceptions){
				Logger.Append(Severity.CRITICAL, "Unexpected error while processing backup task "+taskId+" : "+exception.Message+" ---- Stacktrace : "+exception.StackTrace);
				HubWrite("TSK "+taskId+" 999 "+exception.Message);
			}
			//cancellationTokenSource.Cancel();
			//theSession.Disconnect();
			task.Dispose();

			//backup.Terminate(false);
			GetCurrentBackup(taskId).Terminate(false);
			BackupDone(taskId);
			// rethrow to allow continuations to NOT be processed when they are NotOnFaulted
			throw new Exception("Propagating unexpected exception..."); 
			
		}

		private void BackupDone(long taskId){
			for(int i = backupsList.Count -1; i >=0; i--){
				if(backupsList[i].TaskId == taskId){
					backupsList[i].HubNotificationEvent -= new Backup.HubNotificationHandler(this.HubSendTaskEvent);
					backupsList[i] = null;
					backupsList.RemoveAt(i);
				}
			}
			if(bManager != null)// can be null if BackupDone is called after an exception occured in backup.Prepare
				bManager.BackupDoneEvent -= new BackupManager.BackupDoneHandler(this.BackupDone);
			bManager = null;
			Utils.SetProcInfo("Node (Sleeping)");
		}
		
		/// <summary>
		/// Tells the hub that client/user wants to take a backup
		/// </summary>
		/// <param name="size">total size of backup</param>
		private void SendBackupToHub(int taskId){
				HubWrite("BCK " + taskId);
				Logger.Append(Severity.INFO, "Asked to hub to receive backup for task "+taskId);
		}

		
		/// <summary>
		/// Request a destination for data chunks.
		/// Hub replies with a storage node information (ip, port)
		/// and a budget (max number of chunks allowed to tranfer before needing to call SendPut again).
		/// </summary>

		internal static void SendPut(long taskId, int sessionId, int parallelism){
			HubWrite("PUT " + taskId+ " " +sessionId + " " +parallelism);
		}
		/*internal static void SendPut(long taskId, int storageNodeId, int parallelism){
			HubWrite("PUT " + taskId+ " " +storageNodeId + " " +parallelism);
		}*/
		
		
		/// <summary>
		/// Tells the hub that backup is done.
		/// </summary>
		internal static void SendDoneBackup(long taskId, long originalSize, long finalSize, long totalItems, string indexName, string indexSum, string synthIndexSum, List<int> indexDestinations, int completionPercent){	
			Logger.Append(Severity.INFO, "Task "+taskId+" done, informing hub.");
			//Backup b = GetCurrentBackup(taskId);
			string indexDests = String.Join(",", indexDestinations);
			HubWrite("DBU "+taskId+" "+originalSize+" "+finalSize+" "+totalItems+" "+indexName+" "+indexSum+" "+synthIndexSum+" "+indexDests+" "+completionPercent);
			//CleanBackup(taskId);
			//CleanSessions();
		}
		
		private void CleanSessions(){
			/*for(int i=0; i<sessionsConnect.Count; i++){
				// only end inactive sessions, in case of multiple simultaneous jobs	
				Session sess = (Session)sessionsConnect[i];
				if(tasksListTodo.Count == 0){ // implies cleansessions mus be called after cleanbackup
					sess.SendDisconnect();
					sessionsConnect.RemoveAt(i);
					sess = null;
				}
				else
					Console.WriteLine("BUG BUG taskListTodo not 0, remove taskListToto as this doesn't make sense anymore");
			}*/
		}
		
		private void CancelTask(long taskId){
			Logger.Append(Severity.INFO, "Received order to cancel task "+taskId+". Stopping operations...");
			for(int i=0; i< backupsList.Count; i++){
				if(backupsList[i].TaskId == taskId)
					backupsList[i].AddHubNotificationEvent(790, "", "");
			}
			try{
				Logger.Append(Severity.INFO, "Task "+taskId+" stopping...");
				if(bManager != null) // null when we still are in the "prepare" phase (building storage layout...)
					bManager.cancellationTokenSource.Cancel();
				Backup b = GetCurrentBackup(taskId);
				b.Terminate(false);
			}
			catch{}// task doesn't exist anymore : node has been restarted
		}
		
		private void CleanBackup(long taskId){
			for(int i=0; i< backupsList.Count; i++){
				Backup b = backupsList[i];
				if (b.TaskId == taskId){
					backupsList.RemoveAt(i);
					b = null;
				}
			}
		}
		/// <summary>
		/// Tells the hub that user/client is ready for another client to connect.
		/// </summary>
		/// <param name="clientName">the other clients username</param>
		private void SendAccept(string clientName){
				HubWrite("ACC " + clientName);
		}
		
		// restore a backup in specified destination directory, if we wnat an destination diferent than orignal path
		public void PrepareRecovery(string backupName, string restorePath){
			PrepareRecovery(backupName);
		}
		

		/// <summary>
		/// Does the preparations for a recovery.
		/// </summary>
		/// <param name="backupName">the name of the backup, in this case the same as the name for indexfile</param>
		public void PrepareRecovery(string backupName){
			Directory.CreateDirectory("Temp");
			Console.WriteLine("DEBUG : User.PrepareRecovery : backupName="+backupName);
			//backup = fileHand.GetFromIndexFile(backupName);
			//long total = Int64.Parse(backup.GetBackupSize());
			//UpdateGUIEvent("initprbRecovery", total.ToString());
			SendRecoveryToHub();
		}
		
		
		public static void GetLastFullIndexInfo(int taskId){
			HubWrite("LFI "+taskId);
		}
		
		public static void SendRecoverIndexToHub(string indexName){
			HubWrite("RIX " + indexName); // Define RIX (recover index)
		}
		
		/// <summary>
		/// Tells the hub that client/user wants to fetch file from other client.
		/// When all files are fetched, they are decrypted and temporary folder is deleted.
		/// </summary>
		private void SendRecoveryToHub(){
			/*bool allFilesFetched = true;
			foreach (BFile bf in backup){
				if(bf.Fetched == false){
						HubWrite("REC " + bf.ClientName);
					allFilesFetched = false;;
					break;
				}
			}
			if (allFilesFetched == true){
				long total = Int64.Parse(backup.GetBackupSize());
				backup.GetFileToDecrypt(keyPairCrypto);
				try{
					Directory.Delete("Temp", true);
				}
				catch (Exception ex){
					MessageBox.Show(ex.Message, "User.SendRecoveryToHub");
				}
				backup = null;
			}*/
		}

		/// <summary>
		/// Saves the size and path of the share and sends the size to the hub
		/// </summary>
		/// <param name="size">size of the share</param>
		/// <param name="path">path of the share</param>
		public void UpdateStorageSpace(long taskId, long usedSpace, int budget){
			this.StorageSize -= usedSpace;
			HubWrite("SHA " + taskId+" "+usedSpace+" "+budget);
		}


		private bool SaveCert(byte[] certificate){
			try{
				//System.Security.AccessControl.FileSecurity certSec = new System.Security.AccessControl.FileSecurity();


				System.IO.FileStream certStream = new System.IO.FileStream(ConfigManager.GetValue("Security.CertificateFile"), System.IO.FileMode.Create);
				certStream.Write(certificate, 0, certificate.Length);
				certStream.Close();
				CertificateGeneratedEvent.Set();
			}
			catch(Exception e){
				Logger.Append(Severity.ERROR, "Could not save received certificate to '"+ConfigManager.GetValue("Security.CertificateFile")+"' : "+e.Message);
				return false;

			}
			return true;
		}
		
		internal static void AskIndexDest(long taskId, string indexName, string indexChecksum){
				HubWrite("PIX " + taskId+ " "+ indexName + " " +indexChecksum);
		}

		/// <summary>
		/// Sends the public key to the hub
		/// </summary>
		/*public void SendPublicKey(){
			if (pubKey != null)
					HubWrite("KEY " + pubKey);
		}*/

		/// <summary>
		/// Gets the key pair from file.
		/// Saves public key in User.
		/// </summary>
		/// <param name="keyPair">the key pair for user</param>
		private void GetKeyPair(out RSACryptoServiceProvider keyPair){
			/*byte[] keyByte;
			keyByte = FileHandle.GetKeyPair();
			if(keyByte != null)	{
				keyPair = new RSACryptoServiceProvider();
				StringBuilder sb = new StringBuilder(keyByte.Length);
				for (int Counter = 0; Counter < keyByte.Length; Counter++)
					sb.Append(Convert.ToChar(keyByte[Counter]));
				keyPair.FromXmlString(sb.ToString());
				pubKey = keyPair.ToXmlString(false);
			}
			else
				keyPair = null;	*/
			keyPair = null;
			if(!File.Exists(ConfigManager.GetValue("Security.CertificateFile"))) return;
			X509Certificate2 cert = new X509Certificate2(ConfigManager.GetValue("Security.CertificateFile"), "");
			keyPair =  (RSACryptoServiceProvider)cert.PrivateKey;
		}

		
		
		/// <summary>
		/// Hashes a string
		/// </summary>
		/// <param name="data">string to hash</param>
		/// <returns>hashed string</returns>
		private string HashString(string data){
			byte[] toHash = Encoding.ASCII.GetBytes(data);
			byte [] hashedResult;

			SHA1Managed hashAlg = new SHA1Managed();
			hashedResult = hashAlg.ComputeHash(toHash);
			return ByteArrayToString(hashedResult);
		}
		
		/// <summary>
		///Get stats (upon hub request) about a running task.
		/// </summary>
		/// <param name="taskId">
		/// A <see cref="System.Int64"/>
		/// </param>
		/// <returns>
		/// A <see cref="System.String"/> : originalsize finalsize nbitems completionpercent
		/// </returns>
		private string GetBackupStats(long taskId){
			Backup backup = (from Backup b in backupsList where b.TaskId==taskId select b).Single();
			return backup.OriginalSize+" "+backup.FinalSize+" "+backup.TotalItems+" "+backup.CompletionPercent;
		}
		
		/// <summary>
		/// Takes a bytearray and creates a string.
		/// </summary>
		/// <param name="toConvert">byte[] to convert</param>
		/// <returns>string representation of byte[]</returns>
		private string ByteArrayToString(byte [] toConvert){
			StringBuilder sb = new StringBuilder(toConvert.Length);
			for (int i = 0; i < toConvert.Length - 1; i++){
				sb.Append(toConvert[i].ToString("X"));
			}
			return sb.ToString();
		}
		
		/// <summary>
		/// Takes time and random number received from the hub. 
		/// Hashes it together with password.
		/// Sends result to the hub.
		/// </summary>
		/// <param name="time">time stamp</param>
		/// <param name="no">random number</param>
		private void Respond(string time, string no){
				string data = /*this.Password +*/ time + no;
				HubWrite("RSP " + HashString(data));
		}
		
		private void StartStorageListener(){ //21032011
				storageSocket = new Socket(AddressFamily.InterNetwork,SocketType.Stream,ProtocolType.Tcp);
				string listenIp = ConfigManager.GetValue("Storage.ListenIp");
				int listenPort = 0;
				int.TryParse(ConfigManager.GetValue("Storage.ListenPort"), out listenPort);
				if(listenIp == null || listenPort == 0){
					Logger.Append(Severity.INFO, "No storage listener configuration found.");
					return;
				}
				//storageSocket.SetSocketOption(SocketOptionName.
				IPAddress listenAddress;
				if (listenIp == "*")
					listenAddress = IPAddress.Any;
				else
					listenAddress = IPAddress.Parse(listenIp);
				IPEndPoint src = new IPEndPoint(listenAddress, listenPort);
				if(!storageSocket.IsBound){
					storageSocket.Bind(src);
					storageSocket.Listen(1);
					Logger.Append(Severity.INFO, "Listening on ip "+listenIp+", port "+listenPort);
				}
				else
						Logger.Append(Severity.WARNING, "I am already listening on port "+listenPort);
		}
		/// <summary>
		/// Starts listening for and accepts a connection from other client.
		/// <TODO> Implement timeout in Accept() to not block everything if peer never connects !
		/// </summary>
		private void StartListeningForClient(string clientIp, Session peerSession)	{
			try{
				SendAccept(clientIp);
				// Listen for control commands
				
				
				//Socket clientSession = storageSocket.Accept();
				Socket clientSession;
				IAsyncResult result = storageSocket.BeginAccept(null, storageSocket);
				Console.WriteLine ("before wqitone");
				result.AsyncWaitHandle.WaitOne(30000, true);
				Console.WriteLine ("after wqitone");
				if(result.AsyncState == null){
					Console.WriteLine ("       / / / / / / /client didn't connect in time");
					throw new Exception("Client "+clientIp+" didn't connect in time");
				}
				Console.WriteLine ("before endaccept");
				//Socket temp = (Socket)result.AsyncState;
				//clientSession = temp.EndAccept(result);
				clientSession = storageSocket.EndAccept(result);
				
				Console.WriteLine ("after endaccept");
				//if(!clientSession.Connected)
				//	throw new Exception("Could not create control socket");
				
				try{
					clientSession.ReceiveBufferSize = 0; 
					clientSession.NoDelay = true;
				}
				catch{} //doesn't work at least on freebsd
				if( ((IPEndPoint)clientSession.RemoteEndPoint).Address.ToString() != clientIp){
					Logger.Append(Severity.WARNING, "Received control connection attempt from bad IP (expected "+clientIp+", got "+((IPEndPoint)clientSession.RemoteEndPoint).Address.ToString()+")");	
					//clientSession.Close();
				}
				//else{
					// Listen for data
				
				
				
				//Socket dataSession = storageSocket.Accept();
				
				Socket dataSession;
				IAsyncResult result2 = storageSocket.BeginAccept(null, storageSocket);
				Console.WriteLine ("before waitone");
				result2.AsyncWaitHandle.WaitOne(30000, true);
				if(result2.AsyncState == null){
					throw new Exception("Client "+clientIp+" storage socket didn't connect in time");
				}
				Console.WriteLine ("after waitone");
				Socket temp2 = (Socket)result2.AsyncState;
				dataSession = temp2.EndAccept(result2);
				Console.WriteLine ("after endaccept");
				//if(!dataSession.Connected)
				//	throw new Exception("Could not create data socket");
				
				try{
					dataSession.ReceiveBufferSize = 512*1024;
					storageSocket.NoDelay = false;
				}
				catch{}
				if( ((IPEndPoint)dataSession.RemoteEndPoint).Address.ToString() != clientIp){
					Logger.Append(Severity.WARNING, "Received data connection attempt from bad IP (expected "+clientIp+", got "+((IPEndPoint)clientSession.RemoteEndPoint).Address.ToString()+")");	
					//dataSession.Close();
					//clientSession.Close();
					//return;
				}
				peerSession.ClientSocket = clientSession;
				peerSession.DataSocket = dataSession;
				peerSession.StartListening();
				Logger.Append(Severity.DEBUG, "Listening for "+clientSession.RemoteEndPoint.ToString());
				//}
			}
			catch(ObjectDisposedException){
				// socket closed when user logged out, do nothing
				Logger.Append(Severity.DEBUG, "Storage listener socket was closed.");
			}
			catch(Exception ex){
				Logger.Append(Severity.ERROR, "Error establishing listener for storage node : "+ex.Message+"---"+ex.StackTrace);
			}
			
		}
		
		private void EndAcceptControlSocket(){
			
		}
		/// <summary>
		/// Removes session from session list
		/// </summary>
		/// <param name="s">session to remove</param>
		/// <param name="sessionType">sessionType to remove</param>
		private void RemoveSession(Session s){
			try{
				Console.WriteLine ("RemoveSession() : active sessions="+sessions.Count); 
				lock(sessions){
					for(int i = sessions.Count-1; i>=0; i--){
						if(sessions[i].ClientId == s.ClientId && sessions[i].Type == s.Type){
							

							if(s.Type == SessionType.Backup){
								sessions[i].RemoveSessionEvent -= new Node.Session.RemoveSessionHandler(this.RemoveSession);
								sessions[i].FileReceivedEvent -= new Node.Session.FileReceivedHandler(this.FileReceived);
								HubSendTaskEvent(s.TaskId, 699, ""+s.Id, "");
							}
							sessions.RemoveAt(i);

						}
						//else
						//	Console.WriteLine ("RemoveSession() not what i'm looking for : "+sessions[i].ClientId); 
					}
				}
				Logger.Append(Severity.INFO, "Removed session #"+s.Id+" with node #"+s.ClientId+" from list, "+sessions.Count+" remaining");
				s = null;
			}
			catch(Exception _e){
				Logger.Append(Severity.ERROR, "Could not remove session : "+_e.Message+" ---- "+_e.StackTrace);	
			}
		}

	
		/*private void ChunkSent(bool sent, int bsId, string chunkName, int destination){
			if (sent == true){
				Backup backup = GetCurrentBackup(bsId);
				
				//backup.AddChunkDestination returns true if chunkcurrent  redundancy meets chunk.Redundancy requirement
				if(backup.AddChunkDestination(chunkName, destination)){
					Backup.DeleteChunk(chunkName);
					tasksListDone[bsId] = tasksListDone[bsId] +1;
				}
				Logger.Append(Severity.DEBUG2, "tasklistdone="+tasksListDone[bsId]+", tasklisttodo="+tasksListTodo[bsId]);
				if(tasksListDone[bsId] == tasksListTodo[bsId]-1){ // time to send index
					// Let's create, build and save the backup index
					string indexName = GetCurrentBackup(bsId).BuildIndex();
					AskIndexDest(bsId, indexName);
				}
				if(tasksListDone[bsId] == tasksListTodo[bsId]){
					SendDoneBackup(bsId);
				}
			}
		}*/
		
		/// <summary>
		///	Sends information, errors, statistics to Hub about a running task 
		/// </summary>
		/// <param name="code">
		/// A <see cref="System.Int32"/>
		/// </param>
		/// <param name="data">
		/// A <see cref="System.String"/>
		/// </param>
		private void HubSendTaskEvent(long taskId, int code, string data, string additionalMessage){
			HubWrite("TSK "+taskId+" "+code+" "+data+" "+additionalMessage);
		}
		
		private bool IsRedundancyMet(int current, int wanted){
			if(current == wanted)	return true;
			else return false;
		}
		
		/// <summary>
		/// If file is recovery-file is received SendRecoveryToHub is called
		/// </summary>
		/// <param name="received">true if file has been received</param>
		/// <param name="bFReceived">the file that has been received</param>
		private void FileReceived(bool received, BChunk chunk){
			/*if (received == true){
				foreach (BChunk bc in backup.Chunks){
					if(bc.Name == chunk.Name){
						bc.Fetched = true;
						UpdateGUIEvent("prbRecovery", bc.Size.ToString());
						SendRecoveryToHub();
					}	
				}
			}*/
		}

	
		private void SendBackup(int taskId, int sessionId, string redundancy, /*string chunkName,*/ int nodeId, string nodeIp, int port, string cN, string cK, bool isIndex, int budget){
			Backup backup = GetCurrentBackup(taskId);
			Logger.Append(Severity.DEBUG, "Received DST,  permission to store "+budget+" chunks (redundancy level "+redundancy+") on node "+nodeIp+":"+port+" "+cN);
			Session storageSession = null;
			try{
				storageSession = GetStorageSession(SessionType.Backup, taskId, sessionId, nodeId, nodeIp,  port,  cN,  cK, budget);
				
			}
			catch(Exception e){ // could not get session with storage node, ask alternate destination to hub
				Logger.Append(Severity.WARNING, "Could not connect to storage node "+cN+ " ("+e.Message+"---"+e.StackTrace+"), asking ALTernate destination to hub");
				AskAlternateDestination(taskId, nodeId);
			}
		}
		
		public int GetEffectiveRedundancy(string rawRedundancy){
			return int.Parse(rawRedundancy.Split('/')[1]);
		}
		
		internal static void AskAlternateDestination(long taskId, int blackListedNodeId){
			Logger.Append(Severity.INFO, "Asking alternate storage session to replace session with node #"+blackListedNodeId);
			HubWrite("ALT " + taskId+ " " +"10"+ " "+blackListedNodeId);
		}
		/*private Session GetCleanSession(int taskId, int nodeId, string nodeIp, int port, string cN, string cK){
			Session cleanSession = null;
			foreach(Session sess in sessionsConnect)
				if(sess.Type == SessionType.Clean && sess.ClientId == nodeId){
					Logger.Append(Severity.DEBUG, "Already have an open cleaning session with storage node "+cN+" ("+nodeIp+":"+port+"), reusing it. ");
					cleanSession = sess;
				}
			if(cleanSession == null){
				cleanSession = new Session(SessionType.Backup, nodeId, nodeIp, port, cN, cK, this.keyPairCrypto);
				//with threadpool, ALT doesnt work (no exceptions propagation between threads)
				ThreadPool.QueueUserWorkItem(cleanSession.ConnectToStorageNode);
				//storageSession.ConnectToStorageNode(null);
				cleanSession.RemoveSessionEvent += new Node.Session.RemoveSessionHandler(this.RemoveSession);
				//storageSession.FileSentEvent += new Node.Session.FileSentHandler(this.ChunkSent); //, bsId, chunkName, nodeId);
				cleanSession.FileReceivedEvent += new Node.Session.FileReceivedHandler(this.FileReceived);
				sessionsConnect.Add(cleanSession);
				cleanSession.AuthenticatedAndReadyEvent.WaitOne();
			}	
			return cleanSession;
		}*/
		
		private Session GetStorageSession(SessionType sessionType, int taskId, int sessionId, int nodeId, string nodeIp, int port, string cN, string cK, int budget){
			Session storageSession = null;
			foreach(Session sess in sessions/*Connect*/)
				if(sess.Type == SessionType.Backup && sess.ClientId == nodeId && sess.Id == sessionId){
					Logger.Append(Severity.DEBUG, "Reusing open session #"+sessionId+" with storage node #"+nodeId+" ("+nodeIp+":"+port+"), reusing it. ");
					storageSession = sess;
				}
			if(storageSession == null){
				storageSession = new Session(sessionType, sessionId, nodeId, nodeIp, port, cN, cK, keyPairCrypto);
				//storageSession.Id = sessionId;
				//with threadpool, ALT doesnt work (no exceptions propagation between threads)
				//ThreadPool.QueueUserWorkItem(storageSession.ConnectToStorageNode);
				storageSession.ConnectToStorageNode(null);
				storageSession.RemoveSessionEvent += new Node.Session.RemoveSessionHandler(this.RemoveSession);
				//storageSession.FileSentEvent += new Node.Session.FileSentHandler(this.ChunkSent); //, bsId, chunkName, nodeId);
				storageSession.FileReceivedEvent += new Node.Session.FileReceivedHandler(this.FileReceived);
				
				storageSession.AuthenticatedAndReadyEvent.WaitOne();
				sessions/*Connect*/.Add(storageSession);
			}	
			//return storageSession;
			storageSession.RenewBudget(budget);
			if(StorageSessionReceivedEvent != null){
				
				StorageSessionReceivedEvent(taskId, storageSession/*, budget*/);
			}
			else
				Logger.Append(Severity.ERROR, "StorageSessionReceivedEvent is null");
			return storageSession;
		}
		
		/// <summary>
		/// To be called when a task is done (good or error).
		/// It close sessions with storage peers.
		/// </summary>
		/// <param name="taskId">
		/// A <see cref="System.Int32"/>
		/// </param>
		internal void TerminateTask(int taskId){
			for(int i = sessions/*Connect*/.Count-1; i==0; i--){
				sessions/*Connect*/[i].RemoveSessionEvent -= new Node.Session.RemoveSessionHandler(this.RemoveSession);
				//sessionsConnect[i].FileSentEvent -= new Node.Session.FileSentHandler(this.ChunkSent); //, bsId, chunkName, nodeId);
				sessions/*Connect*/[i].FileReceivedEvent -= new Node.Session.FileReceivedHandler(this.FileReceived);
				sessions/*Connect*/[i].Disconnect();
				sessions/*Connect*/[i] = null;
				sessions/*Connect*/.RemoveAt(i);
			}
		}
		
		private Backup GetCurrentBackup(long taskId){
			foreach(Backup b in backupsList)
				if(b.TaskId == taskId)
					return b;
			return null;
		}
			
		/// <summary>
		/// Creates a session for receiving a backup
		/// </summary>
		/// <param name="cIp">client1 IP</param>
		/// <param name="cN">client1 IP</param>
		/// <param name="cK">client1 public key</param>
		private void ReceiveBackup(int nodeId, long taskId, int sessionId, string cIp, string cN, string peerPubKey, int budget, DataProcessingFlags flags){
			bool alreadyExistingSession = false;
			foreach(Session listeningSess in sessions/*Listen*/){
				if(listeningSess.Type == SessionType.Store && listeningSess.ClientId == nodeId && listeningSess.Id == sessionId){
					alreadyExistingSession = true;
					Logger.Append(Severity.DEBUG, "Reusing existing session "+sessionId+" with node #"+nodeId+", "+cN+" ("+cIp+":"+")");
					listeningSess.RenewBudget(budget);
				}
			}
			if(!alreadyExistingSession){
				Logger.Append(Severity.DEBUG, "Creating new session to receive data from "+cN+"  "+cIp+":");
				Session client1 = new Session(SessionType.Store, nodeId, cIp, cN, peerPubKey, keyPairCrypto, this.ShareRoot, budget, taskId, flags);
				client1.Id = sessionId;
				client1.RemoveSessionEvent += new Node.Session.RemoveSessionHandler(this.RemoveSession);
				//client1.FileSentEvent += new Node.Session.FileSentHandler(this.ChunkSent);
				client1.FileReceivedEvent += new Node.Session.FileReceivedHandler(this.FileReceived);
				client1.UpdateStorageEvent += new Node.Session.UpdateStorageHandler(this.UpdateStorageSpace);
				client1.RenewBudget(budget);
				sessions/*Listen*/.Add(client1);
				try{
					StartListeningForClient(cIp, client1);
				}
				catch(Exception e){
					Logger.Append(Severity.WARNING, "Could not accept session with client node #"+nodeId+" : "+e.Message);
					RemoveSession(client1);
				}
			}
		}
		
		private void ReceiveDelete(int nodeId, long taskId, string cIp, string cN, string cK, string chunkName){
			bool alreadyExistingSession = false;
			foreach(Session listeningSess in sessions/*Listen*/){
				if(listeningSess.Type == SessionType.CleanData && listeningSess.ClientId == nodeId ){
					alreadyExistingSession = true;
					Logger.Append(Severity.DEBUG, "Reusing existing cleaning session with node "+cN+" ("+cIp+":"+")");
				}
			}	
			if(!alreadyExistingSession){
				Logger.Append(Severity.DEBUG, "Creating new session to delete data from "+cN+"  "+cIp+":");
				Session client1 = new Session(SessionType.CleanData, nodeId, cIp, cN, cK, keyPairCrypto, this.ShareRoot, 0, taskId, DataProcessingFlags.None);
				client1.RemoveSessionEvent += new Node.Session.RemoveSessionHandler(this.RemoveSession);
				//client1.FileSentEvent += new Node.Session.FileSentHandler(this.ChunkSent);
				//client1.FileReceivedEvent += new Node.Session.FileReceivedHandler(this.FileReceived);
				client1.UpdateStorageEvent += new Node.Session.UpdateStorageHandler(this.UpdateStorageSpace);
				sessions/*Listen*/.Add(client1);
				StartListeningForClient(cIp, client1);
			}
		}
		/// <summary>
		/// Creates a session for receiving a recovery
		/// </summary>
		/// <param name="cIp">client2 IP</param>
		/// <param name="cN">client2 IP</param>
		/// <param name="cK">client2 public key</param>
		/*private void GetRecovery(string cIp, int port, string cN, string cK){
			foreach(BChunk bf in backup.Chunks){
				if(bf.ClientName ==  cN && bf.Fetched == false){
					Session client2 = new Session(2, this, cIp, port, cN, cK, this.keyPairCrypto,bf);
					client2.RemoveSessionEvent += new Node.Session.RemoveSessionHandler(this.RemoveSession);
					client2.FileSentEvent += new Node.Session.FileSentHandler(this.FileSent);
					client2.FileReceivedEvent += new Node.Session.FileReceivedHandler(this.FileReceived);
					sessionsConnect.Add(client2);
					ConnectToClient(client2);
					break;
				}
			}	
		}*/

		/// <summary>
		/// Creates a session for sending a recovery
		/// </summary>
		/// <param name="cIp">client1 IP</param>
		/// <param name="cN">client1 IP</param>
		/// <param name="cK">client1 public key</param>
		private void SendRecovery(int NodeId, string cIp, string cN, string cK){
			/*Session client1 = new Session(4, nodeId, cIp, cN, cK, this.keyPairCrypto,this.SharePath);
			client1.RemoveSessionEvent += new Node.Session.RemoveSessionHandler(this.RemoveSession);
			client1.FileSentEvent += new Node.Session.FileSentHandler(this.ChunkSent);
			client1.FileReceivedEvent += new Node.Session.FileReceivedHandler(this.FileReceived);
			sessionsListen.Add(client1);
			StartListeningForClient(cN, client1);*/
		}

		private void ApplyConf(){
			this.ShareRoot = ConfigManager.GetValue("Storage.Directory");
			Logger.Reload();
		}
		
		private void DeleteBackup(long cleanTaskId, long taskId, string indexName, string indexSum){
			P2PBackup.Common.Task cleanTask = new P2PBackup.Common.Task();
			cleanTask.Id = cleanTaskId;
			cleanTask.Operation = TaskOperation.HouseKeeping;
			Node.DataProcessing.Index bi = new Node.DataProcessing.Index();
			try{
				/*if(indexName!=null && indexName != String.Empty)
					bi.OpenByName(indexName);
				else*/
					bi.Open(indexName);
				//tasksList.Add(cleanTask);
			}
			catch(Exception e){
				Logger.Append(Severity.ERROR, "Could not start Housekeeping task "+cleanTaskId+" for backup task "+taskId+": "+e.Message);
				HubWrite("EXP "+cleanTaskId+" "+ taskId+" "+810);
				// TODO : remove task from list
				return;
			}
			Node.DataProcessing.DedupIndex dedupDB = Node.DataProcessing.DedupIndex.Instance();
			dedupDB.Initialize(-1);//TODO : pass reference Task when deleting backup, to open correct dedup db
			//BChunk chunk;
			int deletable=0;
			int nonDeletable = 0;
			int dereferenceable = 0;
			// TODO! port to new sql index implementation
			/*while((chunk = bi.ReadChunk()) != null){
				//Console.WriteLine ("read chunk "+chunk.Name+", storage node="+String.Join(",", chunk.StorageDestinations));
				// 1/2 : find if this chunk is referenced by other backups (in dedup db). If not, can be deleted
				int refs = dedupDB.ChunkReferences(chunk.Name);
				
				if(refs ==0){
					deletable++;
					HubWrite("EXP "+cleanTaskId+" "+ taskId+" DEL "+chunk.Name+" "+ string.Join(",", chunk.StorageDestinations));
				}
				else
					nonDeletable++;
				// 2/2 : for this chunk's content, decrement refcounts (if backup ued dedup)
				foreach(IFSEntry f in chunk.Files){
					if(f.BlockMetadata == null)continue;
					foreach(Node.DataProcessing.IFileBlockMetadata metadata in f.BlockMetadata.BlockMetadata){
						if(metadata is Node.DataProcessing.ClientDedupedBlock){
							//Console.WriteLine(f.SnapshottedFullPath+": "+((Node.DataProcessing.ClientDedupedBlock)metadata).Checksum.ToString());
							dereferenceable++;
							dedupDB.DeReference(((Node.DataProcessing.ClientDedupedBlock)metadata).Checksum);
						}
					}
					
				}
				
			}*/
			// TODO : 2nd pass deleting chunks not referenced anymore in dedup list
			// TODO : 3rd pass compacting chunks having more than 50% unused/expired data
			dedupDB.Persist();
			dedupDB = null;
			bi.Terminate();
			HubWrite("EXP "+cleanTaskId+" "+ taskId+" "+710);
			Logger.Append (Severity.INFO, "Expired task "+taskId);
			Console.WriteLine ("task "+taskId+" had "+deletable+" deletable chunks, "+nonDeletable+" referenced chunks, "+dereferenceable+" deduped blocks dereferences");
			// TODO : also delete index/dedupDB chunk
			
		}
		
		/*public bool Equals(P2PBackup.Common.Node other){
		      if (this.Uid == other.Uid)
		         return true;
		      else
		         return false;
		}*/
	}
}

