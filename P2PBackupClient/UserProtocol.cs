using System;
using Node.Utilities;
using P2PBackup.Common;
using Node.StorageLayer;

namespace Node{
	internal partial class User:P2PBackup.Common.Node	{
		/// <summary>
		/// Decodes all messages that arrive from the hub.
		/// </summary>
		/// <param name="message">arrived message</param>
		private void Decode(string message){
			try{
				if (message == null || message == String.Empty){
					Logger.Append(Severity.WARNING, "Received null message, ignoring");
					return;
				}
				message = message.Replace(System.Environment.NewLine, "");
				Logger.Append(Severity.DEBUG2, "Received raw message '"+message+"'");
				char[] separator = {' '};
				string[] decoded = message.Split(separator);
				Utilities.Codes.GetDescription(decoded[0]);
				switch(decoded[0]){
					case "ACS":
						if(decoded.Length == 2 && UpdateGUIEvent != null){
							this.Available = long.Parse(decoded[1]);
							UpdateGUIEvent("ACS",decoded[1]);
						}
						break;
					case "BRW": // hub asks to browse a FS path
						if(decoded.Length>= 2){
							string path = "";
							for(int i=1; i<decoded.Length; i++)	
								path += decoded[i]+" ";
							//path.Trim(); // we don't support paths begining or ending by space. this is a bug
							IPathBrowser pb = PathBrowser.GetPathBrowser();
							HubWrite("BRW "+pb.Browse(path));
						}
						else
							ProtocolViolationException(message, 2, decoded.Length);	
						break;
					case "CHA":
						if(decoded.Length >= 3)
							Respond(decoded[1],decoded[2]);
						else
							ProtocolViolationException(message, 3, decoded.Length);
						break;
					case "CERT": // Hub sends the requested SSL certificate
						if(decoded.Length == 2){
							Logger.Append(Severity.INFO, "Received SSL certificate, saving...");
							SaveCert(Convert.FromBase64String(decoded[1]));
							//Disconnect(true);
							//ConnectToHub(true);
						}
						else
							ProtocolViolationException(message, 2, decoded.Length);	
						break;
					case "CFG": // Hub sends requested configuration upon successful authentication
						if(decoded.Length >=2){
							string full = "";
							for(int i=1; i<decoded.Length; i++)	
								full += decoded[i]+" ";
							Utilities.ConfigManager.BuildConfFromHub(full);
							this.ApplyConf();
							// We check for storage sub-directories, and create them if needed
							if(Utilities.Storage.MakeStorageDirs())
								StartStorageListener();
						}
						else
							ProtocolViolationException(message, 3, decoded.Length);
						break;
					
					case "DRV":
						IPathBrowser pb2 = PathBrowser.GetPathBrowser();
						HubWrite("DRV "+pb2.GetDrives());
						break;
					case "DST":
						if(decoded.Length == 11)
							// SendBackup(int bsId, string redundancy, /*string chunkName,*/ int nodeId, string nodeIp, int port, string cN, string cK, bool isIndex, int budget){
							SendBackup(int.Parse(decoded[2]), int.Parse(decoded[3]), decoded[1], int.Parse(decoded[4]), decoded[5],  int.Parse(decoded[6]), 
						           decoded[7], decoded[8], bool.Parse(decoded[9]), int.Parse(decoded[10]));
						else
							ProtocolViolationException(message, 10, decoded.Length);	
						break;
					case "EXP":
						if(decoded.Length >=3){
							if(tasksList.Count >1){
								HubWrite("TSK "+decoded[1]+" 800");
								Logger.Append(Severity.INFO, "Maximum number of jobs reached, refusing to process task "+decoded[1]);
							}
							else
								DeleteBackup(long.Parse(decoded[1]), long.Parse(decoded[2]), decoded[3], decoded[4]);
						}
						break;
					case "LET"://Destination for file
						if(decoded.Length == 4)
							SendRecovery(int.Parse(decoded[1]),decoded[2],decoded[3], decoded[4]);
						else
							ProtocolViolationException(message, 4, decoded.Length);	
						break;
					case "RCV"://Receive and store chunks
						if(decoded.Length >= 9)
						//ReceiveBackup(int nodeId, long taskId, string cIp, string cN, string cK, int budget){
							ReceiveBackup(int.Parse(decoded[1]), long.Parse(decoded[2]), int.Parse(decoded[3]), decoded[4], decoded[5], 
						              /*node key*/decoded[6], /*budget*/int.Parse(decoded[7]), /*dataprocessingflags*/(DataProcessingFlags)int.Parse(decoded[8]));
						else
							ProtocolViolationException(message, 8, decoded.Length);						
						break;
					case "DEL": // delete 1 stored chunk (housekeeping operation)
						if(decoded.Length == 7)
							ReceiveDelete(int.Parse(decoded[1]), long.Parse(decoded[2]), decoded[3], decoded[4], decoded[5], decoded[6]);
							//ReceiveBackup(int.Parse(decoded[1]), long.Parse(decoded[2]), decoded[3], decoded[4], decoded[5], int.Parse (decoded[6]));
						else
							ProtocolViolationException(message, 6, decoded.Length);						
						break;
					case "SRC"://Receive file
						//if(decoded.Length == 4)
						//	GetRecovery(decoded[1], int.Parse(decoded[2]), decoded[3], decoded[4]);
						//else
						//	ProtocolViolationException(message, 4, decoded.Length);	
						break;
					case "BKS": // order from hub to start backup
					//("BKS "+task.TrackingId+" "+Utils.GetUnixTimeFromDateTime(refStartDate)+" "+Utils.GetUnixTimeFromDateTime(refEndDate)+" "+ConfigurationManager.AppSettings["Backup.MaxChunkSize"]+" "+ConfigurationManager.AppSettings["Backup.MaxChunkSize"]+" "+task.BackupSet.DumpToXml())
						if (decoded.Length >= 7){ // parameters : taskid, refstartdate, refenddate, maxpacksize, maxchunksize, backupset
							string backupset = "";
							for(int i=7; i< decoded.Length; i++)
								backupset += decoded[i]+" ";
							/*long maxPackSize = long.Parse(decoded[2]);long maxChunkSize = long.Parse(decoded[3]);*/
							RunBackup(int.Parse(decoded[1]), int.Parse(decoded[2]), decoded[3], decoded[4], long.Parse (decoded[5]), long.Parse (decoded[6]), BackupSet.GetFromXml(backupset));
						}
						else
							ProtocolViolationException(message, 4, decoded.Length);	
						break;
					case "RCS":
						if (decoded.Length == 3) // parameters : name of backup, target directory
							PrepareRecovery(decoded[1], decoded[2]);
						else
							ProtocolViolationException(message, 3, decoded.Length);	
						break;
					case "SPO":
						HubWrite("SPO "+ObjectsBrowser.BuildObjectsList());
						break;
					case "STATS": // Hub requests statistics about running task
						if (decoded.Length == 2){
							try{
								string stats = GetBackupStats(long.Parse(decoded[1]));
								HubWrite("STATS "+decoded[1]+" "+stats);
							}
							catch(InvalidOperationException){
								HubWrite("UNKNOWN "+decoded[1]);
							}
						}
						break;
					case "VMS":
						Virtualization.VirtualMachinesManager vmm = new Node.Virtualization.VirtualMachinesManager();
						
						HubWrite("VMS "+vmm.BuildVmsJson());
						break;
					/*case "SSS":
						if (decoded.Length == 3) // received order from hub to share storage space
							this.fileHand.SaveShareSize(decoded[1], decoded[2]);
						else
							ProtocolViolationException(message, 3, decoded.Length);	
						break;*/
					case "201": //Verification client-hub ok
						if(UpdateGUIEvent != null) UpdateGUIEvent("201","");
						HubWrite("VER "+Utilities.PlatForm.Instance().NodeVersion+" "+Utilities.PlatForm.Instance().OS);
						AskConfig(); // Now we ask to download our configuration.
						
						break;
					case "202": //OK to take backup
						//if(decoded.Length == 2){
						//Monitor.Exit(HubHasAcceptedBackup);
						//hubHasAcceptedBackup = true;
						break;
					case "208": //Share ok
						if(decoded.Length == 1 && UpdateGUIEvent != null) UpdateGUIEvent("208","");
						else
							ProtocolViolationException(message, 1, decoded.Length);	
						break;
					case "209": //Share ok
						if(decoded.Length == 1 && UpdateGUIEvent != null){
							UpdateGUIEvent("209","");
						}
						else
							ProtocolViolationException(message, 1, decoded.Length);	
						break;
					case "215":
						//if(decoded.Length == 2)	//SendPut(decoded[1]);
							Logger.Append(Severity.DEBUG, "RECEIVED 215 (CHUNK TRANSFER OK) -- DO I NEED TO DO STHG? CALL FILEDONE?");
						break;
					case "301"://Verification klient-hub ok, but need share size
						if(decoded.Length == 1 && UpdateGUIEvent != null) UpdateGUIEvent("301","");
						else
							ProtocolViolationException(message, 1, decoded.Length);	
						break;
					case "302"://Verification klient-hub ok or Share ok, but need public key
						if(decoded.Length == 1 && UpdateGUIEvent != null) UpdateGUIEvent("302","");
						else
							ProtocolViolationException(message, 1, decoded.Length);	
						break;
					case "401"://User doesn't exist
						/*if(decoded.Length == 1 && UpdateGUIEvent != null){
							UpdateGUIEvent("401","");
							Logger.Append(Severity.INFO, "First connection, waiting for approval from hub (401)");
						}*/
						Logger.Append(Severity.INFO, "First connection, waiting for approval from hub (401)");
						break;
					case "402"://Verification client-hub did not succeed
						if(decoded.Length == 1 && UpdateGUIEvent != null){
							UpdateGUIEvent("402","");
						}
						break;
					case "403"://Not enough space for backup
						if(decoded.Length == 1 && UpdateGUIEvent != null){
							//backup.DeleteEncryptedFiles();
							//fileHand.DeleteIndexFile(backup.BackupName);
							//backup = null;
							UpdateGUIEvent("403","");
						}
						else
							ProtocolViolationException(message, 1, decoded.Length);	
						break;
					case "404"://Share is smaller than used space
						if(decoded.Length == 1){
							UpdateGUIEvent("404","");
						}
						else
							ProtocolViolationException(message, 1, decoded.Length);	
						break;
					case "406":case "CANCEL"://Share is smaller than needed space, or no storage space available
						CancelTask(long.Parse(decoded[1]));
						CleanBackup(long.Parse(decoded[1]));
							/*UpdateGUIEvent("406","");*/
						break;
					case "501"://IP-address blocked
						if(decoded.Length == 1){
							
						}
						break;
					case "502":// Pending for approval
						Logger.Append(Severity.INFO, "Received Waiting for Hub approval (502), could not operate right now.");
						break;
					default : 
						string mesg = "";
						for(int i=1; i<decoded.Length; i++)
							mesg += decoded[i]+" ";
						//if(hubWaiter.AddReceived(type, mesg))
						//	Console.WriteLine("DEBUG: User.Decode : Received message we were waiting for, type"+type);
						//else
							Logger.Append(Severity.ERROR, "Received unrecognized code message, type '"+decoded[0]+"', content="+mesg);
						UpdateGUIEvent("999","");
						break;
				}
			}
			catch(Exception ex){
				Logger.Append(Severity.ERROR, "message '"+message+"' :"+ex.Message+" ---- "+ex.StackTrace);
			}
		}
		

		private void ProtocolViolationException(string msg, int expectedFields, int receivedFields){
			Logger.Append(Severity.ERROR, "Protocol violation  : expected "+expectedFields+" fields but received "+receivedFields
			              +", message was "+msg);
		}
	}
}

