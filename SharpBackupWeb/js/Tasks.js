Ext.Loader.setConfig({
    enabled: true,
    paths: {'Ext.ux': '/ext4/ux'},
});

Ext.require([
 	'Ext.data.proxy.Rest',
    'Ext.data.*',
    'Ext.grid.*',
    'Ext.tree.*',
    'Ext.form.*',
    'Ext.window.*',
    'Ext.ux.RowExpander',
	]);
	
Ext.onReady(function () {
	Ext.Loader.setConfig({enabled:true});
	Ext.Loader.setPath('Ext.ux', '/ext4/ux');
	var params = Ext.urlDecode(window.location.search.substring(1));
	var lang;
    if(params.lang)
        lang = params.lang;
	/*i18n = new Ext.i18n.Bundle({
        bundle:'wui', 
        path:'i18n',
        lang:lang
    });*/
    i18n = Ext.create('Ext.i18n.Bundle',{
		bundle: 'wui',
		lang: lang,
		path: 'i18n',
		noCache: true
	});
	/*i18nTask = new Ext.i18n.Bundle({
        bundle:'taskmsg', 
        path:'i18n',
        lang:lang
    });*/
	i18nTask = Ext.create('Ext.i18n.Bundle',{
		bundle: 'taskmsg',
		lang: lang,
		path: 'i18n',
		noCache: true
	});
i18n.onReady(function(){
//i18nTask.onReady(function(){
 Ext.get('runningTasksTitle').dom.innerText = i18n.getMsg('runningTasks.title');
 var refreshInterval; //interval id set by setInterval()
 var detailsToggled = false; 
 
 Ext.define('TaskM', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'id',     type: 'number'},
        {name: 'type',     type: 'string'},
        {name: 'userid',     type: 'number'},
        {name: 'operation', type: 'string'},
        {name: 'runningStatus', type: 'string'}, //started, pendingstart, done...
        {name: 'status', type: 'string'}, // ok, warning, error...
        {name: 'priority', type: 'string'},
        {name: 'bsId', type: 'string'},
        {name: 'bsName', type: 'string'},
        {name: 'bsType', type: 'string'},
        /*{name: 'compress', type: 'string'},
        {name: 'encrypt', type: 'string'},
        {name: 'clientdedup', type: 'string'},*/
        {name: 'flags', type:'string'},
        {name: 'parallelism', type: 'number'},
        {name: 'clientId', type: 'number'},
        {name: 'startDate', type: 'date'},
        {name: 'elapsedTime', type: 'number'},
        {name: 'endDate', type: 'date'},
       	{name: 'percent', type: 'number'},
       	{name: 'originalSize', type: 'number'},
       	{name: 'finalSize', type: 'number'},
       	{name: 'totalItems', type: 'number'},
       	{name: 'currentAction', type: 'string'},
       	{name: 'cls', type: 'string', defaultValue:'gridCell'},
    ],
    hasMany:{model:'NodeM', name:'sessions', associationKey:'children'}
  });
  
  var taskStore = new Ext.data.JsonStore( {
        model: 'TaskM',
        autoLoad:true,
        loadMask:false,
        //groupField:'bsName',
        proxy: {
            type: 'ajax',
            url: '/Get.aspx?w=Tasks',
            reader:{
	        	type:'json',
	        	root:'items',
	        	totalProperty: 'totalCount',
	        },
        },
        listeners:{
        	load:function(){
        		Ext.get('runningTasksTitle').dom.innerText = i18n.getMsg('runningTasks.title')+' ('+taskStore.getTotalCount()+')';
        	}
        }
  });
  
  Ext.define('NodeM', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'id',     		type: 'string'},
        {name: 'userName',     	type: 'string'},
        {name: 'ip', 			type: 'string'},
        {name: 'share', 		type: 'number'},
        {name: 'available', 	type: 'number'},
        {name: 'percent', 		type: 'string'},
        {name: 'quota', 		type: 'string'},
        {name: 'usedquota', 	type: 'string'},
        {name: 'group', 		type: 'number'},
        {name: 'iconCls', 		type: 'string'},
    ],
    
    hasMany:{model:'NodeM', name:'children', associationKey:'children'}
 });

 var nStore = new Ext.data.JsonStore( {
    model: 'NodeM',
    proxy: {
        type: 'ajax',
        url: '/Get.aspx?w=Clients&online=true',
        //root:'children'
    },
 });
 nStore.load();
 
 Ext.define('User', {
        extend: 'Ext.data.Model',
        fields: [
            {name: 'id',     type: 'number'},
            {name: 'name',     type: 'string'},
            {name: 'password', type: 'string'},
            {name: 'email', type: 'string'},
            {name: 'culture', type: 'string'},
            {name: 'language', type: 'string'},
            {name: 'enabled', type: 'boolean'},
            {name: 'lastlogin', type: 'date', defaultValue:'Never'},
        ]
    });
	
    var usersStore = new Ext.data.JsonStore( {
        model: 'User',
        proxy: {
            type: 'ajax',
            url: '/Get.aspx?w=Users'
        },
    });
   usersStore.load();
    
  var groupingFeature = Ext.create('Ext.grid.feature.Grouping',{
        //groupHeaderTpl: '{[i18n.getMsg("runningTasks."+name )]} ({rows.length} Item{[values.rows.length > 1 ? "s" : ""]})',
        groupHeaderTpl: ' {[i18n.getMsg("runningTasks."+Ext.getCmp("groupBy").getValue())]} : {name} ({rows.length})',
        depthToIndent:15,
        
  });
 
  
  
  /*var logEntryExpander = new Ext.ux.RowExpander({
  	rowBodyTpl:['<div style="overflow-y:auto;overflow-x:none" class="logEntries">'],
    listeners:{
	  	expand: function(ex, record, body, rowIndex){
	  		ProcessExpander(record, body, rowIndex);
	  	}
	  }
  });*/
  
  function FormatTime(val){
  	
  	var seconds = Math.round(val%60);
  	if(seconds <10) seconds = "0"+seconds;
  	
  	var hours = Math.floor(val/3600);
  	var minutes = Math.floor((val/60)%60);
  	if(minutes <10) minutes = "0"+minutes;
   	return hours+":"+minutes+":"+seconds;
  }
  /*
  var expander = new Ext.ux.RowExpander({
  	id:'expander',
  	
              rowBodyTpl:[
	              '<table><tr><td><div class="gridCell" style="margin-left:75px;max-height:100;width:550;overflow-x:none;overflow-y:auto;"><table>',
	              '<tpl for=".">',
	              '<tr>',
	              '<td>',
	              '<tpl if="code &gt;= 800 && code &lt; 900"><img class="x-tree-node-icon" src="/images/sq_re.gif"/></tpl>',
	              '<tpl if="code &gt;= 900"><img class="x-tree-node-icon" src="/images/sq_ye.gif"/></tpl>',
	              //'</td>',
	              '{date}</td>',
	              '<td>{message}</td>',
	              '</tr>',
	              '</tpl>',
	              '</table></div></td>',
	              '<td width="50">&nbsp;</td>',
	              '<td><div class="gridCell" style="max-height:100;width:150;overflow-x:none;overflow-y:auto;"><table>',
	              'sessions:<br/><br/><br/><br/><br/>',
	              '</div></td></tr></table>'
              ],
              urlTpl: '/Get.aspx?w=TaskLogEntries&trackingId={id}',
              stateful:true,
              listeners:{
              	onexpand: function(ex, record, body, rowIndex){
              		ProcessExpander(record, body, rowIndex);
              	}
              }
  });
  */
  var combo = new Ext.form.field.ComboBox({
	  name : 'perpage',
	  width: 40,
	  store: new Ext.data.ArrayStore({
	    fields: ['id'],
	    data  : [
	      ['15'],
	      ['20'],
	      ['30'],
	      ['40'],
	    ]
	  }),
	  mode : 'local',
	  value: '20',
	  listWidth     : 40,
	  triggerAction : 'all',
	  displayField  : 'id',
	  valueField    : 'id',
	  editable      : false,
	  forceSelection: true
	});
	var selModel = Ext.create('Ext.selection.CheckboxModel', {
        listeners: {
            selectionchange: function(sm, selections) {
                if(selections.length > 0){
                	Ext.getCmp('cancelTaskBtn').enable();
                	clearInterval(refreshInterval);
                }
                else{
                	Ext.getCmp('cancelTaskBtn').disable();
                	SetAutoRefresh();
                }
            }
        }
  });
  
   function toggleDetails(){
    	var nRows=taskStore.getCount();
    	var theGrid = Ext.getCmp('grid');
    	var exp = theGrid.getPlugin('expander');
    	theGrid.invalidateScroller();
	    for(i=0;i< nRows;i++){
	        exp.toggleRow(i);
	        //grid.plugins.each(function(plugin){	alert(plugin.id)}) ;
	        
	    //theGrid.invalidateScroller();
	    }
	    if(detailsToggled == true) detailsToggled = false;
	    else detailsToggled = true;
	    
	    theGrid.invalidateScroller();
	    theGrid.doLayout();
	}
        	
	var bbar = new Ext.toolbar.Paging({
	  store: taskStore,
	  displayInfo: true,
	  dock: 'bottom',
	 /* height:25,*/
	  items   :    [
	    '-',
	    i18n.getMsg('generic.perPage'),
	    combo,
	    '-',
	   /* {
    		xtype:'displayfield',
    		value:i18n.getMsg('runningTasks.groupBy'),
    	},
	    '-',*/
	   	{
    		text:'&nbsp;&nbsp;'+i18n.getMsg('runningTasks.start'),
    		icon:'/images/start.png',
    		/*height:27,*/
    		scale:'medium',
    	},{
    		id:'cancelTaskBtn',
    		text:'&nbsp;&nbsp;'+i18n.getMsg('runningTasks.stop'),
    		icon:'/images/stop.png',
    		scale:'medium',
    		disabled:true,
    		handler:function(){
    			/*for(var record in selModel.getSelection()){
                		alert(record.get('id'));
				}*/
				var items = "";
				Ext.each(grid.getSelectionModel().getSelection(), function (item) {
				  items += item.data.id+",";
				});
				Ext.Msg.show({
				     title:i18n.getMsg('runningTasks.confirmStop.title'),
				     msg: i18n.getMsg('runningTasks.confirmStop.message', items),
				     buttons: Ext.Msg.YESNO,
				     icon:Ext.Msg.WARNING,
				     fn:function(btn){
				     	if(btn == "yes"){
				     		var conn = new Ext.data.Connection(); 
						    conn.request({ 
						            url: 'Set.aspx?w=ChangeTasks&action=Cancel', 
						            method: 'POST', 
						            scope: this, 
						            params: items, 
						            /*success: function(responseObject) { 
						             var okMsg = Ext.Msg.alert('Status', responseObject.responseText); 
						             //Ext.getCmp('create').disable();
						            }, */
						             failure: function(responseObject) { 
						                 Ext.Msg.alert('Status', 'Unable to apply changes. Error:'+responseObject.responseText); 
						            } 
						    }); 
						    RefreshStores();
				     	}
				     	else{
				     		this.close();
				     	}
				     }
				});
    		}
    	},'-',{
    		xtype:'displayfield',
    		value:i18n.getMsg('runningTasks.groupBy'),
    	},{
    		xtype:'combo',
    		id:'groupBy',
    		stateful:true,
    		stateId:'tasksGroupByState',
    		mode:           'local',
            value:          'none',
            triggerAction:  'all',
            forceSelection: true,
            allowBlank:true,
            editable:       false,
            fieldLabel:     '',
            displayField:   'value',
            valueField:     'name',
            queryMode: 'local',
            width:150,
    		store:Ext.create('Ext.data.Store', {
                  fields : ['name', 'value'],
                    data   : [
                        {name : 'bsName',   value: 'backup set name'},
                        {name : 'operation',  value: i18n.getMsg('runningTasks.operation')},
                        {name : 'status', value: i18n.getMsg('runningTasks.status')},
                        {name : 'runningStatus', value: i18n.getMsg('runningTasks.runningStatus')},
                        {name : 'type', value: i18n.getMsg('runningTasks.type')},
                        {name : 'priority', value: i18n.getMsg('runningTasks.priority')},
                        //{name : 'percent', value: 'completion %'},
                        {name : 'clientId', value: i18n.getMsg('runningTasks.client')},
                       // {name : 'rate', value: i18n.getMsg('runningTasks.rate')},
                        {name : 'startDate', value: i18n.getMsg('runningTasks.startDate')},
                       // {name : 'elapsedTime', value: i18n.getMsg('runningTasks.duration')},
                        //{name : 'finalSize', value: i18n.getMsg('runningTasks.size.final')},
                    ]
	        }),
            listeners:{
            	'change': function( thisCombo, newValue, oldValue, options ){
            		if(newValue == '')
            			taskStore.ungroup();
            		else
            			taskStore.group(newValue);
            	}
            }
    	},{
            text:'&nbsp;&nbsp;'+i18n.getMsg('runningTasks.clearGroupBy'),
            icon:'/images/clearfield.png',
            handler : function(){
            	//groupingFeature.clear();
            	taskStore.clearGrouping();
            	
        	}
      	},'-',{
      		text:'&nbsp;'+i18n.getMsg('runningTasks.details'),
            icon:'/images/plus.gif',
            handler : toggleDetails
      	},'-',
	  ],
      toggleHandler: function(btn, pressed){
        var view = grid.getView();
        view.showPreview = pressed;
        view.refresh();
        if(detailsToggled) {
        	toggleDetails();
        	toggleDetails();
        }
       }
	});

	combo.on('select', function(combo, record) {
	  bbar.pageSize = parseInt(record.get('id'), 10);
	  bbar.doLoad(bbar.cursor);
	}, this);
	
  
  
  var grid = Ext.create('Ext.grid.Panel', {
  		id:'grid',
        renderTo: Ext.get('panel'),
        collapsible: false,
        iconCls: 'icon-grid',
        frame: false,
        stateful:true,
        stateId:'grid1',
        store: taskStore,
        selModel: selModel,
        //title:'Running tasks overview',
        height: 550,
        align:'left',
        scroll:'vertical',
        autoScroll:true,
        overlapHeader:true,
        frameHeader:false,
        multiSelect: true,
        features: [groupingFeature],
       
        viewConfig: {
        	loadMask: false,
    		onStoreLoad: Ext.emptyFn,
    	},
        plugins: [
        	{
              ptype: 'rowexpander',
              pluginId: 'expander',
              id:'expander',
              enableCaching:false,
              rowBodyTpl:[
	              '<table><tr><td><div class="gridCell" style="margin-left:75px;max-height:100;width:600;overflow-x:none;overflow-y:auto;">',
	              '<table style="height:100%;">',
	              '<tr><th colspan="3">Task log</th></tr>',
	              '<tpl for=".">',
	              '<tr>',
	              '<td>',
	              '<tpl if="code &gt;= 900"><img class="x-tree-node-icon" src="/images/sq_ye.gif"/></tpl>',
	              '<tpl if="code &gt;= 800 && code &lt; 900"><img class="x-tree-node-icon" src="/images/sq_re.gif"/></tpl>',
	              '<tpl if="code &gt;= 700 && code &lt; 800"><img class="x-tree-node-icon" src="/images/sq_gr.gif"/></tpl>',
	              '{[ Ext.Date.format(new Date(values.date), "h:i:s") ]}</td>',
	              '<td>({code}) {[i18nTask.getMsg("task."+values.code, values.message1, values.message2)]}</td>',
	              '</tr>',
	              '</tpl>',
	              '</table></div></td>',
	              '<td width="50">&nbsp;</td>',
	              '<td><div class="gridCell" style="max-height:100;width:150;overflow-x:none;overflow-y:auto;">',
	              '<table style="height:100%;">',
	              '<tr><th>Sessions</th></tr><tr><td><br/><br/><br/>',
	              //'{[ taskStore.getById('24481').sessions().each(function(session){	alert(session.id)}) ]}',
	              '</td></tr></table>',
	              '</div></td></tr></table>',
	             
              ],
              urlTpl: '/Get.aspx?w=TaskLogEntries&trackingId={id}',
              stateful:true,
              stateId:'expanderState',
              /*listeners:{
              	onexpand: function(ex, record, body, rowIndex){
              		ProcessExpander(record, body, rowIndex);
              	}
              }*/
		    },
        ],
        columns: [
	        {
	        	text: '%',
	        	width:70,
	        	height:20,
	        	dataIndex:'status',
	        	tdCls:'gridRow',
	        	renderer: function(value, metaData, record, colIndex, store, view){
	        		var roundedPercent=0;
	        		var rawPercent = record.get('percent');
	        		if(rawPercent == 0)
	        			roundedPercent = 0;
	        		else if(rawPercent < 20)
	        			roundedPercent = 20;
	        		else if(rawPercent < 40)
	        			roundedPercent = 40;
	        		else if(rawPercent < 60)
	        			roundedPercent = 60;
	        		else if(rawPercent < 80)
	        			roundedPercent = 80;
	        		else if(rawPercent <= 100)
	        			roundedPercent = 100;
	        		else 
	        			roundedPercent = 0;
	            	//value='<img src="/images/compe_'+record.get('status').substr(0,1).toLowerCase()+"_"+compA[index]+'.png"/>'+/*record.get('percent')*/compA[index]+'%';
	            	value='<img src="/images/c_'+record.get('status').substr(0,1).toLowerCase()+"_"+roundedPercent+'.png" title="'
	            		+i18n.getMsg('runningTasks.status.'+record.get('status'))+'"/><div class="greyV" style="horizontal-align:right;"><b>'+record.get('percent')+'%</b></div>';
	            	return value;
	            }
	        },
	        {
	            text: i18n.getMsg('runningTasks.task'),
	            flex: 2,
	            dataIndex: 'bsName',
	            tdCls:'gridRow',
	            width:300,
	            height:20,
	            //tpl:'#{id} : {bsName}',
	            renderer: function(value, metaData, record, colIndex, store, view){
	            	value = '<div class="taskBox"><b>#'+record.get('id')+' <i>('+record.get('bsName')+')</i></b>'
	            		+'<span><ul><li class="gStatus">'+record.get('currentAction')+'</li></ul></span></div>';
		            return value;
	            }
	        },{
	            text: i18n.getMsg('runningTasks.client'),
	            flex: 0,
	            width:140,
	            height:20,
	            dataIndex: 'clientId',
	            renderer: function(value){
	            	nStore.each( function(trecord){
						trecord.children().each(function(node) {
							if(node.get('id') == value){
								value = '<div class="'+node.get('iconCls')+'" style="padding-left:15px;background-position:top left;">'
									+node.get('userName')+'<br/> <span class="greyV">'+node.get('ip')+'</span></div>';
								//break;
							}
						});
					});
					return value;
	            }
	        },{
	            text: i18n.getMsg('runningTasks.operation'),
	            flex: 1,
	            width:90,
	            height:20,
	            dataIndex: 'operation',
	            renderer: function(value, metaData, record, colIndex, store, view){
	            	if(value == 'Backup'){
	            		value += " ("+record.get('bsType')+")<br>";
	            		/*if(record.get('compress') == 'True')
	            			value += '<img src="/images/compress.png" title="compress"/>&nbsp;';
	            		if(record.get('encrypt') == 'True')
	            			value += '<img src="/images/encrypt.png" title="encrypt"/>&nbsp;';
	            		if(record.get('clientdedup') == 'True')
	            			value += '<img src="/images/clientdedup.png" title="client deduplication"/>&nbsp;';*/
		            	if(value == '') return '';
		            	var capsList = record.get('flags').toLowerCase().split(", ");
		            	for (var i=0; i < capsList.length; i++) 
		            		if(capsList[i] != 'none')
		            			value += '<img src="/images/'+capsList[i].substring(1)+'.png" title="'+capsList[i]+'"/>&nbsp;';
	            	}
	            	return value;
	            }
	        },
	      
			        {
			            text: i18n.getMsg('runningTasks.type')+'<br>'+i18n.getMsg('runningTasks.priority'),
			            flex: 1,
			            witdh:70,
			            height:20,
			            dataIndex: 'priority',
			             renderer: function(value, metaData, record, colIndex, store, view){
			             	var userName = '';
			             	if(record.get('type') == 'Manual'){
			             		usersStore.each( function(trecord){
										if(trecord.get('id') == record.get('userid')){
											userName = ' &nbsp;&nbsp;<img src="/images/me-xs.png" border="0" align="top"/><i>'+trecord.get('name')+'</i>';
										}
								});
			             	}
	            			return ''+i18n.getMsg('runningTasks.type.'+record.get('type'))+' '+userName+'<br/><i>'+ i18n.getMsg('runningTasks.priority.'+record.get('priority'))+'</i>';
	            		}
	       },{
	            text: i18n.getMsg('runningTasks.status'),
	            flex: 1,
	            width:90,
	            height:20,
	            dataIndex: 'runningStatus',
	            renderer: function(value, metaData, record, colIndex, store, view){
	            	return i18n.getMsg('runningTasks.status.'+value)+"<br/>"+FormatTime(record.get('elapsedTime'))+"";
	            }
	        },{
	            text: i18n.getMsg('runningTasks.size'),
	            flex: 0,
	            //width:195,
	            padding:'-10 -10 -10 0',
	            style:'vertical-align:top;',
	            columns:[
	            	{
	            		text: '<i>'+i18n.getMsg('runningTasks.size.original')+'</i>',
	            		flex: 0,
	            		width:65,
	            		height:15,
	            		//padding:'-10 0 0 0',
	            		margins:'-4 0 1 0',
	            		dataIndex:'originalSize',
	            		renderer: function(value, metaData, record, colIndex, store, view){
			            	return FormatSize(value)+'<br/><div align="right"><i>'+record.get('totalItems')+'</i></div>'; //+' items)</i>';
			            }
	            	},
	            	{
	            		text: '<i>'+i18n.getMsg('runningTasks.size.final')+'</i>',
	            		flex: 0,
	            		width:65,
	            		height:15,
	            		//padding:'-10 0 0 0',
	            		margins:'-4 0 1 0',
	            		dataIndex:'finalSize',
	            		renderer: function(value){
			            	return FormatSize(value)+'<br/><div align="left"><i> '+i18n.getMsg('runningTasks.items')+'</i></div>';
			            }
	            	},
	            	
	            ]
	        },{
	            text: i18n.getMsg('runningTasks.rate'),
	            flex: 0,
	            width:85,
	            height:20,
	            dataIndex: 'rate',
	            renderer: function(value, metaData, record, colIndex, store, view){
	            	var content =  FormatSize(record.get('originalSize')/record.get('elapsedTime'))+"/s"
	            	+"<br/>"; //<i>("+record.sessions.getCount()+")</i>";
	            	var sessCount = 0;
	            	record.sessions().each(function(session) {
                		//console.log(comment.get('message'));
                		sessCount++;
            		});
            		content += "<i>("+sessCount+" "+i18n.getMsg('runningTasks.sessions')+"/"+record.get('parallelism')+")</i>";
            		return content;
	            }
	        },{
	        	//xtype:'datecolumn',
	        	
	            text: i18n.getMsg('runningTasks.startDate')
	            	+'<br/>'+i18n.getMsg('runningTasks.endDate'),
	            //format:'l, H:i:s',
	            //style:'line-height:0 !important;',
	            flex: 0,
	            width:120,
	            height:40,
	            align:'left',
	            dataIndex: 'startDate',
	            renderer: function(value, metaData, record, colIndex, store, view){
	            	return ''+Ext.Date.format(record.get('startDate'),'l, H:i:s')
	            		+'<br/>'+ Ext.Date.format(record.get('endDate'), 'l, H:i:s')+'';
	            }
	        },
        ],
        dockedItems: [bbar]
    });
 	/*grid.store.addListener('load', function() {
	  var expander = grid.getPlugin('expander');
	  for(i = 0; i < grid.getStore().getCount(); i++) {
	    expander.toggleRow(i);
	  }
	});*/
 	//automatic refresh
 	function SetAutoRefresh(){
 		refreshInterval = setInterval(RefreshStores,30000)
 	}
 	
 	function RefreshStores(){
 		nStore.load();
 		taskStore.load();
 		
 		if(detailsToggled) {
        	toggleDetails();
        }
        /*	setTimeout(null, 1000);
        	toggleDetails();
        }*/
        grid.view.refresh();
 	}
 	SetAutoRefresh();
 });
  
 });
 //});