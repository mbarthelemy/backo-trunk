Ext.onReady(function () {
 	Ext.Loader.setConfig({enabled:true});
	var params = Ext.urlDecode(window.location.search.substring(1));
	var lang;
    if(params.lang)
        lang = params.lang;
    
	/*var i18n = new Ext.i18n.Bundle({
        bundle:'wui', 
        path:'i18n',
        lang:lang
    });*/
    var i18n = Ext.create('Ext.i18n.Bundle',{
		bundle: 'wui',
		lang: lang,
		path: 'i18n',
		noCache: true
	});
	
    if(lang != null){
		var url = '/ext4/locale/ext-lang-' + lang + '.js';
	    Ext.Ajax.request({
	        url : url,
	        success : function(response) {
	                window.eval(response.responseText);
	        },
	        failure : function() {
	                Ext.Ajax.request({ // don't know for now a better way to handle locales without '-SUFFIX' part
				        url : '/ext4/locale/ext-lang-' + lang.substring(0,2) + '.js',
				        success : function(response) {
				                window.eval(response.responseText);
				        },
				    });
	        },
	        scope : this
	     });
     }
     
    /*var cp = Ext.create('Ext.state.CookieProvider', {
	    path: "/",
	    expires: new Date(new Date().getTime()+(1000*60*60*24*30)), //30 days
	    domain: "localhost"
	});
	
	Ext.state.Manager.setProvider(cp);*/


i18n.onReady(function(){

// write local TZ

function calculate_time_zone() {
	var rightNow = new Date();
	var jan1 = new Date(rightNow.getFullYear(), 0, 1, 0, 0, 0, 0);  // jan 1st
	var june1 = new Date(rightNow.getFullYear(), 6, 1, 0, 0, 0, 0); // june 1st
	var temp = jan1.toGMTString();
	var jan2 = new Date(temp.substring(0, temp.lastIndexOf(" ")-1));
	temp = june1.toGMTString();
	var june2 = new Date(temp.substring(0, temp.lastIndexOf(" ")-1));
	var std_time_offset = (jan1 - jan2) / (1000 * 60 * 60);
	var daylight_time_offset = (june1 - june2) / (1000 * 60 * 60);
	var dst;
	if (std_time_offset == daylight_time_offset) {
		dst = "0"; // daylight savings time is NOT observed
	} else {
		// positive is southern, negative is northern hemisphere
		var hemisphere = std_time_offset - daylight_time_offset;
		if (hemisphere >= 0)
			std_time_offset = daylight_time_offset;
		dst = "1"; // daylight savings time is observed
	}
	var i;
	// check just to avoid error messages
	if (document.getElementById('timezone')) {
		for (i = 0; i < document.getElementById('timezone').options.length; i++) {
			if (document.getElementById('timezone').options[i].value == convert(std_time_offset)+","+dst) {
				document.getElementById('timezone').selectedIndex = i;
				break;
			}
		}
	}
	
}

function convert(value) {
	var hours = parseInt(value);
   	value -= parseInt(value);
	value *= 60;
	var mins = parseInt(value);
   	value -= parseInt(value);
	value *= 60;
	var secs = parseInt(value);
	var display_hours = hours;
	// handle GMT case (00:00)
	if (hours == 0) {
		display_hours = "00";
	} else if (hours > 0) {
		// add a plus sign and perhaps an extra 0
		display_hours = (hours < 10) ? "+0"+hours : "+"+hours;
	} else {
		// add an extra 0 if needed 
		display_hours = (hours > -10) ? "-0"+Math.abs(hours) : hours;
	}
	
	mins = (mins < 10) ? "0"+mins : mins;
	return display_hours+":"+mins;
}

calculate_time_zone();

	var viewMenu = new Ext.menu.Menu({
        id: 'viewMenu',
       /*  overflow: 'visible'     // For the Combo popup
        },*/
        //renderTo:Ext.get('menu'),
        height:190,
        floating:true,
        autoShow:false,
        //frame:false,
       // hideMode:'visibility',
       // preventHeader:true,
        //collapsed:true,
        items: [
        	{
            	id:'w',
                text: i18n.getMsg('menu.view.welcome'), //'Welcome page',
                icon: '/images/w-i.png',
               	iconCls: 'calendar',
               	handler: onItemClick
            },'-',
             {
             	id:'bp',
                 text: i18n.getMsg('menu.view.backupsplan'), //'Backups plan',
                 iconCls: 'calendar',
                 icon: '/images/bp.png',
                 handler: onItemClick
               
            },{
            	id:'bs',
                text: i18n.getMsg('menu.view.backupSets'), //'Backup sets',
                icon: '/images/bs.png',
               	iconCls: 'calendar',
               	handler: onItemClick
            },{
            	id:'bh',
                text: i18n.getMsg('menu.view.backupHistory'), //'Backup sets',
                icon: '/images/histcal.png',
               	iconCls: 'calendar',
               	handler: onItemClick
            },'-',{
             	 id:'clients',
                 text: i18n.getMsg('menu.view.clientNodes'), //'Client nodes',
                 icon: '/images/cl-i.png',
                 iconCls: 'calendar',
                 handler: onItemClick
               
            },{
            	id:'sn',
                text: i18n.getMsg('menu.view.storageNodes'), //'Storage nodes',
                icon: '/images/sg-i.png',
                iconCls: 'calendar',
                handler: onItemClick
            },
        ]
    });
    
    var hubMenu = new Ext.menu.Menu({
        id: 'hubMenu',
        height:140,
       /* style: {
            overflow: 'visible'     // For the Combo popup
        },*/
        items: [
             {
            	id:'hubLogs',
                text: i18n.getMsg('menu.hub.logs'), //'View logs',
                icon:'/images/logs-i.png',
                iconCls: 'calendar',
                handler: onItemClick
                
            },'-',{
            	id:'hubConf',
                text: i18n.getMsg('menu.hub.configuration'), //'Configuration',
               	iconCls: 'calendar',
               	icon:'/images/logout.png',
               	handler: onItemClick
            }
        ]
    });
    
    var addMenu = new Ext.menu.Menu({
        id: 'addMenu',
        height:190,
        enableOverflow:false,
        /*style: {
            overflow: 'visible'     // For the Combo popup
        },*/
        items: [
             {
            	id:'addBs',
                text: i18n.getMsg('menu.add.backupset'), //'New Backup set',
                icon:'/images/bs.png',
                iconCls: 'calendar',
                handler: onItemClick
                
            },{
            	id:'addTpl',
                text: i18n.getMsg('menu.add.bsTemplate'), //'New Backup template',
               	iconCls: 'calendar',
               	icon:'/images/sg-i.png',
               	handler: onItemClick
            
            },{
            	id:'addSg',
                text: i18n.getMsg('menu.add.storageGroup'), //'new Storage group',
               	iconCls: 'calendar',
               	icon:'/images/sg-i.png',
               	handler: onItemClick
            }
        ]
    });
    
    var sysMenu = new Ext.menu.Menu({
        id: 'sysMenu',
        height:150,
        /*style: {
            overflow: 'visible'     // For the Combo popup
        },*/
        items: [
             {
             	// id:'Hub',
                 text: i18n.getMsg('menu.sys.hub'), //'Hub...',
                 icon:'/images/hub-i.png',
                 iconCls: 'calendar',
                 menu:hubMenu
            }
            ,{
            	id:'Users',
                text: i18n.getMsg('menu.sys.users'), //'Users...',
                icon:'/images/users.png',
                iconCls: 'calendar',
                handler: onItemClick
            },{
            	id:'Stats',
                text: i18n.getMsg('menu.sys.stats'), //'View statistics',
               	iconCls: 'calendar',
               	handler: onItemClick
            }
        ]
    });
    
    var meMenu = new Ext.menu.Menu({
        id: 'meMenu',
        /*style: {
            overflow: 'visible'     // For the Combo popup
        },*/
        height:150,
        floating:true,
        autoShow:false,
        items: [
             {
            	id:i18n.getMsg('menu.me.prefs'),
                text: 'Preferences',
                icon:'/images/prefs-i.png',
                iconCls: 'calendar',
                handler: onItemClick
                
            },'-',{
            	id:'Logout',
                text: i18n.getMsg('menu.me.logout'),
               	iconCls: 'calendar',
               	icon:'/images/logout.png',
               	handler: onItemClick
            }
        ]
    });
    
    
   function onItemClick(item){
        //Ext.example.msg('Menu Click', 'You clicked the "{0}" menu item.', item.text);
        if(item.id == 'clients')
        	url = '/Clients4.html';
        else if(item.id == 'sn')
        	url = '/StorageNodes4.html';
        else if(item.id == 'bs')
        	url = '/Tasks.html';
        else if(item.id == 'w')
        	url = '/Welcome.html';
        else if(item.id == 'bp')
        	url = '/TimeLine4.html';
        else if(item.id == 'bh')
        	url = '/BackupHistory.html';
        else if(item.id == 'logout')
        	url = '/Default.aspx?action=logout';
        else if(item.id == 'hubConf')
        	url = '/HubConf4.html';
        else if(item.id == 'hubLogs')
        	url = '/HubLogs.html';
        else if(item.id == 'addBs')
        	url = '/AddBackupSet4.html';
        else if(item.id == 'restore')
        	url = '/Restore.html';
        else if(item.id == 'Users')
        	url = '/Users.html';
        else if(item.id == 'Stats')
        	url = '/GlobalStats.html';
        else if(item.id == 'Logout'){
        	url = '/Default.aspx?action=logout';
        	window.location.href = url;
        }
        var urlParam = Ext.urlDecode(window.location.search.substring(1));
		var language;
	    if(urlParam.lang)
	        language = urlParam.lang;
        window.location.href = url+"?lang="+language;
    }
    
    
   var makeSearchTypes = new Ext.data.ArrayStore({
        fields: ['stype'],
        data : ['Nodes','Backup sets','Storage nodes', 'IP']
    });
    
    
	var tb = new Ext.Toolbar({
	renderTo: Ext.get('menu'),
	floating:false,
	enableOverflow:false,
	layout:'hbox',
	//border:2,
 	items: [
	{
		text: i18n.getMsg('menu.tb.view'), //'&nbsp;&nbsp;View',
		iconCls: 'icon-chart',
		icon:'/images/view.png',
		//layout:'vbox',
		layout:'fit',
		menu:viewMenu 
	}, '-',
	{
		text: i18n.getMsg('menu.tb.add'), //'&nbsp;&nbsp;Add',
		iconCls: 'icon-chart',
		icon:'/images/add.png',
		layout:'fit',
		menu:addMenu
	}, '-',
	{	text: '<b>'+i18n.getMsg('menu.tb.restore')+'</b>', //'&nbsp;&nbsp;<b>Restore</b>',
		iconCls: 'icon-chart',
		icon:'/images/restore.png',
		id: 'restore',
		handler: onItemClick
	}, '-',
	{
		text: i18n.getMsg('menu.tb.system'), //'&nbsp;&nbsp;System',
		iconCls: 'icon-chart',
		icon:'/images/system.png',
		menu:sysMenu 
	}, '-',
	/*{
	text: 'Toggle Button',
	iconCls: 'icon-table',
	enableToggle: true,
	toggleHandler: toggleHandler,
	pressed: true
	}, '->',*/
	{
		text: i18n.getMsg('menu.tb.search'), //'&nbsp;&nbsp;Search :',
		iconCls: 'icon-chart',
		icon:'/images/search.png',
	}, '',
	{
		xtype: 'combo',
		store: makeSearchTypes,
		displayField: 'stype',
		typeAhead: true,
		mode: 'local',
		triggerAction: 'all',
		emptyText: 'enter any search',
		selectOnFocus: true,
		width: 135
	}, '-',
	{
		text: i18n.getMsg('menu.tb.me'), //'&nbsp;&nbsp;Me',
		iconCls: 'icon-chart',
		icon:'/images/me.png',
		menu:meMenu 
	}
	]
	});

	tb.doLayout();

    
    
function clickHandler(btn) {
Ext.Msg.alert('clickHandler', 'button pressed');
}
function toggleHandler(item, pressed) {
Ext.Msg.alert('toggleHandler', 'toggle pressed');
};

//viewMenu.show();

Ext.state.Manager.setProvider(new Ext.state.CookieProvider({
    expires: new Date(new Date().getTime()+(1000*60*60*24*60)), //2 months from now
}));

/* Generic functions */
/*function FormatSize(val){
  	if(val == null || val == '' || isNaN(val)) return '';
	if(val >= 1024 * 1024 *1024 * 1024)
		return Math.round(val/1024/1024/1024/1024) +' TB';
	else if (val > 1024 * 1024 *1024)
		return Math.round(val/1024/1024/1024) +' GB';
	else 
		return Math.round(val/1024/1024) +' MB';
  }*/
  FormatSize = function(val){
	if(val == null || val == '' || isNaN(val)) return '';
	if(val >= 1024 * 1024 *1024 * 1024)
		return Math.round(val/1024/1024/1024/1024) +' TB';
	else if (val > 1024 * 1024 *1024)
		return Math.round(val/1024/1024/1024) +' GB';
	else 
		return Math.round(val/1024/1024) +' MB';
  };

  

});
});