
Ext.Loader.setConfig({
    enabled: true
});
Ext.Loader.setPath('Ext.ux', '/ext4/ux');

Ext.require([
 	'Ext.data.proxy.Rest',
    'Ext.data.*',
    'Ext.grid.*',
    'Ext.tree.*',
    'Ext.form.*',
	]);
	
Ext.onReady(function () {
	var params = Ext.urlDecode(window.location.search.substring(1));
	var lang;
    if(params.lang)
        lang = params.lang;
	i18n = new Ext.i18n.Bundle({
        bundle:'wui', 
        path:'i18n',
        lang:lang
    });
	i18nTask = new Ext.i18n.Bundle({
        bundle:'taskmsg', 
        path:'i18n',
        lang:lang
    });

i18n.onReady(function(){
//i18nTask.onReady(function(){
 Ext.get('globalStatsTitle').dom.innerText = i18n.getMsg('globalStats.title');
 
});

});