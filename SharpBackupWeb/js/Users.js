
Ext.require([
    'Ext.data.*',
    'Ext.grid.*',
    'Ext.tree.*',
    'Ext.form.*',
    'Ext.chart.*',
    'Ext.ux.*',
]);


Ext.onReady(function () {
Ext.Loader.setConfig({enabled:true});
	var params = Ext.urlDecode(window.location.search.substring(1));
	var lang;
    if(params.lang)
        lang = params.lang;
	//var i18n = new Ext.i18n.Bundle({bundle:'wui', path:'i18n', lang:lang});
	var i18n = Ext.create('Ext.i18n.Bundle',{
		bundle: 'wui',
		lang: lang,
		path: 'i18n',
		noCache: true
	});
i18n.onReady(function(){
	var colorsList = null;
	Ext.get('usersTitle').dom.innerText = i18n.getMsg('users.title');
	
    Ext.define('User', {
        extend: 'Ext.data.Model',
        fields: [
           // {name: 'id',     type: 'string'},
            {name: 'name',     type: 'string'},
            {name: 'password', type: 'string'},
            {name: 'email', type: 'string'},
            {name: 'culture', type: 'string'},
            {name: 'language', type: 'string'},
            {name: 'enabled', type: 'boolean'},
            {name: 'lastlogin', type: 'date', defaultValue:'Never'},
            //{name: 'iconCls', type: 'string'},
        ]
    });
	
	
    var usersStore = new Ext.data.Store( {
    	autoLoad:true,
        model: 'User',
        proxy: {
            type: 'ajax',
            url: '/Get.aspx?w=Users'
        },
        reader:{
        	type:'json',
        	//root:'Users',
        },
    });

	Ext.define('Cultures', {
        extend: 'Ext.data.Model',
        fields: ['code', 'name']
    });

    var cultureStore = Ext.create('Ext.data.Store', {
        model: 'Cultures',
        proxy: {
            type: 'ajax',
            url: '/Get.aspx?w=Cultures'
        },
    });
    cultureStore.load();
    cultureStore.sort([
    {property: 'name',  direction: 'ASC'},
	]);

	
    var usersGrid = new Ext.grid.Panel( {
    /*title: i18n.getMsg('users.grid.title'),*/ //'Users',
    id:'usersGrid',
    store: usersStore,
    columns: [
        {
        	header: i18n.getMsg('users.grid.name'), //'Name',  
        	dataIndex: 'name', 
        	xtype:'gridcolumn', 
            field:{
                xtype:'textfield',
                allowBlank:false
            },
            width:160,
            height:25,
            renderer:function(value){
            	return '<span class="u-admin">'+value+'</span>';
            }
        },
        {
			header: "Password",
			dataIndex: 'password',
			width: 100,
			field: new Ext.form.TextField({
				inputType: 'password',
				allowBlank: false
			}),
			renderer: function() {
				return '******'
			}
		},
        {
        	header: i18n.getMsg('users.grid.email'), //'Email', 
        	dataIndex: 'email', 
        	flex:0, 
            field:{
                xtype:'textfield',
                allowBlank:false
            },
            width:210,
        },
        {
        	header: i18n.getMsg('users.grid.language'), //'Language', 
        	dataIndex: 'culture', 
        	flex:0, 
            field:{
                xtype:'combo',
                allowBlank:false,
                store: cultureStore,
                valueField:'code',
                displayField:'name',
                queryMode:'remote',
                typeAhead: true,
                //typeAheadDelay: 150,
                lazyRender: false,
            },
            width:185,
            renderer: function(value, metaData, record, colIndex, store, view){
            	/*if (record.dirty) {
            		alert("dirty");
            		record.commit();
            		usersGrid.getView().refresh();
            	}*/
            	
            	//return "<img src='/i18n/flags/"+value.substring(3).toLowerCase()+".png'/>&nbsp;"+record.get('language');
            	return "<img src='/i18n/flags/"+value.substring(3).toLowerCase()+".png'/>&nbsp;"+cultureStore.findRecord('code',value).get('name');
            },
            
        },
        {
        	header: i18n.getMsg('users.grid.enabled'), //'Active', 
        	dataIndex: 'enabled', flex:0, 
            field:{
                xtype:'checkbox',
            },
            width:60
        },
        {
        	//xtype:'datecolumn',
        	header: i18n.getMsg('users.grid.lastLogin'), //'Name',  
        	dataIndex: 'lastlogin',
        	width:240,
        	renderer:function(value){
        		if(value == null)
        			return "<i>"+i18n.getMsg('generic.never')+"</i>";
        		else
        			return value;
        	}
        },
        {
        	header: i18n.getMsg('users.grid.roles'),
        	dataIndex: '', 
        	flex:1, 
            /*field: 	{
	       		xtype:'fieldset',
	       		//layout:'fit',
	       		width:130,
	       		items:[
	       		{
	       			xtype:'boxselect',
	       			resizable: false,
	       			editable:true,
					name: 'to[]',
					//anchor:'100%',
					width:130,
					store: cultureStore,
					mode: 'local',
					displayField: 'name',
					displayFieldTpl: '{code} ({name})',
					valueField: 'code',
					
					value: ['fr-FR', 'en-US']
					//value: 'AL, NY, MN'
	       		
	       		}
	       		
	       		
	       		]
	       	}*/
       	}
    ],
    selType: 'cellmodel',
    plugins: [
        Ext.create('Ext.grid.plugin.CellEditing', {
            clicksToEdit: 1,
            listeners:{
            	afteredit:function(e){
            		
            		usersGrid.getView().refresh();
            		return false;
            	}
            },
        })
    ],
    bbar: [{
            text: 'Add user',
            icon:'/images/adduser.png',
            handler : function(){
                var r = Ext.ModelManager.create({
                    name: 'New user',
                    culture: 'en-US',
                    
                }, 'User');
                usersStore.insert(0, r);
                cellEditing.startEditByPosition({row: 0, column: 0});
            }
        }],
    height: 500,
    layout:'fit',
    renderTo: Ext.get('panel')
});

function afterEdit(e){
            		alert(value);
            		e.record.set('language', value);
            		e.record.commit();
            		e.grid.update();
            	}
/*usersGrid.on('edit', function(e){
	//alert(e.fi);
            		//e.record.set('language', e.value);
            		e.record.commit();
            		e.grid.update();

});*/
/*usersGrid.getSelectionModel().on("rowdeselect", function(selModel, rowIndex, record) {
    if (record.dirty) {
        // record has been modified
        alert(record.get("language"));
    }
});*/
    
});

});