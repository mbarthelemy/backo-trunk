Ext.require([
    'Ext.data.*',
    'Ext.grid.*',
    'Ext.tree.*',
    'Ext.form.*',
    'Ext.chart.*',
]);


Ext.onReady(function () {
	Ext.Loader.setConfig({enabled:true});
	var params = Ext.urlDecode(window.location.search.substring(1));
	var lang;
    if(params.lang)
        lang = params.lang;
	/*var i18n = new Ext.i18n.Bundle({
        bundle:'wui', 
        path:'i18n',
        lang:lang
    });*/
    var i18n = Ext.create('Ext.i18n.Bundle',{
		bundle: 'wui',
		lang: lang,
		path: 'i18n',
		noCache: true
	});
	
i18n.onReady(function(){
	Ext.QuickTips.init();
	Ext.tip.QuickTipManager.init();
	Ext.apply(Ext.tip.QuickTipManager.getQuickTip(), {
    	showDelay: 500,      // Show 50ms after entering target
    	hideDelay: 1000,
    	dismissDelay: 30000,
    	autoHide:true,
    	
		closable:false,
	});
	
	
	
	var fieldz=[];
	fieldz.push('client');
	for(i=0;i<24;i++){
		fieldz.push("0_"+i+":00");
		fieldz.push("0_"+i+":30");
		fieldz.push("1_"+i+":00");
		fieldz.push("1_"+i+":30");
	}
	fieldz.push('d');
	fieldz.push('type');
	
	Ext.define('BasePath', {
        extend: 'Ext.data.Model',
        fields: [
        	{name: 'path',     		type: 'string'},
        ]
    });
    
	Ext.define('TaskSet', {
        extend: 'Ext.data.Model',
        fields: [
        	{name: 'id',     		type: 'string'},
        	{name: 'name',     		type: 'string'},
        	{name: 'operation',     type: 'string'},
        	{name: 'level',     	type: 'string'},
        	{name: 'compress',     	type: 'string'},
        	{name: 'encrypt',     	type: 'string'},
        	{name: 'clientdedup',   type: 'string'},
        	{name: 'begin',   type: 'string'},
        	{name: 'end',   type: 'string'},
        ],
        hasMany:{model:'BasePath', name:'BasePath', associationKey:'BasePath'}
    });
    
	Ext.define('BPM', {
        extend: 'Ext.data.Model',
        fields: fieldz,
        hasMany:{model:'TaskSet', name:'TaskSet', associationKey:'TaskSet'}
    });
    
	var schedStore = new Ext.data.JsonStore({
		model:'BPM',
		autoLoad: true,
		proxy: {
	        type: 'ajax',
	        url:'/Get.aspx?w=BackupPlan',
	        
	        //root: 'backupSets',
    	},
	});
	var interval = 12;
	
	var columns = []; 
	var d = new Date();
	//adapt grid size
	var gridWidth = document.getElementById('panel').offsetWidth; //ok with Chrome & Firefox
	var hoursWidth = gridWidth-170 ;// substract 'node' column
	//var nbDisplayedHours = 12;
	
	function showTaskDetails(record){
		alert('prout');
		/*var tip = Ext.create('Ext.tip.QuickTip', {
			id:'ttp_t_hlp',
			//target: 'help_toolip',
			bubbleEvents:['add','remove','show','hide'],
			html: record.get('name'),
			width: 200,
			height: 125,
			closable:true,
			bodyStyle: {
				    background: '#00cc00',
					'font-size':'16px',
					color:'#FFF',
	    			padding: '10px'
				},
			anchor:'bottom',
			dismissDelay: 15000, // Hide after 10 seconds hover
			autoHide:false,
		});
	
		tip.show();*/
	}
	
	function GenerateColumns(nbDisplayedHours){
		columns = []
		var halfHourWidth = Math.round(hoursWidth/(nbDisplayedHours*2));
		// by default, we display a 12 hours timeline
		for(i=d.getHours()-1; i<d.getHours()+(nbDisplayedHours-1);i++){
		for(j=0;j<2;j++){ // resolution is : every 1/2h. Does it really make sense to be more precise here?
			var colId = '';
			var colTime="";
			var colText="";
			if(i<0){
				colId = "0_"+(i+24);
				colTime = "0_"+(i+24);
			}
			else if(i == 0){
				colId = "1_0";
				colTime = "1_0";
			}
			else if(i<24){
				colId = "0_"+i;
				colTime = "0_"+i;
			}
			else{
				colId = "1_"+(i-24);
				colTime = "1_"+(i-24);
			}
			if(j==0) colTime = colTime+":00";
			else {
				colTime = colTime+":30";
			}
			var colText = '';
			//if(j==0)
				colText = ""+colTime;
			//else
			//	colText = "";
			colText = colText.substring(2);
			if(d.getHours() == i && j==0 && d.getMinutes() < 30)
				colText = "<b>"+colText+"</b>";
			if(d.getHours() == i && j==1 && d.getMinutes() >= 30)
				colText = "<b>"+colText+"</b>";
				
			var this_column = new Ext.grid.column.Column({
				name:colId,
				//id:colId,
				sortable:false,
				resizable:false, 
				groupable:true,
				menuDisabled:true,
				unlockable:false,
				text:colText+'&nbsp;', // put empty content, else column won't show
				dataIndex:colTime,
				height:19,
				margins:0,
				padding:0,
				flex:0,
				width:halfHourWidth,
				locked:true,
				renderer: function(value, metaData, record, rowIndex, colIndex, store) {
					
					if( (value != null && value.length <= 0) )
						return '<div class="cell">&nbsp;</div>';
					//alert(colIndex);
					metaData.tdAttr += ' margins:-2px !important; padding:-2px !important;';
					var cellClass="";
					record.TaskSet().each(function(taskSet) {
						metaData.tdAttr += ' data-qtip="<center><b><i>'+taskSet.get('name')+'</i></b></center><hr><br/><table>' ;
						metaData.tdAttr += '<tr><td><b>Id</b>:</td><td>#'+taskSet.get('id')+'</td></tr>';
						
						metaData.tdAttr += '<tr><td><b>Level</b>:</td><td>'+taskSet.get('level')+'</td></tr>';
						metaData.tdAttr += '<tr><td><b>Begin</b>:</td><td>'+taskSet.get('begin')+'</td></tr>';
						metaData.tdAttr += '<tr><td><b>Max. end:&nbsp;</b></td><td>'+taskSet.get('end')+'</td></tr>';
						metaData.tdAttr += '<tr><td><b>Options:&nbsp;</b></td><td>';
						if(taskSet.get('compress') == 'True')
            				metaData.tdAttr += '<img src=\'/images/compress.png\' title=\'compress\'/>&nbsp;';
            			if(taskSet.get('encrypt') == 'True')
            				metaData.tdAttr += '<img src=\'/images/encrypt.png\' title=\'encrypt\'/>&nbsp;';
            			if(taskSet.get('clientdedup') == 'True')
            				metaData.tdAttr += '<img src=\'/images/clientdedup.png\' title=\'client deduplication\'/>&nbsp;';
            			metaData.tdAttr += '</td><tr/><tr><td><b>Items: </b></td><td><ul style=\'list-style-type: square;\'>';
            			taskSet.BasePath().each(function(basePath) {
            				metaData.tdAttr += '<li><i>'+basePath.get('path')+'</i></li>';
            			});
            			metaData.tdAttr += '</ul></td></tr>';
            			metaData.tdAttr += '</table>" ';
					
					
					if(taskSet.get('operation') == "Backup"){
						if(record.get('type') == 'Full')
		                	cellClass += " bFull";
		                else if(record.get('type') == 'Differential')
		                	cellClass += " bDiff";	
		                else if(record.get('type') == 'Incremental')
		                	cellClass += " bIncr";	
		                //if (value.substr(0,value.indexOf(':')) == 'b'){
		                	
		                //}
	                }
	                else if(taskSet.get('operation') == "HouseKeeping"){
	                	if(record.get('type') == 'Full')
		                	cellClass += " bSpecialFull";
	                	else if(record.get('type') == 'Differential')
	                		cellClass += " bSpecialDiff";
	                	else if(record.get('type') == 'Incremental')
	                		cellClass += " bSpecialIncr";
	                }
	                if(record.get('d') != '' && record.get('d') > 0){
                		metaData.tdAttr += ' colspan="'+(record.get('d')*2)+'"';
                		cellClass += " cell_bb";
                	}
                	else{
                		cellClass += " cell_bu";
                		metaData.tdAttr += ' colspan="4"';
                	}
		               /* else if (value.substr(0,value.indexOf(':')) == 'e')
		                    return '<div class="cell_be">&nbsp;</div>';
		                else  if (value.substr(0,value.indexOf(':')) == 'r')
		                	return '<div class="cell_b">&nbsp;</div>';*/
		           });  
		           return '<div class="'+cellClass+'" onclick="">&nbsp;'+value.substr(value.indexOf(':')+1,value.length)+'</div>';
		           
		          }
	        });
			columns.push(this_column); 
		}	
		}
		
		var head_col = new Ext.grid.column.Column({
		//id:0,
		text:i18n.getMsg('nodestree.node'),
		flex:0,
		width:170,
		sortable:true,
		resizable:true,
		locked:true,
		groupable:true,
		menuDisabled:true,
		dataIndex:'client',
		height:19,
		renderer:function(value, metaData, record, rowIndex, colIndex, store) {
			metaData.style +='border-right:1px solid lightgrey;';
			if(value.substr(value.indexOf(':')+1,value.length-value.indexOf(':')) == 'on'){
				return '<div style="padding-left:15px;" class="task-on">&nbsp;'+value.substr(0, value.indexOf(':'))+'</div>';
			}
			else{ /*(value.substr(value.indexOf(':')+1,value.length-value.indexOf(':')) == 'off'){*/
				return '<div style="padding-left:15px;" class="task-off">&nbsp;'+value.substr(0, value.indexOf(':'))+'</div>';
			}
			/*else{
				return 'pup='+value.substr(value.indexOf(':')+1,value.length-value.indexOf(':'))+',tit='+value;
			}*/
		}
	});
	columns.unshift(head_col);
	} // enf GenerateColumns
	GenerateColumns(12);
	
	
	
	var combo = new Ext.form.field.ComboBox({
	  name : 'perpage',
	  width: 40,
	  store: new Ext.data.ArrayStore({
	    fields: ['id'],
	    data  : [
	      ['15'],
	      ['20'],
	      ['30'],
	      ['40'],
	    ]
	  }),
	  mode : 'local',
	  value: '20',
	  listWidth     : 40,
	  triggerAction : 'all',
	  displayField  : 'id',
	  valueField    : 'id',
	  editable      : false,
	  forceSelection: true
	});

	var bbar = new Ext.toolbar.Paging({
	  store: schedStore,
	  displayInfo: true,
	  items   :    [
	    '-',
	    i18n.getMsg('generic.perPage'),
	    combo,
	    '-',
	    {
    		xtype:'displayfield',
    		value:i18n.getMsg('runningTasks.groupBy'),
    	},
	    {
    		xtype:'combo',
    		id:'groupBy',
    		mode:           'local',
            value:          'none',
            triggerAction:  'all',
            forceSelection: true,
            allowBlank:true,
            editable:       false,
            fieldLabel:     '',
            displayField:   'value',
            valueField:     'name',
            queryMode: 'local',
            width:150,
            store:          Ext.create('Ext.data.Store', {
              fields : ['name', 'value'],
                data   : [
                    {name : 'client',   value: 'client node'},
                    {name : '', value: 'don\'t group'},
                ]
            }),
            listeners:{
            	'change': function( thisCombo, newValue, oldValue, options ){
            		schedStore.group(newValue);
            	}
            }
    	},
	  ],
      toggleHandler: function(btn, pressed){
        var view = grid.getView();
        view.showPreview = pressed;
        view.refresh();
       }
	});

	combo.on('select', function(combo, record) {
	  bbar.pageSize = parseInt(record.get('id'), 10);
	  bbar.doLoad(bbar.cursor);
	}, this);

	var groupingFeature = Ext.create('Ext.grid.feature.Grouping',{
        groupHeaderTpl: '{name} ({rows.length} Item{[values.rows.length > 1 ? "s" : ""]})',
        depthToIndent:15,
  	});
  
  	
  	
	var bPlanGrid = new Ext.grid.Panel({
	    id:'bPlanGrid', 
	    store: schedStore,
	    renderTo:Ext.get('panel'),
	    width: gridWidth,
	    height: 550,
	    frame: false,
	    enableCtxMenu: true, 
	    enableColumnResize:false,
	    enableHdMenu:false,
	    autoShow:false,
	    viewConfig: {
        	loadMask: false
    	},
	    features: [groupingFeature],
	    groupField:'client',
	    columns:columns,
		tbar:{
			height:27,
			items:[
			  {
                text:' ',
                //iconCls:'icon-prev',
                icon:'/images/prev.png',
                width:45,
                height:22,
                listeners:{
                     scope:this
                    //,click:{fn:this.addRecord,buffer:200}
               	}
               },
               {
                text:'',
                width:230,
                disabled:true
               },
               {
               	text:'Period :',
               	disabled:true,
               	locked:true,
               	align:'center'
               },
              
              {
		    		xtype:'combo',
		    		id:'period',
		    		mode:           'local',
		            value:          '12',
		            triggerAction:  'all',
		            forceSelection: true,
		            allowBlank:true,
		            editable:       false,
		            fieldLabel:     '',
		            displayField:   'value',
		            valueField:     'name',
		            queryMode: 'local',
		            width:150,
		            store:          Ext.create('Ext.data.Store', {
		              fields : ['name', 'value'],
		                data   : [
		                    {name : '4',   value: '4 hours'},
		                    {name : '8', value: '8 hours'},
		                    {name : '12', value: '12 hours', selected:true},
		                    {name : '24', value: '24 hours'},
		                    {name : 'w', value: 'week'},
		                ]
		            }),
		            listeners:{
		            	'change': function( thisCombo, newValue, oldValue, options ){
		            		GenerateColumns(newValue);
		            		if(newValue != null){
		            			interval = newValue;
                				schedStore.load({params:{interval:interval}});
                				var grid = Ext.getCmp('bPlanGrid');
                				grid.reconfigure(schedStore, columns);
                			}
		            	}
		            }
		    	},
               {
                text:'',
                width:240,
                disabled:true
               },
               {
                text:' ',
                //iconCls:'icon-next',
                icon:'/images/next.png',
                width:45,
                align:'right',
                listeners:{
                     scope:this
                    //,click:{fn:this.addRecord,buffer:200}
               }
            }
        ]
        },
	    bbar: bbar,
	    //title: 'Backups to be scheduled within next hours',
	});
	
	//automatic refresh
 	setInterval(function(){
 		schedStore.load({params:{interval:interval}});
 		GenerateColumns(interval);
 		bPlanGrid.reconfigure(schedStore, columns);
 		},60000);
});
});