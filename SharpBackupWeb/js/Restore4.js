/*Ext.Loader.setConfig({
    enabled: true
});
Ext.Loader.setPath('Ext.ux', '../Extensible');*/

Ext.onReady(function () {
	Ext.Loader.setConfig({
        enabled: true,
        disableCaching: false,
        paths: {
            'Extensible': '/Extensible/src',
           /* 'Extensible.example': '/Extensible/examples'*/
        }
    });
    Ext.require([
	 	'Ext.data.proxy.Rest',
	    'Ext.data.*',
	    'Ext.grid.*',
	    'Ext.tree.*',
	    'Ext.form.*',
	    'Ext.window.*',
	    'Extensible.calendar.data.MemoryCalendarStore',
	    'Extensible.calendar.data.EventModel',
	    'Extensible.calendar.data.EventStore',
	    'Extensible.calendar.CalendarPanel',
	    'Extensible.calendar.data.*',
	    'Extensible.calendar.*'
	]);
	
	var params = Ext.urlDecode(window.location.search.substring(1));
	var lang;
    if(params.lang)
        lang = params.lang;
    var i18n = Ext.create('Ext.i18n.Bundle',{
		bundle: 'wui',
		lang: lang,
		path: 'i18n',
		noCache: true
	});
	/*var i18n = new Ext.i18n.Bundle({
        bundle:'wui', 
        path:'i18n',
        lang:lang
    });*/


i18n.onReady(function(){
 Ext.get('restoreTitle').dom.innerText = i18n.getMsg('restore.title');
 var restoreNode = null;
 var restoreDestNode = null;
 var restoreBS = '';
 function GetRestoreBS(){
 	return restoreBS;
 }
 
 Ext.define('NodeM', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'id',     type: 'string'},
        {name: 'userName',     type: 'string'},
        {name: 'ip', type: 'string'},
        {name: 'share', type: 'string'},
        {name: 'available', type: 'string'},
        {name: 'percent', type: 'string'},
        {name: 'version', type: 'string'},
        {name: 'quota', type: 'string'},
        {name: 'usedquota', type: 'string'},
        {name: 'port', type: 'string'},
        {name: 'backupsets', type: 'string'},
        {name: 'certCN', type: 'string'},
    ]
  });
  var ajaxNodesProxy = new Ext.data.proxy.Ajax({
    	model:'NodeM',
        type: 'ajax',
        url: '/Get.aspx?w=Clients'
  });
	            	
  var nStore = new Ext.data.TreeStore( {
        model: 'NodeM',
        proxy: ajaxNodesProxy,
  });
 
 var ajaxDNodesProxy = new Ext.data.proxy.Ajax({
    	model:'NodeM',
        type: 'ajax',
        url: '/Get.aspx?w=Clients&online=true'
  });
	            	
  var dnStore = new Ext.data.TreeStore( {
        model: 'NodeM',
        proxy: ajaxDNodesProxy,
  });
 var clientNodesTree = new Ext.tree.Panel({
        //title: ''+i18n.getMsg('restore.step1.title'), //Select a node',
        id:'clientNodesTree',
        height:350,
        width:475,
        collapsible: false,
        useArrows: true,
        useLines:true,
        rootVisible: false,
        store: nStore,
        multiSelect: false,
        singleExpand: true,
        draggable:false,     
        padding:'10px 20px 0px 10px',  
        //margins:'0px 0px 0px 0px',  
        columns: [
        	{
	            xtype: 'treecolumn', //this is so we know which column will show the tree
	            text: i18n.getMsg('nodestree.node'), //'Node',
	            flex: 1,
	            sortable: true,
	            dataIndex: 'userName',
	            renderer: function(value, metaData, record, colIndex, store, view){
		            if(record.get('certCN').length > 1)
		            	return value+" (<i>"+record.get('certCN')+"</i>)";
		            else
		            	return value;
	            }
	        },{
	            text: i18n.getMsg('nodestree.currentIP'), //'IP',
	            flex: 0,
	            width:110,
	            sortable: true,
	            dataIndex: 'ip',
	        },{
	            text: i18n.getMsg('nodestree.version'), //'Version',
	            flex: 0,
	            width:55,
	            dataIndex: 'version',
	            sortable: true
	        },{
	            text: i18n.getMsg('nodestree.backupsets'), //'Backupsets',
	            flex: 0,
	            dataIndex: 'backupsets',
	            width:70,
	            sortable: true
	        }
        ],
        listeners:{
        	'checkchange': function(node, checked){        	
		       	if (Ext.getCmp('clientNodesTree').getChecked().length == 1 && checked){
		       		restoreNode = node.data['id'];
		       		var firstLevelChild = clientNodesTree.getRootNode().childNodes;
		       		Ext.each(firstLevelChild, function(child, index){
		       			if(child != null){
			       			if((child.hasChildNodes() && !child.contains(node))){
			       				child.remove(false);
			       			}
			       			else{
			       				var childNodes = [].concat(child.childNodes);
								Ext.each(childNodes, function(leafChild){
				       				if (leafChild != null && !leafChild.isRoot() && leafChild.isLeaf() && (leafChild.id != node.id)) {
		        							leafChild.remove(false);
		        					}
			       				});
			       			}
			       		}
        			});
        			destNodesTree.enable();
        			var destNodes = Ext.getCmp('destNodesTree').getRootNode().childNodes;
        			Ext.each(destNodes, function(child, index){
		       			if(child.hasChildNodes()){
		       				var childNodes = [].concat(child.childNodes);
							Ext.each(childNodes, function(leafChild){
			       				if (leafChild != null && !leafChild.isRoot() && leafChild.isLeaf() && (leafChild.data['id'] == restoreNode)) {
	        							//leafChild.data['checked'] = true;
	        							//leafChild.checked = true;
	        							leafChild.parentNode.expand();
	        					}
		       				});
		       			}
		       			else{
		       				if (child != null && !child.isRoot() && child.isLeaf() && (child.data['id'] == restoreNode)) {
	        							//child.data['checked'] = true;
	        							child.parentNode.expand();
	        				}
		       			}
        			});
        			
		       	}
		       	else{
		       		clientNodesTree.getStore().load(); //setProxy(ajaxNodesProxy);
		       	}
		     }
        }
  });
  
  var destNodesTree = new Ext.tree.Panel({
  		//title: ''+i18n.getMsg('restore.step1.title'),
        id:'destNodesTree',
        height: 350,
        width:475,
        collapsible: false,
        useArrows: true,
        useLines:true,
        rootVisible: false,
        store: dnStore,
        multiSelect: false,
        singleExpand: false,
        draggable:false,  
        disabled:true,
        //padding:'0px 0px 0px 0px',  
        style:'margin-top:15px;'   ,
        //margins:{top:'100px'}, 
        columns: [
        	{
	            xtype: 'treecolumn', //this is so we know which column will show the tree
	            text: i18n.getMsg('nodestree.node'), //'Node',
	            flex: 1,
	            sortable: true,
	            dataIndex: 'userName',
	            renderer: function(value, metaData, record, colIndex, store, view){
		            if(record.get('certCN').length > 1)
		            	return value+" (<i>"+record.get('certCN')+"</i>)";
		            else
		            	return value;
	            }
	        },{
	            text: i18n.getMsg('nodestree.currentIP'), //'IP',
	            flex: 0,
	            width:110,
	            sortable: true,
	            dataIndex: 'ip',
	        },{
	            text: i18n.getMsg('nodestree.version'), //'Version',
	            flex: 0,
	            width:55,
	            dataIndex: 'version',
	            sortable: true
	        },{
	            text: i18n.getMsg('nodestree.backupsets'), //'Backupsets',
	            flex: 0,
	            dataIndex: 'backupsets',
	            width:70,
	            sortable: true
	        },
        ],
        listeners:{
        	'checkchange': function(node, checked){        	
		       	if (Ext.getCmp('destNodesTree').getChecked().length == 1 && checked){
		       		restoreDestNode = node.data['id'];
		       		/*var firstLevelChild = destNodesTree.getRootNode().childNodes;
		       		Ext.each(firstLevelChild, function(child, index){
		       			if((!child.contains(node)) && child.hasChildNodes()){
		       				child.remove(false);
		       			}
		       			else{
		       				var childNodes = [].concat(child.childNodes);
							Ext.each(childNodes, function(leafChild){
			       				if (leafChild != null && !leafChild.isRoot() && leafChild.isLeaf() && (leafChild.id != node.id)) {
	        							leafChild.remove(false);
	        					}
		       				});
		       			}
        			});*/
        			bsStore.load({
		  				params:{
		  					nodeId:restoreNode,
		  				}	
		       		});
        			restoreTypePanel.enable();
		       		restoreTypePanel.expand();
		       	}
		       	else{
		       		destNodesTree.getStore().load(); //setProxy(ajaxNodesProxy);
		       	}
		     }
        }
  });  
  var nodesTreesFieldSet = new Ext.form.Panel({
            id:'nodesTreesFieldSet',
            title: '<img src="/images/1.png" class="gIcon"/>'+i18n.getMsg('restore.step1.title'), //Select a node',
             layout: {
	            type: 'table',
	            columns: 2
	        },
            defaultType: 'textfield',
            border: true,
            hidden:false,
            height:365,
            fieldDefaults: {
                labelAlign: 'left',
                hideLabel: true
            },
            defaults:{
            	padding:0,
            	margins:0,
            },
            items: [clientNodesTree, destNodesTree]
 });		
 	                  
 Ext.define('BasePath', {
		    extend: 'Ext.data.Model',
		    fields: [
		        {name: 'id', type: 'string'},
		        {name: 'name', type: 'string'},
				//{name: 'iconCls', type: 'string'},
				{name: 'path', type: 'string'},
				{name: 'includerule', type: 'string'},
				{name: 'excluderule', type: 'string'},
		    ],
		    //belongsTo:'BackupSet'
		});
		
		var bsStore = new Ext.data.TreeStore({
			autoLoad:false,
		    model: 'BasePath',
		    proxy:{
		    	type: 'ajax',
		        url : '/Get.aspx?w=Backupsets',
		        reader: {
		            type: 'json',
		            root: 'children'
		        }
		    },
		});
		
		var bsHistoryStore = new Extensible.calendar.data.EventStore({
	        autoLoad: false,
	        remoteFilter:true,
	        proxy: {
	            type: 'rest',
	            url: '/Get.aspx?w=BackupHistory',
	            noCache: false,
	            reader: {
	                type: 'json',
	                root: 'data'
	            },
	        },
	        listeners:{
	        	beforeload:function(){
	        		Ext.Msg.show({
	        				title:'Information',
	        				msg:i18n.getMsg('restore.step3.waitMsg'),
	        				buttons:false,
	        				icon:'icon-loading',
	        			});
	        	}	
	        }
	    });
	    
	    
		var bSetTree = new Ext.tree.Panel( {
			id:'bSetTree',
	        store: bsStore,
	        autoRender:true,
	        hideHeaders: false,
	        rootVisible: false,
	        useArrows: true,
	        //renderTo: restoreTypeDetails'),
	        collapsible: false,
	        multiSelect: false,
	        singleExpand: false,
	        overlapHeader:true,
	        scroll:false,
	        height:300,
			width:400,
			lines:false,
			frame:false,
			padding:0,
			bodyPadding:'0px 0px 0px 0px',
			border:false,
	        root : { 
			    id : 'src',
			    text : "", 
			    expanded : true,
			},
			listeners:{
	        	'checkchange': function(node, checked){        	
			       	if (checked){
			       		restoreBS = node.data['id'];
			       		var firstLevelChild = bSetTree.getRootNode().childNodes;
			       		Ext.each(firstLevelChild, function(child, index){
			       			if((!child.contains(node)) && !child.hasChildNodes()){
			       				child.data['checked']=false;;
			       			}
			       			else{
			       				var childNodes = [].concat(child.childNodes);
								Ext.each(childNodes, function(leafChild){
				       				if (leafChild != null && !leafChild.isRoot() && leafChild.isLeaf() && (leafChild.data['id'] != node.data['id'])) {
		        							child.data['checked']=false;
		        					}
			       				});
			       			}
	        			});
	        			Ext.Msg.show({
	        				title:'Information',
	        				msg:i18n.getMsg('restore.step3.waitMsg'),
	        				buttons:false,
	        				icon:'icon-loading',
	        			});
			       		bsHistoryStore.load({
			  				params:{
			  					startDate:''+Ext.Date.format(Ext.getCmp('restoreDate').getActiveView().getViewBounds().start, 'Y-m-d'),
			  					endDate: ''+Ext.Date.format(Ext.getCmp('restoreDate').getActiveView().getViewBounds().end, 'Y-m-d'),
			  					bsId:restoreBS,
			  				}	
			       		});
			       		Ext.Msg.close();
			       	}
			       	else{
			       		//clientNodesTree.getStore().load(); //setProxy(ajaxNodesProxy);
			       	}
			     }
        	},
	        columns: [
	        {
	            xtype: 'treecolumn', 
	            text: i18n.getMsg('restore.step2.backupset'), //'path',
	           	flex: 1,
	           	//width:130,
	            sortable: true,
	            dataIndex: 'name',
	            checked:false,
	            renderer: function(value, metaData, record, colIndex, store, view){
					if(record.get('leaf') != null && record.get('leaf') == true){
						return ''; //record.get('path');
						//return bsStore.getNodeById(record.get('id')).data['path'];
					}
					else
						return value;
					//if(value != null)
					//	
	            }
	        },
	        {
	            text: i18n.getMsg('restore.step2.path'), //'path',
	           	flex: 1,
	           	//width:150,
	            sortable: true,
	            dataIndex: 'path',
	            checked:false
	        },
	       /* {
	            text: i18n.getMsg('restore.step2.includeRule'), //'path',
	           	flex: 0,
	            sortable: true,
	            dataIndex: 'includerule',
	            checked:false
	        },
	        {
	            text: i18n.getMsg('restore.step2.excludeRule'), //'path',
	           	flex: 0,
	            sortable: true,
	            dataIndex: 'excluderule',
	            checked:false
	            //iconCls:iconCls
	        },*/
	    ]
	});
	
//Extensible.calendar.data.EventModel.reconfigure(); 
   								
 var restoreTypePanel = new Ext.widget('form', {
        id:'restoreTypePanel',
        title:'<img src="/images/2.png" class="gIcon"/>'+i18n.getMsg('restore.step2.title'), //Choose what to restore',
        monitorValid:true,
        border: false,
        bodyPadding: 5,
		width:'620',
		height:'120',
		disabled:true,
        fieldDefaults: {
            labelAlign: 'left',
        },
        defaults: {
            margins: '0 0 0 0',
            padding: '0px 0px 5px 0px',
        },
        items: [
        		{
                	xtype: 'fieldcontainer',
                	fieldLabel: i18n.getMsg('restore.step2.type'),
                	layout:'hbox',
                	height:40,
                	border:true,
                	items:[
	                    {
				            xtype: 'radiogroup',
				            hidden:false,
				            width:650,
				            height:30,
				            border:true,
				            frame:true,
				            layout:'hbox',
				            items: [
				                {
					                boxLabel: '<img style="vertical-align:middle;border:0;" src="/images/bs.png"/>&nbsp;'+i18n.getMsg('restore.step2.wholeBs'),
					                name: 'rt',
					                inputValue: '0',
					                width:250,
					            }, {
					                boxLabel: '<img style="vertical-align:middle;border:0;" height="18px" src="/images/browse.png"/>&nbsp;'+i18n.getMsg('restore.step2.customPath'),
					                name: 'rt',
					                inputValue: '1',
					                width:350,
					            }
							 ],
							 listeners:{
		                    	change:function(thiscombo, newValue, oldValue, options){
		                    		if(newValue['rt'] == 0){
		                    			Ext.getCmp('restoreTypeDetails').setTitle(i18n.getMsg('restore.step2.chooseBsTitle'));
		                    			Ext.getCmp('restoreTypeDetails').setVisible(true);
		                    			var bSetProxy = new Ext.data.Proxy({
									        type: 'ajax',
									        url : '/Get.aspx?w=Backupsets&nodeId='+restoreNode,
									        reader: {
									            type: 'json',
									            root: 'children'
									        }
									    });
		                    			bSetTree.getStore().setProxy(bSetProxy);
		                    			bSetTree.getStore().load();
										Ext.getCmp('restoreDate').render();
	                    			}
	                    		}
	                    	},
			             }, // end radiogroup
			           ],
                    	}, // end 1st fieldset
		                {
		                    xtype: 'fieldset',
		                    id:'restoreTypeDetails',
		                     layout: {
					            type: 'table',
					            columns: 2
					        },
		                    defaultType: 'textfield',
		                    border: true,
		                    hidden:true,
		                    height:330,
							width:860,
							padding:'0px 0px 5px 0px',
							margins:0,
		                    fieldDefaults: {
		                        labelAlign: 'left',
		                        hideLabel: true
		                    },
		                    defaults:{
		                    	padding:0,
		                    	margins:0,
		                    },
		                    items: [
		                   	bSetTree,
		                    new Extensible.calendar.CalendarPanel( {
						        id:'restoreDate',
						        eventStore:bsHistoryStore,
						        autoRender:true,
						        title: '', //<img src="/images/3.png" class="gIcon"/>Choose a version',
						        height:300,
						        width:450,
						        padding:'0px 0px 0px 5px',
						        showDayView:false,
						        showWeekView :true,
						        showHeader:true,
						        weekViewCfg:{
						        	ddIncrement:60,
						        	scrollStartHour:0,
						        	hideBorders:false,
						        	showHeader:true,
						        	showTime:false,
						        	showTodayText:false,
						        },
						        showMonthView :true,
						        monthViewCfg:{
						        	moreText:'{0} backups',
						        	defaultEventTitleText:'',
						        	showHeader:true,
	        						showWeekLinks:true,
						        },
						        showMultiWeekView:false,
						        showNavJump:false, 
						        enableEditDetails:false,
						        showWeekLinks:true,
						        weekText:i18n.getMsg('generic.week'),
			        			monthText:i18n.getMsg('generic.month'),
						        todayText:Ext.Date.format(new Date(), 'M Y'),
						        //todayText:Ext.Date.format(this.activeView.getViewBounds().start, 'M Y'),
						        listeners:{
						        	datechange:function(thisCal, startDate, viewStart, viewEnd){
						        		Ext.Msg.show({
										     title:'Information',
										     id:'searchRestoreWaitMsg',
										     msg: i18n.getMsg('restore.step3.waitMsg'), //'Searching backups which match your request... <br/> This could be a long operation.',
										     buttons: false,
										     icon: 'icon-loading',
										     renderTo:Ext.getBody(),
										});
										bsHistoryStore.load({
							  				params:{
							  					startDate:''+Ext.Date.format(viewStart, 'Y-m-d'),
							  					endDate: ''+Ext.Date.format(viewEnd, 'Y-m-d'),
							  					bsId:restoreBS,
							  				}	
							       		});
								    },
								    eventsrendered:function(){
								    	Ext.Msg.close();
								    },
								    eventclick:function(thisPanel, eventModelRec, htmlNode){
								    	var backupTimeSelected = document.getElementById(htmlNode.id).innerText;
								    	var backupIdSelected = eventModelRec.data['ID'];
								    	document.getElementById(htmlNode.id).style.background = 'green';
								    	document.getElementById(htmlNode.id).style.fontWeight = 'bold';
								    	document.getElementById(htmlNode.id).innerHTML = '<b>'+backupTimeSelected+'</b>(<i>#'+backupIdSelected+'</i>)';
								    	//alert('choose '+eventModelRec.data['startDate']+', id='+eventModelRec.data['EventId']);
								    	//Ext.getCmp('restoreDate').disable();
								    	restoreOptionsPanel.enable();
								    	restoreOptionsPanel.expand();
								    	return false;
								    }
								}
							}),
		                    ], // end restoreTypeDetails fields
                		},
        				]
 });
 
 var destStore = new Ext.data.TreeStore( {
        model:'NodeM',
        proxy:{
            type:'ajax',
            url:'/Get.aspx?w=Clients&online=true'
        },
        folderSort: true
 });
 
 function setRestoreLocation(val){
 	Ext.getCmp('restoreLocation').setValue(val);
 
 }
 
 var restoreOptionsPanel = new Ext.widget('form', {
        id:'restoreDestPanel',
        title:'<img src="/images/3.png" class="gIcon"/>'+i18n.getMsg('restore.step4.title'), //Restored data destination',
        monitorValid:true,
        border: true,
        bodyPadding: 5,
		width:580,
		height:280,
		disabled:true,
        fieldDefaults: {labelAlign:'left', labelWidth:120,},
        items:[
        	{
				xtype:'fieldset',
				layout:'hbox',
				border:true,
				title:i18n.getMsg('restore.step4.title1'), //'Select the destination path for restored data :',
				items:[
					{
		        		xtype:'radiogroup',
			            width:380,
			            height:30,
			            border:false,
			            frame:false,
			            layout:'hbox',
			            items: [
			                {
				                boxLabel:i18n.getMsg('restore.step4.originalLocation'),
				                inputValue: '0',
				                name:'restoreLoc',
				                width:200,
				            }, {
				                boxLabel:i18n.getMsg('restore.step4.customLocation'),
				                inputValue: '1',
				                name:'restoreLoc',
				                width:150,
				            }
						 ],
						 listeners:{
		                	change:function(thiscombo, newValue, oldValue, options){
		                		if(newValue['restoreLoc'] == 1){
		                			Ext.getCmp('restoreLocation').enable();
		                			Ext.getCmp('restoreLocationBrowseBtn').enable();
		                		}
		                		else{
		                			Ext.getCmp('restoreLocation').disable();
		                			Ext.getCmp('restoreLocationBrowseBtn').disable();
		                		}
		            		}
		            	},
		             }, // end radiogroup
		             {
		             	xtype:'textfield',
		             	id:'restoreLocation',
		             	width:250,
		             	disabled:true
		             
		             },
		             {
		             	xtype:'button',
		             	id:'restoreLocationBrowseBtn',
		             	icon:'/images/browse.png',
		             	width:25,
		             	disabled:true,
		             	handler:function(){
		             		var destPath = (handleBrowse(restoreDestNode, setRestoreLocation, false));
		             		
		             	}
		             },
        		]
        	},
            {
				xtype:'fieldset',
				//layout:'vbox',
				border:true,
				title:i18n.getMsg('restore.step4.title2'), 
				items:[
					{
		             	xtype:'checkbox',
		             	id:'overWrite',
		             	boxLabel:i18n.getMsg('restore.step4.overwrite'),
		            },
					{
		             	xtype:'checkbox',
		             	id:'restorePermissions',
		             	boxLabel:i18n.getMsg('restore.step4.permissions'),
		            },
				]
			}
        ],
        buttons:[
        	{
     			id:'restoreBtn',
     			text:i18n.getMsg('restore.step4.launch'), //'Restore!',
     			icon:'/images/restore.png',
     			handler:function(){
     			
     			}
        	}
        ]
 });
 
 var viewport = new Ext.Panel({
    	renderTo: Ext.get("panel"),
        layout: 'accordion',
        layoutConfig:{animate:true},
        height:550,
        maxHeight:550,
	    layoutConfig: {
	        titleCollapse: false,
	        animate: true,
	        activeOnTop: false,
	        multi:true,
	        fill:false,
	    },
        items: [nodesTreesFieldSet,	restoreTypePanel, restoreOptionsPanel]
 });


});
});