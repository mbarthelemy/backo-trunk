function handleBrowse(nodeId, callBackFn, allowMultiple){
    var params = Ext.urlDecode(window.location.search.substring(1));
	var lang;
    if(params.lang)  lang = params.lang;
	var i18n = new Ext.i18n.Bundle({bundle:'wui', path:'i18n', lang:lang});


    Ext.define('NTreeModel', {
        extend: 'Ext.data.Model',        
        fields: [
            {name: 'n', type: 'string', mapping:'@n'},
            //{name: 'text', type: 'string', mapping:'@n'},
          	{name: 'checked', type: 'string'},
          	{name: 'iconCls', type: 'string', mapping:'@type'},
          	{name: 'size', type: 'numeric', mapping:'@size'},
          	{name: 'avail', type: 'numeric', mapping:'@avail'},
          	{name: 'label', type: 'string', mapping:'@label'},
          	{name: 'fs', type: 'string', mapping:'@fs'},
          	{name: 'snapshottable', type: 'string', mapping:'@snap'},
        ]
    });
    
    Ext.define('VolumeModel', {
        extend: 'Ext.data.Model',        
        fields: [
            {name: 'n', type: 'string', mapping:'@n'},
            //{name: 'text', type: 'string', mapping:'@n'},
          	{name: 'checked', type: 'string'},
          	{name: 'iconCls', type: 'string', mapping:'@type'},
          	{name: 'size', type: 'numeric', mapping:'@size'},
          	{name: 'avail', type: 'numeric', mapping:'@avail'},
          	{name: 'label', type: 'string', mapping:'@label'},
          	{name: 'fs', type: 'string', mapping:'@fs'},
          	{name: 'snapshottable', type: 'string', mapping:'@snap'},
          	{name: 'leaf', type: 'boolean', defaultValue:true},
        ]
    });
    
    Ext.define('SPOModel', {
        extend: 'Ext.data.Model',        
        fields: [
          	{name: 'name', type: 'string', mapping:'name'},
          	{name: 'n', type: 'string', mapping:'name'},
          	{name: 'path', type: 'string', mapping:'path'},
            {name: 'type', type: 'string', mapping:'type'},
          	{name: 'version', type: 'string', mapping:'version'},
          	//{name: 'checked', type: 'string'},
          	{name: 'iconCls', type: 'string', mapping:'type'},
          	{name: 'icon', type: 'string'},
          	{name: 'leaf', type: 'boolean', mapping:'leaf', defaultValue:false},
          	{name: 'child', type: 'boolean', defaultValue:false},
          	{name: 'disabled', type: 'string'},
        ],
        /*associations:[
		{type:'hasMany', model:'SPOModel1',name:'childObject'},
		]*/
    });
    
     Ext.define('VM', {
        extend: 'Ext.data.Model',          
        fields: [
            {name: 'name', type: 'string'},
            {name: 'type', type: 'string'},
          	{name: 'file', type: 'string'},
          	{name: 'leaf', type: 'boolean', defaultValue:true},
          	{name: 'checked', type: 'boolean'},
          	{name: 'iconCls', type: 'string', mapping:'type', defaultValue:"computer"}
        ],
    });
    
 i18n.onReady(function(){    
     var brwStore = new Ext.data.TreeStore( {
     	model:'NTreeModel',
     	clearOnLoad:false,
     	autoLoad:false,
        proxy: {
            type: 'ajax',
            url: '/Get.aspx?w=Browse&curNode='+nodeId+'&path=/',
            timeout:60000,
            extraParams: {isXml: true},
            reader: {
                type: 'xml',
                root: 'root',
                record: 'd',
               	//idProperty: '@n',
            }
        },
       root: {text: '/', id: '//', value:'/', expanded: true}
    });
    
    var volStore = new Ext.data.TreeStore( {
     	model:'VolumeModel',
     	clearOnLoad:false,
     	autoLoad:false,
        proxy: {
            type: 'ajax',
            url: '/Get.aspx?w=Drives&nodeId='+nodeId,
            extraParams: {isXml: true},
            reader: {
                type: 'xml',
                root: 'root',
                record: 'd',
               	//idProperty: '@n',
            }
        },
       root: {text: '/', id: '//', value:'/', expanded: true}
    });
    
    var spoStore = new Ext.data.TreeStore( {
     	model:'SPOModel',
     	clearOnLoad:false,
     	autoLoad:false,
        proxy: {
            type: 'ajax',
            url: '/Get.aspx?w=SpecialObjects&nodeId='+nodeId,
            timeout:90000,
            doRequest: function(operation, callback, scope) {
                var writer  = this.getWriter(),
                    request = this.buildRequest(operation, callback, scope);

                if (operation.allowWrite()) {
                    request = writer.write(request);
                }

                Ext.apply(request, {
                    headers       : this.headers,
                    timeout       : 90000,
                    scope         : this,
                    callback      : this.createRequestCallback(request, operation, callback, scope),
                    method        : this.getMethod(request),
                    disableCaching: false // explicitly set it to false, ServerProxy handles caching
                });

                /*
                * do anything needed with the request object
                */
                console.log('request', request);
                console.log('request.params', request.params);

                Ext.Ajax.request(request);

                return request;
            },
            root: {
	            text: '/',
	            id: '//',
	            value:'/',
	            name:'VSS Writers',
	            expanded: true,
	            checked:false,
	        }
        },
       root: {
            text: '/',
            id: '//',
            value:'/',
            name:'VSS Writers',
            expanded: true,
            checked:false,
        }
       
    });
    
    var vmStore = new Ext.data.TreeStore( {
     	model:'VM',
     	clearOnLoad:false,
     	autoLoad:false,
        proxy: {
            type: 'ajax',
            url: '/Get.aspx?w=VM&nodeId='+nodeId,
        },
       root: {
            text: '/',
            id: '//',
            value:'/',
            name:'Virtual machines',
            expanded: true,
        }
    });
    
	var spoTree = new Ext.tree.Panel({
		id:'spoTree',
		title:i18n.getMsg('browser.tabs.Objects'),
		store:spoStore,
		hideHeaders: false,
        rootVisible: true,
        useArrows: true,
		collapsible: false,
        multiSelect: allowMultiple,
        singleExpand: false,
        overlapHeader:true,
        autoScroll:false,
        layout:'fit',
		frame:true,
		height:430,
		padding:0,
		bodyPadding:'0px 10px 0px 0px',
		border:0,
		columns: [
	         {
	            xtype: 'treecolumn', 
	            text: i18n.getMsg('browser.path'), //'path',
	           	flex: 2,
	            sortable: true,
	            dataIndex: 'path',
	            checked:false,
	            renderer: function(value, metaData, record, colIndex, store, view){
	            	if(record.get('icon') != ""){
	            		if(record.get('disabled') == "True")
		            		return "<strike>"+value+"</strike>";
	            	}
		            else if(record.get('disabled') == "True"){
		            	return "<strike>"+value+"</strike>";
		            	//metadata['disabled'] = true;
		            	metadata['iconCls'] = "spoDisabled";
		            	metadata['cls'] = "disabled";
		            }
		            else{
		            	if(record.get('path') == record.get('name'))
		            		return value;
		            	else
		            		return record.get('path')+'\\'+record.get('name');
		            }
	            }
	        },
	        {
	            text: i18n.getMsg('nodestree.version'), //'size',
	           	flex: 0,
	            sortable: true,
	            dataIndex: 'version',
	            width:70,
	        }
	    ],
	    listeners:{
	    	checkchange: function(node, checked, options){
	         	var hasTocheck = false;
	         	if(checked)
	         		hasTocheck = true;
	       		var firstLevelChild = node.childNodes;
	       		Ext.each(firstLevelChild, function(child, index){
	       			if(child != null){
		       			child.set('checked', hasTocheck);
		       		}
    			});
	         }
        }    
	    	
	});
	
	var vmTree = new Ext.tree.Panel({
		id:'vmTree',
		title:i18n.getMsg('browser.tabs.Virtual'),
		store:vmStore,
		hideHeaders: false,
        rootVisible: true,
        useArrows: true,
		collapsible: false,
        multiSelect: allowMultiple,
        singleExpand: false,
        overlapHeader:true,
        autoScroll:false,
        layout:'fit',
		frame:true,
		height:430,
		padding:0,
		bodyPadding:'0px 10px 0px 0px',
		border:0,
		columns: [
	         {
	            xtype: 'treecolumn', 
	            text: i18n.getMsg('browser.path'), //'path',
	           	flex: 0,
	           	width:230,
	            sortable: true,
	            dataIndex: 'name',
	            checked:false,
	            renderer: function(value, metaData, record, colIndex, store, view){
		            if(record.get('type').length <1){
		            	metaData['iconCls'] = "computer";
		            	return "<strong>"+value+"</strong>";
		            	
		            }
		            else if(record.get('type') == "cdrom"){
		            	metaData['iconCls'] = "cdrom";
		            	return value;
		            }
		            else
		            	return value;
	            }
	        },
	        {
	            text: i18n.getMsg('browser.label'), //'size',
	           	flex: 1,
	            sortable: true,
	            dataIndex: 'file',
	            width:250,
	        },
	       
	    ],
	    listeners:{
	    	checkchange: function(node, checked, options){
	         	var hasTocheck = false;
	         	if(checked)
	         		hasTocheck = true;
	       		var firstLevelChild = node.childNodes;
	       		Ext.each(firstLevelChild, function(child, index){
	       			if(child != null){
		       			child.set('checked', hasToCheck);
		       		}
    			});
	         }
        }    
	    	
	});
	
	var volumesTree = new Ext.tree.Panel({
		id:'volumesTree',
		title:i18n.getMsg('browser.tabs.Volumes'),
		store:volStore,
		hideHeaders: false,
        rootVisible: false,
        useArrows: true,
		collapsible: false,
        multiSelect: allowMultiple,
        singleExpand: false,
        overlapHeader:true,
        autoScroll:false,
        layout:'fit',
		frame:true,
		height:360,
		padding:0,
		bodyPadding:'0px 10px 0px 0px',
		border:0,
		columns: [
	         {
	            xtype: 'treecolumn', //this is so we know which column will show the tree
	            text: i18n.getMsg('browser.path'), //'path',
	           	flex: 2,
	            sortable: true,
	            dataIndex: 'n',
	            checked:false
	        },
	        {
	            text: i18n.getMsg('browser.label'), //'label',
	           	flex: 2,
	            sortable: true,
	            dataIndex: 'label',
	            checked:false
	        },
	        {
	            text: i18n.getMsg('browser.fs'), //'fs',
	           	flex: 0,
	           	width:40,
	            sortable: true,
	            dataIndex: 'fs',
	            checked:false
	        },
	        {
	            text: i18n.getMsg('browser.snapshot'), //'size',
	           	flex: 0,
	            sortable: true,
	            dataIndex: 'snapshottable',
	            width:60,
	            renderer: function(value){
	            	if(value == 'NONE') // no snapshot capability
	            		return '<i>'+i18n.getMsg('generic.no')+'</i>';
	            	else
	            		return value;
	            }
	        },
	        {
	            text: i18n.getMsg('browser.size'), //'size',
	           	flex: 0,
	            sortable: true,
	            dataIndex: 'size',
	            width:60,
	            renderer: function(value){
	            	if (value == null)
	            		return "";
	            	if (value > 1024*1024*1024) {
			            return Math.round(value/1024/1024/1024)+" GB";
			        }
			        else if (value > 1024*1024) {
			            return Math.round(value/1024/1024)+" MB";
			        }
			        else if (value > 1024) {
			            return Math.round(value/1024)+" KB";
			        }
			        else 
			        	return value + '';
			    },
	            checked:false
	        },
	    ]
	});
	var afTree = new Ext.tree.Panel( {
		id:'afTree',
		title:i18n.getMsg('browser.tabs.FS'),
        store: brwStore,
        hideHeaders: false,
        rootVisible: true,
        useArrows: true,
        collapsible: false,
        multiSelect: allowMultiple,
        singleExpand: false,
        overlapHeader:true,
        autoScroll:false,
        layout:'fit',
		frame:true,
		height:430,
		padding:0,
		bodyPadding:'0px 10px 0px 0px',
		border:0,
        columns: [
	         {
	            xtype: 'treecolumn', //this is so we know which column will show the tree
	            text: i18n.getMsg('browser.path'), //'path',
	           	flex: 2,
	            sortable: true,
	            dataIndex: 'n',
	            checked:false
	        },
	        {
	            text: i18n.getMsg('browser.size'), //'size',
	           	flex: 0,
	            sortable: true,
	            dataIndex: 'size',
	            width:60,
	            renderer: function(value){
	            	if (value == null)
	            		return "";
	            	if (value > 1024*1024*1024) {
			            return Math.round(value/1024/1024/1024)+" GB";
			        }
			        else if (value > 1024*1024) {
			            return Math.round(value/1024/1024)+" MB";
			        }
			        else if (value > 1024) {
			            return Math.round(value/1024)+" KB";
			        }
			        else 
			        	return value + '';
			    },
	            checked:false
	        },
	        {
	            text: i18n.getMsg('browser.available'), //'available',
	           	flex: 0,
	            sortable: true,
	            dataIndex: 'avail',
	            width:60,
	            renderer: function(value){
	            	if (value == null)
	            		return "";
	            	if (value > 1024*1024*1024) {
			            return Math.round(value/1024/1024/1024)+" GB";
			        }
			        else if (value > 1024*1024) {
			            return Math.round(value/1024/1024)+" MB";
			        }
			        else if (value > 1024) {
			            return Math.round(value/1024)+" KB";
			        }
			        else 
			        	return value + '';
			    },
	            checked:false
	        },
	        {
	            text: i18n.getMsg('browser.fs'), //'fs',
	           	flex: 0,
	           	width:40,
	            sortable: true,
	            dataIndex: 'fs',
	            checked:false
	        },
	        {
	        	//xtype:'checkbox',
	            text: i18n.getMsg('browser.snapshot'), //'size',
	           	flex: 0,
	            sortable: true,
	            dataIndex: 'snapshottable',
	            width:60,
	            renderer: function(value){
	            	if(value == 'NONE') // no snapshot capability
	            		return '<i>'+i18n.getMsg('generic.no')+'</i>';
	            	else
	            		return value;
	            }
	        },
	        /*{
	            text: i18n.getMsg('browser.label'), //'label',
	           	flex: 2,
	            sortable: true,
	            dataIndex: 'label',
	            checked:false
	        },*/
        
        ],
        listeners:{
	        beforeitemexpand:function(view, record, item, index, event) {
	    		var currentId = "";
	    		var fullPath = [];
	    		var curNode = item;
	    		for(var i=0; i< item.getDepth(); i++){
	    			curNode = curNode.parentNode;
	    			fullPath.unshift(curNode.get('n'));
	    		}
	    		fullPath.push(item.get('n'));
	            afTree.getStore().setProxy( new Ext.data.proxy.Ajax({
	            	model:'NTreeModel',
		            type: 'ajax',
		            url: '/Get.aspx?w=Browse&curNode='+nodeId+'&path='+fullPath.join('/'),
		            extraParams: {
		                isXml: true
		            },
		            reader: {
		                type: 'xml',
		                root: 'root',
		                record: 'd',
		            }
	            }));
	            if(item.get('checked') == true){
		            Ext.each(item, function(child, index){
		       			if(child != null){
			       			//child.checked = hasTocheck;
			       			//child.data['checked'] = hasTocheck;
			       			child.set('checked', true);
			       		}
	    			});
	            }
	         },
	         checkchange: function(node, checked, options){
	         	var hasTocheck = false;
	         	if(checked)
	         		hasTocheck = true;
	       		var firstLevelChild = node.childNodes;
	       		Ext.each(firstLevelChild, function(child, index){
	       			if(child != null){
		       			child.set('checked', hasTocheck);
		       		}
    			});
	         
	         },
	         itemappend: function(thisNode, insertedNode, index, options ){
	         	if(thisNode.get('checked') == true)
		            insertedNode.set('checked', true);
	         }
        }    
    });
    
	var winBrowse = new Ext.Window({ 
 		id:'winBrowse',
        width:650,
       	height:520,
        plain: true,
        title:i18n.getMsg('browser.title'), //"Browse node",
		autoScroll:true,
        modal:true,
        autoDestroy :true,
        monitorValid:true,
        resizable:true,
        BodyStyle:'overflow-y:auto !important; overflow-x:auto !important;',
		items: [
			new Ext.tab.Panel(
			{
			    activeTab: 0,
			    items:[
			    	afTree,
			    	volumesTree,
			    	vmTree,
			    	spoTree
			    ] , 
			})
		],
        buttons: [{
                text:i18n.getMsg('generic.ok'),
                disabled:false,
                handler:function(){
                	//var checkedNode = afTree.getChecked()[0];
                	var valz = new Array();
                	Ext.each(afTree.getChecked(), function(checkedNode){
	                	var depth = checkedNode.getDepth();
	                	var fullPath = [];
	                	var value = "";
	                	if(checkedNode.get('n').length > 1){
			    			value = checkedNode.get('n');
			    			fullPath.push(value);
				    		for(var i=0; i< depth; i++){
				    			checkedNode = checkedNode.parentNode;
				    			fullPath.unshift(checkedNode.get('n'));
				    		}
		                	valz.push(fullPath.join('/'));
		                }
			    		
			    		
                	});
                	Ext.each(spoTree.getChecked(), function(checkedNode){
                		var depth = checkedNode.getDepth();
	                	var fullPath = [];
	                	var value = "";
	                	if(checkedNode.get('path').length > 1)
			    			value = checkedNode.get('path');
			    		//else
			    		fullPath.push(value);
			    		//value = checkedNode.get('name');
		    			//fullPath.push(value);
			    		for(var i=0; i< depth-1; i++){ // depth -1 because we don't want root node to get VSS path
			    			checkedNode = checkedNode.parentNode;
			    			//if(checkedNode.get('path').length > 1)
				    			value = checkedNode.get('path');
				    		//else
				    			//value += "\\"+checkedNode.get('name');
			    			fullPath.unshift(value);
			    		}
	                	valz.push(fullPath.join('\\'));
		               // }
                		/*if (checkedNode.get('name').length > 1)
			    			valz.push(checkedNode.get('name'));
			    		else*/
			    			
                	});
                	Ext.each(volumesTree.getChecked(), function(checkedNode){
		                valz.push(checkedNode.get('label'));
                	});
                	winBrowse.close();
                	callBackFn(valz);
                }
            },{
                text: i18n.getMsg('generic.cancel'),
                handler: function(){
                    winBrowse.close();
                    return null;
                }
            }
        ] 
     });
	winBrowse.show();
	afTree.render();
});
}
 