Ext.Loader.setConfig({
    enabled: true
});
 
 var nodesChecked = [];
 var pathsChecked = [];
 var toBeExpanded = true;
 
 Ext.require([
    'Ext.data.*',
    'Ext.grid.*',
    'Ext.tree.*',
    'Ext.form.*',
    'Ext.window.*',
    'Ext.fx.target.Sprite',
	]);
	

  function ManageLockStatus(areLocked){
  	var checkedNodes = Ext.getCmp('clientNodesTree').getChecked();
	var checkedList = '';
	Ext.each(checkedNodes, function (nodes){
		checkedList += nodes.internalId+",";
	});
	var conn = new Ext.data.Connection(); 
    conn.request({ 
            url: 'Set.aspx?w=ApproveNodes&approve='+areLocked, 
            method: 'POST', 
            scope: this, 
            params: checkedList, 
            //success: function(responseObject){ Ext.Msg.alert('Status', responseObject.responseText);  }, 
            failure: function(responseObject) { 
                 Ext.Msg.alert('Status', 'Unable to save changes. Error:'+responseObject.responseText); 
            } 
    }); 
    Ext.getCmp('clientNodesTree').getStore().load();
  }
  
  function setTempPathFromBrowser(value){
  	Ext.getCmp('Backups.TempFolder').setValue(value);
  }
  function setIndexesPathFromBrowser(value){
  	Ext.getCmp('Backups.IndexFolder').setValue(value);
  }
  function setStorageFolderFromBrowser(value){
  	Ext.getCmp('Storage.Directory').setValue(value);
  }
 Ext.onReady(function () {
  	Ext.Loader.setConfig({enabled:true});
	var params = Ext.urlDecode(window.location.search.substring(1));
	var lang;
    if(params.lang)
        lang = params.lang;
   
	/*var i18n = new Ext.i18n.Bundle({
        bundle:'wui', 
        path:'i18n',
        lang:lang
    });*/
    var i18n = Ext.create('Ext.i18n.Bundle',{
		bundle: 'wui',
		lang: lang,
		path: 'i18n',
		noCache: true
	});
	

i18n.onReady(function(){
 Ext.define('NodeM', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'id',     		type: 'string'},
        {name: 'userName',     	type: 'string'},
        {name: 'certCN',     	type: 'string'},
        {name: 'ip', 			type: 'string'},
        {name: 'share', 		type: 'number'},
        {name: 'available', 	type: 'number'},
        {name: 'percent', 		type: 'string'},
        {name: 'version', 		type: 'string'},
        {name: 'os', 			type: 'string'},
        {name: 'quota', 		type: 'number'},
        {name: 'usedquota', 	type: 'number'},
        {name: 'port', 			type: 'string'},
        {name: 'backupsets', 	type: 'string'},
        {name: 'group', 		type: 'number'},
        {name: 'storagegroup', 	type: 'number'},
        {name: 'certificateStatus', type: 'string'},
        {name: 'description', type: 'string'},
        {name: 'status', type: 'string'},
        {name: 'lastconnection', type: 'datetime'},
    ]
 });

 var nStore = new Ext.data.TreeStore( {
    model: 'NodeM',
    proxy: {
        type: 'ajax',
        url: '/Get.aspx?w=Clients'
    },
    folderSort: true
 });
    
  Ext.define('NG', {
        extend: 'Ext.data.Model',
        fields: [/*'id', 'name', 'description'*/
        	 {name: 'id',     type: 'string'},
        	 {name: 'name',     type: 'string'},
        	 {name: 'description',     type: 'string'},
        ]
    });

    var ngStore = Ext.create('Ext.data.JsonStore', {
    	autoLoad:false,
    	storeId:'ngStore',
        model: 'NG',
        proxy: {
            type: 'ajax',
            url: '/Get.aspx?w=NodeGroups'
        },
    });
    ngStore.load();
    
    
 function GetConfigWindow(){
 	
/*i18n.onReady(function(){ */
 	// for nox we only allow to configure 1 node at a time.
 	var checkedNodes = Ext.getCmp('clientNodesTree').getChecked();
	var checkedId = null;
	Ext.each(checkedNodes, function (nodes){
		checkedId = nodes.internalId;
	});
 
	Ext.define('SG', {
        extend: 'Ext.data.Model',
        fields: ['userName', 'id']
    });

    var sgStore = Ext.create('Ext.data.Store', {
    	storeId:'sgStore',
    	autoLoad:false,
        model: 'SG',
        proxy: {
            type: 'ajax',
            url: '/Get.aspx?w=StorageNodes&groupsonly=true'
        },
        listeners:{
        	load:function(){
        		//Ext.getCmp('Storage.StorageGroup').setValue(Ext.getCmp('clientNodesTree').getStore().getNodeById(checkedId).get('storagegroup'));
        	}
        }
    });
    sgStore.load();
    
   
    
	var form = new Ext.widget('form', {
                /*layout: {
                    type: 'vbox',
                    //type:'accordion',
                    align: 'stretch'
                },*/
                id:'configFormPanel',
                monitorValid:true,
                border: false,
                autoScroll:true,
                bodyPadding: 10,
				height:'460',
                fieldDefaults: {
                    labelAlign: 'left',
                    labelWidth: 100,
                },
                defaults: {
                    padding:{bottom:'0px', top:'5px', left:'10px'},
                    margins:{bottom:'0px'}
                },
                items: [
               	{
                    xtype: 'fieldset',
                    title: i18n.getMsg('nodeconf.useTemplate'), //'Use template(s)',
                    //layout: 'hbox',
                    defaultType: 'textfield',
                    border: true,
                    collapsible:true,
                    fieldDefaults: {
                        labelAlign: 'left',
                        hideLabel: true
                    },
                    defaults:{
                    	 anchor: '100%',
                    },
                    items: [
                    	{
                    	xtype: 'fieldcontainer',
                    	layout:'hbox',
                    	items:[
                    		{
                				xtype:'displayfield',
                				value:i18n.getMsg('nodeconf.selectTemplate'), //'Log level',
                				width:115
                			},
                    		{
                                xtype:          'combo',
                                mode:           'local',
                                value:          '<none>',
                                triggerAction:  'all',
                                forceSelection: true,
                                allowBlank:true,
                                editable:       false,
                                fieldLabel:     '',
                                id:           'template1',
                                displayField:   'value',
                                valueField:     'name',
                                queryMode: 'local',
                                width:210,
                                store:          Ext.create('Ext.data.Store', {
                                  fields : ['name', 'value'],
                                    data   : [
                                        {name : '0',   value: 'none'},
                                        {name : '1',  value: 'linux'},
                                        {name : '2', value: 'windows'}
                                    ]
                                })
                            },
                            {
                                xtype:          'combo',
                                mode:           'local',
                                value:          '<none>',
                                triggerAction:  'all',
                                forceSelection: true,
                                editable:       false,
                                fieldLabel:     '',
                                id:           'template2',
                                displayField:   'value',
                                valueField:     'name',
                                queryMode: 'local',
                                width:210,
                                margins: {left: 15},
                                store:          Ext.create('Ext.data.Store', {
                                  fields : ['name', 'value'],
                                    data   : [
                                        {name : '0',   value: '<none>'},
                                        {name : '1',  value: 'linux'},
                                        {name : '2', value: 'windows'}
                                    ]
                                })
                            },
                           	]// end template                            
                           },
                           ]
                	}, 
                	{
	                    xtype: 'fieldset',
	                    title: i18n.getMsg('nodeconf.generalConf'), //'General configuration',
	                    //labelStyle: 'font-weight:bold;padding:0',
	                    defaultType: 'textfield',
	                    border: true,
	                    collapsible:true,
						anchor: '100%',
	                    fieldDefaults: {
	                        labelAlign: 'left',
	                        labelWidth:100,
	                        hideLabel: true
	                    },
	                    items: [
	                    	{
				                    	xtype: 'fieldcontainer',
				                    	layout: 'hbox',
				                    	defaultMargins: {top: 0, right: 15, bottom: 0, left: 15},
				                    	defaults: {
						                    flex: 0,
						                    hideLabel: true,
						                },
				                    	items:[
				                    		{
			                    				xtype:'displayfield',
			                    				value:i18n.getMsg('nodeconf.group'), 
			                    				width:115
			                    			},
				                    		{
										    	xtype:'combo',
								                align:'left',
								                id:'nodeGroup',
								                store:ngStore,
								                valueField:'id',
								                displayField:'name',
								                typeAhead: false,
								                allowBlank:true,
						                        blankText:'A value is required.',
								                queryMode:'remote',
								                forceSelection:true,
								                selectOnFocus:true,
								                triggerAction: 'all',
								                width:152,
								            },    
				                    		{
			                    				xtype:'displayfield',
			                    				value:i18n.getMsg('nodeconf.logLevel'), //'Log level',
			                    				width:115,
			                    				margins: {left: 15},
			                    			},
				                    		{
					                    		xtype:'combo',
								                align:'left',
								                id:'Logger.Level',
								                store: new Ext.data.ArrayStore({
								                    fields: ['bType'],
								                    data : [['DEBUG2'],['DEBUG'],['INFO'],['WARNING'], ['ERROR']]
								                }),
								                valueField:'bType',
								                displayField:'bType',
								                typeAhead: true,
								                allowBlank:false,
								                mode: 'local',
								                triggerAction: 'all',
								                value:'INFO',
								                width:120,
										      },
				                    	]
				                },
		                    	{
				                    	xtype: 'fieldcontainer',
				                    	layout: 'hbox',
				                    	defaultMargins: {top: 0, right: 15, bottom: 0, left: 15},
				                    	defaults: {
						                    flex: 0,
						                    hideLabel: true,
						                    //anchor:'100%'
						                },
				                    	items:[		
				                    		{
			                    				xtype:'displayfield',
			                    				value:i18n.getMsg('nodeconf.logFile'), //'Log file',
			                    				width:115,
			                    				align:'left',
			                    			},
				                    		{
			                    				xtype:'textfield',
						                        //fieldLabel:'Log file',
						                        id: 'Logger.LogFile',
						                        align:'left',
						                        width:152,
						                        allowBlank:false,
						                        blankText:'A value is required.'
			                    			},                    	
										      {
			                    				xtype:'displayfield',
			                    				value:i18n.getMsg('nodeconf.syslog'), //'Log to syslog',
			                    				width:115,
			                    				margins: {left: 16},
			                    				},
										      {
							                        xtype:'checkbox',
							                        id: 'Logger.Syslog',
							                        width:45,
							                        align:'left'
						                       },
			                    		
			                    	]
	                    		},
								
	                    	]               
                	}, // end field set general conf
                	{
	                    xtype: 'fieldset',
	                    id:'backupContainer',
	                    title: i18n.getMsg('nodeconf.backupConf'), //'Storage configuration',
	                    defaultType: 'textfield',
	                    border: true,
	                    collapsible:true,
	                    //collapsed:false,
						anchor: '100%',
	                    fieldDefaults: {
	                        labelAlign: 'left',
	                        labelWidth:115,
	                        hideLabel: true
	                    },
	                    items:[
	                    	{
				                    	xtype: 'fieldcontainer',
				                    	layout: 'hbox',
				                    	defaultMargins: {top: 0, right: 5, bottom: 0, left: 5},
				                    	defaults: {
						                    flex: 0,
						                    hideLabel: true,
						                },
				                    	items:[
				                    		{
			                    				xtype:'displayfield',
			                    				value:i18n.getMsg('nodeconf.tempFolder'), //'Temporary folder',
			                    				width:115,
			                    				align:'left'
			                    			},
									        {
									            xtype:'textfield',
			                        			id: 'Backups.TempFolder',
			                        			allowBlank:false,
						                        blankText:'A value is required.',
			                        			width:130
									        },
									        new Ext.Button( {
								                 width:23,
								                 id: 'browseButtonTempFolder',
								                 icon:'/images/browse.png', 
								                 flex:0,
								                 handler: function(){
								                 	//handleBrowse('Backups.TempFolder', checkedId);
								                 	handleBrowse(checkedId, setTempPathFromBrowser, false);
								                 },
								                 descriptionText:'' ,
								                 initComponent: function() {
													   if(Ext.getCmp('clientNodesTree').getChecked().length > 1) // if several nodes are selectionned, we disable browsing as it makes no sense.
															this.disable();
												 },
												 defaultMargins: {right: 15},
											}),											
											{
			                    				xtype:'displayfield',
			                    				value:i18n.getMsg('nodeconf.indexesFolder'), //'Indexes folder',
			                    				width:115,
			                    				align:'left',
			                    				margins: {left: 15},
			                    				allowBlank:false,
						                        blankText:'A value is required.'
			                    			},
									        {
									            xtype:'textfield',
			                        			id: 'Backups.IndexFolder',
			                        			width:130
									        },
									        new Ext.Button( { 
								                 width:24,
								                 id: 'browseButtonIndexFolder',
								                 icon:'/images/browse.png', 
								                 flex:0,
								                 handler: function(){
								                 	//handleBrowse('Backups.IndexFolder', checkedId);
								                 	handleBrowse(checkedId, setIndexesPathFromBrowser, false);
								                 },
								                 descriptionText:'' ,
								                 allowBlank:false,
						                         blankText:'A value is required.',
								                 initComponent: function() {
													   if(Ext.getCmp('clientNodesTree').getChecked().length > 1) // if several nodes are selectionned, we disable browsing as it makes no sense.
															this.disable();
												 }
											})
				                    	]
				                  },
				                   {
				                    	xtype: 'fieldcontainer',
				                    	layout: 'hbox',
				                    	defaultMargins: {top: 0, right: 15, bottom: 0, left: 15},
				                    	defaults: {
						                    flex: 0,
						                    hideLabel: true,
						                    anchor:'100%'
						                },
				                    	items:[
			                    			{
			                    				xtype:'displayfield',
			                    				value:i18n.getMsg('nodeconf.hasQuota'), 
			                    				width:115,
			                    				align:'left'
			                    			},
									        {
									            xtype:'checkbox',
			                        			id: 'hasQuota',
			                        			width:153,
			                        			allowBlank:false,
			                        			listeners:{
			                        				change:function(thisCheckbox, newValue, oldValue, options){
			                        					if(newValue == true){
			                        						Ext.getCmp('quotaLabel').enable();
			                        						Ext.getCmp('quota').enable();
			                        						Ext.getCmp('quotaUnit').enable();
			                        					}
			                        					else{
			                        						Ext.getCmp('quotaLabel').disable();
			                        						Ext.getCmp('quota').disable();
			                        						Ext.getCmp('quotaUnit').disable();
			                        					}
			                        				}
			                        			}
									        },
									        {
			                    				xtype:'displayfield',
			                    				value:i18n.getMsg('nodeconf.quota'),
			                    				id:'quotaLabel',
			                    				width:115,
			                    				align:'left',
			                    				margins: {left: 15},
			                    				disabled:true,
			                    			},
											{
									            xtype:'numberfield',
			                        			id: 'quota',
			                        			width:55,
			                        			value:0,
			                        			minValue:0,
			                        			disabled:true,
									        },
									       {
									       		xtype:'combo',
								                height:24,
								                align:'left',
								                id:'quotaUnit',
								                store: new Ext.data.ArrayStore({
								                    fields: ['bType'], data : [['MB'],['GB'],['TB']]
								                }),
								                valueField:'bType',
								                displayField:'bType',
								                typeAhead: false,
								                mode: 'local',
								                triggerAction: 'all',
								                value:'GB',
								                selectOnFocus:true,
								                forceSelection:true,
								                width:45,
								                disabled:true							               
								      		}
								      	]
								      	}
	                    ]
	                },
                	{
	                    xtype: 'fieldset',
	                    id:'storageContainer',
	                    title: i18n.getMsg('nodeconf.storageConf'), //'Storage configuration',
	                    defaultType: 'textfield',
	                    border: true,
	                    collapsible:true,
	                    collapsed:false,
						anchor: '100%',
	                    fieldDefaults: {
	                        labelAlign: 'left',
	                        labelWidth:115,
	                        hideLabel: true
	                    },
	                    height:100,
	                    items:[
	                    		{ // storage section
				                    	xtype: 'fieldcontainer',
				                    	layout: 'hbox',
				                    	defaultMargins: {top: 0, right: 5, bottom: 0, left: 5},
				                    	defaults: {
						                    flex: 0,
						                    hideLabel: true,
						                    anchor:'100%'
						                },
				                    	items:[
			                    			{
			                    				xtype:'displayfield',
			                    				id:'isStorageNodeLabel',
			                    				value:i18n.getMsg('nodeconf.storageNode'), //'Storage node',
			                    				width:115,
			                    				
			                    			},
			                    			{
			                    				xtype:'checkbox',
			                    				id:'isStorageNode',
			                    				value:'',
			                    				width:150,
			                    				checked:true,
			                    				 handler: function() {
					                    			if(this.getValue() == false){
					                    				Ext.getCmp('storageContainer').disable();
					                    				Ext.getCmp('isStorageNode').enable();
					                    				Ext.getCmp('isStorageNodeLabel').enable();
					                    			}
					                    			else{
					                    				Ext.getCmp('storageContainer').enable();
					                    			}
					                			}
			                    			},
			                    			{
			                    				xtype:'displayfield',
			                    				value:i18n.getMsg('nodeconf.storageGroup'), //'Storage group',
			                    				width:115,
			                    				margins: {left: 15},
			                    			},
			                    			{
			                    				xtype:'combo',
								                align:'left',
								                id:'Storage.StorageGroup',
								                store: sgStore,
								                valueField:'id',
								                displayField:'userName',
								                typeAhead: false,
								                allowBlank:true,
						                        blankText:'A value is required.',
								                queryMode: 'remote',
								                triggerAction: 'all',
								                forceSelection:true,
								                selectOnFocus:true,
								                width:173
							        		},
								                
			                    		]
			                    },
			                    {
				                    	xtype: 'fieldcontainer',
				                    	layout: 'hbox',
				                    	defaultMargins: {top: 0, right: 5, bottom: 0, left: 5},
				                    	defaults: {
						                    flex: 0,
						                    hideLabel: true,
						                    anchor:'100%'
						                },
				                    	items:[
				                    		{
			                    				xtype:'displayfield',
			                    				value:i18n.getMsg('nodeconf.listenIP'), //'Listen IP',
			                    				width:115,
			                    				allowBlank:false,
						                        blankText:'A value is required.'
			                    			},
			                    			{
						                        xtype:'textfield',
						                        labelAlign:'left',
						                        id: 'Storage.ListenIp',
						                        width:150,
						                        //regex: new RegExp('(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)', null),
						                        //regexText: 'Invalid IP address'
					                   	 	},
					                   	 	{
			                    				xtype:'displayfield',
			                    				value:i18n.getMsg('nodeconf.listenPort'), //'Port',
			                    				width:115,
			                    				margins: {left: 15},
			                    			},
			                    			{
									            xtype: 'numberfield',
									            id: 'Storage.ListenPort',
									            allowBlank:false,
						                        blankText:'A value is required.',
									            fieldLabel: 'Number field',
									            value: 52562,
									            minValue: 0,
									            maxValue: 65535,
									            width:62
									        }
					                   	 ]
			                    },
			                     {
				                    	xtype: 'fieldcontainer',
				                    	layout: 'hbox',
				                    	defaultMargins: {top: 0, right: 15, bottom: 0, left: 15},
				                    	defaults: {
						                    flex: 0,
						                    hideLabel: true,
						                    anchor:'100%'
						                },
				                    	items:[
			                    			{
			                    				xtype:'displayfield',
			                    				value:i18n.getMsg('nodeconf.storageFolder'), //'Storage folder',
			                    				width:115,
			                    				align:'left'
			                    			},
									        {
									            xtype:'textfield',
			                        			id: 'Storage.Directory',
			                        			width:126,
			                        			allowBlank:false,
						                        blankText:'A value is required.',
									        },
									        new Ext.Button( {
								                 width:24,
								                 id: 'browseButtonStorageDir',
								                 icon:'/images/browse.png', 
								                 flex:0,
								                 handler: function(){
								                 	handleBrowse(checkedId, setStorageFolderFromBrowser, false);
								                 },
								                 descriptionText:'' ,
								                 initComponent: function() {
													   if(Ext.getCmp('clientNodesTree').getChecked().length > 1) // if several nodes are selectionned, we disable browsing as it makes no sense.
															this.disable();
												 }
											}),
											{
			                    				xtype:'displayfield',
			                    				value:i18n.getMsg('nodeconf.storageSize'), //'Storage size',
			                    				width:115,
			                    				align:'left',
			                    				margins: {left: 15},
			                    			},
											{
									            xtype:'numberfield',
			                        			id: 'sharesize',
			                        			width:55,
			                        			value:0,
			                        			minValue:0
									        },
									       {
									       		xtype:'combo',
								                height:24,
								                align:'left',
								                id:'unit',
								                store: new Ext.data.ArrayStore({
								                    fields: ['bType'], data : [['MB'],['GB'],['TB']]
								                }),
								                valueField:'bType',
								                displayField:'bType',
								                typeAhead: false,
								                mode: 'local',
								                triggerAction: 'all',
								                value:'GB',
								                selectOnFocus:true,
								                forceSelection:true,
								                width:45								               
								      		}
										]
									},
									
	                    ]
	                },
	                {
	                    xtype: 'fieldset',
	                    id:'delegationContainer',
	                    title: i18n.getMsg('nodeconf.delegationConf'), //'Storage configuration',
	                    defaultType: 'textfield',
	                    border: true,
	                    collapsible:true,
						anchor: '100%',
	                    fieldDefaults: {
	                        labelAlign: 'left',
	                        labelWidth:115,
	                        hideLabel: true
	                    },
	                    items:[]
	                }      			
                ],
	});
 
            
 var nodeConf =  new Ext.Window({
 		id:'nodeConf',
        width:640,
       	height:500,
        plain: true,
        title:i18n.getMsg('nodeconf.title'), //"Configure node(s)",
		autoScroll:false,
        modal:true,
        autoDestroy :true,
        monitorValid:true,
        resizable:true,
		items:form,
		dockedItems: [{
				    xtype: 'toolbar',
				    dock: 'bottom',
				    ui: 'footer',
				    defaults: {align:'right'},
				    align:'right',
				    items: [
                	 {
	                    text: i18n.getMsg('nodeconf.apply'), //'Apply',
	                    formBind:true,
	                    handler: function() {
	                    	for(formElt in Ext.get('configFormPanel').items){
	                    		if(!formElt.isValid){
	                    			formElt.style = 'background:red;';
	                    			alert(formElt.id);
	                    		}
	                    	}
	                        var postdata = "";
	                        var t1combobox = Ext.getCmp('template1');
	                        var t1v = t1combobox.getValue();
							var t1record = t1combobox.findRecord(t1combobox.valueField || t1combobox.displayField, t1v);
							var t1index = t1combobox.store.indexOf(t1record);
							var t2combobox = Ext.getCmp('template2');
	                        var t2v = t2combobox.getValue();
							var t2record = t2combobox.findRecord(t2combobox.valueField || t2combobox.displayField, t2v);
							var t2index = t2combobox.store.indexOf(t2record);
							postdata += 'template1='+t1index; 
							postdata += '&template2='+t2index;
							postdata += '&Logger.Level='+Ext.getCmp('Logger.Level').getValue();
							postdata += '&Logger.Syslog='+Ext.getCmp('Logger.Syslog').getValue();
							postdata += '&Logger.LogFile='+Ext.getCmp('Logger.LogFile').getValue();
							postdata += '&Storage.ListenPort='+Ext.getCmp('Storage.ListenPort').getValue();
							postdata += '&Storage.ListenIp='+Ext.getCmp('Storage.ListenIp').getValue();
							postdata += '&isStorageNode='+Ext.getCmp('isStorageNode').getValue();
							var sgcombobox = Ext.getCmp('Storage.StorageGroup');
	                        var sgv = sgcombobox.getValue();
							var sgrecord = sgcombobox.findRecord(sgcombobox.valueField || sgcombobox.displayField, sgv);
							var sgindex = sgcombobox.store.indexOf(sgrecord);
							postdata += '&storageGroup='+sgindex;
							//postdata += '&storageGroup='+sgrecord.get('id');
							  
							var ngcombobox = Ext.getCmp('nodeGroup');
	                        var ngv = ngcombobox.getValue();
							var ngrecord = ngcombobox.findRecord(ngcombobox.valueField || ngcombobox.displayField, ngv);
							//var ngindex = ngcombobox.store.indexOf(ngrecord);
							//postdata += '&nodeGroup='+ngindex; 
							
							postdata += '&nodeGroup='+ngrecord.get('id');
							postdata += '&Storage.Directory='+Ext.getCmp('Storage.Directory').getValue();
							postdata += '&Backups.TempFolder='+Ext.getCmp('Backups.TempFolder').getValue();
							postdata += '&Backups.IndexFolder='+Ext.getCmp('Backups.IndexFolder').getValue();
							postdata += '&shareSize=';
							var multiplier = 1024*1024;
							if(Ext.getCmp('unit').getValue() == 'GB')
								multiplier *= 1024;
							if(Ext.getCmp('unit').getValue() == 'TB')
								multiplier *= 1024*1024;	
							postdata += Ext.getCmp('sharesize').getValue() * multiplier;
							postdata += '&quota=';
							if(Ext.getCmp('quotaUnit').getValue() == 'GB')
								multiplier *= 1024;
							if(Ext.getCmp('quotaUnit').getValue() == 'TB')
								multiplier *= 1024*1024;	
							postdata += Ext.getCmp('quota').getValue() * multiplier;
							
							var conn = new Ext.data.Connection(); 
						    conn.request({ 
						            url: 'Set.aspx?w=ConfigureNodes&nodeId='+checkedId, 
						            method: 'POST', 
						            scope: this, 
						            params: postdata, 
						            success: function(responseObject) { 
							            if (responseObject.responseText != null)
							             	var okMsg = Ext.Msg.alert('Status', responseObject.responseText); 
						            }, 
						             failure: function(responseObject) { 
						                 Ext.Msg.alert('Status', 'Unable to save changes. Error:'+responseObject.responseText); 
						            } 
                    		})	
                    	}
                	},
                	{
	                    text: i18n.getMsg('nodeconf.cancel'), //'Cancel',
	                    handler: function() {
                        	//this.up('form').getForm().reset();
                        	this.up('window').close();
                    	}
                	},
                ]
               }] //end dockedItems
    });
	
	Ext.define('ConfModel', {
        extend: 'Ext.data.Model',
        fields: ['Logger_Syslog','Logger_Level', 'Logger_LogFile', 'Backups_IndexFolder', 'Backups_TempFolder', 'Storage_ListenIp', 'Storage_Directory','Storage_ListenPort']
    });
    
	var confStore = new Ext.data.Store( {
        model: 'ConfModel',
        proxy: {
            type: 'ajax',
            url: '/Get.aspx?w=NodeConf&nodeId='+checkedId,
        },
        autoLoad:true
    });
    confStore.load({
    scope   : this,
    callback: function(records, operation, success) {
        //console.log(records);
        Ext.each(records, function(record){
			//alert(record.get('Storage_ListenPort'));
			for (key in record.data) {
        		try{
        			Ext.getCmp(key.replace("_",".")).setValue(record.data[key]);
        		}
        		catch(e){
        			console.log(e);
        		}
    		}
		});
		// let's display the node configuration (hub-side)
		var currentNode = Ext.getCmp('clientNodesTree').getStore().getNodeById(checkedId); //.findRecord('id', checkedId, 0, false, true, true);
		var storageSize = currentNode.get('share');
		var formatedSize = 0;
		var unit = "";
		if(storageSize >= 1024 * 1024 *1024 * 1024){
			formatedSize = Math.round(storageSize/1024/1024/1024/1024);
			unit = 'TB';
		}
		else if (storageSize > 1024 * 1024 *1024){
			formatedSize =  Math.round(storageSize/1024/1024/1024);
			unit = 'GB';
		}
		else{
			formatedSize =  Math.round(storageSize/1024/1024);
			unit = 'MB';
		}
		Ext.getCmp('sharesize').setValue(formatedSize);
		Ext.getCmp('unit').setValue(unit);
		var quota = currentNode.get('quota');
		var formatedQuota = 0;
		var quotaUnit = "";
		if(quota >= 1024 * 1024 *1024 * 1024){
			formatedQuota = Math.round(quota/1024/1024/1024/1024);
			quotaUnit = 'TB';
		}
		else if (storageSize > 1024 * 1024 *1024){
			formatedQuota =  Math.round(quota/1024/1024/1024);
			quotaUnit = 'GB';
		}
		else{
			formatedQuota =  Math.round(quota/1024/1024);
			quotaUnit = 'MB';
		}
		Ext.getCmp('quota').setValue(formatedQuota);
		Ext.getCmp('quotaUnit').setValue(quotaUnit);
		if(quota >0)
			Ext.getCmp('hasQuota').setValue(true);
		var sgcombobox = Ext.getCmp('Storage.StorageGroup');
		var sgrecord = sgcombobox.store.findRecord('id', currentNode.get('storagegroup'));
		sgcombobox.setValue(sgrecord.get('id'));
		var gcombobox = Ext.getCmp('nodeGroup');
		//alert('group='+currentNode.get('group')+', sg='+currentNode.get('storagegroup'));
		var grecord = gcombobox.store.findRecord('id', currentNode.get('group'));
		//alert('found record '+grecord.get('id'));
		//alert('group='+currentNode.get('group')+', sg='+currentNode.get('storagegroup')+'groipname='+grecord.get('name'));
		gcombobox.setValue(grecord.get('id'));
		//gcombobox.setValue(3);
    }
	});
	nodeConf.show();
//}); / end i18n
} // end GetConfigWindow	 
	

	function expandAll(){
		Ext.getCmp('tree').getRootNode().cascadeBy(function(r) {  r.expand();   })
	}
	
	Ext.get('clTitle').dom.innerText = i18n.getMsg('clientnodes.title');
	
	var groupingFeature = Ext.create('Ext.grid.feature.Grouping',{
        groupHeaderTpl: '{name} ({rows.length})',
        depthToIndent:15,
    });
  
    var tree = new Ext.tree.Panel({
       /* title: i18n.getMsg('clientnodes.title'),*/
        id:'clientNodesTree',
        height: 500,
        layout:'fit',
        renderTo: Ext.get('panel'),
        collapsible: false,
        useArrows: true,
        rootVisible: false,
        store: nStore,
        multiSelect: true,
        singleExpand: false,
        draggable:false,    
        stateful:true,   
        stripRows:true,
        autoScroll:true,
        //plugins:[groupingFeature],
        //features: [{ ftype: 'grouping' }],
        features: [groupingFeature],
        columns: [{
            xtype: 'treecolumn', //this is so we know which column will show the tree
            header: i18n.getMsg('nodestree.node'),
            flex: 2,
            width:600,
            sortable: true,
            groupable:false,
            dataIndex: 'userName',
            renderer: function(value, metaData, record, colIndex, store, view){
	            if(record.get('certCN').length > 1)
	            	return value+" (<i>"+record.get('certCN')+"</i>)";
	            else if(record.get('description').length >1)
	            	return value+"<span style='font-weight:normal'> (<i>"+record.get('description')+"</i>)</span>";
	            else
	            	return value;
            }
        },{
            text: i18n.getMsg('nodestree.currentIP'),
            flex: 0,
            width:120,
            sortable: true,
            dataIndex: 'ip',
        },{
            text: ' ',
            flex: 0,
            width:5,
            sortable: false,
            dataIndex: '',
            style:'border:1px 1px 1px 1px solid gray;',
            menuDisabled:true,
        },/*{
            text: i18n.getMsg('nodestree.listenIP'),
            flex: 0,
            width:120,
            sortable: true,
            dataIndex: 'ip',
        },{
            text: i18n.getMsg('nodestree.listenPort'),
            flex: 0,
            width:60,
            dataIndex: 'port',
            sortable: true
        },*/{
            text: i18n.getMsg('nodestree.version'),
            flex: 0,
            width:60,
            dataIndex: 'version',
            sortable: true
        },{
            text: i18n.getMsg('nodestree.os'),
            flex: 0,
            dataIndex: 'os',
            width:40,
            sortable: true,
            renderer:function(value){
            	if(value.toLowerCase() == 'linux')
            		return '<img src="/images/Linux-xs.png" title="'+value+'"/>';
            	else if(value.substr(0,2) == 'NT')
            		return '<img src="/images/Windows-xs.png" title="'+value+'"/>';
            	else if(value.toLowerCase() == 'freebsd')
            		return '<img src="/images/Freebsd-xs.jpg" title="'+value+'"/>';
            	else if(value.toLowerCase() == 'darwin')
            		return '<img src="/images/Apple-xs.png" title="'+value+'"/>';
            	else if(value.toLowerCase() == 'sunos')
            		return '<img src="/images/Sunos-xs.jpg" title="'+value+'"/>';
            	else if(value.length > 1)
            		return '<img src="/images/Unknown-xs.png" title="Unknown os : '+value+'"/>';
            }
        },{
        	//xtype:'actioncolumn',
            text:  i18n.getMsg('nodestree.backupsets'),
            dataIndex: 'backupsets',
            width:70,
            sortable: true,
            renderer: function(value){
            	if(value > 0){
            		return '<div title="'+'XXXXX'/*i18n.getMsg('nodestree.moreDetailsMsg')*/+'" style="padding-left:15px;background:url(/images/plus.gif) no-repeat;background-position:left;">'+value+'</div>';
            	}
            	else
            		return '<div style="padding-left:15px;">'+value+'</div>';
            }
           /* items: [{
	            	icon:'/images/plus.gif',
	            	tooltip:'Click for backups details',
	            	}
            	]*/
        },{
            text:  i18n.getMsg('nodestree.quota'),
            flex: 0,
            dataIndex: 'quota',
            width:65,
            sortable: true,
            renderer:function(value){
            		return FormatSize(value);
            }
        },{
            text:  i18n.getMsg('nodestree.usedQuota'),
            flex: 0,
            width:70,
            dataIndex: 'usedquota',
            sortable: true,
            renderer:function(value){
            		return FormatSize(value);
            	
            }
        },{
            text:  i18n.getMsg('nodestree.certificate'),
            flex: 0,
            dataIndex: 'certificateStatus',
            width:30,
            sortable: true,
            renderer:function(value){
            	// <TODO> warning/error status when sthg is wrong with cert policy
            	if(value == "sec-OK")
            		return '<img src="/images/security-high.png"/>';
            	else if(value == "sec-ERROR")
            		return '<img src="/images/security-low.png"/>';
            	else if(value == "sec-WARNING")
            		return '<img src="/images/security-medium.png"/>';
            	else
            		return "";
            }
        },{
        	//xtype:'datetimecolumn',
        	text:  i18n.getMsg('nodestree.status'),
            flex: 0,
            width:90,
            dataIndex: 'status',
            sortable: true,
            renderer:function(value){
            	return i18n.getMsg('nodestree.status.'+value);
            }
        },{
        	//xtype:'datetimecolumn',
        	text:  i18n.getMsg('nodestree.lastconnection'),
            flex: 1,
            width:100,
            dataIndex: 'lastconnection',
            sortable: true
        },{
            text:  i18n.getMsg('nodestree.delegations'),
            flex: 1,
            dataIndex: '',
            sortable: true
        }
        ],
        dockedItems: [{
		    xtype: 'toolbar',
		    dock: 'bottom',
		    style:'bottom:0px;',
		    padding:0,
		    margins:0,
		    height:32,
		    items: [
		        {
		        	xtype:'button',
	        		text:  i18n.getMsg('nodestree.configureBtn'),
	        		id: 'configureBtn',
	               	iconCls:'icon-btn',
	                icon:'/images/conf.png',
	                width:85,
	                height:29,	  
	                border:1,             
	               	handler:GetConfigWindow,
	               	disabled:true,
	        	},'-',
	        	{
	        		xtype:'button',
	        		text:  i18n.getMsg('nodestree.authorizeBtn'),
	        		id:'authorizeBtn',
	                iconCls:'icon-btn',
	                icon:'/images/unlocked.png',
	                width:85,
	                height:29,
	                listeners:{
	                     scope:this,
	                     click:function(){ManageLockStatus('False');}
	               	},
	               	disabled:true,
	        	},'-',
	        	{
	        		xtype:'button',
	        		text:  i18n.getMsg('nodestree.lockBtn'),
	        		id:'lockBtn',
	                iconCls:'icon-btn',
	                icon:'/images/locked.png',
	                width:85,
	                height:29,
	                listeners:{
	                     scope:this,
	                     click:function(){ManageLockStatus('True');}
	               	},
	               	disabled:true,
	            },'-',
	          	{
	          		text:  i18n.getMsg('nodestree.expand'),
		            icon:'/images/plus.gif',
		            handler : function(){
	                	Ext.getCmp('clientNodesTree').getRootNode().cascadeBy(function(r) {  
	                		if(toBeExpanded == true){
	                			r.expand();  
	                		}
	                		else{
	                			r.collapse();
	                		}
	                		Ext.getCmp('clientNodesTree').getView().refresh();
	                	 
	                	})
	                	if(toBeExpanded == true)
                			toBeExpanded = false;
                		else{
                			toBeExpanded = true;
                			Ext.getCmp('clientNodesTree').getRootNode().expand();
                		}
                		
	            	}
	          	}
		    ]
		}],
    	listeners: {
        'checkchange': function(node, checked){        	
	       	if (Ext.getCmp('clientNodesTree').getChecked().length > 1){
	       		Ext.getCmp('authorizeBtn').enable();
	        	Ext.getCmp('lockBtn').enable();
    			Ext.getCmp('configureBtn').disable();       			
	        }
    		else if (Ext.getCmp('clientNodesTree').getChecked().length == 1){
    			Ext.getCmp('configureBtn').enable();
    			Ext.getCmp('authorizeBtn').enable();
	        	Ext.getCmp('lockBtn').enable();
	        }
	        else{
	        	Ext.getCmp('configureBtn').disable();
	        	Ext.getCmp('authorizeBtn').disable();
	        	Ext.getCmp('lockBtn').disable();
	        }
          }
	 },
    });
});
});