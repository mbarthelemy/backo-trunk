Ext.Loader.setConfig({
        enabled: true,
        disableCaching: false,
        paths: {
            'Extensible': '/Extensible/src',
            'Extensible.example': '/Extensible/examples',
            'Ext.ux':'/ext4/ux',
            'Ext.i18n':'/i18n/'
        }
 });

Ext.example = function(){
    var msgCt;
    function createBox(t, s){
       return '<div class="msg"><h3>' + t + '</h3><p>' + s + '</p></div>';
    }
    return {
        msg : function(title, format){
            if(!msgCt){
                msgCt = Ext.core.DomHelper.insertFirst(document.body, {id:'msg-div'}, true);
            }
            var s = Ext.String.format.apply(String, Array.prototype.slice.call(arguments, 1));
            var m = Ext.core.DomHelper.append(msgCt, createBox(title, s), true);
            m.hide();
            m.slideIn('t').ghost("t", { delay: 5000, remove: true});
        },

        init : function(){
        }
    };
}();


	
Ext.onReady(function () {
	/*Ext.Loader.setConfig({
        enabled: true,
        disableCaching: false,
        paths: {
            'Extensible': '/Extensible/src',
            'Extensible.example': '/Extensible/examples',
            'Ext.ux':'/ext4/ux',
            'Ext.i18n':'/i18n/'
        }
     });*/
    Ext.require([
	 	'Ext.data.proxy.Rest',
	    'Ext.data.*',
	    'Ext.grid.*',
	    'Ext.tree.*',
	    'Ext.form.*',
	    'Ext.window.*',
	    'Ext.ux.RowExpander',
	    'Ext.ux.BoxSelect',
	    /*'Ext.ux.exporter.Button',
		'Ext.ux.exporter.csvFormatter.CsvFormatter',
		'Ext.ux.exporter.excelFormatter.ExcelFormatter',
		'Ext.ux.exporter.excelFormatter.Workbook',
	'Ext.ux.exporter.excelFormatter.Worksheet',
	'Ext.ux.exporter.excelFormatter.Cell',
	'Ext.ux.exporter.excelFormatter.Style',
	'Ext.ux.exporter.Exporter'*/
	]);
	var params = Ext.urlDecode(window.location.search.substring(1));
	var lang;
    if(params.lang)
        lang = params.lang;
    i18n = Ext.create('Ext.i18n.Bundle',{
		bundle: 'wui',
		lang: lang,
		path: 'i18n',
		noCache: true
	});
	
	i18nTask = Ext.create('Ext.i18n.Bundle',{
		bundle: 'taskmsg',
		lang: lang,
		path: 'i18n',
		noCache: true
	});

i18n.onReady(function(){
//i18nTask.onReady(function(){
 Ext.get('histTitle').dom.innerText = i18n.getMsg('history.title');
 
 function FormatTime(val){
  	
  	var seconds = Math.round(val%60);
  	if(seconds <10) seconds = "0"+seconds;
  	
  	var hours = Math.floor(val/3600);
  	var minutes = Math.round((val/60)%60);
  	if(minutes <10) minutes = "0"+minutes;
   	return hours+":"+minutes+":"+seconds;
  }
  
  
  function toggleDetails(){
    	var nRows=taskStore.getCount();
    	var theGrid = Ext.getCmp('grid');
    	var exp = theGrid.getPlugin('expander');
	    for(i=0;i< nRows;i++){
	        exp.toggleRow(i);
	        //grid.plugins.each(function(plugin){	alert(plugin.id)}) ;
	    }
	    if(detailsToggled == true) detailsToggled = false;
	    else detailsToggled = true;
	    //theGrid.refresh();
 }
 
 Ext.define('BasePath', {
    extend: 'Ext.data.Model',
    fields: [
    	{name: 'path',     		type: 'string'},
    ]
  });
    
 Ext.define('TaskM', {
    extend: 'Ext.data.Model',
    idProperty:'id',
    fields: [
        {name: 'id',     type: 'number'},
        {name: 'type',     type: 'string'},
        {name: 'operation', type: 'string'},
        {name: 'runningStatus', type: 'string'}, //started, pendingstart, done...
        {name: 'status', type: 'string'}, // ok, warning, error...
        {name: 'bsId', type: 'string'},
        {name: 'bsName', type: 'string'},
        {name: 'bsType', type: 'string'},
        /*{name: 'compress', type: 'string'},
        {name: 'encrypt', type: 'string'},
        {name: 'clientdedup', type: 'string'},*/
        {name: 'flags', type: 'string'},
        {name: 'clientId', type: 'number'},
        {name: 'startDate', type: 'date'/*, dateFormat:'Y/M/D'*/},
        {name: 'elapsedTime', type: 'string'},
        {name: 'endDate', type: 'date'},
       	{name: 'percent', type: 'number'},
       	{name: 'totalItems', type: 'number'},
       	{name: 'originalSize', type: 'number'},
       	{name: 'finalSize', type: 'number'},
       	{name: 'currentAction', type: 'string'},
       	{name: 'cls', type: 'string', defaultValue:'gridCell'},
    ],
       	hasMany:{model:'BasePath', name:'BasePath', associationKey:'BasePath'}
  });
  
  Ext.define('NodeM', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'id',     		type: 'string', defaultValue:Ext.id()},
        {name: 'userName',     	type: 'string'},
        {name: 'certCN',     	type: 'string'},
        {name: 'ip', 			type: 'string'},
        {name: 'group', 		type: 'number'},
        {name: 'storagegroup', 	type: 'number'},
        {name: 'certificateStatus', type: 'string'},
        {name: 'description', type: 'string'},
        {name: 'checked', type: 'boolean', defaultValue:false},
    ],
    hasMany:{model:'NodeM', name:'children', associationKey:'children'}
 });
 
  Ext.define('BackupM', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'id',     		type: 'string'},
        {name: 'name',     	type: 'string'},
        {name: 'paths',     	type: 'string'},
        {name: 'checked', type: 'boolean', defaultValue:false},
        {name: 'leaf', type: 'boolean', defaultValue:true},
    ],
 });


 
 var nStore = new Ext.data.TreeStore( {
 //Ext.create('Ext.data.TreeStore', {
 	autoLoad:true,
    model: 'NodeM',
    storeId:'nStore',
    id:'nStore',
    proxy: {
        type: 'ajax',
        url: '/Get.aspx?w=Clients&leaf=false'
    },
    //folderSort: true
 });
 var bs,from,to,sizeOperator,size,statuses;
 //nStore.load();
 //var taskStore = new Ext.data.JsonStore( {
 var taskStore = new Ext.data.Store( {
        model: 'TaskM',
        //autoload:{params:{start: 0, limit: 13}},
        autoLoad:false,
        pageSize: 13,
        loadMask:false,
        //groupField:'bsName',
        buffered:false,
        proxy: {
            type: 'ajax',
            //type: 'scripttag',
            url: '/Get.aspx?w=Tasks',
            
            reader:{
	        	type:'json',
	        	root:'items',
	        	totalProperty: 'totalCount',
	        },
	        extraParams : {
            	bs:bs,
				from:from,
				to:to,
				sizeOperator:sizeOperator,
				size:size,
				statuses:statuses,
            
        	}
        },
        
       /* listeners:{
        	load:function(){
        		Ext.getCmp('grid').setTitle(i18n.getMsg('runningTasks.title')+' ('+taskStore.getTotalCount()+')');
        		Ext.getCmp('grid').invalidateScroller();
        		Ext.getCmp('grid').doLayout();
        	}
        }*/
  });
  
  var bsStore = new Ext.data.JsonStore( {
    model: 'BackupM',
    autoLoad:false,
    storeId:'bsStore',
    id:'bsStore',
    proxy: {
        type: 'ajax',
        url: '/Get.aspx?w=Backupsets',
         reader:{
	        	type:'json',
	        	//root:'items',
	        	//totalProperty: 'totalCount',
	        },
    },
  });
 
  var groupingFeature = Ext.create('Ext.grid.feature.Grouping',{
        groupHeaderTpl: '{name} ({rows.length} Item{[values.rows.length > 1 ? "s" : ""]})',
        depthToIndent:15,
  });
  
  
  var tree = new Ext.tree.Panel({
        id:'clientNodesTree',
        height: 450,
        layout:'fit',
        //renderTo: Ext.get('panel'),
        collapsible: false,
        collapsed:false,
        useArrows: true,
        rootVisible: true,
        store: Ext.data.StoreManager.lookup('nStore'),
        multiSelect: true,
        singleExpand: false,
        draggable:false,    
        stateful:false,   
        hideHeaders:true,
        stripeRows:true,
        padding:0,
        margins:0,
        root: {
	        text: "*",
	        expanded: true,
	        iconCls:'',
	        icon:'',
	       
	    },
        columns: [{
            xtype: 'treecolumn', //this is so we know which column will show the tree
            id:Ext.id(),
            flex: 2,
            width:200,
            sortable: false,
            checked:false,
            dataIndex: 'userName',
            renderer: function(value, metaData, record, colIndex, store, view){
	            if(record.get('certCN').length > 1)
	            	return value+" (<i>"+record.get('certCN')+"</i>)";
	            
	            else
	            	return value;
            }
        },
        ],
    	listeners: {
        	'checkchange': function(node, checked){        	
	       		//if(checked){
	       			if(node.getDepth() == 2 ){
	       					       			//alert(checked);
						if(node.get('expanded') == false && checked){
	       				bsStore.on('load', function(){
				       		var theNode = node;
				       		var nodez = [];
				       		bsStore.each( function(trecord){
				       			//alert(trecord.get('id'));
								nodez.push({
			                    	id:'bs'+trecord.get('id'),
			                    	internalId:'bs'+trecord.get('id'),
			                    	'userName': trecord.get('name'),
			                    	text:trecord.get('id')+trecord.get('name'),
			                    	
			                    	//id:'bs'+trecord.get('id'),
			                    	//parentId:node.get('id'),
			                    	//'parentId':node.get('id'),
			                    	leaf:true,
			                    	checked:true,
			                    	icon:'/images/bs.png',
			                    });
			                });
			                node.appendChild(nodez);
			                var fakeChild = node.getChildAt(0);
			                node.removeChild(fakeChild);
			                node.updateInfo();
			                
		                }); // end onload
		                bsStore.load({
		                	scope   : this,
			  				params:{
			  					nodeId:''+node.get('id')
			  				},
		       			});
		       			} // end if expanded
		       			else{
		       				node.eachChild(function (n) {
			                	n.set('checked', checked);
			            	});
		       			}
		       			
	       			}
	       		//}
          	},
          	'itemclick': function(view, record) {
			    //var selModel = tree.getSelectionModel();
			    record.cascadeBy(function(r) {
			    	var checkstatus = record.get('checked');
			        //selModel.select(r, checkstatus);
			        r.set('checked', checkstatus);
			    })
			},
			'beforeitemexpand':function(nodeW, obj){
				//alert('selected='+node.get('id')+',name='+node.get('userName'));
				if(nodeW.getDepth() == 2 && nodeW.childNodes.length == 1){
		       		bsStore.on('load', function(){
			       		var theNode = nodeW;
			       		var nodez = [];
			       		bsStore.each( function(trecord){
			       		//bsStore.getNodeById(nodeW.get('id')).cascadeBy(function(trecord) {
			       			//alert(trecord.get('id')+' - '+trecord.get('name'));
							nodez.push({
								id:'bs'+trecord.get('id'),
		                    	leaf: true,
		                    	'userName': trecord.get('name'),
		                    	text:trecord.get('id')+trecord.get('name'),
		                    	//'id':'bs'+trecord.get('id'),
		                    	
		                    	parentId:trecord.get('id'),
		                    	'parentId':trecord.get('id'),
		                    	//internalId:'bs'+trecord.get('id'),
		                    	//'internalId':'bs'+trecord.get('id'),
		                    	icon:'/images/bs.png',
		                    	checked:trecord.get('checked')
		                    });
		                });
		                //node['leaf'] = false;
		                nodeW.appendChild(nodez);
		                var fakeChild = nodeW.getChildAt(0);
		                nodeW.removeChild(fakeChild);
		               // bsStore.clear();
		                //node.updateInfo();
		                //node.expand();
		                //tree.getView().refresh();
	                }); // end onload
	                bsStore.load({
	                	//scope   : this,
		  				params:{
		  					nodeId:''+nodeW.get('id')
		  				},
	       			});
	       		
				}
				
			}
	 	},
    });
    var gridWidth = document.getElementById('panel').offsetWidth - 454;
    
    
 	var nStore2 = new Ext.data.JsonStore( {
 		autoLoad:true,
	    model: 'NodeM',
	    proxy: {
	        type: 'ajax',
	        url: '/Get.aspx?w=Clients',
	        //root:'children'
	    },
	    listeners:{
        	load:function(){
        		//Ext.getCmp('grid').getView().refresh();
        	}
        }
    });
    //nStore2.sync();
    
    var grid = Ext.create('Ext.grid.Panel', {
    	id:'grid',
        collapsible: false,
        iconCls: 'icon-grid',
        frame: false,
        stateful:false,
        store: taskStore,
        height: 570,
        width:gridWidth,
        layout:'fit',
        align:'left',
        autoScroll:true,
        scroll:'vertical',
        overlapHeader:true,
        title:'',
        multiSelect: true,
        features: [/*groupingFeature, */{ftype: 'groupingsummary'}],
        viewConfig: {
        	loadMask: true,
    		onStoreLoad: Ext.emptyFn,
    	},
    	/*verticalScroller: {
    		xtype: 'paginggridscroller'
  		},
  		invalidateScrollerOnRefresh: false,
    	// infinite scrolling does not support selection
    	disableSelection: true,*/
        plugins: [
        	{
              ptype: 'rowexpander',
              id:'expander',
              pluginId:'expander',
              rowBodyTpl:[
	              '<table><tr><td><div class="gridCell" style="margin-left:75px;max-height:100;width:550px;overflow-x:none;overflow-y:auto;"><table>',
	              '<tpl for=".">',
	              '<tr>',
	              '<td>',
	              '<tpl if="code &gt;= 900"><img class="x-tree-node-icon" src="/images/sq_ye.gif"/></tpl>',
	              '<tpl if="code &gt;= 800 && code &lt; 900"><img class="x-tree-node-icon" src="/images/sq_re.gif"/></tpl>',
	              '<tpl if="code &gt;= 700 && code &lt; 800"><img class="x-tree-node-icon" src="/images/sq_gr.gif"/></tpl>',
	              //'</td>',
	              '{date}</td>',
	              //'<td>{message}</td>',
	             // '<td>{[this.getTaskMsg(values.code)]}</td>', //{i18nTask.get("
	             '<td>({code}) {[i18nTask.getMsg("task."+values.code, values.message1, values.message2)]}</td>',
	              '</tr>',
	              '</tpl>',
	              '</table></div></td>',
	              '<td width="50">&nbsp;</td>',
	              '<td><div class="gridCell" style="max-height:100;width:150;overflow-x:none;overflow-y:auto;"><table>',
	              'sessions:<br/><br/><br/><br/><br/>',
	              '</div></td></tr></table>'
              ],
              urlTpl: '/Get.aspx?w=TaskLogEntries&trackingId={id}',
              stateful:true,
              listeners:{
              	onexpand: function(ex, record, body, rowIndex){
              		ProcessExpander(record, body, rowIndex);
              	}
              }
		    },
        ],
        columns: [
	        {
	        	text: '%',
	        	width:65,
	        	height:10,
	        	dataIndex:'percent',
	        	tdCls:'gridRow',
	        	renderer: function(value, metaData, record, colIndex, store, view){
	        		var roundedPercent=0;
	        		var rawPercent = record.get('percent');
	        		if(rawPercent == 0)
	        			roundedPercent = 0;
	        		else if(rawPercent < 20)
	        			roundedPercent = 20;
	        		else if(rawPercent < 40)
	        			roundedPercent = 40;
	        		else if(rawPercent < 60)
	        			roundedPercent = 60;
	        		else if(rawPercent < 80)
	        			roundedPercent = 80;
	        		else if(rawPercent <= 100)
	        			roundedPercent = 100;
	        		else 
	        			roundedPercent = 0;
	            	//value='<img src="/images/compe_'+record.get('status').substr(0,1).toLowerCase()+"_"+compA[index]+'.png"/>'+/*record.get('percent')*/compA[index]+'%';
	            	value='<img src="/images/c_'+record.get('status').substr(0,1).toLowerCase()+"_"+roundedPercent+'.png"/><div class="greyV" style="horizontal-align:right;"><b>'+record.get('percent')+'%</b></div>';
	            	return value;
	            }
	        },
	        {
	            text: i18n.getMsg('runningTasks.task'),
	            flex: 1,
	            dataIndex: 'bsName',
	            tdCls:'gridRow',
	            width:160,
	            //tpl:'#{id} : {bsName}',
	            renderer: function(value, metaData, record, colIndex, store, view){
	            	value = '<div class="taskBox"><b>#'+record.get('id')+' <i>('+record.get('bsName')+')</i></b>'
	            		+'<span><ul><li class="gStatus">'+i18n.getMsg('runningTasks.status.'+record.get('runningStatus'))+'</li></ul></span></div>';
		            return value;
	            }
	        },{
	            text: i18n.getMsg('runningTasks.client'),
	            flex: 1,
	            width:100,
	            id:'tClient',
	            dataIndex: 'clientId',
	            renderer: function(value){
	            	//Ext.data.StoreManager.lookup('nStore').each( function(trecord){
	            	nStore2.each( function(trecord){
						trecord.children().each(function(node) {
							if(node.get('id') == value){
								value = '<div>'
									+node.get('userName')+'<br/> <span class="greyV">'+node.get('ip')+'</span></div>';
							}
						});
					});
					return value;
	            },
	           /* summaryType: 'count',
		        summaryRenderer: function(value){
		            return Ext.String.format('{0} '+i18n.getMsg('runningTasks.client'), value);
		        }*/
	        },{
	            text: i18n.getMsg('runningTasks.operation'),
	            flex: 0,
	            width:100,
	            dataIndex: 'operation',
	            renderer: function(value, metaData, record, colIndex, store, view){
	            	if(value == 'Backup'){
	            		value += " ("+record.get('bsType')+")"
	            			+"<br/>"+i18n.getMsg('runningTasks.type.'+record.get('type'))+"&nbsp;&nbsp;";
	            		
	            		var capsList = record.get('flags').toLowerCase().split(", ");
		            	for (var i=0; i < capsList.length; i++) 
		            		if(capsList[i] != 'none')
		            			value += '<img src="/images/'+capsList[i].substring(1)+'.png" title="'+capsList[i]+'"/>&nbsp;';
	            	}
	            	
	            	return value;
	            },
	            summaryType: 'count',
		        summaryRenderer: function(value){
		            return Ext.String.format('{0} ', value);
		        }
	        },{
	            text: i18n.getMsg('runningTasks.size'),
	            flex: 0,
	            //width:195,
	            height:35,
	            padding:'-10 -10 -10 0',
	            //margins:'-4 0 -4 0',
	            style:'vertical-align:top;',
	            columns:[
	            	{
	            		text: '<i>'+i18n.getMsg('runningTasks.size.original')+'</i>',
	            		flex: 0,
	            		width:65,
	            		height:15,
	            		//padding:'-10 0 0 0',
	            		margins:'-4 0 1 0',
	            		dataIndex:'originalSize',
	            		sortable:true,
	            		renderer: function(value, metaData, record, colIndex, store, view){
			            	return FormatSize(value)+'<br/><div align="right"><i>'+record.get('totalItems')+'</i></div>'; 
			            },
			            summaryType: 'sum',
				        summaryRenderer: function(value){
				            return FormatSize(value);
				        }
	            	},
	            	{
	            		text: '<i>'+i18n.getMsg('runningTasks.size.final')+'</i>',
	            		flex: 0,
	            		width:65,
	            		height:15,
	            		//padding:'-10 0 0 0',
	            		margins:'-4 0 1 0',
	            		dataIndex:'finalSize',
	            		sortable:true,
	            		renderer: function(value, metaData, record, colIndex, store, view){
			            	return FormatSize(value)+'<br/><div align="left"><i> items</i></div>';
			            },
			            summaryType: 'sum',
				        summaryRenderer: function(value){
				            return FormatSize(value);
				        }
	            	},
	            	/*{
			            text: '<i>'+i18n.getMsg('runningTasks.transferredSize')+'</i>',
			            flex: 0,
			            width:70,
			            height:15,
			            //padding:'-10 0 0 0',
			            margins:'-4 0 1 0',
			            dataIndex: 'transferredSize',
			            renderer: function(value){
			            	return FormatSize(value);
			            }
			        }*/
	            ]
	        },{
	            text: i18n.getMsg('runningTasks.duration')+"<br/>"+i18n.getMsg('runningTasks.rate'),
	            flex: 0,
	            width:60,
	            sortable:true,
	            dataIndex: 'rate',
	            renderer: function(value, metaData, record, colIndex, store, view){
	            	return FormatTime(record.get('elapsedTime'))+"<br/><i>"+FormatSize(record.get('originalSize')/record.get('elapsedTime'))+"/s</i>";
	            }
	            
	        },{
	        	//xtype:'datecolumn',
	        	//format: 'Y-m-d H:i:s',
	        	
	        	//margins:'-10 0 0 0',
	        	//padding:'0 0 0 0',
	            text: i18n.getMsg('runningTasks.startDate')
	            	+'<br/>'+i18n.getMsg('runningTasks.endDate')+'',
	            //format:'mm H:i:s',
	            //style:'line-height:0 !important;',
	            flex: 1,
	            width:155,
	            align:'left',
	            sortable:false,
	            dataIndex: 'startDate',
	            renderer: function(value, metaData, record, colIndex, store, view){
	            
	            	return ''+new Date(record.get('startDate'))
	            		+'<br/>'+ record.get('endDate')+'';
	            }
	        },
        ],
        dockedItems: [{
		    xtype: 'pagingtoolbar',
		    pageSize:13,
		    store:taskStore,
		    dock: 'bottom',
		    ui: 'footer',
		    height:25,
		    defaults: {align:'right'},
		    align:'right',
		    items: [
			        	{
			        		xtype:'displayfield',
			        		value:i18n.getMsg('runningTasks.groupBy'),
			        		width:80,
			        	},
			        	{
			        		xtype:'combo',
			        		id:'groupBy',
			        		mode:           'local',
			                value:          'none',
			                triggerAction:  'all',
			                forceSelection: true,
			                allowBlank:true,
			                editable:       false,
			                fieldLabel:     '',
			                displayField:   'value',
			                valueField:     'name',
			                queryMode: 'local',
			                width:150,
			                store:          Ext.create('Ext.data.Store', {
			                  fields : ['name', 'value'],
			                    data   : [
			                        {name : 'bsName',   value: 'backup set name'},
			                        {name : 'operation',  value: i18n.getMsg('runningTasks.operation')},
			                        {name : 'status', value: i18n.getMsg('runningTasks.status')},
			                        {name : 'runningStatus', value: i18n.getMsg('runningTasks.runningStatus')},
			                        //{name : 'percent', value: 'completion %'},
			                        {name : 'clientId', value: i18n.getMsg('runningTasks.client')},
			                       // {name : 'rate', value: i18n.getMsg('runningTasks.rate')},
			                        {name : 'startDate', value: i18n.getMsg('runningTasks.startDate')},
			                       // {name : 'elapsedTime', value: i18n.getMsg('runningTasks.duration')},
			                        {name : 'originalSize', value: i18n.getMsg('runningTasks.size.original')},
			                        //{name : 'finalSize', value: i18n.getMsg('runningTasks.size.final')},
			                        //{name : '', value: 'don\'t group'},
			                    ]
			                }),
			                listeners:{
			                	'change': function( thisCombo, newValue, oldValue, options ){
			                		if(newValue == '')
			                			taskStore.ungroup();
			                		else
			                			taskStore.group(newValue);
			                	}
			                }
			        	},
			        	{
				            text:'&nbsp;&nbsp;'+i18n.getMsg('runningTasks.clearGroupBy'),
				            icon:'/images/clearfield.png',
				            handler : function(){
			                	taskStore.clearGrouping();
			            	}
			          	},
			          	'-',
			          	{
				      		text:'&nbsp;'+i18n.getMsg('runningTasks.details'),
				            icon:'/images/plus.gif',
				            handler : toggleDetails
				      	},
			          	{
			          		xtype:'button',
						    icon:'/images/excel.png',
						    handler:function(button){
						        var gridPanel=button.up('grid');
						        var dataURL='data:application/vnd.ms-excel;base64,'+Ext.ux.exporter.Exporter.exportAny(taskStore, 'excel');
						        window.location.href=dataURL;
						    }
						},
					  
            	]
            }]
    });
    
    
     window.generateData = function(n, floor){
        var data = [],
            p = (Math.random() *  11) + 1,
            i;
            
        floor = (!floor && floor !== 0)? 20 : floor;
        
        for (i = 0; i < (n || 12); i++) {
            data.push({
                name: Ext.Date.monthNames[i % 12].substring(0,3),
                data1: Math.floor(Math.max((Math.random() * 100), floor)),
                data2: Math.floor(Math.max((Math.random() * 100), floor)),
                data3: Math.floor(Math.max((Math.random() * 100), floor)),
                data4: Math.floor(Math.max((Math.random() * 100), floor)),
                data5: Math.floor(Math.max((Math.random() * 100), floor)),
                data6: Math.floor(Math.max((Math.random() * 100), floor)),
                data7: Math.floor(Math.max((Math.random() * 100), floor)),
                data8: Math.floor(Math.max((Math.random() * 100), floor)),
                data9: Math.floor(Math.max((Math.random() * 100), floor))
            });
        }
        return data;
    };
    window.store1 = Ext.create('Ext.data.JsonStore', {
        fields: ['name', 'data1', 'data2', 'data3', 'data4', 'data5', 'data6', 'data7', 'data9', 'data9'],
        data: generateData()
    });
    store1.loadData(generateData(12));
    
    
 var dateNow = new Date();
 dateNow = Ext.Date.add(dateNow, Ext.Date.DAY, 1);
 
 var viewport = Ext.create('Ext.form.Panel', {
            layout:'border',
            renderTo:Ext.get('panel'),
            //title:'blub',
            height:570,
            items:[{
                region:'west',
                id:'west-panel',
                //split:true,
                width: 200,
                height:570,
                //minSize: 175,
               // maxSize: 400,
                frame:false,
                collapsible: false,
                //margins:'35 0 5 5',
               // cmargins:'35 5 5 5',
                layout:'accordion',
                layoutConfig:{
                    animate:true
                },
                items: [{
                    title: i18n.getMsg('nodestree.node'),
                    autoScroll:true,
                    border:false,
                    iconCls:'nav',
                    //height:440,
                    items:[
                    	tree,
                    ]
                },{
                    title: i18n.getMsg('history.dateRange'),
                    border:false,
                    //height:290,
                    //width:190,
                    autoScroll:true,
                    iconCls:'settings',
                    //layout:'fit',
                   // columns:2,
                  // margins:'5 0 0 0',
                    
                    defaults: {
	                    flex: 0,
	                    padding:5,
	                    hideLabel: false,
	                    margins:'5 20 0 0',
	                    //anchor:'100%'
	                },
                    items:[
                    	{
                    		xtype:'label',
                    		forId:'dateFrom',
                    		align:'left',
                    		text:i18n.getMsg('history.from'),
                    		width:50,
                    		id:'fromlabel',
                    	},
                    	 {
		                	//fieldLabel:i18n.getMsg('history.from'),
		                	labelWidth:50,
		                	labelAlign:'left',
		                	//anchor: '100%',
		                	id:'dateFrom',
		                	xtype:'datefield',
		                	width:140,
		                	value: new Date(Date.now() -7),
		                	maxValue: new Date(),
		                	//margins:'5 5 5 5',
		                	//format:'l, M d',
		                },
		                {
                    		xtype:'label',
                    		forId:'dateTo',
                    		align:'left',
                    		text:i18n.getMsg('history.to'),
                    		width:50,
                    		id:'tolabel',
                    		//margins:'5 5 5 5',
                    	},
		                {
		                	//fieldLabel:i18n.getMsg('history.to'),
		                	labelWidth:50,
		                	labelAlign:'left',
		                	//margins:'5 15 0 0',
		                	//anchor: '100%',
		                	id:'dateTo',
		                	xtype:'datefield',
		                	width:140,
		                	value: dateNow,
		                	maxValue: dateNow,
		                	
		                	//value: new Date(Date.now()).add('d',1),
		                	//format:'l, M d',
		                },
		                {
                    		xtype:'label',
                    		forId:'statuses',
                    		align:'left',
                    		text:i18n.getMsg('runningTasks.status'),
                    		width:50,
                    		id:'statusLabel',
                    	},
                    	{
			        		//xtype:'combo',
			        		xtype:'boxselect',
			        		id:'statuses',
			        		resizable: false,
			        		stacked:true,
							autoScroll:false,
							hideTrigger:true,
			        		mode:     'local',
			                value:[
			                	'Done', 'Error', 'Cancelled', 'Expiring'
			                ],
			                triggerAction:  'all',
			                forceSelection: true,
			                allowBlank: 	false,
			                editable:       false,
			                fieldLabel:     '',
			                displayField:   'value',
			                valueField:     'name',
			                queryMode: 		'local',
			                width:180,
			                labelWidth:0,
			                height:110,
			                store:          Ext.create('Ext.data.Store', {
			                  fields : ['name', 'value'],
			                    data   : [
			                        {name : 'Started',   value: i18n.getMsg('runningTasks.status.Started')},
			                        {name : 'Done',  value: i18n.getMsg('runningTasks.status.Done')},
			                        {name : 'Cancelling', value: i18n.getMsg('runningTasks.status.Cancelling')},
			                        {name : 'Cancelled', value: i18n.getMsg('runningTasks.status.Cancelled')},
			                        {name : 'Paused', value: i18n.getMsg('runningTasks.status.Paused')},
			                        {name : 'Expiring', value: i18n.getMsg('runningTasks.status.Expiring')},
			                        {name : 'Expired', value: i18n.getMsg('runningTasks.status.Expired')},
			                        {name : 'Error', value: i18n.getMsg('runningTasks.status.Error')},
			                        {name : 'PendingStart', value: i18n.getMsg('runningTasks.status.PendingStart')},
			                        
			                    ]
			                }),
			                
			        	},
			        	{
                    		xtype:'label',
                    		forId:'sizeType',
                    		align:'left',
                    		text:i18n.getMsg('runningTasks.size'),
                    		width:40,
                    		id:'sizelabel',
                    	},
			        	{
			        		xtype:'fieldset',
			        		layout:'hbox',
			        		border:0,
			        		overlapHeader:true,
			        		frame:false,
			        		//borders:'0 0 0 0',
			        		padding:5,
			        		//margins:'0 0 0 0',
			        		items:[
			        	
			        	{
			        		xtype:'combo',
			        		id:'sizeType',
			        		mode:           'local',
			                value:          'originalSize', //i18n.getMsg('runningTasks.size.original'),
			                triggerAction:  'all',
			                forceSelection: true,
			                allowBlank:		false,
			                editable:       false,
			                //fieldLabel:     'size',
			                displayField:   'value',
			                valueField:     'name',
			                queryMode: 'local',
			                width:70,
			                store:          Ext.create('Ext.data.Store', {
			                  fields : ['name', 'value'],
			                    data   : [
			                        {name : 'originalSize',   value: i18n.getMsg('runningTasks.size.original')},
			                        {name : 'finalSize',  value: i18n.getMsg('runningTasks.size.final')},
			                    ]
			                })
			            },
			            {
			        		xtype:'combo',
			        		id:'sizeOperator',
			        		mode:           'local',
			                value:          '>',
			                triggerAction:  'all',
			                forceSelection: true,
			                allowBlank:false,
			                editable:       false,
			                displayField:   'value',
			                valueField:     'name',
			                queryMode: 'local',
			                width:33,
			                store:          Ext.create('Ext.data.Store', {
			                  fields : ['name', 'value'],
			                    data   : [
			                        {name : '>',   value: '>'},
			                        {name : '<',  value: '<'},
			                    ]
			                })
			            },
			            {
			            	xtype:'numberfield',
                			id: 'size',
                			width:48,
                			value:0,
                			minValue:0,
			            },
			            {
			        		xtype:'combo',
			        		id:'sizeUnit',
			        		mode:           'local',
			                value:          'MB',
			                triggerAction:  'all',
			                forceSelection: true,
			                allowBlank:false,
			                editable:       false,
			                displayField:   'value',
			                valueField:     'name',
			                queryMode: 'local',
			                width:45,
			                store:          Ext.create('Ext.data.Store', {
			                  fields : ['name', 'value'],
			                    data   : [
			                        {name : 'KB',   value: 'KB'},
			                        {name : 'MB',  value: 'MB'},
			                        {name : 'GB',  value: 'GB'},
			                        {name : 'TB',  value: 'TB'},
			                    ]
			                })
			            },
			            ]
			            },
			            
		                {
		                	//margins:'5 0 20 0',
		                	margins:'20 20 20 20',
		                	anchor: '0%',
		                	id:'go',
		                	xtype:'button',
		                	align:'right',
		                	text:'Generate',
		                	icon:'/images/view.png',
		                	width:80,
		                	scale:'small',
		                	//columnSpan:2,
		                	handler:function(){
		                		//alert(tree.getChecked().join(','));
		                		Ext.Msg.show({
			        				title:'Information',
			        				msg:i18n.getMsg('restore.step3.waitMsg'),
			        				buttons:false,
			        				icon:'icon-loading',
			        			});	
			        			var multiplier = 1024*1024;
								if(Ext.getCmp('sizeUnit').getValue() == 'GB')
									multiplier *= 1024;
								if(Ext.getCmp('sizeUnit').getValue() == 'TB')
									multiplier *= 1024*1024;	
								size = Ext.getCmp('size').getValue() * multiplier;
		                		var tasksRaw = tree.getChecked();
		                		var tasks = [];
		                		for(var i=0; i< tasksRaw.length; i++){
		                			if(tasksRaw[i].get('id').substring(0,2) == "bs")
		                				tasks.push(tasksRaw[i].get('id').substring(2));
		                		}
		                		//var statuses = [];
		                		bs=tasks.join(',');
					  			from=Ext.getCmp('dateFrom').getValue();
					  			to=Ext.getCmp('dateTo').getValue();
					  			sizeOperator=Ext.getCmp('sizeOperator').getValue();
					  			statuses=Ext.getCmp('statuses').getValue();
					  			
		                		taskStore.on('load', function(){
				       				Ext.Msg.close();
				       				
				       			});
		                		taskStore.load({
				                	scope   : this,
					  				params:{
					  					/*bs:''+tasks.join(','),
					  					from:Ext.getCmp('dateFrom').getValue(),
					  					to:Ext.getCmp('dateTo').getValue(),
					  					sizeOperator:Ext.getCmp('sizeOperator').getValue(),
					  					size:size,
					  					statuses:Ext.getCmp('statuses').getValue(),*/
					  					bs:bs,
					  					from:from,
					  					to:to,
					  					sizeOperator:sizeOperator,
					  					size:size,
					  					statuses:statuses,
					  					start:0,
					  					limit:13,
					  				},
				       			});
		                	}
		                },
                    ]
                }
               
                ]
            },{
                region:'center',
                //margins:'35 5 5 0',
                layout:'column',
                height:500,
                //width:500,
                //title:'History',
                autoScroll:false,
                overlapHeaders:true,
                items: [grid]
            },{
                region:'east',
                //margins:'35 5 5 0',
                layout:'column',
                height:570,
                width:250,
                //title:'History',
                autoScroll:true,
                defaults: {
                    layout: 'anchor',
                    defaults: {
                        anchor: '100%'
                    }
                },
               // xtype:'fieldset',
                
                items: [
                	{
				        xtype:'fieldset',
				        layout: {
		                    type: 'vbox',
		                    align:'stretch'
		                },
				        margin: '0 0 0 5',
				        width:240,
				        height:185,
				        title: i18n.getMsg('welcome.sgChart.title'),
				        items: [
				        	{
					            xtype: 'chart',
					            title:'backup size',
					            animate: true,
					            shadow: true,
					            theme: 'Green',
					            /*background: {
								    image: '/images/graphbg.png'
								},*/
								insetPadding:5,
					            width:230,
					            height:160,
					            store: store1,
					            /*mask: 'horizontal',*/
						        listeners: {
						            select: {
						                fn: function(me, selection) {
						                    me.setZoom(selection);
						                    me.mask.hide();
						                }
						            }
						        },
					            axes: [{
					                type: 'Numeric',
					                position: 'left',
					                title:'Space',
					                fields: ['data1'],
					                title: false,
					                grid: true
					            }, {
					                type: 'Category',
					                position: 'bottom',
					                title:'Date',
					                fields: ['name'],
					                title: false
					            }],
					            series: [
						            {
						                type: 'line',
						                axis: 'left',
						                gutter: 80,
						                smooth: true,
						                xField: 'name',
						                yField: ['data1'],
						                listeners: {
						                  itemmouseup: function(item) {
						                      Ext.example.msg('Item Selected', item.value[1] + ' visits on ' + Ext.Date.monthNames[item.value[0]]);
						                  }  
						                },
						               tips: {
						                    trackMouse: true,
						                    width: 80,
						                    height: 40,
						                    renderer: function(storeItem, item) {
						                        this.setTitle(storeItem.get('name') + '<br />' + storeItem.get('data1'));
						                    }
						                },
						               
						            }
				        
				        	]
				    	},
                ]
			        },
			        {
				        xtype:'fieldset',
				        layout: {
		                    type: 'vbox',
		                    align:'stretch'
		                },
				        margin: '0 0 0 5',
				        width:240,
				        height:185,
				        title: i18n.getMsg('welcome.sgChart.title'),
				        items: [
				        	{
					            xtype: 'chart',
					            title:'backup size',
					            animate: true,
					            shadow: true,
					            theme: 'Green',
					            /*background: {
								    image: '/images/graphbg.png'
								},*/
								insetPadding:5,
					            width:230,
					            height:160,
					            store: store1,
					            /*mask: 'horizontal',
						        listeners: {
						            select: {
						                fn: function(me, selection) {
						                    me.setZoom(selection);
						                    me.mask.hide();
						                }
						            }
						        },*/
					            axes: [{
					                type: 'Numeric',
					                position: 'left',
					                title:'Space',
					                fields: ['data1'],
					                title: false,
					                grid: true
					            }, {
					                type: 'Category',
					                position: 'bottom',
					                title:'Date',
					                fields: ['name'],
					                title: false
					            }],
					            series: [
						            {
						                type: 'line',
						                axis: 'left',
						                gutter: 80,
						                smooth: true,
						                xField: 'name',
						                yField: ['data1'],
						                listeners: {
						                  itemmouseup: function(item) {
						                      Ext.example.msg('Item Selected', item.value[1] + ' visits on ' + Ext.Date.monthNames[item.value[0]]);
						                  }  
						                },
						               tips: {
						                    trackMouse: true,
						                    width: 80,
						                    height: 40,
						                    renderer: function(storeItem, item) {
						                        this.setTitle(storeItem.get('name') + '<br />' + storeItem.get('data1'));
						                    }
						                },
						               
						            }
				        
				        	]
				    	},
                ]
			        },
			        
			        {
				        xtype:'fieldset',
				        layout: {
		                    type: 'vbox',
		                    align:'stretch'
		                },
				        margin: '0 0 0 5',
				        width:240,
				        height:185,
				        title: i18n.getMsg('welcome.sgChart.title'),
				        items: [
				        	{
					            xtype: 'chart',
					            title:'backup size',
					            animate: true,
					            shadow: true,
					            theme: 'Green',
					            /*background: {
								    image: '/images/graphbg.png'
								},*/
								insetPadding:5,
					            width:230,
					            height:160,
					            store: store1,
					            /*mask: 'horizontal',
						        listeners: {
						            select: {
						                fn: function(me, selection) {
						                    me.setZoom(selection);
						                    me.mask.hide();
						                }
						            }
						        },*/
					            axes: [{
					                type: 'Numeric',
					                position: 'left',
					                title:'Space',
					                fields: ['data1'],
					                title: false,
					                grid: true
					            }, {
					                type: 'Category',
					                position: 'bottom',
					                title:'Date',
					                fields: ['name'],
					                title: false
					            }],
					            series: [
						            {
						                type: 'line',
						                axis: 'left',
						                gutter: 80,
						                smooth: true,
						                xField: 'name',
						                yField: ['data1'],
						                listeners: {
						                  itemmouseup: function(item) {
						                      Ext.example.msg('Item Selected', item.value[1] + ' visits on ' + Ext.Date.monthNames[item.value[0]]);
						                  }  
						                },
						               tips: {
						                    trackMouse: true,
						                    width: 80,
						                    height: 40,
						                    renderer: function(storeItem, item) {
						                        this.setTitle(storeItem.get('name') + '<br />' + storeItem.get('data1'));
						                    }
						                },
						               
						            }
				        
				        	]
				    	},
                ]
			        }
        
        
                ] // end eastpanel items
            }]
        });
    //});
 
 });
 });
 //});