
Ext.onReady(function () {

	//Ext.Loader.setConfig({enabled:true});
	Ext.Loader.setConfig({
            enabled: true,
            disableCaching: false,
            paths: {
                'Extensible': '/Extensible/src',
                'Extensible.example': '/Extensible/examples',
                'Ext.ux':'/ext4/ux',
                'Ext.i18n':'/i18n/'
            }
        });
        Ext.require([ 
    'Ext.data.*',
    'Ext.data.ArrayReader',
    'Ext.grid.*',
    'Ext.tree.*',
    'Ext.form.*',
    'Ext.window.*',
    'Ext.Loader',
    'Ext.ux.BoxSelect',
    'Extensible.form.*',
    'Extensible.data.*' ,
    'Extensible.calendar.data.MemoryCalendarStore',
    'Extensible.calendar.data.EventStore',
    'Extensible.calendar.CalendarPanel',
]);
	var params = Ext.urlDecode(window.location.search.substring(1));
	var lang;
    if(params.lang)
        lang = params.lang;
	//var i18n = new Ext.i18n.Bundle({bundle:'wui', path:'i18n', lang:lang});
	var i18n = Ext.create('Ext.i18n.Bundle',{
		bundle: 'wui',
		lang: lang,
		path: 'i18n',
		noCache: true
	});
i18n.onReady(function(){

	Ext.get('addTitle').dom.innerText = i18n.getMsg('addbs.title');
	Ext.QuickTips.init();

    //Ext.form.Field.prototype.msgTarget = 'side';
    var nodesChecked = [];
    var pathsChecked = [];
	
	Ext.define('NodeM', {
        extend: 'Ext.data.Model',
        fields: [
            {name: 'id',     type: 'string'},
            {name: 'userName',     type: 'string'},
            {name: 'certCN',     type: 'string'},
            {name: 'ip', type: 'string'},
            {name: 'share', type: 'string'},
            {name: 'available', type: 'string'},
            {name: 'percent', type: 'string'},
            {name: 'version', type: 'string'},
            {name: 'os', type: 'string'},
            {name: 'quota', type: 'string'},
            {name: 'usedquota', type: 'string'},
            {name: 'port', type: 'string'},
            {name: 'backupsets', type: 'string'},
        ]
    });

    var nStore = new Ext.data.TreeStore( {
        model: 'NodeM',
        proxy: {
            type: 'ajax',
            url: '/Get.aspx?w=Clients'
        },
        folderSort: true
    });

    var nodesTree = new Ext.tree.Panel({
        title: '<img src="/images/1.png" class="gIcon"/>'+i18n.getMsg('clientnodes.title'),
        id:'nodesTtree',
        height: 450,
        layout:'fit',
        collapsible: false,
        useArrows: true,
        rootVisible: false,
        store: nStore,
        multiSelect: true,
        singleExpand: false,
        draggable:false,    
        stateful:true,   
        columns: [{
            xtype: 'treecolumn', //this is so we know which column will show the tree
            text: i18n.getMsg('nodestree.node'),
            flex: 1,
            sortable: true,
            dataIndex: 'userName',
            renderer: function(value, metaData, record, colIndex, store, view){
	            if(record.get('certCN').length > 1)
	            	return value+" (<i>"+record.get('certCN')+"</i>)";
	            else
	            	return value;
            }
        },{
            text: i18n.getMsg('nodestree.currentIP'),
            flex: 0,
            width:150,
            sortable: true,
            dataIndex: 'ip',
        },{
            text: ' ',
            flex: 0,
            width:5,
            sortable: false,
            dataIndex: '',
            style:'border:1px 1px 1px 1px solid gray;',
            menuDisabled:true,
        },{
            text: i18n.getMsg('nodestree.listenIP'),
            flex: 0,
            width:150,
            sortable: true,
            dataIndex: 'ip',
        },{
            text: i18n.getMsg('nodestree.listenPort'),
            flex: 0,
            width:60,
            dataIndex: 'port',
            sortable: true
        },{
            text: i18n.getMsg('nodestree.version'),
            flex: 0,
            width:60,
            dataIndex: 'version',
            sortable: true
        },{
            text: i18n.getMsg('nodestree.os'),
            flex: 0,
            dataIndex: 'os',
            width:40,
            sortable: true,
            renderer:function(value){
            	if(value.toLowerCase() == 'linux')
            		return '<img src="/images/Linux-xs.png" title="'+value+'"/>';
            	else if(value.substr(0,2) == 'NT')
            		return '<img src="/images/Windows-xs.png" title="'+value+'"/>';
            	else if(value.toLowerCase() == 'freebsd')
            		return '<img src="/images/Freebsd-xs.jpg" title="'+value+'"/>';
            	else if(value.toLowerCase() == 'darwin')
            		return '<img src="/images/Apple-xs.png" title="'+value+'"/>';
            	else if(value.toLowerCase() == 'sunos')
            		return '<img src="/images/Sunos-xs.jpg" title="'+value+'"/>';
            	else if(value.length > 1)
            		return '<img src="/images/Unknown-xs.png" title="Unknown os : '+value+'"/>';
            }
        },{
            text: i18n.getMsg('nodestree.backupsets'),
            flex: 0,
            dataIndex: 'backupsets',
            width:70,
            sortable: true,
            renderer: function(value){
            	if(value > 0){
            		return '<div title="'+i18n.getMsg('nodestree.moreDetailsMsg')+'" style="padding-left:15px;background:url(/images/plus.gif) no-repeat;background-position:left;">'+value+'</div>';
            	}
            	else
            		return '<div style="padding-left:15px;">'+value+'</div>';
            }
           /* items: [{
	            	icon:'/images/plus.gif',
	            	tooltip:'Click for backups details',
	            	}
            	]*/
        },{
            text: i18n.getMsg('nodestree.quota'),
            flex: 0,
            dataIndex: 'quota',
            width:60,
            sortable: true
        },{
            text: i18n.getMsg('nodestree.usedQuota'),
            flex: 0,
            width:70,
            dataIndex: 'usedquota',
            sortable: true
        },{
            text: i18n.getMsg('nodestree.certificate'),
            flex: 0,
            dataIndex: '',
            width:40,
            sortable: true,
            renderer:function(value){
            	// <TODO> warning/error status when sthg is wrong with cert policy
            	return '<img src="/images/security-high.png"/>';
            }
        },{
            text: i18n.getMsg('nodestree.delegations'),
            flex: 1,
            dataIndex: '',
            sortable: true
        }
        ],
        listeners:{
        	'checkchange': function(node, checked){        	
		       	if (nodesTree.getChecked().length == 1)
		       		Ext.getCmp('browseBtn').enable();
	    		else
	    			Ext.getCmp('browseBtn').disable();
          	}
        }
    });				 
    
    Paths = Ext.define('Paths', {
        extend: 'Ext.data.Model',
        fields: ['path', 'includerule', 'excluderule']
    });
    
    var p = new Ext.data.proxy.Client({ });
	/*var pathsStore = new Ext.data.Store({
		autoLoad:false,
        model: 'Paths',
        //proxy:p
        proxy: {
            type: 'ajax',
            url: '/Get.aspx?w=Browse',
            extraParams: {isXml: true},
            reader: {
                type: 'xml',
                root: 'root',
                record: 'd',
               	//idProperty: '@n',
            }
        },
        root: {
            text: '/',
            id: '//',
            value:'/',
            expanded: true,
        }
    });*/
	Ext.define('PathModel', {
        extend: 'Ext.data.Model',        
        fields: [
            {name: 'path', type: 'string', mapping:'@fn'},
            {name: 'text', type: 'string', mapping:'@n'},
            {name: 'cls', type: 'string', mapping:'@type'}
          	//{name: 'checked', type: 'string'},
          	//{name: 'iconCls', type: 'string', mapping:'@type'},
          	//{name: 'size', type: 'numeric', mapping:'@size'},
          	//{name: 'avail', type: 'numeric', mapping:'@avail'},
          	//{name: 'label', type: 'string', mapping:'@label'},
          	//{name: 'fs', type: 'string', mapping:'@fs'},
        ]
    });
     var pathsStore = new Ext.data.Store( {
     	model:'PathModel',
     	clearOnLoad:false,
        proxy: {
            type: 'ajax',
            url: '/Get.aspx?w=Browse&flat=true&curNode=1',
            extraParams: {isXml: true},
            reader: {
                type: 'xml',
                root: 'root',
                record: 'd',
               	//idProperty: '@n',
            }
        },
       root: {
            text: '/',
            id: '//',
            value:'/',
            expanded: true,
        }
    });
	var valz = new Array();
	function setPaths(valuez){		
    	for(var i=0; i<valuez.length; i++){
    		pathsStore.add({path:valuez[i]});
    		valz.push(valuez[i]);
    	}
    	Ext.getCmp('selectF').setValue(valz);
	}
	     	    	    	    	    
    var fs = new Ext.widget('form', { //new Ext.form.Panel({
    	id:'fs',
        title:'<img src="/images/2.png" class="gIcon"/>'+i18n.getMsg('addbs.whatToBackup.title'), // Configure what to backup and how'
        bodyStyle: 'padding:10px;',
        waitMsgTarget: true,
		fieldDefaults: {
	        labelAlign: 'left',
	        labelWidth:140,
	        hideLabel: false
	    },
	    defaults: {
	        labelAlign: 'left',
	        hideLabel: false
	    },
        items: [
        	{
        		xtype: 'fieldset',
                title: i18n.getMsg('addbs.whatToBackup.title1'), //'Directories and files to backup',
                border: true,
	            //collapsible:true,
	            height:180,
				anchor: '100%',
                layout: 'anchor', 
                defaultType: 'textfield',
                defaults: {
	                   labelAlign:'left',
	                    hideLabel: false,
	                },
	                fieldDefaults: {
	                   labelAlign:'left',
	                    hideLabel: false,
	                },
                items: [
           		{
                	xtype: 'fieldcontainer',
                	layout: 'hbox',
                	defaultMargins: {top: 0, right: 15, bottom: 0, left: 15},
            		items:[ 	
                		{
                            xtype: 'label',
                            text: i18n.getMsg('addbs.whatToBackup.path'), //'root path',
                            width:130,
                    	},
	                    {
	                    xtype:'boxselect',
							id:'selectF',
							resizable: false,
							autoScroll:false,
							editable:true,
							forceSelection:false,
							//hideTrigger:true,
							style:'padding:5px; margin-left:5px; overflow-y:auto;',
							bodyStyle:'overflow-y:auto;',
							name: 'to[]',
							width:320,
							height:100,
							store: pathsStore,
							queryParam:'path',
							queryMode:'remote',
							//typeAhead:false,
							//mode: 'local',
							//displayField: 'path',
							//displayFieldTpl: '<img src="/images/f-g.png"/>{path}',
							labelTpl: '<img src="/images/f-g.png" valign="middle" height="16px" border="0"/>&nbsp;{path}',
							valueField: 'path',
							addUniqueValues: true,
							//value: 	new Ext.data.reader.Array({id:'0', model:'Paths'}, Paths).readRecords(pathsStore.getRange(0,5)).records
							/*listeners:{
								specialkey: function(field, e){
				                    // e.HOME, e.END, e.PAGE_UP, e.PAGE_DOWN,
				                    // e.TAB, e.ESC, arrow keys: e.LEFT, e.RIGHT, e.UP, e.DOWN
				                    if (e.getKey() == e.NUM_DIVISION) {
										//alert('space pressed');
										//nodesTree.getChecked()[0].data['id']
											this.store.setProxy( new Ext.data.proxy.Ajax({
							            	model:'Paths',
								            type: 'ajax',
								            url: '/Get.aspx?w=Browse&curNode=1',
								            extraParams: {
								                isXml: true
								            },
								            reader: {
								                type: 'xml',
								                root: 'root',
								                record: 'd',
								            }
							            }));
				                    }
				                }
							}*/
							
						},
	                    new Ext.Button( {
			                 width:25,
			                 height:25,
			                 id: 'browseBtn',
			                 icon:'/images/browse.png', 
			                 flex:0,
			                 handler: function(){
			                 	var selectedNode = nodesTree.getChecked()[0].data['id'];
			                 	handleBrowse(selectedNode, setPaths, true);
			                 },
			                 descriptionText:i18n.getMsg('addbs.whatToBackup.browse'), 
			                 initComponent: function() {
								   if(nodesTree.getChecked().length > 1 || nodesTree.getChecked().length == 0) // if several nodes are selectionned, we disable browsing as it makes no sense.
										this.disable();
							 },
							 style:'margin-top:50px;margin-left:15px;'
						}),
						{
							xtype: 'fieldcontainer',
		                	margins: {top: 30, right: 15, bottom: 0, left: 40},
		            		items:[ 	
								{
				               		xtype:'checkbox',
				               		id:'useSnapshots',
				               		boxLabel:i18n.getMsg('addbs.whatToBackup.useSnapshots'),
				               		checked:true,
				               		listeners:{
				               			change:function(thisCB, newValue, oldValue, options){
				               				if(newValue == true)
				               					Ext.getCmp('keepSnapshots').enable();
				               				else
				               					Ext.getCmp('keepSnapshots').disable();
				               			}
				               		}
				               	},
				               	{
				               		xtype:'checkbox',
				               		id:'keepSnapshots',
				               		boxLabel:i18n.getMsg('addbs.whatToBackup.keepSnapshots'),
				               		checked:true,
				               	}
				            ]
			            }
																								
					]		
                 //}),
               	},
               	{
               		xtype:'checkbox',
               		id:'backupPermissions',
               		boxLabel:i18n.getMsg('addbs.whatToBackup.permissions'),
               		checked:true,
               	}
              ]
              },
                new Ext.form.FieldSet({
                title: i18n.getMsg('addbs.whatToBackup.title2'), //'Files and subdirectories selection rules',
                defaultType: 'textfield',
                items: [
                	 {
	                	xtype: 'fieldcontainer',
	                	layout: 'hbox',
		                defaultType: 'textfield',
                		items:[ 
		                	{
		                        xtype: 'label',
		                        text: i18n.getMsg('addbs.whatToBackup.includeRule'), //'root path',
		                        width:140,
		                    },
		                    {
		                        emptyText: 'enter a rule',
		                        name: 'includePolicy',
		                        width:200
		                    }
		                ]
		            },
                    {
	                	xtype: 'fieldcontainer',
	                	layout: 'hbox',
		                defaultType: 'textfield',
	                	items:[ 
		                    {
		                        xtype: 'label',
		                        text:i18n.getMsg('addbs.whatToBackup.excludeRule'), 
		                        width:140,
		                    },
		                    {
		                        emptyText: 'enter a rule',
		                        name: 'excludePolicy',
		                        width:200
		                    }
		                ]
		            }
                ]
            }),
            
           	new Ext.form.FieldSet({
                title: i18n.getMsg('addbs.whatToBackup.title3'), //'Storage options',
                autoHeight: true,
                layout: "column", 
                defaultType: 'textfield',
                items: [
                 		{
	                 		xtype:'checkbox',
	                        boxLabel:i18n.getMsg('addbs.whatToBackup.encrypt'), //'Encrypt',
	                        columnWidth: .1,
	                        id:'encrypt'
                    	},
                    	{
	                 		xtype:'checkbox',
	                        columnWidth: .1,
	                        id:'compress',
							boxLabel:i18n.getMsg('addbs.whatToBackup.compress'), //'Compress',
	                        checked:true
                    	},
                    	{
	                 		xtype:'checkbox',
	                        columnWidth: .1,
	                        id:'clientdedup',
							boxLabel:i18n.getMsg('addbs.whatToBackup.clientdedup'), //'Compress',
	                        checked:true
                    	},
                    	{
	                 		xtype:'checkbox',
	                        columnWidth: .1,
	                        id:'storagededup',
							boxLabel:i18n.getMsg('addbs.whatToBackup.storagededup'), //'Compress',
	                        checked:false
                    	}
                ]
            }) 
          ]
          });
            
        // 3 - preops and postops
        var opsPanel = new Ext.form.Panel({
    	id:'opsPanel',
        frame: false,
        title:'<img src="/images/3.png" class="gIcon"/> '+i18n.getMsg('addbs.ops.title'), //Configure pre and post-backup custom operations',
        labelAlign: 'right',
        labelWidth: 85,
        autoWidth:true,
        autoHeight:true,
        bodyStyle: 'padding:10px;',
        waitMsgTarget: true,
        items: [
        	   new Ext.form.FieldSet({
                title: i18n.getMsg('addbs.ops.title1'), //'Commands to execute on client node',
                autoHeight: true,
                layout: "column", 
                height: 190,
                defaultType: 'textfield',
                items: [
                	{
                            xtype: 'label',
                            text: i18n.getMsg('addbs.ops.pre'), //'Pre-backup operations',
                            columnWidth: .10
                    },
                	{
                		xtype:'textarea',
                        fieldLabel: '',
                        label: '',
                        textLabel: '',
                        emptyText: 'put script or commands to execute',
                        name: 'preops',
                        columnWidth: .40,
                        height:150,
                        align:'left',
                        autoCreate: {
							tag: "textarea",
							rows:15,
							height: 80,
							columns:10,
							autocomplete: "off",
							wrap: "off"
						},
                    },
                    {
                            xtype: 'label',
                            text: i18n.getMsg('addbs.ops.post'), //'Post-backup operations',
                            columnWidth: .10
                    },
                	{
                		xtype:'textarea',
                        fieldLabel: '',
                        label: '',
                        textLabel: '',
                        emptyText: 'put script or commands to execute',
                        name: 'postops',
                        columnWidth: .40,
                        height:150,
                        align:'left',
                        autoCreate: {
							tag: "textarea",
							rows:15,
							height: 80,
							columns:10,
							autocomplete: "off",
							wrap: "off"
						},
                    },
                ]
                }),
                {
                        xtype: 'checkbox',
                        id:'keepShellOpen',
                        boxLabel: i18n.getMsg('addbs.ops.keepShellOpen'), 
                },
                {
                        xtype: 'checkbox',
                        id:'noBackupIfShellError',
                        boxLabel: i18n.getMsg('addbs.ops.noBackupIfShellError'), 
                },
           ]
    });
    // 4 - scheduling
    var schedP = new Ext.data.proxy.Client({  });
    var schedStore = new Extensible.calendar.data.EventStore({
	       autoLoad: true,
	       proxy:schedP
	});
	var calStore = new Extensible.calendar.data.MemoryCalendarStore({
            data:[{
                id:0,
                title:"Fulls",
                color:2
            },{
                id:1,
                title:"Differentials",
                color:8
            },{
                id:2,
                title:"Incrementals",
                color:10
            },
            {
                id:3,
                title:"BSDiffs",
                color:31
            },]
	}); 
	    
     var schedPanel = new Ext.form.Panel({
    	id:'schedPanel',
        frame: false,
        title:'<img src="/images/4.png" class="gIcon"/>'+i18n.getMsg('addbs.sched.title'), // Choose when and how to backup',
        bodyStyle: 'padding:10px;',
        waitMsgTarget: true,
     	items:[
     		{
	            xtype: 'radiogroup',
	            title:' ',
	            hidden:false,
	            height:25,
	            border:true,
	            frame:true,
	            layout:'hbox',
	            margins:{top:0},
	            items: [
	                {
		                boxLabel: i18n.getMsg('addbs.sched.useSchedulingPeriodic'),
		                name: 'schedType',
		                inputValue: '0',
		                width:680,
		                checked:true,
		            }, {
		                boxLabel: i18n.getMsg('addbs.sched.useSchedulingCDP'),
		                name: 'schedType',
		                inputValue: '1',
		                width:350,
		            }
				 ],
				 listeners:{
                	change:function(thiscombo, newValue, oldValue, options){
                		if(newValue['schedType'] == 0){
                			Ext.getCmp('backupScheduleCalendar').enable();
                			Ext.getCmp('schedCDPFields').disable();
            			}
            			else
            				Ext.getCmp('backupScheduleCalendar').disable();
            				Ext.getCmp('schedCDPFields').enable();
            		}
            	},
             }, // end radiogroup
             {
             xtype:'fieldcontainer',
             layout:'table',
             columns:2,
            // width:660,
             //height:460,
             items:[
           
	     		new Extensible.calendar.CalendarPanel( {
	     			calendarStore:calStore,
			        eventStore: schedStore,
			        id:'backupScheduleCalendar',
			        autoRender:true,
			        height:440,
			        width:650,
			        showDayView:true,
			        showWeekView :true,
			        showMonthView :true,
			        showHeader:true,
			        showNavNextPrev:false,
			        showTodayText:false,
			        todayText:i18n.getMsg('addbs.sched.recurrence'),//'Recurrence :',
			        dayText:i18n.getMsg('addbs.sched.everyDay'),
			        weekText:i18n.getMsg('addbs.sched.everyWeek'),
			        monthText:i18n.getMsg('addbs.sched.everyMonth'),
			        showTime:false,
			        weekViewCfg:{
			        	dayCount:7,
			        	ddIncrement:15,
			        	scrollStartHour:0,
			        	hideBorders:false,
			        	showHeader:true,
			        	showWeekLinks:true,
			        	hourHeight:25,
			        	startDay:1,
			        	showTime:false,
			        	enableContextMenus:false,
			        	
			        },
			        monthViewCfg:{
			        	defaultEventTitleText:'',
			        	showHeader:true,
			        	showWeekLinks:true,
			        	prevMonthCls:'cal-disabled-day',
			        	nextMonthCls:'cal-disabled-day',
			        	startDay:1, //start on monday
			        	enableContextMenus:false,
			        },
			        viewCfg:{
			        	showHeader:true,
			        	showWeekLinks:true,
			        	enableContextMenus:false,
			        },
			        showMultiWeekView:false,
			        showNavJump:false, 
			        enableEditDetails:false,
			        listeners:{
					    dayclick:function(thisCal,clickedDate, isAallday, extElement){
					    	
					    	backupTimeConf(clickedDate, null);
					    	return false;
					    },
					    rangeselect: function(thisCal, datesObject, callbackFn){
					    	backupTimeConf(datesObject.StartDate, datesObject.EndDate);
					    	return false;
					    },
					    eventClick:function(thisCal, eventRecord, htmlEl){
					    	return false;
					    },
					    eventresize:function(thisCal, eventRecord){
					    	//alert('resize: rec '+eventRecord.get('StartDate')+' , '+eventRecord.get('EndDate'));
					    	//eventRecord.set(EndDate,);
					    	schedStore.sync();
					    	thisCal.refresh(true);
					    	//Ext.getCmp('backupScheduleCalendar')
					   	},
					   	eventmove:function(thisCal, eventRecord){
					    	//alert('resize: rec '+eventRecord.get('StartDate')+' , '+eventRecord.get('EndDate'));
					    	//eventRecord.set(EndDate,);
					    	schedStore.sync();
					    	thisCal.refresh(true);
					    	//Ext.getCmp('backupScheduleCalendar')
					   	},
					}
				}),
				{
					xtype:'panel',
					id:'schedCDPFields',
					margins:{left:35, top:0},
					//bodyMargins:{left:15},
					hideLabels:true,
					border:false,
					frame:false,
					layout:'table',
					rowspan:2,
					columns:2,
					disabled:true,
					items:
					[
						{
							xtype:'displayfield',
							value:'maximum backup frequency',
							width:180,
							padding:{left:30},
						},
						{
							xtype:'combo',
							label:'maximum backup frequency',
							mode:           'local',
                            value:          '15',
                            triggerAction:  'all',
                            forceSelection: true,
                            allowBlank:false,
                            editable:false,
                            fieldLabel:'',
                            id:           'cdpFrequency',
                            displayField:   'name',
                            valueField:     'value',
                            queryMode: 'local',
                            labelWidth:250,
                            store:          Ext.create('Ext.data.Store', {
                              fields : ['name', 'value'],
                                data   : [
                                	{name : '5 mn',   value: '5'},
                                    {name : '10 mn',   value: '10'},
                                    {name : '15 mn',  value: '15'},
                                    {name : '20 mn', value: '20'},
                                    {name : '30 mn', value: '30'},
                                    {name : '45 mn', value: '45'},
                                    {name : '60 mn', value: '60'},
                                ]
                            })
						}
					]
				}
			
			
			
			
			]
			}
			
			
			
			
			
			
     	]
    });
    	
    	
    var stoPanel = new Ext.form.Panel({
    	id:'stoPanel',
        frame: false,
        title:'<img src="/images/5.png" class="gIcon"/> Configure storage and archiving policy',
        labelAlign: 'left',
        labelWidth: 95,
        bodyStyle: 'padding:10px;',
        waitMsgTarget: true,
        items: [
        		new Ext.form.FieldSet({
                title: 'Redundancy',
                autoHeight: true,
                layout: "column", 
                defaultType: 'textfield',
                items: [
                	{
                        xtype: 'label',
                        text: 'Store ',
                        columnWidth: .05
                    },
                    new Ext.form.ComboBox({
		    			columnWidth: .04,
		                height:24,
		                align:'left',
		                id:'redundancy',
		                store: new Ext.data.ArrayStore({
		                    fields: ['bType'],
		                    data : [['1'],['2'],['3']]
		                }),
		                valueField:'bType',
		                displayField:'bType',
		                typeAhead: true,
		                mode: 'local',
		                triggerAction: 'all',
		                emptyText:'1',
		                selectOnFocus:true,
		                width:50
		        	}),
                    {
                        xtype: 'label',
                        text: 'copies. ',
                        columnWidth: .05
                    }
                ]
                }),
                
        	   new Ext.form.FieldSet({
                title: 'Retention',
                autoHeight: true,
                layout: "column", 
                defaultType: 'textfield',
                items: [
                	{
                        xtype: 'label',
                        text: 'Keep full sets during ',
                        columnWidth: .2
                    },
                    new Ext.form.ComboBox({
		    			columnWidth: .09,
		                height:24,
		                align:'left',
		                id:'retention',
		                store: new Ext.data.ArrayStore({
		                    fields: ['bType', 'daysV'],
		                    data : [['1 week (7 days)','7'],['2 weeks (14 days)','14'],['3 weeks (21 days)','21'],['1 month (31 days)','31'],['2 months (62 days)','61'], ['6 months (183 days)','183'], ['1 year (365 days)','365'], ['2 years (731 days)','731']]
		                }),
		                valueField:'daysV',
		                displayField:'bType',
		                typeAhead: true,
		                mode: 'local',
		                triggerAction: 'all',
		                emptyText:'1 month',
		                selectOnFocus:true,
		                width:50
		        	})
                ]
                }),
                new Ext.form.FieldSet({
	                title: 'Archiving',
	                autoHeight: true,
	                layout: "column", 
	                defaultType: 'textfield',
	                items: [
	                	{
	                            xtype: 'label',
	                            text: 'After normal retention period: ',
	                            columnWidth: .1
	                    },
	                ]
                })
          ],
          buttons:[
     			{
     			id:'create',
     			text:'Create!',
     			handler:function(){
					var nodesTreeV = Ext.getCmp('nodesTree').getChecked();
					var fsV = fs.getForm().getValues(); 
					var opsPanelV = opsPanel.getForm().getValues(); 
					var schedPanelV = schedPanel.getForm().getValues(); 
					var stoPanelV = stoPanel.getForm().getValues(); 
					var pampelle = [];
					pampelle.push(Ext.encode(nodesChecked));
					pampelle.push(Ext.encode(pathsChecked));
					pampelle.push(Ext.encode(fsV));
					pampelle.push(Ext.encode(opsPanelV));
					pampelle.push(Ext.encode(schedPanelV));
					pampelle.push(Ext.encode(stoPanelV));
					//alert('pampelle='+pampelle);
					
					var postdata = "";
					var conn = new Ext.data.Connection(); 
				    conn.request({ 
				            url: 'Set.aspx?w=AddBackupSet', 
				            method: 'POST', 
				            scope: this, 
				            params: pampelle, 
				            success: function(responseObject) { 
				             var okMsg = Ext.Msg.alert('Status', responseObject.responseText); 
				             //Ext.getCmp('create').disable();
				            }, 
				             failure: function(responseObject) { 
				                 Ext.Msg.alert('Status', 'Unable to save changes. Error:'+responseObject.responseText); 
				            } 
				    }); 
				}	
     			},
     			{text:'Cancel'}
     		]
    });
         
    var viewport = new Ext.form.Panel({
    	renderTo: Ext.get('panel'),
        layout: 'accordion',
        layoutConfig:{animate:true},
        fieldDefaults: {
                        labelAlign: 'left',
                        hideLabel: false
        },
        height:615,
        items: [nodesTree, fs, opsPanel, schedPanel, stoPanel]
	});


function backupTimeConf(startDate, endDate){
	var timeDayNumbers = new Ext.form.FieldContainer({
     	id:'timeDayNumbers',
     	layout: {type: 'table', columns: 4},
     	items:[
     		{
        		xtype:'displayfield',
        		id:'startDayLabel',
        		width:90,
        		align:'left'
        	},
        	{
                xtype: 'numberfield',
                id:'startDay',
                allowDecimals:false,
                allowBlank:false,
                align:'left',
                minValue:1,
                maxValue:31,
                width:40,
     		},
     		{
        		xtype:'displayfield',
        		id:'endDayLabel',
        		value:i18n.getMsg('addbs.sched.toDay'), //'to day',
        		width:70,
        	},
        	{
                xtype: 'numberfield',
                id:'endDay',
                allowDecimals:false,
                allowBlank:false,
                minValue:1,
                maxValue:31,
                width:40
     		},
     	]
     });
     
     var timeDaysOfWeek = new Ext.form.FieldContainer({
     	id:'timeDaysOfWeek',
     	layout: {type: 'table', columns: 4},
     	items:[
     		{
        		xtype:'displayfield',
        		id:'startDayLabel',
        		value:i18n.getMsg('addbs.sched.every'), //'Every',
        		width:90,
        		colspan:1
        	},
        	{
                xtype: 'combo',
                id:'weekDayName',
                width:90,
				mode:           'local',
                triggerAction:  'all',
                forceSelection: true,
                allowBlank:false,
                editable:       false,
                displayField:   'value',
                valueField:     'name',
                queryMode: 'local',
                width:150,
                store:          Ext.create('Ext.data.Store', {
                  	fields : ['name', 'value'],
                    data   : [
                        {name : '1',  value: i18n.getMsg('generic.monday')},
                        {name : '2',  value: i18n.getMsg('generic.tuesday')},
                        {name : '3',  value: i18n.getMsg('generic.wednesday')},
                        {name : '4',  value: i18n.getMsg('generic.thursday')},
                        {name : '5',  value: i18n.getMsg('generic.friday')},
                        {name : '6',  value: i18n.getMsg('generic.saturday')},
                        {name : '7',  value: i18n.getMsg('generic.sunday')}
                    ]
                }),
                colspan:3
     		},
     	]
     });
     
     var timeGenericPart = new Ext.form.FieldContainer({
     	id:'timeGenericPart',
     	layout: {type: 'table', columns: 4},
     	items:[
     		{
        		xtype:'displayfield',
        		id:'typeLabel',
        		value:i18n.getMsg('addbs.sched.backupType'), //'Backup type',
        		width:120
        	},
     		{
     			xtype:'combo',
                height:24,
                align:'left',
                id:'bType',
                store: new Ext.data.ArrayStore({
                    fields: ['bType'],
                    data : [['Full'],['Incremental'],['Differential'],['BSDiff']]
                }),
                valueField:'bType',
                displayField:'bType',
                typeAhead: true,
                mode: 'local',
                triggerAction: 'all',
                emptyText:'...',
                selectOnFocus:true,
                width:90,
                colspan:3,
        	},
        	{
        		xtype:'displayfield',
        		id:'startAtLabel',
        		value:i18n.getMsg('addbs.sched.startAt'), //'Start backup at',
        		width:120
        	},
        	{
        		xtype:'timefield',
        		id:'startAt',
        		format:'H:i',
        		increment: 30,
        		width:90,
        		colspan:3
        	},
        	{
        		xtype:'displayfield',
        		id:'endAtLabel',
        		value:i18n.getMsg('addbs.sched.endAt'), //'Backup window ends at',
        		width:150
        	},
        	{
        		xtype:'timefield',
        		id:'endAt',
        		format:'H:i',
        		increment: 30,
        		width:90,
        		colspan:3
        	},
     	]
     });
	var bTimeWin = new Ext.Window({ 
 		id:'bTimeWin',
        width:470,
       	height:230,
        plain: true,
        title:i18n.getMsg('addbs.sched.title1'), 
		autoScroll:false,
        modal:true,
        autoDestroy :true,
        monitorValid:true,
        resizable:true,
		items: [],
        buttons: [{
                text:'Ok',
                disabled:false,
                handler:function(){
                	var dt = new Date();
                	var theStartDay = Ext.getCmp('startDay').getRawValue();
                	if(theStartDay > 0 && theStartDay < 10) theStartDay = '0'+ theStartDay;
                	var theEndDay = Ext.getCmp('endDay').getRawValue();
                	if(theEndDay > 0 && theEndDay < 10) theEndDay = '0'+ theEndDay;
                	var sDate = /*new Date(*/Ext.Date.format(dt, 'Y-m')+'-'+theStartDay+' '+Ext.getCmp('startAt').getRawValue();//);
                	var eDate = null;
                	if(theEndDay.length <1) 	// 1-day backup
                		eDate = /*new Date(*/Ext.Date.format(dt, 'Y-m')+'-'+theStartDay+' '+Ext.getCmp('endAt').getRawValue(); //);
                	else if(Ext.getCmp('weekDayName').getValue() == null) 				//backup is selected for several days
                		eDate = /*new Date(*/Ext.Date.format(dt, 'Y-m')+'-'+theEndDay+' '+Ext.getCmp('endAt').getRawValue();//);
                	else { 											//backup is recurring the 'd' day every week
                		alert('pipule');
                	}
                	var newId = Math.floor(Math.random()*1001); //Ext.getCmp('schedStore').getTotalCount()+1;
                	var typeTitle = Ext.getCmp('bType').getRawValue();
                	var t1combobox = Ext.getCmp('bType');
	                var t1v = t1combobox.getValue();
					var t1record = t1combobox.findRecord(t1combobox.valueField || t1combobox.displayField, t1v);
					var t1index = t1combobox.store.indexOf(t1record);
					//alert('EventId:'+newId+', CalendarId:'+t1index+', StartDate:'+sDate+', EndDate:'+eDate+', Title:'+typeTitle);
                	schedStore.add({EventId:newId, CalendarId:t1index, StartDate:''+sDate, EndDate:''+eDate, Title:typeTitle});
                	bTimeWin.close();
                }
            },{
                text: 'Close',
                handler: function(){
                    //winBrowse.hide();
                    bTimeWin.close();
                }
            }
        ] 
     }); // end bTimeWin
     
     var timeWinPanel =  new Ext.form.Panel({
		id:'schedCalPanel',
        monitorValid:true,
        border: false,
        autoScroll:false,
        bodyPadding: 10,
        bodyStyle:'font-size:0.9em !important;',
		height:'180',
        fieldDefaults: {
            labelAlign: 'left',
            labelWidth: 90,
            labelStyle: 'font-size:0.9em !important;'
        },
        defaults: {
            padding:{bottom:'0px', top:'5px', left:'10px', right:'5px'},
            margins:{bottom:'0px'}
        },
        items: []
	});
			
     if(endDate == null || Ext.Date.format(startDate, 'd') != Ext.Date.format(endDate, 'd')){
     	timeWinPanel.add(timeDayNumbers);
     	if(endDate == null){
     		Ext.getCmp('endDayLabel').hide();
     		Ext.getCmp('endDay').hide();
     		Ext.getCmp('startDayLabel').setValue(i18n.getMsg('addbs.sched.day'));
     		Ext.getCmp('startDayLabel').show();
     	}
     	else{
     		Ext.getCmp('endDay').setValue(Ext.Date.format(endDate, 'd'));
     		//if(Ext.getCmp('endDay').setValue(Ext.Date.format(endDate, 'd'))
     		Ext.getCmp('endAt').setValue(endDate);
     		Ext.getCmp('startDayLabel').setValue(i18n.getMsg('addbs.sched.fromDay')); 
     	}
     }
     else if(Ext.Date.format(startDate, 'd') == Ext.Date.format(endDate, 'd')){
     	timeWinPanel.add(timeDaysOfWeek);
     	Ext.getCmp('endAt').setValue(endDate);
		Ext.getCmp('weekDayName').setValue(Ext.Date.format(endDate, 'N'));
     }
     Ext.getCmp('startDay').setValue(Ext.Date.format(startDate, 'd'));
     Ext.getCmp('startAt').setValue(startDate);
     timeWinPanel.add(timeGenericPart);
     bTimeWin.add(timeWinPanel);
     
	 bTimeWin.show();
 } // end backupTimeConf()


}); //end i18n
});

