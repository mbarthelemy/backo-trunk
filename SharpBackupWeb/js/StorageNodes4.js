Ext.require([
    'Ext.data.*',
    'Ext.grid.*',
    'Ext.tree.*',
    'Ext.form.*',
    'Ext.window.*',
    'Ext.chart.*',
    'Ext.fx.target.Sprite',
]);

function GetConfigWindow(){

}


Ext.onReady(function () {
	Ext.Loader.setConfig({enabled:true});
	var params = Ext.urlDecode(window.location.search.substring(1));
	var lang;
    if(params.lang)
        lang = params.lang;
	/*var i18n = new Ext.i18n.Bundle({
        bundle:'wui', 
        path:'i18n',
        lang:lang
    });*/
     var i18n = Ext.create('Ext.i18n.Bundle',{
		bundle: 'wui',
		lang: lang,
		path: 'i18n',
		noCache: true
	});

i18n.onReady(function(){
	var toBeExpanded = true;
	 
    Ext.define('sgM', {
        extend: 'Ext.data.Model',
        fields: [
            {name: 'id',     type: 'string'},
            {name: 'userName',     type: 'string'},
            {name: 'certCN',     type: 'string'},
            {name: 'ip', type: 'string'},
            {name: 'port', type: 'string'},
            {name: 'priority', type: 'string'},
            {name: 'share', type: 'number'},
            {name: 'available', type: 'number'},
            {name: 'percent', type: 'string'},
            {name: 'version', type: 'string'},
            {name: 'os', type: 'string'},
            {name: 'cls', type: 'string'},
            {name: 'listenip', type: 'string'},
            {name: 'port', 			type: 'number'},
	        {name: 'storagegroup', 	type: 'number'},
	        {name: 'status', type: 'string'},
        	{name: 'lastconnection', type: 'datetime'},
        	{name: 'capabilities', type: 'string'},
        ]
    });
	var fieldz=[];
	
	
    var sgStore = new Ext.data.TreeStore( {
        model: 'sgM',
        storeId:'sgStore',
        autoLoad:true,
        proxy: {
            type: 'ajax',
            url: '/Get.aspx?w=StorageNodes'
        },
       
    });
    
	
	
    var tree = new Ext.tree.Panel({
       /* title: i18n.getMsg('storagenodes.title'), */
        id:'clientNodesTree',
        layout:'fit',
        height: 500,
        width:900,
        //renderTo: Ext.get('panel'),
        overlapHeader:true,
        collapsible: false,
        useArrows: true,
        rootVisible: false,
        store: sgStore,
        multiSelect: true,
        singleExpand: false,
        draggable:false,       
        columns:[{
        	xtype: 'treecolumn',
            text: i18n.getMsg('nodestree.node'), //'Storage group / Nodes',
            width:190,
            flex:2,
            dataIndex:'userName',
            id:'userName',
            //checked:false,
            sortable:true,
            renderer: function(value, metaData, record, colIndex, store, view){
            	//if(record.get('leaf') == true)
            	//	value = "&nbsp;&nbsp;"+value;
            	//else
            		value = "(#"+record.get("id")+")&nbsp;"+value;
            	if(record.get('certCN').length > 1)
	            	return value+" (<i>"+record.get('certCN')+"</i>)";
	            else
	            	return value;
	         }
        },{
        	xtype: 'gridcolumn',
            header:i18n.getMsg('nodestree.listenIP'), //'Hostname / IP',
            width:95,
            flex:0,
            dataIndex:'listenip',
            id:'ip',
            sortable:true,
            padding:'0',
            renderer: function(value, metaData, record, colIndex, store, view){
            	if(record.get('leaf') == true)
            		value = "&nbsp;&nbsp;"+value;
            	
            	if(record.get('cls').length > 1)
	            	return '<span class="'+record.get('cls')+'">'+value+"</span>";
	            else
	            	return ' '+value;
	         }
            
        },{
            text: i18n.getMsg('nodestree.listenPort'),
            flex: 0,
            width:55,
            sortable: true,
            dataIndex: 'port',
        },{
        	header: i18n.getMsg('nodestree.priority'), //'Priority',
            width:45,
            flex:0,
            dataIndex:'priority',
            id:'priority',
            sortable:true,
            renderer: function(value, metaData, record, colIndex, store, view){
            	if(record.get('leaf') == true)
            		value = "&nbsp;&nbsp;"+value;
            	if(record.get('cls').length > 1)
	            	return '<span class="'+record.get('cls')+'">'+value+"</span>";
	            else
	            	return ' '+value;
	         }
        },{
        	header:i18n.getMsg('nodestree.size'), //'Storage space',
            width:70,
            flex:1,
            dataIndex:'share',
            id:'share',
            sortable:true,
            renderer: function(value, metaData, record, colIndex, store, view){
            	if(record.get('cls').length > 1)
	            	value = '<span class="'+record.get('cls')+'">'+FormatSize(value)+"</span>";
	            else
	            	value = '  '+FormatSize(value);
	            if(record.get('leaf') == true)
            		value = "&nbsp;"+value;
	            return value;
	         }
        },{
        	header:i18n.getMsg('nodestree.available'), //'Available',
            width:65,
            flex:0,
            dataIndex:'available',
            sortable:true,
            renderer: function(value, metaData, record, colIndex, store, view){
            	
            	if(record.get('cls').length > 1)
	            	value = '<span class="'+record.get('cls')+'">'+FormatSize(value)+"</span>";
	            else
	            	value = '  '+FormatSize(value);
	            if(record.get('leaf') == true)
            		value = "&nbsp;"+value;
	            return value;
	         }
        },{
        	header:i18n.getMsg('nodestree.percentUsed'), //'% used',
            width:60,
            flex:0,
            dataIndex:'percent',
            renderer: function(value, metaData, record, colIndex, store, view){
            	var iconStyle="";
            	if(value > 90)
                	iconStyle="sq_re.gif";
                else if(value < 80)
                	iconStyle="sq_gr.gif";
                else
                	iconStyle="sq_ye.gif";
              	value = '<img style="vertical-align:middle;border:0;" src="/images/'+iconStyle+'">'+value+' %';
            	if(record.get('cls').length > 1)
	            	value= '<span class="'+record.get('cls')+'">'+value+"</span>";
	            else
	            	value= '  '+value;
	            if(record.get('leaf') == true)
            		value = "&nbsp;"+value;
	            return value;
	         }
         },{
         	header: i18n.getMsg('nodestree.version'), //'Version',
            width:50,
            flex:0,
            dataIndex:'version'
         },{
            text: i18n.getMsg('nodestree.os'),
            flex: 0,
            dataIndex: 'os',
            width:35,
            sortable: true,
            renderer:function(value, metaData, record, colIndex, store, view){
            	
            	if(value.toLowerCase() == 'linux')
            		return '<img src="/images/Linux-xs.png" title="'+value+'"/>';
            	else if(value.substr(0,2) == 'NT')
            		return '<img src="/images/Windows-xs.png" title="'+value+'"/>';
            	else if(value.toLowerCase() == 'freebsd')
            		return '<img src="/images/Freebsd-xs.jpg" title="'+value+'"/>';
            	else if(value.toLowerCase() == 'darwin')
            		return '<img src="/images/Apple-xs.png" title="'+value+'"/>';
            	else if(value.toLowerCase() == 'sunos')
            		return '<img src="/images/Sunos-xs.jpg" title="'+value+'"/>';
            	else if(value.length > 1)
            		return '<img src="/images/Unknown-xs.png" title="Unknown os : '+value+'"/>';
            }
        },{
        	header:i18n.getMsg('nodestree.storagePath'), //'Storage path',
            width:150,
            dataIndex:'shareroot',
            flex:1,
        },{
        	header:i18n.getMsg('nodestree.status'), //'Status',
            width:80,
            dataIndex:'status',
            sortable: true,
            renderer:function(value){
            	return i18n.getMsg('nodestree.status.'+value);
            }
        },{
            text: i18n.getMsg('nodestree.capabilities'),
            flex: 0,
            width:100,
            sortable: true,
            dataIndex: 'capabilities',
            renderer: function(value, metaData, record, colIndex, store, view){
            	var formattedCaps="";
            	if(value == '') return '';
            	var capsList = value.toLowerCase().split(", ");
            	for (var i=0; i < capsList.length; i++) 
            		formattedCaps += '<img src="/images/'+capsList[i].substring(1)+'.png" title="'+capsList[i]+'"/>&nbsp;';
        		/*if(record.get('compress') == 'True')
        			value += '<img src="/images/compress.png" title="compress"/>&nbsp;';
        		if(record.get('encrypt') == 'True')
        			value += '<img src="/images/encrypt.png" title="encrypt"/>&nbsp;';
        		if(record.get('clientdedup') == 'True')
        			value += '<img src="/images/clientdedup.png" title="client deduplication"/>&nbsp;';*/
            	return formattedCaps;
            }
        
       // },{
       //     header:'#',
       //     width:'auto',
       //     dataIndex:'',
       //     checked:'false'
        }],
        dockedItems: [{
		    xtype: 'toolbar',
		    dock: 'bottom',
		    style:'bottom:0px;',
		    padding:0,
		    margins:0,
		    height:32,
		    items: [
		        {
		        	xtype:'button',
	        		text:i18n.getMsg('nodestree.configureBtn'), //'&nbsp;&nbsp;Configure',
	        		id: 'configureBtn',
	               	iconCls:'icon-btn',
	                icon:'/images/conf.png',
	                width:80,
	                height:29,	  
	                border:1,             
	               	handler:GetConfigWindow,
	               	disabled:true,
	        	},'-',
	        	{
	          		text:''+i18n.getMsg('nodestree.expand'),
		            icon:'/images/plus.gif',
		            handler : function(){
	                	Ext.getCmp('clientNodesTree').getRootNode().cascadeBy(function(r) {  
	                		if(toBeExpanded == true){
	                			r.expand();  
	                		}
	                		else{
	                			r.collapse();
	                		}
	                	 
	                	})
	                	if(toBeExpanded == true)
                			toBeExpanded = false;
                		else{
                			toBeExpanded = true;
                			Ext.getCmp('clientNodesTree').getRootNode().expand();
                		}
                		
	            	}
	          	}
	        ]
	      }],
    	listeners: {
        'checkchange': function(node, checked){        	
	       	if (Ext.getCmp('clientNodesTree').getChecked().length > 1){
	       		Ext.getCmp('authorizeBtn').enable();
	        	Ext.getCmp('lockBtn').enable();
    			Ext.getCmp('configureBtn').disable();       			
	        }
    		else if (Ext.getCmp('clientNodesTree').getChecked().length == 1){
    			Ext.getCmp('configureBtn').enable();
    			Ext.getCmp('authorizeBtn').enable();
	        	Ext.getCmp('lockBtn').enable();
	        }
	        else{
	        	Ext.getCmp('configureBtn').disable();
	        	Ext.getCmp('authorizeBtn').disable();
	        	Ext.getCmp('lockBtn').disable();
	        }
          }
		 },
    });
    
   /* Ext.define('sgM2', {
        extend: 'Ext.data.Model',
        fields: [
            
            {name: 'userName',     type: 'string'},
            
            {name: 'percent', type: 'string'},
           
        ]
    });*/
    
    var sgStore =  Ext.create('Ext.data.JsonStore', { //new Ext.data.JsonStore( {
    	autoLoad:true,
        model: 'sgM',
        proxy: {
            type: 'ajax',
            url: '/Get.aspx?w=StorageNodes&groupsOnly=rtrue'
        },
       // reader:{
       // 	type:'json',
       // },
       
    });
     /*   
        
    var globalG = Ext.create('widget.panel', {
        width: 280,
       	height: 170,
        //title: i18n.getMsg('welcome.sgChart.title'),
        renderTo: Ext.get('sgChart'), //Ext.getBody(),
        layout: 'fit',
       	padding:'10 0 0 0',
       	align:'right',
       	margins:5,
       	border:false,
       	bodyStyle:'background:transparent;',
       	iconCls:'task-sg',
       	align:'top',
        items: {
            xtype: 'chart',
            id: 'chartCmp',
            animate: true,
            store: sgStore,
            field:'userName',
            shadow: true,
            title: i18n.getMsg('welcome.sgChart.title'),
            legend: {
                position: 'right',
                itemSpacing:2,
                padding:2,
                labelFont:'9px Arial',
                zIndex:0,
                field:'userName',
            },
            insetPadding: 5,
            theme: 'Base:gradients',
            series: [{
            	//title:'Storage groups space',
                type: 'pie',
                field: 'share',
                showInLegend: true,
                legendItem:'userName',
                //donut: 25,
                style: {
                	//opacity: 0.9,
            	},
            	mask: 'horizontal',
		        listeners: {
		            select: {
		                fn: function(me, selection) {
		                    me.setZoom(selection);
		                    me.mask.hide();
		                }
		            }
		        },
                tips: {
                  trackMouse: true,
                  width: 150,
                  height: 28,
                  anchor:'left',
                  renderer: function(storeItem, item) {
                    //calculate percentage.
                    var total = 0;
                    sgStore.each(function(rec) {
                        total += rec.get('share');
                    });
                    var iconStyle = "";
                    var percentFree =  (storeItem.get('available')/storeItem.get('share')*100);
                    if(percentFree < 10)
                    	iconStyle="sq_re.gif";
                    else if(percentFree > 800)
                    	iconStyle="sq_gr.gif";
                    else
                    	iconStyle="sq_ye.gif";
                    this.setTitle('<img style="vertical-align:middle;border:0;" src="/images/'+iconStyle+'">'+storeItem.get('userName') + ': ' +storeItem.get('percent')+ '%');
                  }
                },
                highlight: {
                  segment: {
                    margin: 5
                  }
                },
                label: {
                    field: 'share',
                    display: 'rotate',
                    //display: 'insideEnd',
                    contrast: true,
                    font: '12px Arial',
                    minMargin:0,
                    renderer:function(value){
                    	return FormatSize(value);
                    }
                }
            }]
        }
    });*/
  
  var radarChart = Ext.create('Ext.chart.Chart', {
      //  margin: '0 0 0 0',
      	xtype:'chart',
        width: 700,
       	height: 380,
       	//layout:'fit',
        insetPadding: 20,
        flex:1,
        animate: true,
        store: sgStore,
        //shadow:true,
        id:'radarChart',
        //theme:'Category2',
       /* legend: {
                position: "bottom"
                //ADDING LISTENERS HERE DOESN'T WORK
            },*/
       /* legend: {
                position: 'right',
                itemSpacing:2,
                padding:2,
                labelFont:'9px Arial',
                zIndex:1000,
                field:'percent',
            },*/
        axes: [{
            steps: 5,
            type: 'Radial',
            position: 'radial',
            maximum: 100,
            label:{
	            display:true,
	            
	            
	        }
        }],
        series: [{
            type: 'radar',
            xField:'userName',
            yField:'percent',
           // labelField: 'userName',
            //labelOrientation :'horizontal',
            showInLegend:true,
            showMarkers:true,
            markerConfig: {
                radius: 3,
                size: 4
            },
            style: {
                fill: '#797D8B',
                opacity: 0.5,
                'stroke-width': 2
            }
        }
        ]
    });
    
    
	
	Ext.define('BPM', {
        extend: 'Ext.data.Model',
        fields: [fieldz],
    });
    
    window.generateData = function(n, floor){
        var data = [],
            p = (Math.random() *  11) + 1,
            i;
            
        floor = (!floor && floor !== 0)? 20 : floor;
        
        for (i = 0; i < (n || 12); i++) {
            data.push({
                date: Ext.Date.monthNames[i % 12].substring(0,3),
                'Storage pool 1': Math.floor(Math.max((Math.random() * 100), floor)),
                'Unreliable pool': Math.floor(Math.max((Math.random() * 100), floor)),
                'ClientNodes': Math.floor(Math.max((Math.random() * 100), floor)),
               
            });
        }
        return data;
    };
    
    window.sgHistoryStore = Ext.create('Ext.data.JsonStore', {
        /*model:
        autoLoad: false,
		proxy: {
	        type: 'ajax',
	        url:'/Get.aspx?w=sgHistory',
	        //root: 'backupSets',
    	},*/
    	//autoLoad:false,
    	fields:fieldz,
    	data:generateData(),
    	
    });
    
    var fieldzWithoutDate = [];
       
    var seriez = [];
    
    var spaceHistory = Ext.create('Ext.chart.Chart', {
     	id:'spaceHistory',
       	layout:'fit',
        insetPadding: 20,
        flex:1,
        animate: true,
        store: sgHistoryStore,
        shadow:true,
        theme:'Category2',
       	width:260,
       	height:260,
        axes: [{
	                type: 'Numeric',
	                position: 'left',
	                title:'Space',
	                //fields: ['Unreliable pool' , 'Storage pool 1'],
	                fields:fieldzWithoutDate,
	                title: false,
	                grid: false
	            }, {
	                type: 'Category',
	                position: 'bottom',
	                title:'Date',
	                fields: ['date'],
	                title: true
	            }
	    ],
	    //series:seriez,
        series: [{
            type: 'line',
            xField:'date',
            yField:fieldzWithoutDate,
           
            showInLegend:true,
            showMarkers: true,
            markerConfig: {
                radius: 3,
                size: 3
            },
            style: {
                fill: '#797D8B',
                opacity: 0.5,
                'stroke-width': 0.5
            }
        }
        ]
    });
    
    sgStore.load({
	    scope   : this,
	    callback: function(records, operation, success) {
	    		fieldz.push('date');
	        //console.log(records);
				Ext.each(records, function(record){
					//alert(record.data['userName']);
					fieldz.push(record.data['userName']);
					fieldzWithoutDate.push(record.data['userName']);
					/*var serie = {
			            type: 'line',
			            highlight: {
			                size: 7,
			                radius: 7
			            },
			            axis: 'left',
			            fill: true,
			            xField: 'date',
			            yField: record.data['userName'],
			            smooth:true,
			            markerConfig: {
			                type: 'circle',
			                size: 4,
			                radius: 4,
			                'stroke-width': 0
			            }
			        };
			        
			        
			        spaceHistory.series.add(serie);*/
			        //seriez.push(serie);
        
				});
				/*var axis= {
	                type: 'Numeric',
	                position: 'left',
	                title:'Space',
	                fields: fieldzWithoutDate,
	                title: true,
	                grid: true
	            	};
	            spaceHistory.axes.add(axis);*/
				//sgHistoryStore.load();
				//spaceHistory.redraw();
		}
	});	
    
    
   //Ext.getCmp('spaceHistory').redraw();
    
    var snPanel =  new Ext.Panel({
		id:'snPanel',
		renderTo: Ext.get('panel'),
        monitorValid:true,
        border: false,
        autoScroll:false,
        bodyPadding: 0,
		height:'500',
        fieldDefaults: {
            labelAlign: 'left',
            labelWidth: 90,
            labelStyle: 'font-size:0.9em !important;'
        },
        defaults: {
            padding:0,
            margins:0
        },
        layout:'hbox',
        align:'right',
        items: [tree, 
           {
	        xtype:'fieldset',
	        layout: {
	                    type: 'vbox',
	                    align:'stretch'
	                },
	        margin: '0 0 0 5',
	        width:280,
	        height:500,
	        title: i18n.getMsg('welcome.sgChart.title'),
	        items: [radarChart, spaceHistory]
        }
        ]
	});
});
});