<%@ Page Language="C#" Inherits="SharpBackupWeb.Error" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
<title>SharpBackup :: Hub configuration</title>
	
   
	<link rel="stylesheet" type="text/css" href="./ext4/resources/css/ext-all-gray.css" />
    <!-- Common Styles for the examples -->
    <link rel="stylesheet" type="text/css" href="./ext4/examples/shared/example.css" /> 
    
    <!-- GC -->
    <!-- LIBS -->
	<script type="text/javascript" src="/ext4/ext-all.js"></script>
    <!-- i18n -->
    <script type="text/javascript" src="/i18n/Bundle.js"></script>
    
    <!-- Common sharpbackup page template style -->
	<link rel="stylesheet" type="text/css" href="/css/g.css" />
	<script type="text/javascript" src="/js/g.js"></script>
	 
    
	
</head>
<body>
	 <h1 class="weTitle">:: Error</h1>

    
  <div id="toolbar"></div>
   	<div id="menu" class="menu"></div>
	<div id="panel" class="GeneralPanel">
	<form id="form" runat="server">
	<asp:Label id="lblError" runat="server"/>
	</form>
	</div>
	
</body>
</html>