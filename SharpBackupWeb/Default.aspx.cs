using System;
using System.Web;
using System.Web.UI;
using System.Web.SessionState;
using System.Web.Security;
using System.Configuration;
using P2PBackup.Common;
using SharpBackupWeb.Utilities;

namespace SharpBackupWeb{


	public partial class Default : System.Web.UI.Page{
		
		private void InitializeComponent() {    
            this.cmdSubmit.Click += new System.EventHandler(this.cmdSubmit_Click);
            this.Load += new System.EventHandler(this.Page_Load);

        }
		
		protected  void  Page_Load (object sender, EventArgs args){
			if(Request.QueryString["action"] == "logout"){
				Console.WriteLine("logout");
				FormsAuthentication.SignOut();
				Session["user"] = null;
				Session.Abandon();
				Session.Clear();
				Session.Remove(Session.SessionID);
				Response.Redirect("/Default.aspx");
			
			}
			if(Session.Count >0)
				Response.Redirect("/Welcome.html?lang="+((User)Session["user"]).Culture.Name+"&u="+((User)Session["user"]).Name);
		}
		
        protected void cmdSubmit_Click(object sender, System.EventArgs e){
            if (Page.IsValid){
				try{
					//RemoteOperations remoteOperation = (RemoteOperations)Activator.GetObject(typeof (RemoteOperations),"tcp://127.0.0.1:9999/RemoteOperations");
					IRemoteOperations remoteOperation = RemotingManager.GetRemoteObject();
					//User authUser = remoteOperation.BeginSession(login.Text, password.Text);
					remoteOperation.Login(login.Text, password.Text);
					User authUser = remoteOperation.GetCurrentUser();
                	if (authUser != null)      {
						Logger.Append(P2PBackup.Common.Severity.INFO, "User "+authUser.Name+" has connected");
						Session["user"] = authUser;
						Session["remote"] = remoteOperation;
              			FormsAuthentication.RedirectFromLoginPage("Welcome.html?lang="+authUser.Culture.Name, true);
                	}
                	else {
                 		lblMessage.Text = "Invalid credentials.";
                	}
				}
				catch(Exception ex){
					lblMessage.Text = "Could not connect to Hub. (error is: "+ex.Message+"<br>"+ex.StackTrace+")";	
				}
            }

        }
	}
}

