
using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SharpBackupWeb{

	
	public partial class Error : System.Web.UI.Page{
		
		protected Label lblError;
		protected  void  Page_Load (object sender, EventArgs args){
			lblError.Text = "<p>    An error has occured.<br/>";
			if(Session["lastError"] != null)
				lblError.Text += Session["lastError"].ToString();
			else
				lblError.Text += "Unfortunately we cannot provide you with more specific details."
					+"<br/>The error context has been logged. Contact application administrator.";
			
			lblError.Text += "</p>";
		}
	}
}

