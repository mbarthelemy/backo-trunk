
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.Script;
//using System.Runtime.Serialization.Json;
/*using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Tcp;*/
using P2PBackup.Common;
using SharpBackupWeb.Utilities;

namespace SharpBackupWeb{


	public partial class Hub : System.Web.UI.Page{
		
		IRemoteOperations remoteOperation;
		private  void  Page_Load (object sender, EventArgs args){
				remoteOperation = RemotingManager.GetRemoteObject();
				
				if(Request.QueryString["w"] == "Config"){
						Dictionary<string, string> confTable = remoteOperation.GetConfigurationParameters();
						BuildConfigJson(confTable);
						
				}
		}
		
		
		private void BuildConfigJson(Dictionary<string, string> config){
			StringBuilder jsb = new StringBuilder();	
			//jsb.Append("{conf:[");
			jsb.Append("[");
			foreach (string key in config.Keys){
				jsb.Append("{");
				jsb.Append("'key':'"+key+"','value':'"+config[key].ToString()+"'");
				jsb.Append("},");
			}
			//if(config.Count >0)
			//	jsb.Remove(jsb.Length-1,1);
			jsb.Append("]");
			Response.Write(jsb.ToString());
		}
	}
}

