
using System;
using System.Collections;
using System.ComponentModel;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.SessionState;
using System.Diagnostics;
using System.Runtime.InteropServices;
using SharpBackupWeb.Utilities;

namespace SharpBackupWeb{


	public class Global : System.Web.HttpApplication{
	
		protected Label error;
		
		protected virtual void Application_Start (Object sender, EventArgs e){
			Logger.Append(P2PBackup.Common.Severity.INFO, "Web UI application starting");
		}

		protected virtual void Session_Start (Object sender, EventArgs e){
			Logger.Append(P2PBackup.Common.Severity.INFO, "New session starting");
		}

		protected virtual void Application_BeginRequest (Object sender, EventArgs e){
		}

		protected virtual void Application_EndRequest (Object sender, EventArgs e){
		}

		protected virtual void Application_AuthenticateRequest (Object sender, EventArgs e){
		}

		protected virtual void Application_Error (Object sender, EventArgs e){
			Exception objErr = Server.GetLastError().GetBaseException();
			string err =	"Error Caught in Application_Error event\n" +
			"Error in: " + Request.Url.ToString() +
			"\nError Message:" + objErr.Message.ToString() + 
			"\nStack Trace:" + objErr.StackTrace.ToString();
			Logger.Append(P2PBackup.Common.Severity.CRITICAL, err);
			HttpContext.Current.Session["lastError"] = " "+err;
			Server.Transfer("/Error.aspx");
			
		}

		protected virtual void Session_End (Object sender, EventArgs e){
		}

		protected virtual void Application_End (Object sender, EventArgs e){
			Logger.Append(P2PBackup.Common.Severity.INFO, "Web UI application stopping...");
		}
	}
}

