<%@ Page Language="C#" Inherits="SharpBackupWeb.Default" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
<head>
	<title>SharpBackup :: Login</title>
	<link rel="stylesheet" type="text/css" href="/css/g.css" /></head>
	<script type="text/javascript" src="js/jquery-1.2.6.min.js"></script>
	<script type="text/javascript">
		
		$(function(){
			$(".edge").stop().fadeIn();
		});
	</script>
</head>
<body>
	 <h1 class="loTitle">:: Login</h1>

    <footer class="clients legend">
      

    </footer>
   <div class="edge" id="left"></div>
	<div class="edge" id="right"></div>
	<div class="edge" id="top"></div>
	<div class="edge" id="bottom"></div>
	
	<div class="x-tree login">
	<form id="loginForm" runat="server">
	<br/><br/>
	&nbsp;&nbsp;&nbsp;Please enter your user name and password to connect to administration interface.
	<br/><br/>
	<table border="0"><tr>
		<td><b>Login :</b></td><td><asp:Textbox id="login" class="textbox" runat="server"/></td></tr>
		<tr><td><b>Password :</b></td><td><asp:Textbox TextMode="Password" id="password" class="textbox" runat="server"/></td>
	</tr>
	<tr><td> &nbsp;</td><td></td></tr>
	<tr><td></td><td><asp:Button id="cmdSubmit" runat="server" OnClick="cmdSubmit_Click"  class="textbox" Text="Connect"/></td></tr>
	<tr><td> &nbsp;</td><td></td></tr>
	<tr><td colspan="2"><asp:Label id="lblMessage" runat="server"/></td></tr>
	</table>
	
	</form>
	</div>
</body>
</html>
