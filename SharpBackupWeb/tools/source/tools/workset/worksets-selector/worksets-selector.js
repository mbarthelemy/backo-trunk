/**
 * @class Monk.component.WorksetsSelector
 * @description Allows user to select two worksets.
 * @extends Workbench.component.Component
 * @author Stéfan
 */

Monk.component.WorksetsSelector = function(args) {
    
    Monk.component.WorksetsSelector.superclass.constructor.call(this, args);
    
}

Workbench.extend(Monk.component.WorksetsSelector, Workbench.component.Component, {

    label: 'Worksets Selector',
    description: 'A tool to choose two worksets',
    "window": this.window
	
	,store : new Ext.data.Store({
		reader : new Ext.data.XmlReader({record : 'workset', id : '@id'}
			, new Ext.data.Record.create([{name : 'id', mapping : '@id'}, 'label', 'workList']))
	})
    
    ,handle: function(monkEvent, data){
		if (monkEvent.instanceOf(Monk.event.project.WorksetsReceived)) {
			Ext.getCmp("dirty").setVisible(false);
			this.store.loadData(data.worksets.responseXML)
		}
		else if (monkEvent.instanceOf(Monk.event.workset.WorksetSaved) || monkEvent.instanceOf(Monk.event.workset.WorksetDeleted) || monkEvent.instanceOf(Monk.event.workset.WorksetCreated)) {
			// reload data
			Monk.data.project.getWorksets(Monk.component.dataManager.getProjectId());
		}
		else if (monkEvent.instanceOf(Monk.event.chunk.ChunkChecked) || monkEvent.instanceOf(Monk.event.chunk.ChunkChecked) || monkEvent.instanceOf(Monk.event.workset.WorksetReset) || monkEvent.instanceOf(Monk.event.workset.WorksetCreated)) {
			// warn about dirty data
			Ext.getCmp("dirty").setVisible(true);
		}
	}
	
	,checkWorksets : function() {
		var w1 = Ext.getCmp('w1');
		var w2 = Ext.getCmp('w2');
		if (w1.isValid() && w2.isValid()) {
			var v1 = w1.getValue();
			var v2 = w2.getValue();
			if (v1==v2) {return Monk.component.messenger.alert('Monk Workbench', 'Please be sure to select two different worksets', this.window.parent ? this.window.parent.window : this.window);}
			var d1 = this.store.getById(w1.getValue()).data;
			var d2 = this.store.getById(w2.getValue()).data;
			
			// this should probably be done by the receiving tool instead
			if (d1.workList=='' || d2.worklist=='') {return Monk.component.messenger.alert('Monk Workbench', 'Please select a workset that contains text chunks.', this.window.parent ? this.window.parent.window : this.window);}
			
			this.notify(new Monk.event.project.WorksetsSelected({
				label : 'Worksets selected'
			}), [d1,d2])
		}
	}
	
	,init : function() {
		var combo = {
			xtype: 'combo'
			,forceSelection : true
			,store : this.store
			,valueField : 'id'
			,displayField : 'label'
			,mode : 'local'
			,allowBlank : false
            ,maxHeight: 75
			,listeners : {
				select : {fn : this.checkWorksets, scope : this}
			}
		}
		new Ext.Panel({
			title : 'Please select two worksets'
			,renderTo : document.body
			,border : false
			,autoScroll : true
			,items : [
				{
					id : 'dirty'
					,html : '<span class="dirty">Please save the current workset to see up-to-date data.</span>'
					,border : false
				},{
					xtype : 'form'
					,labelAlign : 'right'
					,labelWidth: 125
					,border : false
					,items : [
						{
							id : 'w1'
							,fieldLabel : 'Reference Workset'
							,blankText : 'Select Workset #1'
							,emptyText : 'Select Workset #1'
							,xtype: 'combo'
							,forceSelection : true
							,store : this.store
							,valueField : 'id'
							,displayField : 'label'
							,mode : 'local'
							,allowBlank : false
				            ,maxHeight: 75
				            ,editable: false
				            , triggerAction: 'all'
							,listeners : {
								select : {fn : this.checkWorksets, scope : this}
							}
						},{
							id : 'w2'
							,fieldLabel : 'Analysis Workset'
							,blankText : 'Select Workset #2'
							,emptyText : 'Select Workset #2'
							,xtype: 'combo'
							,forceSelection : true
							,store : this.store
							,valueField : 'id'
							,displayField : 'label'
							,mode : 'local'
							,allowBlank : false
				            ,maxHeight: 75
				            ,editable: false
				            , triggerAction: 'all'
							,listeners : {
								select : {fn : this.checkWorksets, scope : this}
							}
						}
					]
				}
			]
		})
		Monk.data.project.getWorksets(Monk.component.dataManager.getProjectId());
	}
});
