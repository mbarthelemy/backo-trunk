/**
 * @author Andrew
 */

Monk.component.WorksetTreeComponent = function(args) {
    
    this.loadDialog = null;
    
    this.saveDialog = null;
    
    this.cachedData = null;
    
    // following variables are used by the updateTree method
    // they're properties of the component so that they're accessible
    
    // temp hash of nodes to use for checking off tree nodes
    this.tempHash = [];
    
    // temp storage for nodes that were checked
    this.tempNodes = [];
    
    // temp hash to keep track of what collections the workset contains
    this.tempCollectionsList = [];
    
    Monk.component.WorksetTreeComponent.superclass.constructor.call(this, args);
}

Workbench.extend(Monk.component.WorksetTreeComponent, Workbench.component.Component, {

    label : "Workset Tree Manager",
    description : "For managing worksets of text chunks in Monk.",
    "window" : this.window,

    handle : function(monkEvent, data) {
        if (monkEvent.instanceOf(Workbench.event.ComponentLoaded)) {
            if (monkEvent.component == this) {
                var data = Monk.component.dataManager.getWorklist();
                var chunks = [];
                for (var chunkId in data) {
                    var chunk = data[chunkId];
                    chunk.id = chunkId;
                    chunks.push(chunk);
                }
                if (chunks.length > 0) {
                    this.cachedData = chunks;
                }
            }
        } else if (monkEvent.instanceOf(Monk.event.workset.WorksetLoaded)) {
            var worklist = Monk.component.dataManager.getWorklist();
            this.updateTree(worklist);
        } else if (monkEvent.instanceOf(Monk.event.workset.WorksetCreated)) {
            this.saveDialog.hide();
        } else if (monkEvent.instanceOf(Monk.event.workset.WorksetReset)) {
            // clear checks
            this.updateTree([]);
        }
    },

    init : function() {
        if (this.tree == null) {
            var metaData = Monk.data.collection.getMetaDataHierarchy();
            // customize the nodes for this tree: add a uiProvider
            for (var i = 0; i < metaData.length; i++) {
                var collection = metaData[i];
                collection.uiProvider = Ext.tree.ColumnNodeUI;
                for (var j = 0; j < collection.children.length; j++) {
                    var node = collection.children[j];
                    node.uiProvider = Ext.tree.ColumnNodeUI;
                    node.userRating = '';
                    node.systemRating = '';
                }
            }
            this.initializeTree(metaData);
            if (this.cachedData != null) {
                this.updateTree(this.cachedData);
                this.cachedData = null;
            }
        }
    },

    /**
     * Update the tree with a set of nodes to have checked
     * (all others will be unchecked)
     * @param {Object} worklist The nodes that should be checked
     */
    updateTree : function(worklist) {
            var maskedElement = Ext.get("worksetTree");
            maskedElement.mask("working...");

            this.updating = true;

            this.tempCollectionsList = {};
            this.tempHash = {};
            this.tempNodes = [];
            
            for (var chunkId in worklist) {
                var collectionId = worklist[chunkId].collectionId;
                if (this.tempCollectionsList[collectionId] == undefined) {
                    this.tempCollectionsList[collectionId] = true;
                }
            }
            
            this.tempHash = worklist;
            
            this.recursiveNodeUpdater = function(node){
                var collection = node.attributes.collectionId;
                var tokens = node.attributes.id.split('-');
                if (tokens.length >= 2) {
                    // it's a work or workpart
                    
                    // check if there are workset workparts within this node 
                    var expand = false;
                    for (var chunk in this.tempHash) {
                        // append a dash to the node ID to prevent false positives 
                        // the test workparts against it
                        if (chunk.indexOf(node.attributes.id+'-') != -1) {
                            expand = true;
                            break;
                        }
                    }
                    if (expand) {
                        if (!node.isExpanded()) {
                            node.expand(false, false, function(node) {
                                node.eachChild(
                                    this.worksetTreeComponent.recursiveNodeUpdater,
                                    this.worksetTreeComponent
                                );
                            });
                        } else { 
                        }
                    }
                    
                    if (node.getUI().isChecked() && !this.tempHash[node.attributes.id]) {
                        node.getUI().setColumnValue(0, '');
                        node.getUI().setColumnValue(1, '');
                        node.getUI().toggleCheck(false);
                    } else if (node.getUI().isChecked() && this.tempHash[node.attributes.id]) {
                        var data = this.tempHash[node.attributes.id];
                        if (data.systemRating) node.getUI().setColumnValue(0, data.systemRating);
                        if (data.userRating) node.getUI().setColumnValue(1, data.userRating);
                        // node's already checked so remove it from the list
                        delete this.tempHash[node.attributes.id];
                    } else if (!node.getUI().isChecked() && this.tempHash[node.attributes.id]) {
                        var data = this.tempHash[node.attributes.id];
                        if (data.systemRating) node.getUI().setColumnValue(0, data.systemRating);
                        if (data.userRating) node.getUI().setColumnValue(1, data.userRating);
                        
                        node.getUI().toggleCheck(true);
                        this.tempNodes.push({
                            id: node.attributes.id,
                            collectionId: node.attributes.collectionId,
                            text: node.text,
                            chunkType: node.attributes.chunkType
                        });
                        // remove nodes we've taken care of
                        delete this.tempHash[node.attributes.id];
                    } else {
                        node.getUI().setColumnValue(0, '');
                        node.getUI().setColumnValue(1, '');
                    }
                    
					if (this.updating) {
	                    var itemCount = 0;
	                    for (var id in this.tempHash) {
	                        itemCount++;
	                    }
	                    if (itemCount == 0) {
	                        // then we're done
	                        this.notify(
	                            new Monk.event.chunk.ChunksChecked({
	                                label: "Text chunks checked"
	                            }),
	                            this.tempNodes
	                        );
	                        
	                        this.tempNodes = [];
	                        this.updating = false;
	                    }
					}
                    
                } else {
                    // it's a collection branch
                    // only expand a branch if it belongs to a collection that is in our collectionsList
                    if (this.tempCollectionsList[collection]) node.expand(false, false);
                }
            }
            
            // cascade through nodes, updating them as needed
            this.tree.root.expand(false, false, function() {
                this.worksetTreeComponent.tree.root.cascade(
                    this.worksetTreeComponent.recursiveNodeUpdater,
                    this.worksetTreeComponent
                );
            });
            
            maskedElement.unmask();
    },
    
    initializeLoadDialog : function() {
        var projectId = Monk.component.dataManager.getProjectId();
        
        var xmlReader = new Ext.data.XmlReader({
                record: 'workset',
                id: '@id'
            }, [
                {name: 'id', mapping: '@id'},
                'label',
                'trainingList',
                'trainingListRating',
                'workList',
                'workListRating'
            ]
        );
        
        var worksetsStore = new Ext.data.Store({
            reader: xmlReader,
            proxy: new Ext.data.HttpProxy({
                url: Monk.data.PROXY_URL + 'get/ProjectManager.getWorksets',
                method: 'GET'
            }),
            baseParams: {projectId: projectId}
        });
        
        var worksetsCombo = new Ext.form.ComboBox({
            fieldLabel: 'Workset',
            name: 'workset',
            width: 175,
            store: worksetsStore,
            triggerAction: 'all',
            allowBlank: false,
            editable: false,
            forceSelection: true,
            displayField: 'label',
            hiddenName: 'worksetId',
            valueField: 'id'
        });
        
        var loadForm = new Ext.form.FormPanel({
            labelAlign: 'right',
            labelWidth: 70,
            border: false
        });
        
        loadForm.add(worksetsCombo);
        
        this.loadDialog = new Ext.Window({
            title: 'Load Workset',
            height: 120,
            width: 300,
            modal: true,
            shadow: true,
            plain: true,
            border: false,
            collapsible: false,
            items: loadForm,
            buttons: [
                {
                    text: 'Load',
                    handler: function(){
                        if (loadForm.getForm().isValid()) {
                            var values = loadForm.getForm().getValues();
                            var worksetId = values.worksetId;
                            var record = worksetsStore.getById(worksetId);
                            var label = record.get('label');
                            var trainingList = record.get('trainingList');
                            var trainingListRating = record.get('trainingListRating');
                            var workList = record.get('workList');
                            var workListRating = record.get('workListRating');
                            
                            var workset = {
                                id: worksetId,
                                label: label,
                                projectId: projectId,
                                trainingList: trainingList,
                                trainingListRating: trainingListRating,
                                workList: workList,
                                workListRating: workListRating
                            }
                            
                            Monk.component.dataManager.setWorkset(workset);
                            
                            loadForm.getForm().reset();
                            this.loadDialog.hide();
                        }
                    },
                    scope: this
                },
                {
                    text: 'Cancel',
                    handler: function(){
                        this.loadDialog.hide();
                    },
                    scope: this
                }
            ]
        });
        
        this.loadDialog.show();
    },
    
    initializeSaveDialog : function() {
        var worksetName = new Ext.form.TextField({
            fieldLabel: 'Name',
            name: 'name',
            width: 175,
            allowBlank: false
        });
        
        var saveForm = new Ext.form.FormPanel({
            labelAlign: 'right',
            labelWidth: 70,
            border: false
        });
        
        saveForm.add(worksetName);
        
        this.saveDialog = new Ext.Window({
            title: 'Save Workset',
            height: 120,
            width: 300,
            modal: true,
            shadow: true,
            plain: true,
            border: false,
            collapsible: false,
            items: saveForm,
            buttons: [
                {
                    text: 'Save',
                    handler: function(){
                        if (saveForm.getForm().isValid()) {
                            var values = saveForm.getForm().getValues();
                            var worksetName = values.name;
                            this.createNewWorkset(worksetName);
                            
                            saveForm.getForm().reset();
                        }
                    },
                    scope: this
                },
                {
                    text: 'Cancel',
                    handler: function(){
                        this.saveDialog.hide();
                    },
                    scope: this
                }
            ]
        });
        
        this.saveDialog.show();
    },
    
    /**
     * Compiles the workset and saves it.
     * @param {Boolean} saveAs If true, save workset as a copy
     */
    saveWorkset : function(saveAs) {
        var worksetId = Monk.component.dataManager.getWorksetId();
        if (worksetId == null || saveAs == true) {
            // create new workset (or save a new copy)
            if (this.saveDialog == null){
                this.initializeSaveDialog();
            } else {
                this.saveDialog.show();
            }
        } else {
            // save the workset
            var workset = Monk.component.dataManager.getWorkset();
            if (workset.trainingSet == null) {
                // dummy data
                workset.trainingSet = workset.worklist.split(',')[0];
                workset.trainingSetRating = 3;
            }
            if (workset.worklistRating == null) {
                var count = workset.worklist.split(',').length;
                var rating = "3";
                for (var i = 0; i < count - 1; i++){
                    rating += ",3"
                }
                workset.worklistRating = rating;
            }
            Monk.data.workset.saveWorkset(workset);
        }
    },
    
    createNewWorkset : function(worksetName) {
        Monk.component.dataManager.setWorksetName(worksetName); 
        
        // TODO: make sure training list is being sent with training list ratings
        var workset = Monk.component.dataManager.getWorkset();
        Monk.data.workset.createWorkset(workset);
    },

    initializeTree : function(collectionsHierarchy) {
       
        // custom treeloader for handling XML
        Ext.ux.CollectionTreeLoaderXML = function(args) {
            Ext.ux.CollectionTreeLoaderXML.superclass.constructor.call(this, args);
        }
        
        Workbench.extend(Ext.ux.CollectionTreeLoaderXML, Ext.tree.TreeLoader, {
            processResponse : function(response, node, callback){
                try {
                    var children = response.responseXML.getElementsByTagName('workpart');
                    var collectionId = null;
                    for (var i = 0; i < children.length; i++) {
                        var child = children[i];
                        if (child.nodeType == 1) {
                            var label = child.getElementsByTagName('label')[0].childNodes[0].data;
                            var id = child.getAttribute("id");
                            var numChildren = parseInt(child.getAttribute("numChildren"));
                            // parse the ID to get the collection ID
                            if (collectionId == null) {
                                collectionId = id.match(/^(\w+)\-(.+)$/)[1];
                            }
                            
                            var treeNode = null;
                            if (numChildren > 0) {
                                var treeLoader = new Ext.ux.CollectionTreeLoaderXML({
                                    requestMethod:'GET',
                                    dataUrl:Monk.data.PROXY_URL + 'get/CorpusManager.getWorkInfo?corpus='+collectionId+'&id='+id,
                                    baseAttrs: {checked: false, iconCls:'chunk-tree-node'},
                                    uiProviders: {
                                        'col': Ext.tree.ColumnNodeUI
                                    }
                                });
                                treeNode = new Ext.tree.AsyncTreeNode({
                                    workLabel: label,
                                    uiProvider: Ext.tree.ColumnNodeUI,
                                    id: id,
                                    collectionId: collectionId,
                                    userRating: '',
                                    systemRating: '',
                                    loader: treeLoader,
                                    leaf: false,
                                    checked: false,
                                    iconCls:'chunk-tree-node'
                                });
                            } else {
                                treeNode = new Ext.tree.TreeNode({
                                    workLabel: label,
                                    uiProvider: Ext.tree.ColumnNodeUI,
                                    id: id,
                                    collectionId: collectionId,
                                    userRating: '',
                                    systemRating: '',
                                    leaf: true,
                                    checked: false,
                                    iconCls: 'chunk-tree-node'
                                });
                            }
                            node.appendChild(treeNode);
                        }
                    }
                    
                    if(typeof callback == "function"){
                        callback(this, node);
                    }
                }catch(e){
                    alert(e);
                    this.handleFailure(response);
                }
            }
        });
        
        var treeLoader = new Ext.ux.CollectionTreeLoaderXML({
            requestMethod: 'GET',
            dataUrl: Monk.data.PROXY_URL + 'get/CorpusManager.getWorkInfo',
            baseAttrs: {checked: false, iconCls:'chunk-tree-node'},
            uiProviders:{
                'col': Ext.tree.ColumnNodeUI
            }
        });
        
        treeLoader.on('beforeload', function(loader, node) {
            loader.baseParams.corpus = node.attributes.collectionId;
            loader.baseParams.id = node.attributes.id;
        }, this);
            
        this.tree = new Ext.tree.ColumnTree({
            el: 'worksetTree',
            width: 535,
            height: 500,
            rootVisible: true,
            autoScroll: true,
            enableDD: false,
            loader: treeLoader,
            title: 'Workset',
            tbar: [{
                    text:'Load Workset',
                    iconCls:'load-icon',
                    handler: 
                        function(){
                            if (this.loadDialog == null) {
                                this.initializeLoadDialog();
                            } else {
                                this.loadDialog.show();
                            }
                        },
                    scope: this
                },{
                    xtype:'tbseparator'
                },{
                    text:'Save Workset',
                    tooltip: 'Save Workset',
                    iconCls:'save-icon',
                    handler: 
                        function() {
/*                        
                            var json = this.tree.toJsonString(null,
                                function(key, val) {
                                    return (key == 'id' || key =='userRating'|| key == 'systemRating');
                                }, {
                                    userRating: 'userRating',
                                    systemRating: 'systemRating'
                                }
                            );
                            alert(json);
*/                            
                            this.saveWorkset();
                        },
                        scope: this
                },{
                    xtype:'tbseparator'
                },{
                    text:'Save Workset As',
                    iconCls:'save-as-icon',
                    handler:
                        function(){
                            this.saveWorkset(true);
                        },
                        scope: this
                },{
                    xtype:'tbseparator'
                }
            ],
            
            columns:[{
                header: 'Workparts',
                width: 375,
                dataIndex: 'workLabel'
            },{
                header: 'SysRating',
                width: 70,
                editable: false,
                dataIndex: 'systemRating'
            },{
                header: 'UserRating',
                width: 70,
                dataIndex: 'userRating'
            }],
            
            root : new Ext.tree.AsyncTreeNode({
                text: 'Monk Collections', 
                draggable: false, 
                id: 'collections-root',
                children: collectionsHierarchy,
                checked: false,
                expanded: true,
                iconCls: 'chunk-tree-node',
                uiProvider: Ext.tree.ColumnNodeUI
            })
        });
        this.tree.render();
        
        this.tree.on("click", function(node, ev) {
            if (node.hasChildNodes()) {
                if (this.tree.getTarget(ev.target) == 'tree') {
                    node.toggle();
                }
            } else {
                // only load text from leaf nodes
                this.handleNodeSelect(node);
            }
        }, this);
        
        this.tree.on("checkchange", function(node,isChecked) {
            this.handleNodeChange(node, isChecked);
        }, this);
        
        var te = new Ext.tree.ColumnTreeEditor(this.tree, {
            editTree: false,
            events: {
                completeOnEnter: true,
                ignoreNoChange: true
            }
        });
    },
    
    handleNodeSelect : function(node) {
        this.notify(
            new Monk.event.chunk.ChunkSelected({
                label: 'Text chunk selected: '+'"'+node.text+'"',
                displayText: true
            }),
            {
                id: node.attributes.id,
                corpus: node.attributes.collectionId,
                text: node.text,
                chunkType: node.attributes.chunkType,
                displayText: true
            }
        );
    },
    
    handleNodeChange : function(node, isChecked){
        this.notify(
            new Monk.event.chunk.ChunkChecked({
                label: "Text chunk "+(isChecked ? "" : "un")+'checked: "'+node.text+'"',
                checked: isChecked
            }),
            {
                id: node.attributes.id,
                corpus: node.attributes.collectionId,
                text: node.text,
                chunkType: node.attributes.chunkType,
                checked: isChecked
            }
        );
    }
});
