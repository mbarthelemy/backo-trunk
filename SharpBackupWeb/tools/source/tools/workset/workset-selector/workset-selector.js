/**
 * @class Monk.component.WorksetLoader
 * @description Allows user to select two worksets.
 * @extends Workbench.component.Component
 * @author Stéfan
 * @modified Amit -changed it for one workset
 */

Monk.component.WorksetLoader = function(args) {            

    this.worksetStore = new Ext.data.Store({
        reader: new Ext.data.XmlReader({record : 'workset', id : '@id'}
            ,new Ext.data.Record.create([
            {name : 'id', mapping : '@id'},
            {name : 'workList', mapping : 'workList'},
            {name: 'label', mapping:'label'},
            {name: 'type', mapping: 'label', convert: function(value){return 'workset'}},
            {name:'count', mapping:'workList', type: 'int', convert: function(value){
                if(value==null){
                    return 0;
                }
                return value.split(',').length;
            }}
        ])),
        url: Monk.data.PROXY_URL + 'get/ProjectManager.getWorksets',
        baseParams: {projectId: Monk.component.dataManager.getProjectId()},
        autoLoad: true,
        listeners: {
            load: function(store){
                var worksetId = Monk.component.dataManager.getWorksetId();
                if (worksetId != null) {
                    var combo = Ext.getCmp('w1');
                    combo.setValue(worksetId);
                }
            }
        }
    });
   
    Monk.component.WorksetLoader.superclass.constructor.call(this, args);
}

Workbench.extend(Monk.component.WorksetLoader, Workbench.component.Component, {

    label: 'Workset Selector',
    description: 'A tool to select a workset',
    "window": this.window,
    
     beforeExit : function() {
     	  Workbench.console.info("destroying results and panel");
          var results = Ext.getCmp('w1');
          results.destroy();
          var panelComp = Ext.getCmp("panelId");
          panelComp.destroy();
    },
    
    handle: function(monkEvent, data){
		if (monkEvent.instanceOf(Monk.event.workset.WorksetSaved) || monkEvent.instanceOf(Monk.event.workset.WorksetDeleted) || monkEvent.instanceOf(Monk.event.workset.WorksetCreated)) {
            this.worksetStore.reload();
		}
		
	},
	checkWorksets : function() {
	    var w1 = Ext.getCmp('w1');
            var appendWorkset = Ext.getCmp('append').getValue();
		if (w1.isValid()) {
			var v1 = w1.getValue();
			var d1 = this.worksetStore.getById(w1.getValue()).data;
			
			// this should probably be done by the receiving tool instead
			if (d1.workList=='') {
    			return Monk.component.messenger.alert('Monk Workbench', 
    			 'Please select a workset that contains text chunks.', 
    			 this.window.parent ? this.window.parent.window : this.window);
			}

            if (!appendWorkset) {
    			this.notify(new Monk.event.workbench.ClearSearch({
                    label: 'clear all search sent'
                }), {clearAll: true});
            }
            var paramValues={'worksetIdCriterion':v1};
			this.notify(
			   new Monk.event.workbench.AdvancedSearchQuery({label: 'workset search request'}), [paramValues, appendWorkset]
            );
		}
	},
	init : function() {
		var panel = new Ext.Panel({
			 title : 'Please select a workset',
			 id: 'panelId',
			 renderTo : document.body,
			 border : false,
			 autoScroll : true,
             bodyStyle: 'padding: 10px;',
			 items : [{
				 xtype : 'form',
				labelAlign : 'right',
				labelWidth: 150,
				border : false,
				items : [{
					id : 'w1',
					fieldLabel : 'Workset',
					blankText : 'Select Workset ',
					emptyText : 'Select Workset',
					xtype: 'combo',
					forceSelection : true,
					store : this.worksetStore,
					valueField : 'id',
					displayField : 'label',
					mode : 'local',
					allowBlank : false,
                    maxHeight: 75,
                    editable: false,
                    triggerAction: 'all'
				},{
                    id: 'append',
                    xtype: 'checkbox',
                    fieldLabel: 'Add to current works'
                }],
                buttons: [{
                    text: 'Load Workset',
                    handler: this.checkWorksets,
                    scope: this
                }]
			}]
		});
	}
});
