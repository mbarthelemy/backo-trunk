/**
 * Fixes problem wherein ComboBox in EditorGrid doesn't accept typed in values.
 * From http://extjs.com/forum/showthread.php?p=138900
 */
Ext.override(Ext.form.ComboBox, {
    getValue : function(){
        if(!this.forceSelection){
            var v = Ext.form.ComboBox.superclass.getValue.call(this);
            if(v != this.lastSelectionText)
                this.setValue(v);
        }
        if(this.valueField){
            return typeof this.value != 'undefined' ? this.value : '';
        }else{
            return Ext.form.ComboBox.superclass.getValue.call(this);
        }
    }
});