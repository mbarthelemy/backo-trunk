/**
 * @class Monk.component.WorksetManagerComponent
 * @description A {@link Worbench.component.Component} for viewing a text chunk.
 * @extends Workbench.component.Component
 * @author Stéfan Sinclair
 * @version 0.1
 * @since Monk 0.1
 */
Monk.component.WorksetManagerComponent = function(args) {
    
    this.worksetGrid = null;
    
    this.worksetId = null;
    
    // initialize worksets store
    this.worksetsStore = new Ext.data.Store({
        reader: new Ext.data.XmlReader(
            {record: 'workset', id: '@id'},
            [{name: 'id', mapping: '@id'},
            'label',
            'trainingList',
            'trainingListRating',
            'workList',
            'workListRating',
            'itinerary',
            'feature']
        ),
        proxy: new Ext.data.HttpProxy({
            url: Monk.data.PROXY_URL + 'get/ProjectManager.getWorksets',
            method: 'GET'
        }),
        baseParams: {projectId: Monk.component.dataManager.getProjectId()}
    });
    this.worksetsStore.load();
    
    this.loadDialog = null;
    
    this.saveDialog = null;
    
    this.cachedData = {chunks: null, ratings: null, hasSystemRating: false, modifier: null, minConfidence: null, maxConfidence: null};
    
    this.ratingStore = null;
    
    this.pageSize = 10; // how many workparts to display per page
    
    this.isValid = function() {
        if (Monk.component.dataManager.isWorksetValid()) {
            return true;
        } else {
            return 'Your workset is empty and/or it does not appear to have any training data - please load a workset and rate at least 2 items before proceeding.'
        }
    };
    
    Monk.component.WorksetManagerComponent.superclass.constructor.call(this, args);
}

Workbench.extend(Monk.component.WorksetManagerComponent, Workbench.component.Component, {

    label : "Workset Manager",
    description : "For managing worksets of text workparts in Monk.",
    "window" : this.window,
    handle : function(monkEvent, data){
    
    	if (monkEvent.instanceOf(Monk.event.workbench.LocalWindowFocus)) {
            //this.initializeWorksetGrid();
        }else if (monkEvent.instanceOf(Workbench.event.ComponentLoaded)) {
            if (monkEvent.component == this) {
                this.cacheChunks(Monk.component.dataManager.getWorklist());
            }
        }else if (monkEvent.instanceOf(Monk.event.analysis.GotPrediction)) {
        	
        	var json = Monk.utils.xml2json(data.response.responseXML, " ");
            var workset = "";
            (eval('workset='+json));
            
            var worklistRating = workset.workset.workListRating.split(',');
            var worklist = workset.workset.workList.split(',');
            
            // update system ratings
            var store = this.worksetGrid.getStore();
            for (var i = 0; i < worklist.length; i++) {
                var id = worklist[i];
                var record = store.getById(id);
                record.set('systemRating', worklistRating[i]);
            }
        } else if (monkEvent.instanceOf(Monk.event.workset.WorksetLoaded)) {
          
            this.cacheChunks(Monk.component.dataManager.getWorklist());
            if (this.worksetGrid == null) {
                this.initializeWorksetGrid();
            } else {
                var systemRatingColumn = this.worksetGrid.getColumnModel().getIndexById('systemRating');
                this.worksetGrid.getColumnModel().setHidden(systemRatingColumn, !this.cachedData.hasSystemRating);
                this.worksetGrid.getStore().proxy.update({nodes:this.cachedData.chunks}, false);
                this.worksetGrid.getStore().load({params:{start:0, limit: this.pageSize}});
                this.ratingStore.loadData(this.cachedData.ratings, false);
            }
            var worksetName = Monk.component.dataManager.getWorksetName();
            Monk.component.messenger.growl('Monk Workbench','Workset Loaded: '+worksetName);
            this.worksetGrid.setTitle(Monk.component.dataManager.getWorksetLabel());
            //Ext.get('componentContent').unmask();
        } else if (monkEvent.instanceOf(Monk.event.chunk.ChunkChecked)) {
            // TODO add handling for chunkschecked
            var store = this.worksetGrid.getStore();
            var index = store.proxy.contains(data.id);
            if (index != -1) {
                if (!data.checked) {
                    store.proxy.remove(data.id);
                    this.worksetGrid.getBottomToolbar().refresh();
                }
            } else if (data.checked) {
                data.workLabel = data.text;
                var work = Monk.data.collection.getWork(data.id);
                if (work != null) {
                    data.authorsLabel = work.authorsLabel;
                    data.publicationDates = work.publicationDates;
                }
                store.proxy.update({nodes:[data]}, true);
                this.worksetGrid.getBottomToolbar().moveLast();
            }
        } else if (monkEvent.instanceOf(Monk.event.chunk.ChunksRated)) {
            this.worksetGrid.getStore().proxy.update({nodes:data}, true);
            this.worksetGrid.getStore().load({params:{start:0, limit: this.pageSize}});
            
            for (var i = 0, len = data.length; i < len; i++) {
                var rating = data[i].userRating;
                if (this.ratingStore.getById(rating) == null && rating != '') {
                    var ratingData = []
                    ratingData['value'] = rating;
                    var record = new Ext.data.Record(ratingData, rating);
                    this.ratingStore.add(record);
                }
            }
        } else if (monkEvent.instanceOf(Monk.event.workset.WorksetCreated)) {
            var recordDef = Ext.data.Record.create([
                {name: 'id'},
                {name: 'label'},
                {name: 'trainingList'},
                {name: 'trainingListRating'},
                {name: 'workList'},
                {name: 'workListRating'},
                {name: 'itinerary'},
                {name: 'feature'}
            ]);
            var workset = Monk.component.dataManager.getWorkset();
            workset.id = workset.worksetId;
            workset.label = workset.worksetName;
            var record = new recordDef(workset); // creates a record with auto-ID
            var recordCopy = record.copy(workset.id); // make a copy with the correct ID
            this.worksetsStore.add(recordCopy);
            
            this.saveDialog.hide();
            var worksetName = Monk.component.dataManager.getWorksetName();
            Monk.component.messenger.growl('Monk Workbench','Workset Created: '+worksetName);
        } else if (monkEvent.instanceOf(Monk.event.workset.WorksetSaved)) {
            var worksetName = Monk.component.dataManager.getWorksetName();
            Monk.component.messenger.growl('Monk Workbench','Workset Saved: '+worksetName);
        } else if (monkEvent.instanceOf(Monk.event.workset.WorksetReset)) {
            this.resetGrid();
            //document.getElementById("count").innerHTML = 0;
        } else if (monkEvent.instanceOf(Monk.event.chunk.ChunksContainingFeatureWithinWorksetRetrieved)) {
            var json = Monk.utils.xml2json(data.response.responseXML, " ");
            var works = "";
            (eval('works='+json));

            var combinedWorks = [];
            if (works.works.work != null) { // json array fix
                if (works.works.work.length == null) {
                    works.works.work = [works.works.work];
                }
                combinedWorks = combinedWorks.concat(works.works.work);
            }
            if (works.works.workpart != null) { // json array fix
                if (works.works.workpart.length == null) {
                    works.works.workpart = [works.works.workpart];
                }
                combinedWorks = combinedWorks.concat(works.works.workpart);
            }
            
            var columns = this.worksetGrid.getColumnModel();
            if (columns.isHidden(4)) {
                // show features if hidden
                columns.setHidden(4, false);
            }
            
            for (var i = 0, length = combinedWorks.length; i < length; i++) {
                var workId = combinedWorks[i].id;
                var posTest = /\s+\(.+?\)$/;
                var feature = data.feature.split(posTest)[0];
                Monk.component.dataManager.addTermsToChunk(workId, [feature]);
                //var features = Monk.component.dataManager.getTermsForChunk(workId).join(', ');
                //this.worksetGrid.getStore().proxy.updateRecord(workId, {features: features});
            }
            this.worksetGrid.getBottomToolbar().refresh();
        }
        
    },
    
    cacheChunks : function(worklist) {
        var chunks = [];
        var ratingsObject = {};
        this.cachedData.hasSystemRating = false;
        var minConfidence = null;
        var maxConfidence = null;
        
        for (var chunkId in worklist) {
            var work = Monk.data.collection.getWork(chunkId);
            var chunk = worklist[chunkId];
            
            if (chunk.userRating != null) {
                ratingsObject[chunk.userRating] = [chunk.userRating];
            }
            if (chunk.systemRating) this.cachedData.hasSystemRating = true;
            
            var confidence = parseFloat(chunk.confidence);
            if (confidence < minConfidence || minConfidence == null) minConfidence = confidence;
            if (confidence > maxConfidence || maxConfidence == null) maxConfidence = confidence;

            chunk.id = chunkId;
            if (work != null) {
                chunk.workLabel = work.text;
                chunk.authorsLabel = work.authorsLabel;
                chunk.publicationDates = work.publicationDates;
            }
            chunks.push(chunk);
        }
        if (chunks.length > 0) {
            var ratingsArray = [];
            for (var rating in ratingsObject) {
                ratingsArray.push(ratingsObject[rating]);
            }
            this.cachedData.chunks = chunks
            this.cachedData.ratings = ratingsArray;
            
            this.cachedData.modifier = minConfidence * -1;
            this.cachedData.minConfidence = minConfidence + this.cachedData.modifier;
            this.cachedData.maxConfidence = maxConfidence + this.cachedData.modifier;
        }       
    },
    
    getFeatures : function(pager) {
        pager.store.each(function(record){
            var id = record.get('id');
            var features = Monk.component.dataManager.getTermsForChunk(id);
            if (features != null) {
                record.set('features', features.join(', '));
            }
        }, this);
    },
    
    getWorksetFromStore : function(worksetId) {
        var record = this.worksetsStore.getById(worksetId);
        return {
            id: worksetId,
            label: record.get('label'),
            projectId: Monk.component.dataManager.getProjectId(),
            trainingList: trainingList = record.get('trainingList'),
            trainingListRating: record.get('trainingListRating'),
            workList: record.get('workList'),
            workListRating: record.get('workListRating'),
            itinerary: record.get('itinerary'),
            feature: record.get('feature')
        }
        
    },
    
    initializeLoadDialog : function() {
        var worksetsCombo = new Ext.form.ComboBox({
            fieldLabel: 'Workset',
            name: 'workset',
            width: 175,
            maxHeight: 75,
            store: this.worksetsStore,
            triggerAction: 'all',
            allowBlank: false,
            editable: false,
            forceSelection: true,
            displayField: 'label',
            hiddenName: 'worksetId',
            valueField: 'id'
        });
        
        var loadForm = new Ext.form.FormPanel({
            labelAlign: 'right',
            labelWidth: 70,
            border: false
        });
        
        loadForm.add(worksetsCombo);
        this.loadDialog = new Ext.Window({
            title: 'Load Workset',
            height: 100,
            width: 300,
            modal: true,
            shadow: true,
            plain: true,
            border: false,
            collapsible: false,
            items: loadForm,
            buttons: [
                {
                    text: 'Load',
                    handler: function(){
                        if (loadForm.getForm().isValid()) {
                        	var values = loadForm.getForm().getValues();
                            var worksetId = values.worksetId;
                            
                            var workset = this.getWorksetFromStore(worksetId);
                            
                            // reset grid for repopulating
                            var store = this.worksetGrid.getStore();
                            store.removeAll();
                            
                            //Ext.get("componentContent").mask("Loading workset...");
                            Monk.component.dataManager.setWorkset(workset);
                            
                            loadForm.getForm().reset();
                            this.loadDialog.hide();
                        }
                    },
                    scope: this
                },
                {
                    text: 'Cancel',
                    handler: function(){
                        this.loadDialog.hide();
                    },
                    scope: this
                }
            ]
        });
        
        this.loadDialog.show();
    },
    
    initializeSaveDialog : function() {
        var worksetName = new Ext.form.TextField({
            fieldLabel: 'Name',
            name: 'name',
            width: 175,
            allowBlank: false
        });
        
        var saveForm = new Ext.form.FormPanel({
            labelAlign: 'right',
            labelWidth: 70,
            border: false
        });
        
        saveForm.add(worksetName);
        
        this.saveDialog = new Ext.Window({
            title: 'Save Workset',
            height: 100,
            width: 300,
            modal: true,
            shadow: true,
            plain: true,
            border: false,
            collapsible: false,
            items: saveForm,
            buttons: [
                {
                    text: 'Save',
                    handler: function(){
                        if (saveForm.getForm().isValid()) {
                            var values = saveForm.getForm().getValues();
                            var worksetName = values.name;
                            this.createNewWorkset(worksetName);
                            
                            saveForm.getForm().reset();
                        }
                    },
                    scope: this
                },
                {
                    text: 'Cancel',
                    handler: function(){
                        this.saveDialog.hide();
                    },
                    scope: this
                }
            ]
        });
        
        this.saveDialog.show();
    },
    
    createNewWorkset : function(worksetName) {
        Monk.component.dataManager.setWorksetName(worksetName); 
        
        // TODO: make sure training list is being sent with training list ratings
        var workset = Monk.component.dataManager.getWorkset();
        Monk.data.workset.createWorkset(workset);
    },
    
    loadWorkset : function() {
        if (this.loadDialog == null) {
            this.initializeLoadDialog();
        } else {
            this.loadDialog.show();
        }
    },
    
    saveWorksetAs : function() {
        this.saveWorkset(true);
    },
    
    /**
     * Compiles the workset and saves it.
     * @param {Boolean} saveAs If true, save workset as a copy
     */
    saveWorkset : function(saveAs) {
        var worksetId = Monk.component.dataManager.getWorksetId();
        if (worksetId == null || saveAs == true) {
            // create new workset (or save a new copy)
            if (this.saveDialog == null){
                this.initializeSaveDialog();
            } else {
                this.saveDialog.show();
            }
        } else {
            // save the workset
            var workset = Monk.component.dataManager.getWorkset();
            if (workset.trainingSet == null) {
                // dummy data
                workset.trainingSet = workset.workList.split(',')[0];
                workset.trainingSetRating = 3;
            }
            if (workset.workListRating == null) {
                var count = workset.workList.split(',').length;
                var rating = "3";
                for (var i = 0; i < count - 1; i++){
                    rating += ",3"
                }
                workset.workListRating = rating;
            }
            Monk.data.workset.saveWorkset(workset);
        }
    },
    
    resetWorkset : function() {
        this.notify(new Monk.event.workset.WorksetReset({
                label: 'Workset reset'
            })
        );
    },
    
    initializeWorksetGrid : function() {
      
       // debugger;
        var recordDef = Ext.data.Record.create([
          {name: 'id'},
          {name: 'corpus'},
          {name: 'text'},
          {name: 'chunkType'},
          {name: 'checked'},
          {name: 'userRating'},
          {name: 'systemRating'},
          {name: 'confidence', type: 'float'},
          {name: 'features'},
          {name: 'workLabel'},
          {name: 'authorsLabel'},
          {name: 'publicationDates'}
        ]);
        
        var reader = new Ext.data.JsonReader({
            root: 'nodes',
            id: 'id'
        }, recordDef);
        
        var store = new Ext.data.GroupingStore({
            reader: reader,
            proxy: new Ext.ux.data.PagingMemoryProxy({nodes:this.cachedData.chunks}, {reader: reader}),
            remoteSort: true,
            groupField: 'corpus',
            sortInfo:{field: 'text', direction: "ASC"}
        });
        
        this.ratingStore = new Ext.data.SimpleStore({
            fields: ['value'],
            id: 0,
            data: this.cachedData.ratings == null ? [] : this.cachedData.ratings
        });
        
        var ratingCombo = new Ext.form.ComboBox({
            lazyRender: true,
            triggerAction: 'all',
            mode: 'local',
            displayField:'value',
            valueField:'value',
            store: this.ratingStore,
            editable: true,
            forceSelection: false,
            validateOnBlur: false,
            listeners: {
                specialkey: {
                    fn: function(field, event) {
                        if (event.getKey() == event.ENTER) {
                            // stop the editor from moving to the next row
                            this.worksetGrid.stopEditing();
                        }
                    }, scope: this
                }
            }
        });
        
        var confidenceGradient = function(value, metadata, record, rowIndex, colIndex, store, modifier, maxConfidence){
            var rValue = 255;
            var gbValue = Math.floor((value + modifier) / maxConfidence * 255);
            metadata.attr = 'style = "background-color: rgb('+rValue+','+gbValue+','+gbValue+');"';
            return '<div ext:qtip="The confidence in the rating predicted by the system." style="width: 50px;">&#160;</div>'; // just show the colour
        }
        
        var columnModel = new Ext.grid.ColumnModel([
            {header: 'Corpus', width: 50, sortable: true, dataIndex: 'corpus', renderer: function(content, el, data) {return '<span ext:qtip="' + data.data.workLabel + ': ' + data.data.authorsLabel+'">'+content+"</span>"}},
            {header: 'Workpart ID', hidden: true, sortable: true, dataIndex: 'id', renderer: function(content, el, data) {return '<span ext:qtip="' + data.data.workLabel + ': ' + data.data.authorsLabel+'">'+content+"</span>"}},
            {header: 'User Rating', id: 'userRating', sortable: true, dataIndex: 'userRating', editor: ratingCombo, renderer: function(content){
                if (content == '') return '<span style="color: #999999;" ext:qtip="Click to assign a rating.">Click to Rate</span>';
                else return '<span ext:qtip="The rating assigned by the user.">'+content+'</span>';
            }},
            {header: 'Predicted Rating', id: 'systemRating', hidden: !this.cachedData.hasSystemRating, sortable: true, dataIndex: 'systemRating', renderer: function(content, el, data) {return '<span ext:qtip="The rating predicted by the system.">'+content+"</span>"}},
            {header: 'Confidence', width: 60, id: 'confidence', hidden: !this.cachedData.hasSystemRating, sortable: true, dataIndex: 'confidence', renderer: confidenceGradient.createDelegate(this, [this.cachedData.modifier, this.cachedData.maxConfidence], true)},
            {header: 'Title', sortable: true, dataIndex: 'text', renderer : function (content) {return content.length>22 ? '<span ext:qtip="'+content+'">'+content.substring(0,22)+"...</span>" : content}},
            {header: 'Features', sortable: true, dataIndex: 'features', hidden: true},
//			{header: 'Work', width: 150, sortable: true, dataIndex: 'workLabel'},
            {header: 'Author', sortable: true, dataIndex: 'authorsLabel', renderer : function (content) {return content.length>15 ? '<span ext:qtip="'+content+'">'+content.substring(0,15)+"...</span>" : content}},
            {header: 'Date', sortable: true, dataIndex: 'publicationDates', renderer : function (content) {return content.split(/-/)[0]}}
        ]);
        
//        var selectionModel = new Ext.grid.CheckboxSelectionModel();
        
        var thisComponent = this;
        
        var viewport = new Ext.Viewport({
            layout: 'fit',
            renderTo: 'gridParent',
            items: this.worksetGrid
        });
        
        Ext.EventManager.onWindowResize(function(w, h) {
            this.worksetGrid.setHeight(h);
        }, this);
        
        this.worksetGrid = new Ext.grid.EditorGridPanel({
            id : 'workset-manager-grid',
            store: store,
            /*view: new Ext.grid.GroupingView({
                            forceFit:true,
                            groupTextTpl: '{text} ({[values.rs.length]} {[values.rs.length > 1 ? "Items" : "Item"]})'
             }),
             */
            cm: columnModel,
            sm: new Ext.grid.RowSelectionModel(),
            clicksToEdit: 1,
            renderTo: 'worksetGrid',
            height: 300,
            viewConfig: {forceFit: false},
            autoWidth: true,
            listeners: {
                'render': function(grid){
                    var h = viewport.getBox().height;
                    grid.setHeight(h);
                }
            },
            title : 'Workset',
            tbar : new Ext.Toolbar([
                /*new Ext.Button({
                    text : "New",
                    iconCls : 'clearBtn',
                    handler : function() {Monk.component.messenger.confirm(
                        'Monk Workbench',
                        'This will create a new, empty, workset. Do you wish to continue?',
                        function(buttonId) {
                            if (buttonId == 'yes') {this.resetWorkset();}
                        }, this, this.window.parent ? this.window.parent.window : this.window)},
                        scope : this
                }),
                */
                new Ext.Button({
                    text : "Load",
                    iconCls : 'loadBtn',
                    handler : this.loadWorkset,
                    scope : this
                }),
                new Ext.Button({
                    text : "Save",
                    menu : [
                        new Ext.Action({
                            text : 'Save',
                            iconCls : 'saveBtn',
                            handler : this.saveWorkset,
                            scope : this
                        }),
                        new Ext.Action({
                            text : 'Save As...',
                            iconCls : 'saveasBtn',
                            handler : this.saveWorksetAs,
                            scope : this
                        })
                    ],
                    iconCls : 'saveBtn'
                }),
                '-',
                new Ext.Button({
                    text : "Revert",
                    iconCls : 'revertBtn',
                    handler : function() {
                        Monk.component.messenger.confirm(
                        'Monk Workbench',
                        'This will revert the workset to the last time it was saved (and remove any changes made since then). Do you wish to continue?',
                        function(buttonId) {
                            if (buttonId == 'yes') {

                                // reset grid for repopulating
                                this.resetGrid();
                                
                                //Ext.get("componentContent").mask("Loading workset...");
                                var worksetId = Monk.component.dataManager.getWorksetId();
                                if (worksetId == null) {
                                    Monk.component.messenger.alert('Monk Workbench', 'You have no workset to revert back to.', this.window.parent ? this.window.parent.window : this.window);
                                } else {
                                    Monk.component.dataManager.setWorkset(this.getWorksetFromStore(worksetId));
                                }
                                
                            }
                        }, this, this.window.parent ? this.window.parent.window : this.window)						
                    },
                    scope : this
                }),
                new Ext.Button({
                    text : "Import/Export",
                    iconCls : 'exportxmlBtn',
                    menu : [
                        new Ext.Action({
                            text : 'Import tab separated values',
                            handler : function () {
                                Monk.component.messenger.show({
                                   title: 'Import Workset Data',
                                   msg: 'Paste in rows (copied from Excel, for instance) of only two columns where the workset ID and the optional rating are separated by a tab (or space):',
                                   width:600,
                                   defaultTextHeight: 25,
                                   buttons: Ext.MessageBox.OKCANCEL,
                                   multiline: true,
                                   fn: function handleImport(btn, text) {
                                        var rows = text.split(/(\r\n|\r|\n)+/);
                                        var nodes = [];
                                        var parts;
                                        var id;
                                        var workId;
                                        for (var i=0;i<rows.length;i++) {
                                            parts = rows[i].split(/\s+/);
                                            id = parts[0];
                                            workId  = id.split("-");
                                            workId = workId[0]+'-'+workId[1];
                                            if (parts[0]) {
												var work = Monk.data.collection.getWork(workId);
                                                nodes.push({
                                                    id: id,
                                                    userRating: parts[1] || "",
                                                    text: (workId==id ? '' : "[from] ") + (work && work.text ? work.text : 'unknown')
                                                });
                                            }
                                        }
                                        this.notify(
                                            new Monk.event.chunk.ChunksRated({
                                                label: "number of text workparts rated: "+nodes.length
                                            }), nodes);
                                    },
                                   scope: this
                                  }, this.window.parent ? this.window.parent.window : this.window);
                            },
                            scope : this
                        }),
                        new Ext.Action({
                            text : 'Export as tab separated values',
                            handler : function() {
                                var worklist = Monk.component.dataManager.getWorklist();
                                var output = ["Workpart ID","User Rating","System Rating","Workpart Type","Workpart Label"].join("\t") + "\n";
                                for (key in worklist) {
                                    output += [worklist[key].id,worklist[key].userRating,worklist[key].systemRating,worklist[key].chunkType,worklist[key].text].join("\t") + "\n";
                                }
                                Monk.component.messenger.show({
                                    title : 'Export Workset as Tab Separated Values (TSV)',
                                    msg : 'Select all the data below, copy, and paste directly into your favourite spreadsheet program.',
                                    width: 600,
                                    defaultTextHeight: 25,
                                   buttons: Ext.MessageBox.OK,
                                   multiline : true,
                                   value : output,
                                   scope : this
                                }, this.window.parent ? this.window.parent.window : this.window)
                            },
                            scope : thisComponent
                        })/*,
                        
                        new Ext.Action({
                            text : 'export as XML',
                            handler : function() {
                                var worklist = Monk.component.dataManager.getWorklist();
                                var output = ["Workpart ID","User Rating","System Rating","Workpart Type","Workpart Label"].join("\t") + "\n";
                                for (key in worklist) {
                                    output += [worklist[key].id,worklist[key].userRating,worklist[key].systemRating,worklist[key].chunkType,worklist[key].text].join("\t") + "\n";
                                }
                                Monk.component.messenger.show({
                                    title : 'Export Workset as Tab Separated Values (TSV)',
                                    msg : 'Select all the data below, copy, and paste directly into your favourite spreadsheet program.',
                                    width: 600,
                                    defaultTextHeight: 25,
                                   buttons: Ext.MessageBox.OK,
                                   multiline : true,
                                   value : output,
                                   scope : this
                                }, this.window.parent ? this.window.parent.window : this.window)
                            },
                            scope : thisComponent
                        }),
                        */
                    ]
                }),
                '-',
                new Ext.Button({
                    text : "Remove Workpart",
                    iconCls : 'deleteBtn',
//					handler: this.resetWorksetWithConfirmation,
                    handler : function() {
                        var selections = this.worksetGrid.getSelectionModel().getSelections();
                        if (selections.length>0) {
                            Monk.component.messenger.confirm(
                                'Monk Workbench',
                                'Are you sure you want to remove the selected text(s) from the workset?',
                                function(buttonId) {
                                    if (buttonId == 'yes') {
                                        for (var i=0;i<selections.length;i++) {
                                            var node = selections[i];
                                            this.notify(new Monk.event.chunk.ChunkChecked({
                                                label: "Text workpart unchecked: " + node.data.text + '"',
                                                checked: false
                                            }), {
                                                id: node.id,
                                                checked: false
                                            }, this);
                                        }
                                    }
                                }, this, this.window.parent ? this.window.parent.window : this.window)							
                        }
                        else {
                            Monk.component.messenger.alert('No Workpart Selected', 'Please select a workpart before attempting to remove it.', this.window.parent ? this.window.parent.window : this.window);
                        }
                    },
                    scope : this
                })
            ]),
            bbar: new Ext.PagingToolbar({
                store: store,
                pageSize: this.pageSize,
                displayInfo: true,
                displayMsg: '{0} - {1} of {2}',
                emptyMsg: 'No workparts to display',
                listeners: {
                    'pagechange': this.getFeatures,
                    scope: this
                },
                plugins: [new Ext.ux.PageSizePlugin()]
            })
        });
        
        this.worksetGrid.on('cellclick', function(grid, rowIndex, columnIndex, event) {
            var store = grid.getStore();
            var record = store.getAt(rowIndex);
            
            // send chunk selected event, unless it's the user rating column
            var userRatingIndex = grid.getColumnModel().getIndexById('userRating');
            if (columnIndex != userRatingIndex) {
                var text = record.get('text');
                var chunkType = record.get('chunkType');
                var id = record.get('id');
                var corpus = record.get('corpus');
                
                this.notify(new Monk.event.chunk.ChunkSelected({
                        label: 'Text workpart selected: '+'"'+text+'"'
                    }),
                    {id: id, corpus: corpus, text: text, chunkType: chunkType, displayText:true}
                );
            }
        }, this);
        
        this.worksetGrid.on('afteredit', function(editEvent){
            var rating = editEvent.value;
            var rowId = editEvent.record.id;
            
            var selections = editEvent.grid.getSelectionModel().getSelections();
            
            for (var i=0;i<selections.length;i++) {
                var id = selections[i].id;
                
                if (rowId != id) {
                    if (selections.length > 1) {
                        selections[i].set("userRating", rating);
                    } else {
                        // the user rated a chunk, then click on another chunk
                        // so assign the rating to the original chunk
                        id = rowId;
                    }
                }
    
                if (this.ratingStore.getById(rating) == null) {
                    var data = []
                    data['value'] = rating;
                    var record = new Ext.data.Record(data, rating);
                    this.ratingStore.add(record);
                }
                
                this.worksetGrid.getStore().proxy.updateRecord(id, {userRating: rating});
                
                this.notify(new Monk.event.chunk.ChunkRated({
                        label: 'Workpart '+id+' was given a rating of '+rating
                    }),{chunkId: id, rating: rating});				
            }
        }, this);
        
        var autoSizeColumns = function() {
            for (var i = 0; i < this.colModel.getColumnCount(); i++) {
                autoSizeColumn(i, this);
            }
            this.store.removeListener('load', autoSizeColumns, this); // only run this the first time
        }
    
        var autoSizeColumn = function(c, grid) {
            var w = grid.view.getHeaderCell(c).firstChild.scrollWidth;
            for (var i = 0, l = grid.store.getCount(); i < l; i++) {
                w = Math.max(w, grid.view.getCell(i, c).firstChild.scrollWidth);
            }
            grid.colModel.setColumnWidth(c, w+2);
            return w;
        }
        
        store.on('load', autoSizeColumns, this.worksetGrid);
        
        if (this.cachedData.chunks != null) {
            store.load({params:{start:0, limit: this.pageSize}});
            this.cachedData == {chunks: null, ratings: null};
            this.worksetGrid.setTitle(Monk.component.dataManager.getWorksetLabel());
        }
        
    },
    
    resetGrid : function() {
        this.worksetGrid.getStore().removeAll();
        this.worksetGrid.getStore().proxy.update({nodes:[]});
        this.worksetGrid.getStore().load();
        this.ratingStore.removeAll();
        this.worksetGrid.setTitle('Workset');
    }
});
