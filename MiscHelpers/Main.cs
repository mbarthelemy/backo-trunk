using System;
using System.IO;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;

namespace MiscHelpers
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			Console.WriteLine ("Tool reserved for internal development, useless otherwise.");
			Console.WriteLine ("Generate Base64 dummy X509certificate...");
			//byte[] dummyClientCert = File.ReadAllBytes("dummy.pfx");
			X509Certificate2 dummy = new X509Certificate2("dummy.pfx", "");
			byte[] dummyClientCert = dummy.Export(X509ContentType.Pkcs12, "");
			Console.WriteLine (Convert.ToBase64String(dummyClientCert));
		}
	}
}
