using System;
using P2PBackup.Common;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace P2PBackupHub {
	
	/// <summary>
	/// a Task is an operation (backup, restore (later there will be Check tasks)), scheduled or user-requested, 
	/// 	put on scheduler queue.
	/// It must me used in the following order:
	/// -instanciate, 		new Task()
	/// -creation : 		task.Create()    (after this step, task has an unique tracking ID)
	/// -execution : 		task.Start()
	/// -completed, save info : 	task.Complete()
	/// </summary>
	/*[Serializable]*/
	
	//[DataContract(Name = "Task", Namespace= "http://schemas.datacontract.org/2004/07/")]
	/*[DataContract]
	[KnownType(typeof(TaskRunningStatus))]
	[KnownType(typeof(TaskStatus))]
	[KnownType(typeof(TaskType))]
	[KnownType(typeof(TaskAction))]
	[KnownType(typeof(TaskOperation))]
	[KnownType(typeof(TaskPriority))]
	[KnownType(typeof(Node))]
	[KnownType(typeof(BackupSet))]
	[KnownType(typeof(P2PBackup.Common.Task))]*/
	public class RunTask : P2PBackup.Common.Task{
		
		private TaskRunningStatus runningStatus;
		

		internal void Create() {
			this.Id = (new DBHandle()).AddBackupTracking(this);
			base.RunStatus = TaskRunningStatus.Unknown;
			base.Status = TaskStatus.Null;
			base.StartDate = DateTime.Now;
			base.EndDate = DateTime.Now;
			
		}

		internal static void Create(Task t) {
			t.Id = (new DBHandle()).AddBackupTracking(t);
			//t.RunningStatus = TaskRunningStatus.Unknown;
			//t.Type = TaskType.Scheduled; 
			t.Status = TaskStatus.Null;
			t.StartDate = DateTime.Now;
			t.EndDate = DateTime.Now;
			
		}
		
		internal void Start(){
			base.RunStatus = TaskRunningStatus.PendingStart;
			base.Status = TaskStatus.Ok;
		}

		internal static void Start(Task t){
			t.RunStatus = TaskRunningStatus.PendingStart;
			t.Status = TaskStatus.Ok;
		}
		
		internal void Complete() {
			//this.RunningStatus = TaskRunningStatus.Done;
			(new DBHandle()).CompleteBackupTracking(this);
		}

		internal static void Complete(Task t) {
			//this.RunningStatus = TaskRunningStatus.Done;
			(new DBHandle()).CompleteBackupTracking(t);
		}

		internal RunTask(){
			base.LogEntries = new List<Tuple<DateTime, int, string, string>>();
			base.IndexStorageNodes = new List<int>();
			base.RunStatus = TaskRunningStatus.Unknown;
		}
		
		internal  TaskRunningStatus RunningStatus{
			get {
				return this.runningStatus;
			}
			set {
				if(this.runningStatus != value){
					this.runningStatus = value;
					TaskPublisher.Instance().Notify(this);
				}
			}
		
		}

		internal P2PBackup.Common.Task GetBase(){
			return (P2PBackup.Common.Task)this ;
		}
	}
}

