using System;
using System.Collections;
using P2PBackupHub.Utilities;
using P2PBackup.Common;

namespace P2PBackupHub{
	/// <summary>
	/// Holds all online users
	/// </summary>
	
	[Serializable]
	public class NodeList:ArrayList	{

		private static readonly object _locker = new object();

		public NodeList(){
	
		}

		internal void AddNode(ClientNode node){
			node.RemoveUserEvent += new P2PBackupHub.ClientNode.RemoveUserHandler(RemoveNode);
			//node.CalculateDestinationEvent += new P2PBackupHub.Node.CalculateDestinationHandler(CalculateDestination);
			//node.GetClientEvent += new P2PBackupHub.Node.GetClientHandler(GetClient);
			lock(_locker){
				if(GetNode(node.Uid) != null)
					RemoveNode(GetNode(node.Uid));
				
				this.Add(node);	
			}
		}

		/// <summary>
		/// Removes a user from the UserList, the user is no longer online
		/// </summary>
		/// <param name="user">the user to remove</param>
		internal void RemoveNode(ClientNode node){
			try{
				lock(_locker)
					this.Remove(node);
				node.RemoveUserEvent -= new P2PBackupHub.ClientNode.RemoveUserHandler(RemoveNode);
			}
			catch(Exception e){
				Logger.Append("HUBRN", Severity.WARNING, "Could not remove node from online list :"+e.Message);
			}
		}

		
		public ClientNode GetNode(int id){
			foreach(ClientNode n in this)
				if(n.Uid == id)
					return n;
			return null;
		}
		
		public bool IsOnline(int nodeId){
			foreach(ClientNode n in this)
				if(n.Uid == nodeId)
					return true;
			return false;
		}
	}
}
