using System;
using System.Net;
using System.Net.Sockets;
using System.Net.Security;
using System.IO;
using System.Text;
using System.Xml;
using System.Linq;
using System.Threading;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Configuration;
using System.Collections.Generic;

//using System.Xml.Serialization;
using System.Runtime.Serialization.Formatters;
using System.ServiceModel;
using System.Runtime.Serialization;

using P2PBackupHub.Utilities;
using P2PBackup.Common;

namespace P2PBackupHub{

	/// <summary>
	/// Holds all information about an online node
	/// </summary>
	//public class Node:IClientNode, IEquatable<IClientNode>{
	public class ClientNode : P2PBackup.Common.Node {
		/*private string userName;
		private bool isUnixClient;
		private long available;
		//private long filesize;
		private string ip;
		private string listenIp;
		private int listenPort; 
		private bool locked;
		private string pubKey;
		private string certCN;
		//[field: NonSerialized]	private byte[] readBuffer;
		//private bool run = false;
		private NodeStatus status;
		private long share;
		private long quota;
		private long usedQuota;
		private string shareRoot;
		private int userId;
		private int storageGroup;
		private int groupId;
		private int backupSets;
		private string nodeName;
		private string os;
		[field: NonSerialized] private string verificationHash;
		private string version = "";*/
		
		[field: NonSerialized] private DBHandle dbhandle;	
		[field: NonSerialized] private SslStream hubStream;
		[field: NonSerialized] private bool verified = false;
		[field: NonSerialized] private string browseData ="";	
		[field: NonSerialized] private string specialObjects = "";
		[field: NonSerialized] private string drivesData = "";
		[field: NonSerialized] private string vms = "";
		[field: NonSerialized]	private ActionType action = ActionType.Default;		// 0 = default	1 = backup	2 = recovery
		
		/*public string UserName{
			set { userName = value; }
			get { return userName; }
		}
		
		public bool IsUnixClient{
			get{return isUnixClient;}
			set{ isUnixClient = value;}
		}
		
		public int Uid {
			get {return this.userId;}
			set{userId = value;}
		}

		public string NodeName{
			get { return nodeName; }
			set { nodeName = value; }
			
		}
		
		public string CertCN{
			get{ return certCN;}	
			set{certCN = value;}
		}
		
		public string PubKey{
			get{ return pubKey;}	
			set{pubKey = value;}
		}
		
		public string OS{
			get{ return os;}	
			set{os = value;}
		}
		
		public string IP{
			get { return ip; }
			set{ip = value;}
		}
		
		public string ListenIp{
			get{return listenIp;}
			set{listenIp = value;}
		}
		
		public int ListenPort{
			get{return listenPort;}
			set{listenPort = value;}
		}
		//public string Password{
		//	get { return password; }
		//}
		public long Available{
			get { return available; }
			set{
				//if(available != value){
					available = (long)value;
					//dbhandle.UpdateStorageHistory(this);
				//}
			}
		}

		public long Quota{
			get { return quota; }
			set{ quota = (long)value;}
		}
		public long UsedQuota{
			get { return usedQuota; }
			set{usedQuota = (long)value;}
		}
		public long StorageSize{
			get{return share;}
			set{share = value;}
		}

		public int StorageGroup{
			get{ return storageGroup;}
			set{storageGroup = value;}
		}
		public int Group{
			get{ return groupId;}
			set{groupId = value;}
		}
		public int NbBackupSets{
			get{ return backupSets;}
			set{backupSets = value;}
		}
		public bool Locked{
			get{return locked;}	
			set{locked = value;}
		}
		

		
		public string ShareRoot{
			get{return shareRoot;}	
			set{shareRoot = value;}
		}
		
		public NodeStatus Status{
			get{return status;}	
			set{status = value;}
		}
		
		public string Version{
			get{return version;}
			set{version = value;}
		}*/
		/// <summary>
		/// Gets or sets the storage space reserved (and not yet consumed) by backup operations.
		/// </summary>
		/// <value>
		/// The reserved space.
		/// </value>
		internal long ReservedSpace{get;set;}
		
		public delegate void NodeOnline(ClientNode node);
		[field: NonSerialized]
		public event NodeOnline NodeOnlineEvent;
		public delegate void RemoveUserHandler(ClientNode node);
		[field: NonSerialized]		
		public event RemoveUserHandler RemoveUserEvent;
		public delegate void LoggHandler (string user, bool received, string message);
		[field: NonSerialized]
		public event LoggHandler LoggEvent;
		//public delegate ClientNode CalculateDestinationHandler(ClientNode node, string nodeToExclude);
		//[field: NonSerialized]
		//public event CalculateDestinationHandler CalculateDestinationEvent;
		public delegate void SessionChangedHandler(bool added, SessionType sessType, int id, ClientNode fromNode, ClientNode toNode, long taskId, int budget);
		public event SessionChangedHandler SessionChanged;

		public delegate ClientNode GetClientHandler(string nodeName);
		[field: NonSerialized]
		public event GetClientHandler GetClientEvent;
		
		//private static Queue<string> MessageQueue;
		
		//public Socket Connection{
		//	get { return connection; }
		//}
		
		
		[NonSerialized]
		internal Socket Connection;
		
		public string BrowseData {
			get {return this.browseData;}
		}
		
		public string DrivesData {
			get {return this.drivesData;}
		}
		
		public string VMS {
			get {return this.vms;}
		}
		
		public string SpecialObjects{ // NT VSS writers
			get {return this.specialObjects;}	
		}
		
		internal void AskStats(long taskId){
			SendMessage("STATS "+taskId);
		}
		
		/*public List<BackupSet> GetBackupSets(){
			return (new DBHandle()).GetNodeBackupSets(this.Uid);
		}*/
		
		/*public ClientNode (){

		}*/

		/*public ClientNode (string name, string ip, string listenIp, int listenPort, string key, long share, long avail, bool locked, 
		             int uid, int sg, int groupId, string version, string os, long quota, long usedQuota, int backupsets){
			this.NodeName = name;
			//this.password = pass;
			this.IP = ip;
			this.ListenIp = listenIp;
			this.Port = listenPort;
			this.PubKey = key;
			//this.certCN = GetCertCN();
			this.StorageSize = share;
			//this.Accessible = access;
			this.Available = avail;
			this.ReservedSpace = 0; // test 20120705
			//this.Locked = locked;
			if(locked)
				this.Status = NodeStatus.Locked;
			this.Uid = uid;
			this.StorageGroup = sg;
			this.Group = groupId;
			//this.shareRoot = root;
			this.Version = version;
			this.OS = os;
			this.Quota = quota;
			this.UsedQuota = usedQuota;
			this.NbBackupSets = backupsets;
			//this.Status = NodeStatus.Idle;
			//Logger.Append("HUBRN", Severity.DEBUG2, "Created new node : Uid="+this.Uid+", NodeName="+this.NodeName
			//+",IP="+this.IP+", listenIp="+this.ListenIp+", listenPort="+this.ListenPort);
		}*/

		/*public ClientNode (string name){
			dbhandle = new DBHandle();
			this.NodeName = name;
		}*/

		
		private string GetCertCN(X509Certificate2 cert){
			return cert.SubjectName.Name;
		}
		
		internal ClientNode(SslStream hubSSlStream, Socket connection){
			try{
				
				dbhandle = new DBHandle();
				this.Connection = connection;
				hubStream = hubSSlStream;
				//MessageQueue = new Queue<string>();
				//ThreadPool.QueueUserWorkItem(ProcessMessages);
				if(LoggEvent != null)
					LoggEvent(this.NodeName, false, "VER "+this.Version);
			}
			catch(Exception ex){
				Logger.Append("HUBRN", Severity.ERROR, ex.Message); 
			}
		}
		
		/*private  void ProcessMessages(Object o){
			while(true){
				if(MessageQueue.Count > 0)
					this.Decode((string)MessageQueue.Dequeue());	
				else
					Thread.Sleep(100);
			}
		}*/
		
		/// <summary>
		/// Decodes incoming messages from client
		/// </summary>
		/// <param name="message">incoming message from client</param>
		private void Decode(string message){
			try{
				if (message == null) return;
				//message = message.Replace(System.Environment.NewLine, ""); // not needed anymore since we have a msg header indicating size
				if (LoggEvent != null) LoggEvent(this.NodeName,true,message);
				string type="";
				char[] separator = {' '};
				string[] decoded = message.Split(separator);
				type = decoded[0];
				switch(type.Trim()){
					case "ACC":
						/*if((decoded.Length == 2) && (verified))
							GetClient(decoded[1]);*/
						break;
					case "BCK":
						if((decoded.Length == 2) && (verified))
							Backup(int.Parse(decoded[1]));
						break;
					case "BRW": // response to browse FS path request
						if(decoded.Length >1 && verified){
							string xml = "";
							for(int i=1; i<decoded.Length; i++) xml += decoded[i]+" ";
							Logger.Append("HUBRN", Severity.DEBUG2, "browse node result : got "+xml);
					   		browseData = xml;
						}
					   	break;
					case "CERTREQ": // new node asks for certificate
						Logger.Append("CLIENT", Severity.DEBUG, "Node asked for certificate");
						CreateAndSendCertificate();
						break;
					case "DBU": // Backup task completed
						//("DBU "+taskId+" "+originalSize+" "+finalSize+" "+indexName+" "+indexSum);
						if((decoded.Length == 10) && (verified)){
							SendAvailableSpace();
							TaskScheduler.UpdateTask(long.Parse(decoded[1]), long.Parse(decoded[2]), long.Parse(decoded[3]), 
						        	long.Parse(decoded[4]), decoded[5], decoded[6], decoded[7],
						                decoded[8].Split(',').Select(n => int.Parse(n)).ToList<int>(), int.Parse(decoded[9]));
							//this.dbhandle.CompleteBackupTracking(int.Parse(decoded[1]), long.Parse(decoded[2]), long.Parse(decoded[3]));
						}
						break;
					case "DRV": // response to mounted drives/partitions request
						if(decoded.Length >1 && verified ){
							string xml = "";
							for(int i=1; i<decoded.Length; i++) xml += decoded[i]+" ";
							Logger.Append("HUBRN", Severity.DEBUG2, "getdrives  result : got "+xml);
					   		drivesData = xml;
						}
					   	break;
					case "CNF": // request for configuration
						if(verified) SendNodeConfiguration();
						break;
					case "EMERGENCY": // unhandled error
						string errorMsg = "";
						for(int i=1; i<decoded.Length; i++) errorMsg += decoded[i]+" ";
						Logger.Append("HUBRN", Severity.ERROR, "Node #"+this.Uid+" has crashed due to the followinf unrecoverable error : "+errorMsg);
						break;
					case "EXP": // expire
						if(verified && decoded.Length >=3){
							Task task = TaskScheduler.Instance().GetTask(long.Parse(decoded[1]));
							// verify if cleaning task exists (for security reasons)
							if(task == null){
								Logger.Append("CLEAN", Severity.ERROR, "Suspect clean request from node "+this.Uid);
								return;
							}
							if(decoded[3] == "DEL" && decoded.Length ==6){ // request to delete stored chunk
								//dbhandle.DeleteTask(long.Parse(decoded[2]));
								Logger.Append("CLEAN", Severity.DEBUG, "Task "+decoded[2]+" asks to delete chunk "+decoded[4]+" from node "+decoded[5]);
								//HubWrite("EXP "+cleanTaskId+" "+ taskId+" DEL "+chunk.Name+" "+ string.Join(",", chunk.StorageDestinations));
							
								int[] storageNodes = Array.ConvertAll(((string)decoded[5]).Split(new char[]{','}, StringSplitOptions.RemoveEmptyEntries), s=>int.Parse(s));
								foreach(int nodeId in storageNodes){
									ClientNode storageNode = Hub.NodesList.GetNode(nodeId);
									if(storageNode != null){
										//ReceiveDelete(int nodeId, long taskId, string cIp, string cN, string cK, string chunkName)
										storageNode.SendMessage("DEL "+this.Uid+" "+decoded[2]+" "+this.IP + " " + this.NodeName + " " + this.PubKey+" "+decoded[4]);
									}
								}
							}
							else if(int.Parse(decoded[3]) == 810){ // unable to find or read backup index
								dbhandle.DeleteTask(long.Parse(decoded[2]));
								Logger.Append("CLEAN", Severity.WARNING, "Deleting damaged task "+decoded[2]);
								TaskScheduler.AddTaskLogEntry(long.Parse(decoded[1]), int.Parse(decoded[3]), decoded[2], decoded[2], "");
								
							}
							else if(int.Parse(decoded[3]) == 710){ // cleaning done
								dbhandle.UpdateTaskStatus(long.Parse(decoded[2]), TaskRunningStatus.Expired);
								//dbhandle.DeleteTask(long.Parse(decoded[2]));
								Logger.Append("CLEAN", Severity.INFO, "Task "+decoded[2]+" cleaned.");
							}
							
							else
								throw new ProtocolViolationException("Incorrect number of parameters for message "+message);
							task.Percent += 1/ task.TotalItems *100;
						}
					
						break;
					case "LFI":
						if(decoded.Length == 2 && verified)	GetLastFullBackupInformation(int.Parse(decoded[1]));
						break;
					case "LOG": // asks for authentication
						CheckApproval();
						break;
					/*case "NST":
						if((decoded.Length == 2) && (verified))
							GetDestination(filename, filesize);
						break;
					case "OFF":
						if((decoded.Length == 2) && (verified))
							GetDestination(filename, filesize);
						break;*/
					case "PIX": // asks storage destination to put index(es)
						if((decoded.Length == 4) && (verified))
							//ChooseDestinations(int.Parse(decoded[1]), decoded[2], long.Parse(decoded[3]), true);7
							//ChooseDestinations(int.Parse(decoded[1]), int.Parse(decoded[2]), true);
							GetIndexDestination(int.Parse(decoded[1]), decoded[2], decoded[3]);
						break;
					/*case "PUT": // client node asks where to store chunk
						if((decoded.Length == 4) && (verified)){
							//GetDestination(int.Parse(decoded[1]), decoded[2], decoded[3], false, null);
							ChooseDestinations(int.Parse(decoded[1]), decoded[2], long.Parse(decoded[3]), false);
						}
						break;*/
					case "PUT": // client node asks where to store chunks
						if((decoded.Length == 4) && (verified)){
							//GetDestination(int.Parse(decoded[1]), decoded[2], decoded[3], false, null);
							ChooseDestinations(long.Parse(decoded[1]), int.Parse(decoded[2]),  int.Parse(decoded[3]), false, false);
						}
						break;
					case "ALT": // client node couldn't connect to storage node, ask alternate destination
						if((decoded.Length == 4) && (verified))
							ChooseDestinations(long.Parse(decoded[1]), int.Parse(decoded[2]),  int.Parse(decoded[3]), false, true);
						break;
					case "REC":
						if((decoded.Length == 2) && (verified))
							GetSource(decoded[1]);
						break;
					case "RIX":
						if((decoded.Length == 2) && (verified))
							GetIndexSource(decoded[1]);
						break;
					/*case "RSP":
						if (decoded.Length == 2)
							VerifyUser(decoded[1]);
						break;*/
					case "SHA": // tells to update storage space
						if ((decoded.Length == 4) && (verified))
							//SaveShare(decoded [1]);
							UpdateStorageSpace(long.Parse(decoded[1]), long.Parse(decoded[2]), int.Parse(decoded[3])); 
							//Console.WriteLine("TODO ! !! save share size");
						break;
					case "SPO": // lists special backupable application objects
						if ( (verified)){
							string xml = "";
							for(int i=1; i<decoded.Length; i++) xml += decoded[i]+" ";
							specialObjects = xml;
						}
						break;
					case "STATS": //response to a task's STATS request
						if ( (verified)){
						// backup.OriginalSize+" "+backup.FinalSize+" "+backup.TotalItems+" 0";
							TaskScheduler.Instance().UpdateTaskStats(long.Parse(decoded[1]), long.Parse(decoded[2]), long.Parse(decoded[3]), long.Parse(decoded[4]), int.Parse(decoded[5]));
						}
						break;
					case "TSK": //"TSK "+taskId+" "+code+" "+data+" "+additionalMessage
						int taskId = int.Parse(decoded[1]);
						int code = int.Parse(decoded[2]);
						string data = String.Empty;
						string msg = String.Empty;
						if (decoded.Length > 3){
							data = decoded[3];
							for(int i=4; i<decoded.Length; i++) msg += decoded[i]+" ";
						}
							//for(int i=3; i<decoded.Length; i++) data += decoded[i]+" ";
						if(code <700){
							if(code == 699){ // session ended
								//TaskScheduler.Instance().RemoveTaskSession(taskId, int.Parse(data));
								if (SessionChanged != null) SessionChanged(false, SessionType.Backup, short.Parse(data), this, null, taskId, 0);
							}
						}
						if(code == 700){ // messages updating CurrentAction, not archived
							TaskScheduler.Instance().SetTaskCurrentActivity(taskId, code, msg);
							return;
						}
						else if(code <800) // INFO class messages
							TaskScheduler.Instance().SetTaskCurrentActivity(taskId, code, data);
						else if(code <900){// WARNING class messages
							TaskScheduler.Instance().SetTaskStatus(taskId, TaskStatus.Warning);
						}
						else{ // ERROR messages
							TaskScheduler.Instance().SetTaskCurrentActivity(taskId, code, data);
							TaskScheduler.Instance().SetTaskStatus(taskId, TaskStatus.Error);
						}
						TaskScheduler.AddTaskLogEntry(taskId, code, data, data, msg);
						break;
					case "UNKNOWN": // unknown task
						if(decoded.Length == 2 && verified){
							TaskScheduler.Instance().SetTaskStatus(long.Parse(decoded[1]), TaskStatus.Error);
							TaskScheduler.Instance().SetTaskRunningStatus(long.Parse(decoded[1]), TaskRunningStatus.Cancelled);
							Logger.Append("HUBRN", Severity.ERROR, "Task "+decoded[1]+" is unknown to node #"+this.Uid+", node was probably restarted.");
						}
						break;
					case "VER": // send version and OS
						if(decoded.Length == 3 && verified){
							this.Version = decoded[1];
							this.OS = decoded[2];
							dbhandle.UpdateInfo(this);
						}
						break;
					case "VMS": // send hosted VMs
						if(decoded.Length >1 && verified){
							string xml = "";
							for(int i=1; i<decoded.Length; i++) xml += decoded[i]+" ";
							Logger.Append("HUBRN", Severity.DEBUG2, "getvms  result : got "+xml);
					   		vms = xml;
						}
					   	break;
					case "701":
						Logger.Append("HUBRN", Severity.WARNING, "Backupset "+decoded[1]+", path \""+decoded[2]+"\" does not exist");
						dbhandle.AddBackupSetError(int.Parse(decoded[1]), type.Trim(), decoded[2]);
						break;
					case "702":
						Logger.Append("HUBRN", Severity.WARNING, "Backupset "+decoded[1]+", path \""+decoded[2]+"\" acces denied");
						dbhandle.AddBackupSetError(int.Parse(decoded[1]), type.Trim(), decoded[2]);
						break;
					
					case "800":
						Logger.Append("HUBRN", Severity.WARNING, "Backupset "+decoded[1]+" cannot be processed by client : too many jobs. |TODO| maintain job on queue");
						break;
					default : 
						Logger.Append("HUBRN", Severity.WARNING, "Received non decodable message (was:'"+type+"')");
						break;
				}
			}
			catch(Exception ex){
				Logger.Append("CLIENT", Severity.WARNING, ex.Message+"--"+ex.StackTrace + "(method : "+ex.TargetSite+")"); 
			}
		}


		/// <summary>
		/// Creates a new thread to listen for messeages from this user
		/// </summary>
		internal void StartListening(){
			try{
				StateObject state = new StateObject();
        			//state.workSocket = hubSocket;
				state.stream = hubStream;
				byte[] header = new byte[4];
				//hubSocket.BeginReceive(state.buffer, 0, header.Length, 0, this.HeaderReceived, state);
				hubStream.BeginRead(state.buffer, 0, header.Length, this.HeaderReceived, state);
			}
			catch(Exception ex){
				Logger.Append("HUBRN", Severity.WARNING, ex.Message); 
			}
		}
		
		private void HeaderReceived(IAsyncResult ar){
			StateObject so = (StateObject) ar.AsyncState;
			hubStream = so.stream;
			try{
				int read = hubStream.EndRead(ar);
				if(read == 0){
					Logger.Append("HUBRN", Severity.INFO, "Node #"+this.Uid+" ("+this.NodeName+") has disconnected");
					Disconnect();
					return;
				}
			}
			catch{
				Logger.Append("HUBRN", Severity.INFO, "Node #"+this.Uid+" ("+this.NodeName+") has disconnected");
				Disconnect();
				return;
			}
			int msg_length = BitConverter.ToInt32(so.buffer, 0);
			//Logger.Append("rcvd", Severity.DEBUG2, "Received message header, msg size will be "+msg_length);
			hubStream.BeginRead(so.buffer, 0, msg_length, new AsyncCallback(MessageReceived), so);
		}
		
		private void MessageReceived(IAsyncResult ar){
			try{
				StateObject so = (StateObject) ar.AsyncState;
				hubStream = so.stream;
				int read = hubStream.EndRead(ar);
				Decode(Encoding.UTF8.GetString(so.buffer, 0, read));
				hubStream.BeginRead(so.buffer, 0, 4, new AsyncCallback(HeaderReceived), so);
			}
			catch(Exception ioe){
				Logger.Append(this.NodeName,  Severity.ERROR, "Error reading data, Disconnecting session. " + ioe.Message);
				Disconnect();
			}
		}	
		
		
		/*private string Receive(string rawData){
			int idx = rawData.IndexOf(System.Environment.NewLine);
			if(idx > 0){
				string m = rawData.Substring(0, idx);
				//Decode(m);
				MessageQueue.Enqueue(m);
				if(idx < rawData.Length-1) 
					Receive(rawData.Substring(idx,rawData.Length-1-idx));
			}
			return rawData;
		}*/
		

		/// <summary>
		/// Logs the user out
		/// </summary>
		public void Logout (){
			Disconnect();
		}

		internal void Approve(){
			verified = true;
			SendMessage("201");
		}
		
		private void CheckApproval(){
			// 1st case : node sent a null certificate (new node?)
			if(hubStream.RemoteCertificate == null){
				SendMessage("401");
				return;
			}
			X509Certificate2 remoteCert = new X509Certificate2(hubStream.RemoteCertificate);
			string nodePubKey = remoteCert.PublicKey.Key.ToXmlString(false);
			P2PBackup.Common.Node temp = dbhandle.NodeApproved(remoteCert.Subject.ToLower().Replace("cn=",""), remoteCert.SerialNumber);
			if(temp != null){
				//attempt = 0;
				this.IP = Connection.RemoteEndPoint.ToString().Split(':')[0]; //temp.IP;
				this.ListenIp = temp.ListenIp;
				this.Port = temp.Port;
				this.NodeName = temp.NodeName;
				this.InternalId = temp.InternalId;
				//this.PubKey = temp.PubKey;
				this.PubKey = remoteCert.PublicKey.Key.ToXmlString(false);
				this.StorageSize = temp.StorageSize;
				this.Available = temp.Available;
				this.Uid = temp.Uid;
				this.Group = temp.Group;
				this.OS = temp.OS;
				this.Quota = temp.Quota;
				this.UsedQuota = temp.UsedQuota;
				this.LastConnection = DateTime.Now;
				this.StorageGroup = temp.StorageGroup;
				this.StoragePriority = temp.StoragePriority;
				this.Kind = temp.Kind;

				Logger.Append("HUBRN", Severity.DEBUG2, "Instanciated new node : Uid="+this.Uid+", NodeName="+this.NodeName
					+",IP="+this.IP+", listenIp="+this.ListenIp+", listenPort="+this.Port);
				Logger.Append("HUBRN", Severity.INFO, "Node #"+this.Uid+" ("+this.NodeName+") is online.");
				if (NodeOnlineEvent != null) NodeOnlineEvent(this);
				if (temp.Status != NodeStatus.Locked){
					verified = true;
					SendMessage("201");
					this.Status = NodeStatus.Idle;
					//Challenge();
				}
				else{
					SendMessage("502"); // pending for approval
					if(LoggEvent!=null) LoggEvent(this.NodeName, false, "502");
				}
			}
			else{
				SendMessage("401"); // new node, unknown by hub. let's add it in "pending for approval" status
				string nodeIP = Connection.RemoteEndPoint.ToString().Split(':')[0];
				dbhandle.AddNode("", Dns.GetHostEntry(nodeIP).HostName , nodeIP, remoteCert);
			}
		}
		
		private void GetLastFullBackupInformation(int bsId){
			SendMessage("LFI_"+bsId+ dbhandle.GetLastFullBackupInformation(bsId));
		}
		/// <summary>
		/// Verifies the user by comparing the two hashed strings
		/// calls a method to add the users ip to the database 
		/// and sends an "OK" message to the user if they are equal
		/// </summary>
		/// <param name="response">the hashed string received from the user</param>
		/*private void VerifyUser(string response){	
			// mbarthelemy : buggy? datetime format??
			//if (verificationHash == response)
			if(response != ""){
				Console.WriteLine("lilute ="+response);
				attempt = 0;
				verified = true;
				if( ((IPEndPoint)hubStream. .RemoteEndPoint).Address.ToString() != this.ip){
					Console.WriteLine("client ip has changed");
					if(ConfigurationManager.AppSettings["Security.AllowClientIpChange"].ToLower()=="true"){
						Console.WriteLine("lilute1");
						if(ConfigurationManager.AppSettings["autoStoreClientIpChange"].ToLower()=="true"){
							this.ip = ((IPEndPoint)Connection.RemoteEndPoint).Address.ToString();
							dbhandle.SaveIP(this.nodeName, this.ip);
							Logger.Append("HUBRN","Hub.User.VerifyUser", Severity.WARNING, "client "+this.nodeName+" connected with different IP ("
					              +((IPEndPoint)Connection.RemoteEndPoint).Address.ToString()+") than hub expected ("
					              +this.ip+"). Updated information");
						}
					}
					else{
						Console.WriteLine("lilute2");
						Logger.Append("HUBRN","Hub.User.VerifyUser", Severity.WARNING, "client "+this.nodeName+" connected with different IP ("
					              +((IPEndPoint)Connection.RemoteEndPoint).Address.ToString()+") than hub expected ("
					              +this.ip+"). Hub parameters do not allow that, refusing user."); 
						verified = false;
					}
					Console.WriteLine("lilute3");		
				}
				Console.WriteLine("verifyrequiredparameters");
				VerifyRequiredParameters();
			}
			else{
				attempt++;
				if (attempt < 3){
					SendMessage("402");
					if(LoggEvent!=null) LoggEvent(this.nodeName, false, "402");
				}
				else{
					SendMessage("502");
					if(LoggEvent!=null) LoggEvent(this.nodeName, false, "502");
				}
				
			}
		}*/
		
		/*private void VerifyUser(string response){	
			// mbarthelemy : buggy? datetime format??
			//if (verificationHash == response)
			if(response != ""){
				attempt = 0;
				verified = true;
				
				VerifyRequiredParameters();
			}
			else{
				attempt++;
				if (attempt < 3){
					SendMessage("402");
					if(LoggEvent!=null) LoggEvent(this.nodeName, false, "402");
				}
				else{
					SendMessage("502");
					if(LoggEvent!=null) LoggEvent(this.nodeName, false, "502");
				}
				
			}
		}*/
		
		

		/// <summary>
		/// Creates a timestamp and a random number and sends it to the client
		/// </summary>
		/// <param name="pass">the user's password</param>
		/*private void Challenge (){
			string timeString;
			string trimmedTime;

			DateTime time = DateTime.Now;
			Random rand = new Random((int)time.Ticks);
			int no = rand.Next();
			timeString = time.ToString();
			trimmedTime = timeString.Remove(10, 1);

			string result;
			result = password + trimmedTime + no;
			verificationHash = HashString(result);
			SendMessage("CHA " + trimmedTime + " " + no.ToString ());
			if(LoggEvent!=null) LoggEvent(this.nodeName, false, "CHA " + trimmedTime + " " + no.ToString ());
		}*/


		/// <summary>
		/// Calls a method to delete this user
		/// </summary>
		/// <param name="uN">username</param>
		/*public void Delete(string uN){
			dbhandle.DeleteUser(uN);
		}*/

		
		internal void SendMessage(string message){
			byte[] byteMsg = Encoding.UTF8.GetBytes(message);
			int msgSize = byteMsg.Length;
			byte[] header = BitConverter.GetBytes(msgSize); // header always has int size (4 bytes)
			try{
				lock(hubStream){
					hubStream.Write(header);
					hubStream.Write(byteMsg);
					hubStream.Flush();
				}
				Logger.Append("HUBRN", Severity.DEBUG2, "Sent message to node #"+this.Uid+" ("+this.NodeName+") : "+message);
			}
			catch(Exception ex){
				Disconnect();	
				Logger.Append("HUBRN", Severity.ERROR, "Unable to send message to node #"+this.Uid+" ("+this.NodeName+") : "+ex.Message);
			}
			
		}
		
		internal void Browse(string baseDir){
			browseData = "";
			SendMessage("BRW "+baseDir);	
		}
		
		internal void GetSpecialObjects(){
			SendMessage("SPO");
		}
		
		internal void GetDrives(){
			SendMessage("DRV");
		}
		
		internal void GetVMs(){
			SendMessage("VMS");	
		}
		/// <summary>
		/// Checks if the user has a share
		/// </summary>
		/// <returns>true or false</returns>
		private bool CheckShare(){
			if (this.StorageSize == 0)
				return false;
			else
				return true;
		}

		/// <summary>
		/// Checks if the user has sent its key
		/// </summary>
		/// <returns>true or false</returns>
		private bool CheckKey(){
			if (this.PubKey == "")
				return false;
			else
				return true;
		}
		
		/// <summary>
		/// Updates the storage space when a storage budget is exhausted
		/// </summary>
		/// <param name='taskId'>
		/// Task identifier.
		/// </param>
		/// <param name='usedSpace'>
		/// Used space.
		/// </param>
		private void UpdateStorageSpace(long taskId, long usedSpace, int budget){
			this.Available -= usedSpace;
			if(usedSpace > 0)
				dbhandle.UpdateStorageHistory(this, taskId);
			
			this.ReservedSpace -= TaskScheduler.Instance().GetTask(taskId).BackupSet.MaxChunkSize*budget;
			dbhandle.UpdateStorageSpace(this);
		}
		
		/// <summary>
		/// Calls a method to save the user's public key
		/// Sends a message to the user that the key is received
		/// </summary>
		/// <param name="key">the public key</param>
		/*private void SaveKey(string key){
			dbhandle.SaveKey(nodeName, key);

			try	{
				if(pubKey == ""){
					this.pubKey = key;
					SendMessage("209");
					if(LoggEvent != null)
						LoggEvent(this.nodeName, false, "209");
					if (AddUserEvent != null)
						AddUserEvent(this);
					SendAvailableSpace();
				}
				else{
					this.pubKey = key;
					SendMessage("209");
					if(LoggEvent != null)
						LoggEvent(this.nodeName, false, "209");
				}
			}
			catch(Exception ex){
				Utilities.Logger.Append("", "User.Savekey", Severity.ERROR, ex.Message);			
			}
		}*/

		/// <summary>
		/// Closes the socket and streams for this user
		/// if the connected user has disconnected
		/// Sets the bool attributes to false
		/// </summary>
		public void Disconnect(){
			try	{
				verified = false;
				//run = false;
				//Thread.Sleep(1024);
				if(this.Connection != null){
					try{
					
						this.hubStream.Close();
						this.Connection.Shutdown(SocketShutdown.Both);
						this.Connection.Close();
						this.Connection.Dispose();
					}
					catch(Exception e){
						Utilities.Logger.Append("", Severity.INFO, "user :"+this.NodeName+" : The socket was already shutdown by sudden client disconnection ("+e.Message+")");	
					}
				}
				if (RemoveUserEvent != null)
					RemoveUserEvent(this);
			}
			catch(Exception ex){
				Utilities.Logger.Append("", Severity.WARNING, "user :"+this.NodeName+" : "+ex.Message);			
			}
		}
		
		
		/// <summary>
		/// Checks if there's enough space for the requested backup
		/// this method should also consider the estimated size
		/// of the index file
		/// </summary>
		/// <param name="s">size of the backup</param>
		private void Backup(int bsId){
			if(LoggEvent!=null)
				LoggEvent(this.NodeName, false, "202");
			this.Status = NodeStatus.Backuping;
			SendMessage("202 "+bsId);
		}
		
		private void GetIndexDestination(long taskId, string indexName, string indexSum){
			//int destination = GetDestination(bsid, indexName, size.ToString(), true, null);
			List<ClientNode> dests = ChooseDestinations(taskId, -1, 1, true, false);
			//if(dests.Count > 0)
			//	dbhandle.UpdateBackupTracking(taskId, indexName, indexSum, 0);
		}
		
		/// <summary>
		/// Gets destinations for storing data chunks. Destinations count can be 1 to R (redundancy level) or 1 to p (p=paralleism)
		/// 
		/// </summary>
		/// <param name="bsId">
		/// A <see cref="System.Int32"/>
		/// </param>
		/// <param name="chunkName">
		/// A <see cref="System.String"/>
		/// </param>
		/// <param name="size">
		/// A <see cref="System.Int64"/>
		/// </param>
		/// <param name="isIndex">
		/// A <see cref="System.Boolean"/>. If set to true, this tells the client to send a confirmation when chunk has effectively 
		/// been transfered and stored : this way  we can track index location(s) into database.
		/// </param>
		private List<ClientNode> ChooseDestinations(long taskId, /*int nodeId*/int sessionId, int parallelism, bool isIndex, bool isAlternateRequest){
			Task currentTask = TaskScheduler.Instance().GetTask(taskId);
			PeerSession existingSession = null;
			int budget = 0; 
			List<ClientNode> dests = new List<ClientNode>();
			if(isAlternateRequest){ // request for new destination after failure with an already existing session
				dests = CalculateChunkDestinations(taskId, parallelism, budget, currentTask.StorageNodes);
			}
			else if(isIndex){
				Console.WriteLine("ChooseDestinations() : requested index storage");
				budget = 1;
				//dests = CalculateChunkDestinations(taskId, 1, budget, null);
				ClientNode n = null;
				foreach(PeerSession s in Hub.SessionsList){
					if(s.TaskId == taskId){
						n = Hub.NodesList.GetNode(s.ToNode);
						if(n != null) break;
					}
				}
				if(n != null){
					Console.WriteLine("ChooseDestinations() : index CAN reuse existing storage sessions");
					if(n.Available-n.ReservedSpace > currentTask.BackupSet.MaxChunkSize*budget)
						dests.Add(n);
				}
				else{
					Console.WriteLine("ChooseDestinations() : index CANNOT reuse existing storage sessions. current dests count="+dests.Count);
					dests = CalculateChunkDestinations(taskId, 1, budget, null);
				}

				/*foreach(ClientNode existingSession in currentTask.StorageNodes){
					ClientNode n = Hub.NodesList.GetClient(existingSession.Uid);
					if(n == null ) continue; // storage node has disconnected
						
				 	// reuse session with previous storage node
					Console.WriteLine("testing for index storage : node "+n.Uid);
					if(n.Available-n.ReservedSpace > currentTask.BackupSet.MaxChunkSize*budget)
						dests.Add(n);
					else
						Logger.Append("HUBRN", Severity.INFO, "Node #"+n.Uid+" unable to receive task "+taskId+" index (available space="+n.Available+")");
					if(dests.Count > 0)break;
				}
				if(dests == null)
					dests = CalculateChunkDestinations(taskId, 1, budget, currentTask.StorageNodes);
				*/
			}
			/*else if(currentTask.StorageNodes.Count == 0){ // if 0 storage session, use budget based upon previous backup
				budget = currentTask.StorageBudget;
				if(budget == 0) budget = 20; // default value if task has never run before.
				dests = CalculateChunkDestinations(taskId, parallelism, budget, null);
			}*/
			else {
				if(sessionId >0){ // else (insufficient previous budget, node is asking for more) use default budget
					budget = 20;
					/*ClientNode n = Hub.NodesList.GetClient(Hub.SessionsList.);
					if(n == null ) // storage node has disconnected
						dests = CalculateChunkDestinations(taskId, 1, budget, currentTask.StorageNodes);
					else { // reuse session with previous storage node*/

					ClientNode n = null;
					foreach(PeerSession s in Hub.SessionsList){
						if(s.Id == sessionId){
							n = Hub.NodesList.GetNode(s.ToNode);
							existingSession = s;
							existingSession.Budget += budget;
							break;
						}
					}
					if(n == null){ // storage node has disconnected
						dests = CalculateChunkDestinations(taskId, 1, budget, currentTask.StorageNodes);
					}
					else{
						if(n.Available-n.ReservedSpace > currentTask.BackupSet.MaxChunkSize*budget)
							dests.Add(n);
						else // previous node has not enough storage space, find a new one
							dests = CalculateChunkDestinations(taskId, 1, budget, currentTask.StorageNodes);

					}
				}
				else{ // first storage space request
					budget = currentTask.StorageBudget;
					if(budget == 0) budget = 20; // default value if task has never run before.
					dests = CalculateChunkDestinations(taskId, parallelism, budget, null);
				}

			}
			if(dests.Count == 0){
				if(!isAlternateRequest){
					Utilities.Logger.Append("HUBRN", Severity.WARNING, "Task #"+taskId+" : No storage space available for request of client #"+this.Uid+" ("+this.NodeName+"), <TODO> handle that and report" );	
					TaskScheduler.AddTaskLogEntry(taskId, 806, "","", "");
					TaskScheduler.Instance().SetTaskStatus(taskId, TaskStatus.Error);
					TaskScheduler.Instance().SetTaskRunningStatus(taskId, TaskRunningStatus.Cancelled);
					SendMessage("406 "+taskId);
				}
				else{
					Utilities.Logger.Append("HUBRN", Severity.WARNING, "Task #"+taskId+" : No storage space available for ALT request of client #"+this.Uid+" ("+this.NodeName+")");
					SendMessage("407 "+taskId);
				}
				// <TODO> track into backup log
				return null;
			}
			int level = 1;
			// prepare chunk handling with avg max size of BackupSet.MaxChunkSize
			long size = currentTask.BackupSet.MaxChunkSize;
			// for each storage node, budget = budget/nbnodes+1
			budget = budget/dests.Count+1;
			int processingFlags = (int)currentTask.BackupSet.DataFlags;

			// only 1 node in 'dests' if asking more busdget for existing session
			foreach(ClientNode chunkDestNode in dests){
				try{
					int sessId = -1;
					// if already existing session with this node
					if(existingSession != null)
						sessId = existingSession.Id;
					else
						sessId = Hub.SessionsList.Count+1;
					// 1 - we tell storage node to accept transfer from client
					chunkDestNode.SendMessage("RCV " + this.Uid + " " + taskId+" "+sessId+" "+this.IP + " " + this.NodeName + " " + this.PubKey+" "+budget+" "+processingFlags);
					if(LoggEvent != null) LoggEvent(chunkDestNode.NodeName, false, "RCV " + this.IP + " " + this.NodeName + " " + this.PubKey+" "+budget+" "+processingFlags);
					// temporarily change destination's available space
					//chunkDestNode.Available = chunkDestNode.Available - size*budget;
					Thread.Sleep(500);
					chunkDestNode.ReservedSpace += size*budget;
					// 2 - we tell client node where to put chunk
					SendMessage("DST " + level + "/" + dests.Count + " " + taskId + " "+ sessId+ " " + chunkDestNode.Uid + " " + chunkDestNode.IP 
					            + " " +chunkDestNode.Port + " " + chunkDestNode.NodeName + " " + chunkDestNode.PubKey +" "+isIndex+" "+budget);
					level++;
					if (SessionChanged != null && existingSession == null) SessionChanged(true, SessionType.Backup, sessId, this, chunkDestNode, currentTask.Id, budget);
					// 3 - we add the storage node(s) to task
					currentTask.AddStorageNode(chunkDestNode);
				}
				catch(IOException ioe){
					// change back destination's available space
					chunkDestNode.Available = chunkDestNode.Available + size*budget;
					Utilities.Logger.Append("HUBRN", Severity.ERROR, "dest "+chunkDestNode.NodeName+" not available ("+ioe.Message+"), looking for an alternate one");
					//GetDestination(bsId, chunkName, size, isIndex, destination.NodeName);
				}
				catch(Exception ex){
					Utilities.Logger.Append("HUBRN", Severity.ERROR, "dest "+chunkDestNode.NodeName+" : "+ex.Message);
				}
			}
			return dests;
		}
		
		/// <summary>
		/// Overloaded; this one takes two strings
		/// Calls a method to get a destination for the backup
		/// Sends information to the destination user about the backup
		/// </summary>
		/// <param name="n">filename</param>
		/// <param name="s">filesize</param>
		/*private int GetDestination(int bsId, string chunkName, string size, bool isIndex, string nodeToExclude){
			Node destination = null;
			action = ActionType.Backup;
			int result = -1;
			long filesize = Int64.Parse(size);
			// temporarily change accessible space
			//this.Accessible = this.Accessible - filesize;
			this.Available -= filesize;
			Utilities.Logger.Append("HUBRN", Severity.DEBUG, "trying to calculate destination for chunk "+ chunkName);
			if (CalculateDestinationEvent != null){
				destination = CalculateDestinationEvent(this, nodeToExclude);	
				//List <Node> destinations = CalculateDestinations(Get);
			}
			else
				Utilities.Logger.Append("HUBRN", Severity.WARNING, "could not get a destination for chunk "+ chunkName+", handle and report this");
			if (destination != null){
				try{
					// 1 - we tell storage node to accept transfer from client
					destination.SendMessage("RCV " + this.Uid + " " + this.IP + " " + this.NodeName + " " + this.PubKey);
					if(LoggEvent != null) LoggEvent(destination.NodeName, false, "RCV " + this.IP + " " + this.NodeName + " " + this.PubKey);
					if(LoggEvent != null) LoggEvent(this.NodeName, false, "301");
					// temporarily change destination's available space
					destination.Available = destination.Available - filesize;	
					result = destination.Uid;
					// 2 - we tell client node where to put chunk
					SendMessage("DST " + bsId + " " +chunkName + " " + destination.Uid + " " + destination.ListenIp + " " 
					            +destination.ListenPort + " " + destination.NodeName + " " + destination.PubKey +" "+ isIndex);
				}
				catch(IOException ioe){
					// change back destination's available space
					destination.Available = destination.Available + filesize;
					Utilities.Logger.Append("HUBRN", Severity.ERROR, "dest "+destination.NodeName+" not available ("+ioe.Message+"), looking for an alternate one");
					GetDestination(bsId, chunkName, size, isIndex, destination.NodeName);
				}
				catch(Exception ex){
					Utilities.Logger.Append("HUBRN" ,Severity.ERROR, "dest "+destination.NodeName+" : "+ex.Message);
				}
			}
			else{
				Utilities.Logger.Append("HUBRN", Severity.WARNING,"No storage space available for client "+this.Uid+" ("+this.NodeName+"), <TODO> handle that and report" );	
				SendMessage("406");
			}
			return result;
		}*/
		

		/// <summary>
		/// Calculates the chunk destination(s) upon receiving client node request for storage space.
		/// The algorithm is the following
		/// -take all online storage nodes
		/// -restrict to those eligible from task conf (members of required storagegroup(s)
		/// -restrict to those having enough space to store (task chunk's max size)*task budget
		/// -order by reverse  current load (the less loaded node(s) will be elected)
		/// - if needed, restrict to those not present in exclusion list (unreachable nodes)
		/// <b>Note: </b> Several requests to this method may be made before nodes CurrentLoad is updated (when real transfer session starts),
		/// which means we might endup selecting some nodes too many times.
		/// This is an expected behavior, since CurrentLoad has to be considered more as a hint than as an 
		/// absolute, always-right, basis for storage destinations calculation.
		/// </summary>
		/// <returns>
		/// The chunk destinations.
		/// </returns>
		/// <param name='taskId'>
		/// Task identifier.
		/// </param>
		/// <param name='parallelism'>
		/// Parallelism.
		/// </param>
		/// <param name='budget'>
		/// Budget.
		/// </param>
		/// <param name='nodesToExclude'>
		/// Nodes to exclude (because signaled as unreachable by client node).
		/// </param>
		private List<ClientNode> CalculateChunkDestinations(long taskId, int parallelism, int budget, List<P2PBackup.Common.Node> nodesToExclude){

			List<ClientNode> destinations = new List<ClientNode>();
			// budget for each storage node will be budget/parallelism
			int perNodeBudget = (budget/parallelism)+1;
			BackupSet bs = TaskScheduler.Instance().GetTask(taskId).BackupSet;

			foreach(int sgRequired in bs.StorageGroups){
				// to select the storage node(s), all the magic happends here. How cool Linq is!
				// query seems self-explainatory
				List<ClientNode> destinationRawList = (from ClientNode node in Hub.NodesList.Cast<ClientNode>()
						where  node.StorageGroup == sgRequired && node.StoragePriority > 0
						&& node.Available-node.ReservedSpace > bs.MaxChunkSize*perNodeBudget
						&& node.Uid != this.Uid
						orderby node.CurrentLoad descending
				                select node).Take(parallelism).ToList<ClientNode>();

				Console.WriteLine("Selected "+destinationRawList.Count+" raw storage nodes, online nodes="+Hub.NodesList.Count);
				if(nodesToExclude != null)
					destinationRawList = (from ClientNode node in destinationRawList
						where !nodesToExclude.Contains(node)
					        orderby node.CurrentLoad descending
						select node).ToList<ClientNode>();
				//List<Node> destsAsList = destinationRawList.ToList();
				if(destinationRawList.Count == 0){
					Logger.Append("HUBRN", Severity.ERROR, "Could not choose any storage node from group "+sgRequired+" for task "+taskId+" of node #"+this.Uid+" with budget="+budget+" ("+budget*bs.MaxChunkSize/1024/1024+"MB required)");
					continue;
				}
				//if we can't find enough storage nodes, we will use fewer nodes but require more storage space from each one
				if(destinationRawList.Count < parallelism){
					Logger.Append("HUBRN", Severity.WARNING, "Not enough storage nodes to satisfy parallelism ("+parallelism+") of task "+taskId);
					CalculateChunkDestinations(taskId, destinationRawList.Count, budget, null);
				}
				// found enough nodes, pick up randomly
				/*Random rand = new Random();
				int randPosition = rand.Next(destinationRawList.Count);*/
				//Console.WriteLine("users online="+Hub.NodesList.Count+", bs requires redundancy="+bs.StorageGroups.Count+", on sg="+sgRequired+", nb dests found="+destsAsList.Count+", randpos="+randPosition);
				for(int i=0; i< Math.Min(parallelism, destinationRawList.Count); i++){
					/*int nodePos = i;
					//Console.WriteLine("calculatechunkdestinations() parallelism instance="+i);
					if(randPosition+i < destinationRawList.Count && destinationRawList[randPosition+i] != null)
						nodePos = randPosition+i;
					else if(randPosition-i >0 && destinationRawList[randPosition-i] != null)
						nodePos = randPosition-i;
					else{
						Logger.Append("HUBRN", Severity.INFO, " Task "+taskId+": Not enough storage nodes ("+destinationRawList.Count+") available to satisfy required parallelism("+parallelism+")");  
						continue;
					}*/
					destinationRawList[i].ReservedSpace += bs.MaxChunkSize*perNodeBudget;
					destinations.Add(destinationRawList[i]);
				}
			}
			if(destinations.Count < bs.StorageGroups.Count){ //redundancy level is not reached
				string sgList = "";
				foreach(int sgRequired in bs.StorageGroups)
					sgList += sgRequired+",";
				Logger.Append("HUBRN", Severity.WARNING, "Node #"+this.Uid+", Task "+taskId+", backup set storage groups:'"+sgList+"' : could only satisfy a redundancy level of "+destinations.Count+"/"+bs.StorageGroups.Count+" | TODO : track in bslog");
				// <TODO> : treat that as backup error, and track this into bslog table.
			}
			return destinations;
		}
		

		/// <summary>
		/// Sends the available space to this user
		/// </summary>
		private void SendAvailableSpace(){
			action = ActionType.Default;
			SendMessage("ACS " + this.Available);
		}
		
		
		internal void SendNodeConfiguration(){
			SendMessage("CFG "+dbhandle.GetNodeConfig(this.Uid)+"Node.Id="+this.Uid);
		}
		
		private void GetIndexSource(string indexName){
			action = ActionType.Restore;
			List<int> sourceNodeId = dbhandle.GetIndexSources(indexName);
			foreach(int sid in sourceNodeId){
				if(Hub.NodesList.IsOnline(sid)){
					ClientNode sourceNode = Hub.NodesList.GetNode(sid);
					SendMessage("LET" + " " + this.IP + " " + this.NodeName + " " + this.PubKey);
					if(LoggEvent != null)	LoggEvent(sourceNode.NodeName, false, "LET" + " " + this.IP + " " + this.NodeName + " " + this.PubKey);
				}
			}
		}
		
		/// <summary>
		/// Gets the user storing the file at recovery time
		/// </summary>
		/// <param name="nodeName">username for storing user</param>
		private void GetSource(string nodeName){
			ClientNode source = null;
			action = ActionType.Restore;

			if (GetClientEvent != null)
				source = GetClientEvent(nodeName);
			if (source != null){
				SendMessage("LET" + " " + this.IP + " " + this.NodeName + " " + this.PubKey);
				if(LoggEvent != null) LoggEvent(source.NodeName, false, "LET" + " " + this.IP + " " + this.NodeName + " " + this.PubKey);
			}
			else{
				SendMessage("405");
				if(LoggEvent != null) LoggEvent(nodeName, false, "405");
			}
		}

		/// <summary>
		/// Gets the client who is requesting backup or recovery
		/// </summary>
		/// <param name="nodeName">the requesting client</param>
		private void GetClient(string nodeName){
			ClientNode reqClient = null;
			if (GetClientEvent != null)
				reqClient = GetClientEvent(nodeName);
			if (reqClient != null){
				// if the requesting user is backing up, call requesting user's SendDestination
				//if (reqClient.action == ActionType.Backup)
				//	reqClient.SendDestination(this, chunkName);
				// if the requesting user is recovering, call requesting user's SendSource
				if (reqClient.action == ActionType.Restore)
					reqClient.SendSource(this);
			}
			// if the requesting client is not online
			else{
				SendMessage("STP " + reqClient.NodeName);
				if(LoggEvent != null)	LoggEvent(nodeName, false, "STP " + reqClient.NodeName);
			}
		}

		/// <summary>
		/// Sends information about the storing client to the requesting client
		/// at recovery time
		/// </summary>
		/// <param name="source">the user storing the file</param>
		private void SendSource(ClientNode source){
			SendMessage("SRC " + source.IP + " " +source.Port + " " + source.NodeName + " " + source.PubKey);
			if(LoggEvent != null)	LoggEvent(this.NodeName, false, "SRC " + source.IP + " " +source.Port + " " + source.NodeName + " " + source.PubKey);
		}

		private void CreateAndSendCertificate(){
			byte[] certificate = GenerateNewClientCertificate(this.Connection);
			SendMessage("CERT "+Convert.ToBase64String(certificate));
		}

		private byte[] GenerateNewClientCertificate(Socket client){
			Logger.Append("CLIENT", Severity.INFO, "Generating TLS certificate for new node "+client.RemoteEndPoint.ToString());
			CertificateManager cm = new CertificateManager();
			string clientHostName = System.Net.Dns.GetHostEntry(client.RemoteEndPoint.ToString().Substring(0, client.RemoteEndPoint.ToString().LastIndexOf(":")) ).HostName;
			Logger.Append("CLIENT", Severity.DEBUG, "Resolved new client to host '"+clientHostName+"'");
			return cm.GenerateCertificate(false, false, clientHostName, null);
		}

		public bool Equals(P2PBackup.Common.Node other){
		      if (this.Uid == other.Uid)
		         return true;
		      else
		         return false;
		}

		/*public
		 * Node GetBase(){
			return base.GetBase();
		}*/
	}
}
