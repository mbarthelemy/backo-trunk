using System;
using System.Globalization;
using System.Collections;
using System.Collections.Generic;
using P2PBackupHub.Utilities;
using P2PBackupHub.Virtualization;
using System.Configuration;
using System.Threading;
using System.Text;
using System.Security.Principal;
using System.Security.Permissions;
using System.Security.Policy;
using System.Linq;

using P2PBackup.Common;

using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Channels;

namespace P2PBackupHub{
	//namespace Remoting{
	/*[Serializable]*/
	/*[DataContract]*/
	[KnownType(typeof(TaskRunningStatus))]
	[KnownType(typeof(TaskStatus))]
	[KnownType(typeof(P2PBackup.Common.Task))]
	[KnownType(typeof(TaskStartupType))]
	[KnownType(typeof(TaskPriority))]
	[KnownType(typeof(TaskAction))]
	[KnownType(typeof(TaskOperation))]
	[KnownType(typeof(BackupSet))]
	[KnownType(typeof(P2PBackup.Common.Node))]
	[KnownType(typeof(LogEntry))]
	[KnownType(typeof(Severity))]
	[KnownType(typeof(NodeCertificate))]
	[KnownType(typeof(Hypervisor))]
	[ServiceBehavior(InstanceContextMode=InstanceContextMode.PerSession)]
	public class RemoteOperations:  IRemoteOperations{
		
		private bool hasSession;
		private User sessionUser;
		GenericPrincipal principal;
		IIdentity identity;
		
		public RemoteOperations (){
			//Logger.Append("HUBRN", Severity.DEBUG2, "Remoting connection initiated.");
			hasSession = false;
		}
		
		
		public bool Login(string userName, string password){
			//Console.WriteLine ("RemoteOperations Login 1");
			sessionUser = (new DBHandle()).AuthenticateUser(userName, password);
			hasSession = (sessionUser != null);
			if(!hasSession) return false;
			//Console.WriteLine ("RemoteOperations Login 2");
			//Thread.CurrentPrincipal = new GenericPrincipal(
            		//new GenericIdentity(sessionUser.Name), new string[] { "SuperAdmin" });
			
			identity = new GenericIdentity(sessionUser.Name);

 
            //Console.WriteLine( ((RemoteEndpointMessageProperty)OperationContext.Current.IncomingMessageProperties[RemoteEndpointMessageProperty.Name]).Address);


			//Console.WriteLine ("RemoteOperations Login 3");
			string[] roles = null;
			//if(sessionUser.Name == "admin")
			//	roles = new string[] { "SuperAdmin" };
			var rList = from  role in sessionUser.Roles select role.Role.ToString();
			principal = new GenericPrincipal(identity, rList.ToArray());
				
			//Console.WriteLine ("RemoteOperations Login(), roles="+string.Join(",", rList.ToArray()));
			
			/*AppDomain.CurrentDomain.SetThreadPrincipal(principal);*/
			if(Thread.CurrentPrincipal.Identity.Name != principal.Identity.Name)
				Thread.CurrentPrincipal = principal;
			//Console.WriteLine ("RemoteOperations Login 5");
			
			
			//CheckSession();
			return hasSession; 
		}
		/*public User[] GetClients(){
			DBHandle db = new DBHandle();
			Logger.Append("HUBRN","DEBUG","RemoteOperations","Called GetClients...");
			User[] clients = (User[])db.GetClients().ToArray(typeof(User));
			Logger.Append("HUBRN","DEBUG","RemoteOperations","Called GetClients, got "+clients.Length);                                                       
			return clients;
		}*/
		
		/*private void CheckSession(){
			Console.WriteLine("current user name: "+sessionUser.Name);
			Console.WriteLine("current user principal: "+Thread.CurrentPrincipal.Identity.Name);
			//Console.WriteLine("current user principal has role SuperAdmin: "+principal.IsInRole("SuperAdmin"));
			//Console.WriteLine("current user principalhas role SuperViewer: "+principal.IsInRole("SuperViewer"));
			//Console.WriteLine("");
			Console.WriteLine("current user principal has role SuperAdmin: "+Thread.CurrentPrincipal.IsInRole("SuperAdmin"));
			Console.WriteLine("current user principal has role SuperViewer: "+Thread.CurrentPrincipal.IsInRole("SuperViewer"));
			Console.WriteLine("----------------");
			if(!hasSession) throw new Exception ("Login denied, or session expired");
		}*/

		public string WhoAmI(){
			return "thread: "+Thread.CurrentPrincipal.Identity.Name+", session:"+sessionUser.Name;
		}
		/*[PrincipalPermission(SecurityAction.Demand, Role="Viewer")]
		[PrincipalPermission(SecurityAction.Demand, Role="SuperViewer")]
		[PrincipalPermission(SecurityAction.Demand, Role="Admin")]
		[PrincipalPermission(SecurityAction.Demand, Role="SuperAdmin")]*/
		/*public ArrayList GetClients(){
			ArrayList iClientNodes = new ArrayList();
			CheckSession();
			DBHandle db = new DBHandle();                                                     
			foreach(Node n in db.GetClients())
			        iClientNodes.Add((IClientNode)n);
			return iClientNodes;
		}*/
		
		/*[PrincipalPermission(SecurityAction.Demand, Role="SuperViewer")]
		[PrincipalPermission(SecurityAction.Demand, Role="Admin")]
		[PrincipalPermission(SecurityAction.Demand, Role="SuperAdmin")]*/
		public List<P2PBackup.Common.Node> GetNodes(){
			//List<P2PBackup.Common.Node> nodes = new List<P2PBackup.Common.Node>();
			DBHandle db = new DBHandle(sessionUser); 
			return db.GetNodes();
			/*foreach(P2PBackup.Common.Node n in db.GetNodes()){
				RoleEnum roleForThisGroup = sessionUser.GetRoleForGroup(n.Group);
				Console.WriteLine("role for group#"+n.Group+" : "+roleForThisGroup);
				if(sessionUser.GetRoleForGroup(n.Group) >= RoleEnum.Viewer)
					nodes.Add((P2PBackup.Common.Node)n);
			}
			return nodes;*/
		}

		public P2PBackup.Common.Node GetNode (int id){
			DBHandle db = new DBHandle(sessionUser); 
			return db.GetNode(id);
		}

		[PrincipalPermission(SecurityAction.Demand, Role="SuperViewer")]
		[PrincipalPermission(SecurityAction.Demand, Role="SuperAdmin")]
		public List<NodeCertificate> GetCertificates(){
			//List<NodeCertificate> certs = new List<NodeCertificate>();

			return (new DBHandle()).GetCertificates();
		}
		/*public User[] GetStorageNodes(){
			DBHandle db = new DBHandle();
			Logger.Append("HUBRN","DEBUG","RemoteOperations","Called GetStorageNodes...");
			User[] clients = (User[])db.GetStorageNodes().ToArray(typeof(User));
			Logger.Append("HUBRN","DEBUG","RemoteOperations","Called GetStorageNodes, got "+clients.Length);                                                       
			return clients;
		}*/

		[PrincipalPermission(SecurityAction.Demand, Role="SuperViewer")]
		[PrincipalPermission(SecurityAction.Demand, Role="SuperAdmin")]
		public List<Hypervisor> GetHypervisors(){
			return (new DBHandle()).GetHypervisors();
		}

		public List<P2PBackup.Common.Node> Discover (int hypervisorId){
			return Hub.DiscoverVms(hypervisorId);
		}

		[PrincipalPermission(SecurityAction.Demand, Role="SuperViewer")]
		[PrincipalPermission(SecurityAction.Demand, Role="SuperAdmin")]
		public List<PeerSession> GetSessions(){
			return Hub.SessionsList;
		}
		public List<P2PBackup.Common.Node> GetStorageNodes(){
			DBHandle db = new DBHandle(sessionUser);                                       
			return db.GetStorageNodes();
		}
		
		/*public StorageGroup[] GetStorageGroups(){
			DBHandle db = new DBHandle();
			Logger.Append("HUBRN","DEBUG","RemoteOperations","Called GetStorageGroups...");
			StorageGroup[] sg = (StorageGroup[])db.GetStorageGroups().ToArray(typeof(StorageGroup));
			Logger.Append("HUBRN","DEBUG","RemoteOperations","Called GetStorageGroups, got "+sg.Length);                                                       
			return sg;
		}*/
		
		public List<P2PBackup.Common.StorageGroup> GetStorageGroups(){
			DBHandle db = new DBHandle(sessionUser);
			                                                       
			return db.GetStorageGroups();
		}
		
		/*public Node GetUser(string username){
			return (new DBHandle()).NodeExists(username);	
		}*/
		
		/*public P2PBackup.Common.Node GetNode(string hostName, string pubKey){
			return (P2PBackup.Common.Node)(new DBHandle()).NodeApproved(hostName, pubKey);
		}*/
		
		/*public P2PBackup.Common.Node GetNode(int nodeId){
			return (P2PBackup.Common.Node)(new DBHandle()).GetNode(nodeId);
		}*/
		
		public Dictionary<string, string> GetNodeConf(int nodeId){
			return (new DBHandle(sessionUser)).GetNodeConfigTable(nodeId);
		}
		
		public List<NodeGroup> GetNodeGroups(){
			return 	(new DBHandle(sessionUser)).GetNodeGroups();
		}
		
	
		[PrincipalPermission(SecurityAction.Demand, Role="SuperAdmin")]
		public void ApproveNode(int nodeId, bool lockStatus){
			(new DBHandle()).ApproveNode(nodeId, lockStatus);
			if(lockStatus == false)
				Hub.NodesList.GetNode(nodeId).Approve();
			else{ // lock immediately : an online node is disconnected
				if(Hub.NodesList.GetNode(nodeId).Status != NodeStatus.Offline)
					Hub.NodesList.GetNode(nodeId).Disconnect();
			}
				
		}
		
		public int Ping(){
			Logger.Append("HUBRN", Severity.DEBUG, "Called Ping");
			return 1;	
		}
		
		/*public User BeginSession(string userName, string password){
			User u = (new DBHandle()).AuthenticateUser(userName, password) ;
			hasSession = (u==null);
			return 	u;
		}*/

		public User GetCurrentUser(){
			return sessionUser;
		}
		
		/*public List<P2PBackup.Common.Node> GetOnlineClients(){
			List<P2PBackup.Common.Node> cnList = new List<P2PBackup.Common.Node>();
			foreach(ClientNode cn in Hub.NodesList)
				cnList.Add(cn.GetBase() );
			return cnList;
			//return (List<Node>)Hub.NodesList;
			
		}*/

		/// <summary>
		/// Gets the online clients IDs.
		/// </summary>
		/// <returns>
		/// The online clients.
		/// </returns>
		public Dictionary<int, NodeStatus> GetOnlineClients(){
			Dictionary<int, NodeStatus> cnList = new Dictionary<int, NodeStatus>();
			foreach(ClientNode cn in Hub.NodesList)
				cnList.Add(cn.Uid, cn.Status );
			return cnList;
			//return (List<Node>)Hub.NodesList;
			
		}
		
		public List<BackupSet> GetBackupPlan(int nbHours){
			return 	(new DBHandle()).GetTaskSets(DateTime.Now.Subtract(new TimeSpan(1,0,0) ), DateTime.Now.Add(new TimeSpan(nbHours-1,0,0) ), true );
		}

		public List<BackupSet> GetTaskSets(){
			return 	(new DBHandle()).GetTaskSets();
		}

		public BackupSet GetTaskSet(int bsid){
			return (new DBHandle(sessionUser)).GetTaskSet(bsid);
		}

		[PrincipalPermission(SecurityAction.Demand, Role="SuperAdmin")]
		public Dictionary<string, string> GetConfigurationParameters(){
			Dictionary<string, string> config = new Dictionary<string, string>();
			if(sessionUser.IsSuperAdmin())
				foreach(string key in ConfigurationManager.AppSettings.AllKeys)
					config.Add(key, ConfigurationManager.AppSettings[key]);
			return config;
		}


		public List<BackupSet> GetNodeBackupSets(int nodeId){
			return (new DBHandle()).GetNodeBackupSets(nodeId);	
		}
		
		public Hashtable GetBackupHistory(int bsId, DateTime startDate, DateTime endDate){
			return 	(new DBHandle()).GetBackupHistory(bsId, startDate, endDate);
		}

		public List<P2PBackup.Common.Task> GetTasksHistory(string[] bs, DateTime from, DateTime to, List<TaskRunningStatus> status, string sizeOperator, long size, int limit, int offset, out int totalCount){
			return 	(new DBHandle()).GetTaskHistory(bs, from, to, status, sizeOperator, size, limit, offset, out totalCount);
		}

		public Task GetTaskHistory(long taskId){
			return 	(new DBHandle()).GetTaskHistory(taskId);
		}

		public List<P2PBackup.Common.Task> GetRunningTasks(){
			/*List<P2PBackup.Common.Task> displayableRunningTasks = new List<Task>();
			List<P2PBackup.Common.Task> allRunningTasks = TaskScheduler.Instance().GetTasks();
			List<Node> nodes = GetNodes();
			foreach(Task t in allRunningTasks){
				foreach(Node n in nodes)
					if(t.BackupSet.ClientId == n.Uid)
						displayableRunningTasks.Add(t);
			}
			return displayableRunningTasks;*/
			return TaskScheduler.Instance().GetTasks();
				
		}
		
		public List<Tuple<DateTime, int, string, string>> GetArchivedTaskLogEntries(int taskId){
			return (new DBHandle()).GetTaskLogEntries(taskId);
		}
		
		public string Browse(int nodeId, string path){
			foreach(ClientNode n in Hub.NodesList){
				if(n.Uid == nodeId){
					n.Browse(path);
					int i=0;
					while(n.BrowseData.Length ==0 && i<600){
						System.Threading.Thread.Sleep(100);
						i++;
					}
					//string utf8 = Enc
					return n.BrowseData;
				}
			}
			return null;
		}
		
		// should be relevant only for NT systems (VSS providers)
		public string GetSpecialObjects(int nodeId){
			foreach(ClientNode n in Hub.NodesList){
				if(n.Uid == nodeId){
					n.GetSpecialObjects();
					int i=0;
					while(n.SpecialObjects.Length ==0 && i<900){
						System.Threading.Thread.Sleep(100);
						i++;
					}
					return n.SpecialObjects;
				}
			}
			return null;
		}
		
		public string GetDrives(int nodeId){
			foreach(ClientNode n in Hub.NodesList){
				if(n.Uid != nodeId)
					continue;
				n.GetDrives();
				int i=0;
				while(n.DrivesData.Length ==0 && i<1000){
					System.Threading.Thread.Sleep(10);
					i++;
				}
				return n.DrivesData;
				
			}
			return null;
		}
		
		public string GetVMs(int nodeId){
			foreach(ClientNode n in Hub.NodesList){
				if(n.Uid != nodeId)
					continue;
				n.GetVMs();
				int i=0;
				while(n.VMS.Length ==0 && i<1000){
					System.Threading.Thread.Sleep(10);
					i++;
				}
				return n.VMS;
				
			}
			return null;
			
		}

		[PrincipalPermission(SecurityAction.Demand, Role="Admin")]
		public string CreateBackupSet(BackupSet bs){
			DBHandle db = new DBHandle();
			db.AddBackupSet(bs);
			return null;	
		}

		[PrincipalPermission(SecurityAction.Demand, Role="Admin")]
		public string ChangeTasks(List<long> tasks, TaskAction action){
			foreach(long taskId in tasks){
				if(action == TaskAction.Cancel) TaskScheduler.Instance().CancelTask(taskId, sessionUser);
				else if(action == TaskAction.Pause) TaskScheduler.Instance().PauseTask(taskId, sessionUser);
			}
			return "not implemented";	
		}
		/*public void SetNodeConfig(int nodeId, ArrayList templates, string logLevel, bool syslog, string logFile, string listenIP, int listenPort, string backupTempFolder, string backupIndexFolder, long sharesize){
			// we parse loglevel here, to avoid to have to define the enum in common or in webui
			try{
				Severity loglevel = Enum.Parse(typeof(Severity), logLevel);
				DBHandle db = new DBHandle();
				db.SaveNodeConfig();
			}
			catch(Exception e){
				
			}
		}*/

		[PrincipalPermission(SecurityAction.Demand, Role="Admin")]
		public void SetNodeConfig(int nodeId, Hashtable confTable){
			Logger.Append("HUBRN", Severity.DEBUG, "Trying to update configuration for node #"+nodeId);
			DBHandle db = new DBHandle();
			db.SaveNodeConfig(nodeId, confTable);
			Hub.NodesList.GetNode(nodeId).SendNodeConfiguration();
		}

		[PrincipalPermission(SecurityAction.Demand, Role="Admin")]
		public void UpdateNodeGeneralConf(int nodeId, long? storageSize, long? quota, int? storageGroup, int? nodeGroup){
			DBHandle db = new DBHandle();
			db.UpdateNodeGeneralConf(nodeId, storageSize, quota, storageGroup, nodeGroup);
		}

		[PrincipalPermission(SecurityAction.Demand, Role="SuperAdmin")]
		public LogEntry[] GetLogBuffer(){
			return Logger.GetBuffer();	
		}
		
		//used to manage users session time zones and formats, languages
		public Hashtable GetCultures(){
			Hashtable cultures = new Hashtable();
			CultureInfo[] allCultures = CultureInfo.GetCultures(CultureTypes.SpecificCultures);
			foreach (CultureInfo culture in allCultures){
				cultures.Add(culture.Name, culture.NativeName);
			}
			return cultures;
		}

		[PrincipalPermission(SecurityAction.Demand, Role="SuperAdmin")]
		public List<User> GetUsers(){
			return (new DBHandle(sessionUser)).GetUsers();	
		}
		
		/// <summary>
		/// Gets stats about last 24hrs tasks
		/// </summary>
		/// <returns>
		/// The last stats.
		/// </returns>
		public List<Tuple<string, int>> GetLastTasksStats(){
			return (new DBHandle()).GetLastTasksStats();
		}

		public long StartTask(int bsid, BackupLevel? level){
			return TaskScheduler.Instance().StartImmediateTask(bsid, sessionUser, level);
		}

		public void StopTask(long taskId){
			TaskScheduler.Instance().CancelTask(taskId, sessionUser);
		}

		[PrincipalPermission(SecurityAction.Demand, Role="SuperAdmin")]
		public void ShutdownHub(){
			Hub.Shutdown(sessionUser, false);
		}
	}
	
	
//}
	
}
