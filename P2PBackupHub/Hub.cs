using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Net.Sockets;
using System.Net;
using System.Net.Security;
using System.Security;
using System.Security.Authentication;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
//using System.Security;
using System.Security.Permissions;
using System.Security.Principal;
using System.Threading;
using System.Configuration;
using P2PBackupHub.Utilities;
using P2PBackup.Common;

namespace P2PBackupHub{
	/// <summary>
	/// Handles all connections from users, the interface
	/// </summary>
	public class Hub {
		
		private static NodeList nodeList;
		private static List<PeerSession> sessionsList;
		private static Socket connection;
		private static bool running;	
		//static bool setupMode = false; 
		
		private static X509Certificate2 cert;
		private X509Certificate2 rootCert;
		//private static RSA motherKey;

		public static List<PeerSession> SessionsList{
			get{
				List<PeerSession> sessions = new List<PeerSession>();
				lock(sessionsList){
					sessions.AddRange(sessionsList);
				}
				return sessions;
			}
		}



		internal static X509Certificate2 Certificate{
			get{
				if(cert == null){
					if(ConfigurationManager.AppSettings["Security.CertificateFile"] == null || !System.IO.File.Exists(ConfigurationManager.AppSettings["Security.CertificateFile"])){
						Logger.Append("HUBRN", Severity.CRITICAL, "Could not load hub SSL certificate (configuration told it would be '"+ConfigurationManager.AppSettings["Security.CertificateFile"]+"')");
						throw new Exception("FATAL : unable to load HUB certificate  ");
					}
					cert = new X509Certificate2(ConfigurationManager.AppSettings["Security.CertificateFile"], "");
				}
				return cert;
			}
		}
		
		private X509Certificate RootCA{
			get{
				if(rootCert == null){
					if(ConfigurationManager.AppSettings["Security.CACertificate"] == null || !System.IO.File.Exists(ConfigurationManager.AppSettings["Security.CACertificate"])){
						Logger.Append("HUBRN", Severity.CRITICAL, "Could not load hub SSL CA certificate (configuration told it would be '"+ConfigurationManager.AppSettings["Security.CACertificate"]+"')");
						throw new Exception("FATAL : unable to load HUB CA certificate");
					}
					rootCert = new X509Certificate2(ConfigurationManager.AppSettings["Security.CACertificate"], "");
				}
				return rootCert;
			}
		}
		
		
		/*internal static  RSA MotherKey{
			get{
				if(motherKey == null){
					if(ConfigurationManager.AppSettings["Security.MotherCertificateFile"] == null || !System.IO.File.Exists(ConfigurationManager.AppSettings["Security.MotherCertificateFile"])){
						Logger.Append("HUBRN", Severity.CRITICAL, "Could not load hub RSA mother key (configuration told it would be '"+ConfigurationManager.AppSettings["Security.MotherCertificateFile"]+"')");
						throw new Exception("FATAL : unable to load HUB mother key");
					}
					System.IO.StreamReader sr = new System.IO.StreamReader(ConfigurationManager.AppSettings["Security.MotherCertificateFile"]);
					
					motherKey.FromXmlString(sr.ReadToEnd());
				}
				return motherKey;
			}
		}*/
		
		public Hub(){
			nodeList = new NodeList();
			sessionsList = new List<PeerSession>();
		}
		
		public static bool SetupMode{get; private set;}
		
		internal static NodeList NodesList{
			get{return nodeList;}
		}
		
		
		[STAThread]
		
		static void Main(string[] args){

			// usually false, unless first start
			bool generateCA = false; // true to generate a root CA
			bool generateHubCert = false; // true to generate the hub's certificate
			string setupPassword = "";
			SetupMode = false;
			int param = 0;
			while(param < args.Length){
				string currentParam = args[param];
				switch (currentParam.ToLower()){
					case "-h": case "--help":
						PrintHelp();
						return;
					case "--config":case "-c":
						param++;
						ConfigurationManager.OpenExeConfiguration(args[param]);
						
						break;
					case "--setup": 
						Console.WriteLine("Running Setup mode");
						SetupMode = true;
						break;
					case "--gencert": 
						//CertificateManager cm = new CertificateManager();
						generateHubCert = true;
						generateCA = true;
						break;
					case "--password":
						if(args.Length <= param+1)
							throw new Exception ("syntax : --password <the_password>");
						param++;
						setupPassword = args[param];
						
						break;
				}
				param = param +1;
			}	
			
			if(!Utils.ConfigFileExists() && ! SetupMode){
				Console.WriteLine("Configuration file does not exist, exiting...");
				PrintHelp();
				Environment.Exit(4);
			}
			if(generateCA ){
				P2PBackupHub.CertificateManager cm = new P2PBackupHub.CertificateManager();
				cm.GenerateCertificate(true, false, Environment.MachineName, null);
			} 
			if(generateHubCert){
				P2PBackupHub.CertificateManager cm = new P2PBackupHub.CertificateManager();
				byte[] hubCert = cm.GenerateCertificate(false, true, Environment.MachineName, null);
				if(ConfigurationManager.AppSettings["Security.CertificateFile"] == null){
					Console.WriteLine("Cannot save certificate: Parameter Security.CertificateFile is missing. Consider updating configuration file.");
					Environment.Exit(5);
				}
				try{
					System.IO.FileStream certStream = new System.IO.FileStream(ConfigurationManager.AppSettings["Security.CertificateFile"], System.IO.FileMode.CreateNew);
					certStream.Write(hubCert, 0, hubCert.Length);
				}
				catch(Exception e){
					Console.WriteLine("Cannot save certificate to '"+ConfigurationManager.AppSettings["Security.CertificateFile"]+"': "+e.Message);
					Environment.Exit(6);
				}
			}
			
			if(SetupMode){
				if(setupPassword == string.Empty)
					throw new Exception("--password must be followed by a value");
				
			}
			
			// catch fatal exceptions to log them before exiting
			AppDomain.CurrentDomain.UnhandledException += UnhandledExceptionHandler;
			
			// ** Start the real stuff **
			try{ 

				//SecurityManager.SecurityEnabled = true; // obsolete.
				Hub hub = new Hub();
				hub.Run();
				Logger.Append("START", Severity.INFO, "Security is "+(SecurityManager.SecurityEnabled ? "ON" : "OFF"));
			}
			catch(Exception e){
				Utilities.Logger.Append("HUBRN", Severity.ERROR, e.Message+"---"+e.StackTrace);
			}
			/*Password totototo = new Password();
			totototo.Value = "Ausvad10+";
			PasswordManager.Add(totototo);*/
		}
		
		private static void PrintHelp(){
			Console.WriteLine("Usage : ");
			Console.WriteLine(AppDomain.CurrentDomain.FriendlyName+" is normally started as a service, without any argument/parameter.");
			Console.WriteLine("However, it can accept the following parameters :");
			Console.WriteLine("\t--config, <config_file>, -c <config_file>: \t Specifies a custom config file (default is "+AppDomain.CurrentDomain.FriendlyName+".config)");
			Console.WriteLine("\t--setup: \t starts in setup mode, allowing to do first-run configuration through the web GUI");
			Console.WriteLine("\t--password <the_password>: \t used with --setup, sets the password used for authentication in th GUI");
			Console.WriteLine("\t--gencert: \t when used with --setup, generate root CA cert and server certificates");
			Console.WriteLine("* If you run the server for the first time or haven't configured it yet, run :");
			Console.WriteLine("\t"+AppDomain.CurrentDomain.FriendlyName+" --gencert --setup --password my_pass");
			Console.WriteLine("Then run the web Gui, that will by default connect to the server using port 9999, and follow the instructions ");
			
		}
		
		/// <summary>
		/// Accepts a connection from a client, creates a User with this connection
		/// Calls method to place the user in listening mode
		/// Adds the user to the UserList and Online Users's listbox after verification
		/// </summary>
 		private void AcceptClient(IAsyncResult iar){
			Socket hub = (Socket)iar.AsyncState;
			//Node u = null;
			try{
				Socket client = hub.EndAccept(iar);
				Logger.Append("HUBRN", Severity.DEBUG, "Connection attempt from "+client.RemoteEndPoint.ToString());
				/*try{
				client.ReceiveBufferSize = 0;
				client.NoDelay = true;
				}catch{}*/
				/*IAsyncResult result = client.BeginReceive();
				Console.WriteLine ("before wqitone");
				result.AsyncWaitHandle.WaitOne(30000, true); //*/
				StateObject so = new StateObject();
				so.buffer = new byte[1024];
				so.workSocket = client;
				SocketError se = new SocketError();
				client.BeginReceive(so.buffer, 0, so.buffer.Length, SocketFlags.None, out se, ClientSSLAccept, so);
				
			}
			catch(ObjectDisposedException)	{
				// socket closed when user logged out, do nothing
			}
			catch(Exception ex){
				Utilities.Logger.Append("HUBRN", Severity.ERROR, "Could not accept logging-in node : "+ ex.Message);
				//nodeList.RemoveUser(u);
			}
			finally{

				try{
					if(running) // don't accept new connections if shutdown requested
						hub.BeginAccept(new AsyncCallback(AcceptClient), hub);// Loop...	
				}
				catch(Exception e){
					Logger.Append("HUBRN", Severity.WARNING, "Error accepting new node connection : "+e.Message);	
				}
			}
		}
		
		/// <summary>
		/// Waits for messages from a client not (yet) authenticated.
		/// Only 2 messages are accepted : 
		/// -SSL (which asks to process SSL authentication by calling ClientSSLAccept())
		/// -CERTREQ (which asks to generate and send a new SSL certificate to the node)
		/// </summary>
		/*private void ClientNoSSLAccept(IAsyncResult ar){
			Console.WriteLine("ClientNoSSLAccept 1");
			StateObject so = (StateObject)ar.AsyncState;
			Socket s = so.workSocket;
			int read = s.EndReceive(ar);
			Console.WriteLine("ClientNoSSLAccept 2");
			if (read > 0) {
				Console.WriteLine("ClientNoSSLAccept 3");
				string msg = System.Text.Encoding.UTF8.GetString(so.buffer, 0, read);
				switch(msg){
					case "SSL":
						ClientSSLAccept(s);
						break;
					case "CERTREQ":
						ClientInitiateSSL(s);
						break;
					default:
						Logger.Append("CLIENT", Severity.ERROR, "Unrecognized start message '"+msg+"' from "+s.RemoteEndPoint.ToString());
						break;
				}
			}
		}*/
		
		/// <summary>
		/// Called when a client node indicates it doesn't have an SSL certificate.
		/// We create a SSL channel without using regular client-cert authentication, then generate a certificate for
		/// 	the new client node, and send it to him. Node will have to re-login and will then be placed in a 'locked' state, 
		/// 	until manually approved and configured by an administrator
		/// </summary>
		/// <param name='client'>
		/// Client socket.
		/// </param>
		/*private void ClientInitiateSSL(IAsyncResult ar){
			StateObject so = (StateObject)ar.AsyncState;
			Socket client = so.workSocket;
			//int read = client.EndReceive(ar);
			System.Threading.Tasks.Task sslT = System.Threading.Tasks.Task.Factory.StartNew(() =>{
				NetworkStream clientStream = new NetworkStream(client);
				Logger.Append("CLIENT", Severity.DEBUG, "Establishing temporary SSL connection with "+client.RemoteEndPoint.ToString());
				//SslStream sslStream = new SslStream(clientStream);
				SslStream sslStream = new SslStream(clientStream, false, ClientFakeAuthenticate);
				Console.WriteLine("ClientInitiateSSL : 1");
				//X509Certificate hubCert = GetHubCertificate();
				try{
					sslStream.AuthenticateAsServer(this.Certificate, false, SslProtocols.Tls, false);
					//sslStream.AuthenticateAsServer(this.Certificate);
					int read = sslStream.Read(so.buffer, 0, so.buffer.Length);
					Console.WriteLine("ClientNoSSLAccept 2");
					if (read > 0) {
						Console.WriteLine("ClientNoSSLAccept 3");
						string msg = System.Text.Encoding.UTF8.GetString(so.buffer, 0, read);
						switch(msg){
							case "SSL":
								client.BeginReceive(so.buffer, 0, so.buffer.Length, SocketFlags.None, out se, ClientSSLAccept, so);
								break;
							case "CERTREQ":
								//ClientInitiateSSL(s);
								sslStream.Write(GenerateNewClientCertificate(client));
								break;
							default:
								Logger.Append("CLIENT", Severity.ERROR, "Unrecognized start message '"+msg+"' from "+s.RemoteEndPoint.ToString());
								break;
						}
					}

				}
				catch(AuthenticationException){
					Logger.Append("HUBRN", Severity.ERROR, "Client node "+client.RemoteEndPoint.ToString()+" tried to connect without certificate, kicked out.");
				}
				Logger.Append("HUBRN", Severity.DEBUG2, "Connection attempt from "+client.RemoteEndPoint.ToString()+" : SSL authentication done.");
				
				
			});
			sslT.ContinueWith(t => 
				 {
				         var aggException = t.Exception.Flatten();
				         foreach(var exception in aggException.InnerExceptions)
					Logger.Append("HUBRN", Severity.ERROR, "Unexpected error : "+exception.Message);
				  }, 
			System.Threading.Tasks.TaskContinuationOptions.OnlyOnFaulted);
		}*/
			


		private void ClientSSLAccept(IAsyncResult ar){
			StateObject so = (StateObject)ar.AsyncState;
			Socket client = so.workSocket;
			//int read = client.EndReceive(ar);
			System.Threading.Tasks.Task sslT = System.Threading.Tasks.Task.Factory.StartNew(() =>{
				NetworkStream clientStream = new NetworkStream(client);
				SslStream sslStream = new SslStream(clientStream, false, ClientAuthenticate);
				//X509Certificate hubCert = GetHubCertificate();
				Logger.Append("HUBRN", Severity.DEBUG2, "SSL Connection attempt from "+client.RemoteEndPoint.ToString()+" : beginning SSL authentication");
				try{
					sslStream.AuthenticateAsServer(Certificate, true, SslProtocols.Default, false);
					//sslStream.AuthenticateAsServer(this.Certificate);
					/*if(sslStream.RemoteCertificate == null){
						Logger.Append("HUBRN", Severity.WARNING, "Client node "+client.RemoteEndPoint.ToString()+" has sent a null certificate");
					}*/
				}
				catch(AuthenticationException){
					Logger.Append("HUBRN", Severity.INFO, "Client node "+client.RemoteEndPoint.ToString()+" tried to connect without certificate");
				}
				Logger.Append("HUBRN", Severity.DEBUG2, "Connection attempt from "+client.RemoteEndPoint.ToString()+" : SSL authentication done.");
				DisplayCertificateInformation(sslStream);
				ClientNode u = new ClientNode(sslStream, client);
				
				u.LoggEvent += new P2PBackupHub.ClientNode.LoggHandler(Logg);
				// adds the user to the UserList after verification
				u.NodeOnlineEvent += new P2PBackupHub.ClientNode.NodeOnline(nodeList.AddNode);
				u.NodeOnlineEvent += new P2PBackupHub.ClientNode.NodeOnline(TaskScheduler.Instance().Clean);
				//u.AddUserEvent += new P2PBackupHub.User.AddUserHandler(AddOnlineUser);
				// removes a user gone offline
				u.RemoveUserEvent += new P2PBackupHub.ClientNode.RemoveUserHandler(nodeList.RemoveNode);
				// SessionChangedHandler(bool added, short id, ClientNode fromNode, ClientNode toNode, long taskId);
				u.SessionChanged += PeerSessionChanged;
				u.StartListening();
			});
			sslT.ContinueWith(t =>{
			         var aggException = t.Exception.Flatten();
			         foreach(var exception in aggException.InnerExceptions)
					Logger.Append("HUBRN", Severity.ERROR, "Unexpected error : "+exception.Message);
			}, System.Threading.Tasks.TaskContinuationOptions.OnlyOnFaulted);
		}


	 	private bool ClientAuthenticate(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors){
				
			// case where client cert is null : node has lost its cert or, 
			// more probably, this a new client node
			if(certificate == null){
				Logger.Append("HUBRN", Severity.DEBUG2, "client send empty/null certificate!");
				return true; // accept anyway, as this could be a new node requesting a certificate.
			}



			// validate chain (node cert + root CA)
			try{
				var certificates = new X509Certificate2Collection();
				certificates.Add(new X509Certificate2(this.RootCA));
				var ckChain = new X509Chain();
				ckChain.ChainPolicy.RevocationMode = X509RevocationMode.NoCheck;
				ckChain.ChainPolicy.ExtraStore.AddRange(certificates);
				ckChain.ChainPolicy.VerificationFlags = (X509VerificationFlags.NoFlag); // .AllowUnknownCertificateAuthority | X509VerificationFlags.IgnoreWrongUsage);
				bool isValidCertificate = ckChain.Build(new X509Certificate2(certificate));
					
					
				if(!isValidCertificate){
					//Logger.Append("HUBRN", Severity.WARNING, "Errors found when validating certificates chain");
					bool requireMatchingCA = true;
					bool.TryParse(ConfigurationManager.AppSettings["Security.RequireCAMatchingHubCA"], out requireMatchingCA);
					bool requireNotExpiredCert = true;
					bool.TryParse(ConfigurationManager.AppSettings["Security.RequireCertNotExpired"], out requireNotExpiredCert);
					string node = certificate.Subject;
					string advancedErrors = "";
					foreach(X509ChainStatus chainStatus in ckChain.ChainStatus){
						if(chainStatus.Status == X509ChainStatusFlags.CtlNotTimeValid){
							bool retStatus = true;
							Severity sev = Severity.WARNING;
							if(requireNotExpiredCert){
								retStatus = false;
								sev = Severity.ERROR;
							}
							Logger.Append("HUBRN", sev, "Certificate for node '"+node+"' has expired (expiration date: '"+certificate.GetExpirationDateString()+"' or has invalid dates"+( (sev == Severity.ERROR)?", rejecting node.":""));
							return retStatus;
						}
						else if(chainStatus.Status == X509ChainStatusFlags.PartialChain){
							bool retStatus = true;
							Severity sev = Severity.WARNING;
							if(requireMatchingCA){
								retStatus =  false;
								sev = Severity.ERROR;
							}
							Logger.Append("HUBRN", sev, "Certificate for node '"+node+"' has not been generated using Hub root CA"+( (sev == Severity.ERROR)?", rejecting node.":""));	
							return retStatus;
						}
						// we tolerate UntrustedRoot errors, provided the client certificcate has been signed by the Hub CA.
						else if (chainStatus.Status != X509ChainStatusFlags.NoError && chainStatus.Status != X509ChainStatusFlags.UntrustedRoot)
							advancedErrors += chainStatus.StatusInformation+", ";
							
					}
					if(advancedErrors != ""){ // other types of errors : should not happen, so reject client.
						Logger.Append("HUBRN", Severity.WARNING, "Certificate for node '"+node+"' is invalid, reason(s): "+advancedErrors);	
						return false;
					}
				}	
			}
			catch(Exception e){
				Logger.Append("HUBRN", Severity.WARNING, "error trying to validate client cert : "+e.Message+" ---- "+e.StackTrace);
				return false;
			}
            		return true;
        	}
		

		
		
		private static void DisplayCertificateInformation(SslStream stream){ 
		    Logger.Append("CLIENT", Severity.DEBUG, "Certificate revocation list checked: " + stream.CheckCertRevocationStatus);
		
		    X509Certificate localCertificate = stream.LocalCertificate;
		    if (stream.LocalCertificate != null){
		        Logger.Append("CLIENT", Severity.DEBUG, string.Format("Local (Hub) cert was issued to {0} and is valid from {1} until {2}.",
		            localCertificate.Subject,
		            localCertificate.GetEffectiveDateString(),
		            localCertificate.GetExpirationDateString()));
		     } else{
		        Logger.Append("CLIENT", Severity.DEBUG, "Local (Hub) certificate is null.");
		    }
		    // Display the properties of the client's certificate.
		    X509Certificate remoteCertificate = stream.RemoteCertificate;
		    if (stream.RemoteCertificate != null){
		    Logger.Append("CLIENT", Severity.DEBUG, string.Format("Remote (Node) cert was issued to {0} and is valid from {1} until {2}.",
		        remoteCertificate.Subject,
		        remoteCertificate.GetEffectiveDateString(),
		        remoteCertificate.GetExpirationDateString()
			));
		    } 
		    else{
		        Logger.Append("CLIENT", Severity.DEBUG, "Remote (Node) certificate is null.");
		    }
		}
		
		
		/// <summary>
		/// Periodically watch for disconnected nodes to kick out of online list
		/// </summary>
		/*private void MonitorNodes(){
			try{
				while(running){
					Thread.Sleep (2000);
					//foreach(Node u in userList)	{		
					for(int i=nodeList.Count - 1; i==0; i--){
						if(!running) return;
						ClientNode n = (ClientNode)nodeList[i];
						Console.WriteLine(DateTime.Now+", polling node "+n.Uid);
						if (n.Connection.Poll(1000, SelectMode.SelectRead))

							if (n.Connection.Available == 0){
								n.Logout();	
								Utilities.Logger.Append("HUBRN", Severity.INFO, "Node "+n.NodeName+" removed from online list");
								nodeList.RemoveAt(i);
							}
					}
				}	
			}
			catch(Exception ex){
				Utilities.Logger.Append("HUBRN", Severity.ERROR, "unable to logout user :"+ex.Message);
				if(running)
					MonitorNodes();
			}
		}*/

		/// <summary>
		/// Creates a socket for incoming connections from clients
		/// Starts a thread that removes lost connections
		/// </summary>

		private void Run(){
			/*try{*/
			if(running)
				throw new Exception("Already running");
			int maxClientNodes = 300;
			int.TryParse(ConfigurationManager.AppSettings["Nodes.MaxConnectedClients"], out maxClientNodes);
			ThreadPool.SetMaxThreads(maxClientNodes+5, maxClientNodes);
			Utilities.Logger.Append("START", Severity.INFO, "######## Starting Hub as '"+Thread.CurrentPrincipal.Identity.Name+"'. Date: "+DateTime.Now.ToString()+", Version: "+Utilities.PlatForm.Instance().NodeVersion
	      			+", OS: "+Utilities.PlatForm.Instance().OS+", Runtime: "+Utilities.PlatForm.Instance().Runtime
			        +", DB Provider: "+ConfigurationManager.AppSettings["Storage.DBHandle.Provider"]);
			running = true;
			Socket hub = new Socket(AddressFamily.InterNetwork,SocketType.Stream,ProtocolType.Tcp);
			connection = hub;
			
			string listenIp = ConfigurationManager.AppSettings["Nodes.ListenIp"];
			int listenPort = int.Parse(ConfigurationManager.AppSettings["Nodes.ListenPort"]);
			IPAddress listenAddress;
			if (listenIp == "*")
				listenAddress = IPAddress.Any;
			else
				listenAddress = IPAddress.Parse(listenIp);
			IPEndPoint src = new IPEndPoint(listenAddress, listenPort);
			
			hub.Bind(src);
			hub.Listen(100);
			hub.BeginAccept(new AsyncCallback(AcceptClient),hub);
			//Utilities.Logger.Append("START", Severity.INFO,"Starting nodes watchdog...");
			//Thread t = new Thread(new ThreadStart(this.MonitorNodes));
			//t.IsBackground = false;
			//t.Start();

			Utilities.Logger.Append("START", Severity.INFO,"Starting Scheduler...");
			//Scheduler.Start();
			TaskScheduler.Start();
			TasksMonitor.Start();
			Utilities.Logger.Append("START", Severity.INFO, "Starting Remoting Server...");
			Remoting.RemotingServer.Start();
			Utilities.Logger.Append("START", Severity.INFO,"Hub started.");
				//Utils.SetProcInfo("Hub (Run)");
			/*}
			catch(Exception ex)	{
				Utilities.Logger.Append("START", Severity.ERROR, ex.Message+" ---- "+ex.StackTrace);
				//Utilities.Logger.Append("START", Severity.ERROR, "inner exception : "+ex.InnerException.Message+" ---- "+ex.InnerException.StackTrace);
			}*/
		}


		/// <summary>
		/// Logs all messages between user and hub
		/// </summary>
		/// <param name="name">sending/receiving user</param>
		/// <param name="received">direction of the transfer - received or sent</param>
		/// <param name="message">the message received or sent</param>
		private void Logg(string name, bool received, string message){
			string transfer = "rcvd";
			if(!received)
				transfer = "sent";
			string[] item = {name,transfer,message};
			String code = message.Substring(0,3);
			if(received) Utilities.Logger.Append(transfer /* operation*/,  Severity.DEBUG, message +" "+ Codes.GetDescription(code));
		}

		private void PeerSessionChanged(bool added, SessionType sessType, int id, ClientNode fromNode, ClientNode toNode, long taskId, int budget){
			if(added)
				lock(sessionsList){
					bool found = false;
					// add it to the geenral session list, if not exists
					/*foreach(PeerSession sess in sessionsList){
						if(id == sess.Id){
							sess.Budget += budget;
							found = true;
						}
					}
					if(!found){*/
						sessionsList.Add((new PeerSession(id, sessType, fromNode, toNode, taskId, budget)));
						// adjust the storage node Load accordingly
						nodeList.GetNode(toNode.Uid).CurrentLoad += 1/(toNode.StoragePriority);
					//}
				}
			else{
				lock(sessionsList){
					for(int i = sessionsList.Count-1; i>=0; i--)
						if(sessionsList[i].Id == id){
							sessionsList.RemoveAt(i);
							return;
						}

				}

			}
		}

		internal static List<P2PBackup.Common.Node> DiscoverVms(int hypervisorId){
			List<P2PBackup.Common.Node> newNodes = new List<P2PBackup.Common.Node>();
			using (P2PBackupHub.Virtualization.HypervisorManager hvm = new P2PBackupHub.Virtualization.HypervisorManager()){
				DBHandle dbh = new DBHandle();
				Hypervisor hv = dbh.GetHypervisor(hypervisorId);
				hvm.Id = hv.Id;
				hvm.Kind = hv.Kind;
				hvm.Url = hv.Url;
				hvm.UserName = hv.UserName;
				hvm.Password = hv.Password;
				dbh.UpdateDiscoveryDate(hv.Id);
				Console.WriteLine("DiscoverVms()  0");
				List<P2PBackup.Common.Node> discoveredNodes = hvm.Discover();
				Console.WriteLine("DiscoverVms()  1");
				Logger.Append("HUBRN", Severity.DEBUG, "Discovered "+discoveredNodes.Count+" VMs");
				Console.WriteLine("DiscoverVms()  2, count="+discoveredNodes.Count);
				
				foreach(P2PBackup.Common.Node newN in  discoveredNodes){
					Console.WriteLine("DiscoverVms()  3, cur newn="+newN.NodeName);
					try{
						P2PBackup.Common.Node existingNode = dbh.GetNode(newN.InternalId);
						Console.WriteLine("DiscoverVms()  3.1");
						if(existingNode == null){
							Logger.Append("HUBRN", Severity.DEBUG, "The VM "+newN.NodeName+" has been added to the client nodes.");
							newN.Hypervisor = hypervisorId;
							existingNode = dbh.AddNode(newN);
							if(existingNode != null){
								existingNode.Status = NodeStatus.New;
								newNodes.Add(existingNode);
							}
						}

					}
					catch(Exception e){
						Logger.Append("HUBRN", Severity.ERROR, "Could not add new discovered node : "+e.ToString());
					}
				}
			}
			return newNodes;
		}

		/// <summary>
		/// Logs out all online users and stops the hub
		/// </summary>
		internal static void Shutdown(User user, bool cancelTasks){
			ClientNode u;
			try{
				Utilities.Logger.Append("STOP", Severity.INFO, "Hub shutdown requested by '"+user.Name+"', stopping...");
				Utilities.Logger.Append("STOP", Severity.INFO, "Informing client nodes of shutdown...");
				if(running){
					running = false;
					Thread.MemoryBarrier();
					Thread.Sleep(1000);
					TaskScheduler.Stop();
					TasksMonitor.Stop();
					//Scheduler.Stop();
					for (int i = 0; i < nodeList.Count; i++){
						u = (ClientNode)nodeList[i];
						u.Logout();
						i = i - 1;
					}
					if(connection != null){
						//connection.Close();
						connection.Disconnect(true);
						connection.Dispose();
						
					}
				}

				Utilities.Logger.Append("STOP", Severity.INFO, "Hub stopped.");
				//return true;
				Environment.Exit(0);
			}
			catch(Exception ex){
				Utilities.Logger.Append("STOP", Severity.ERROR, "Could not stop Hub : "+ex.Message);
			}
		}
		
		static void UnhandledExceptionHandler(object sender, UnhandledExceptionEventArgs e){
			try{
				Logger.Append("HUBRN", Severity.CRITICAL, "Unhandlable error : "+e.ExceptionObject);	
			}catch{}
    			Console.WriteLine("Unhandlable error : "+e.ExceptionObject);
		}
	}
}
