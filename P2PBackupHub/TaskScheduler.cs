#define DEBUG
using System;
using System.Linq;
using System.Threading;
using System.Collections;
using System.Collections.Generic;
using P2PBackup.Common;
using P2PBackupHub.Utilities;
using System.Configuration;


namespace P2PBackupHub{
	// The scheduler runs an infinite thread that launchs backupsSets when it's time to.
	// It maintains a list of running tasks (backups).
	// every minute we poll the database to retrieve the next backups to start
	public class TaskScheduler{
		
		private static TaskScheduler _instance;
		private static bool run; // must run
		private static int maxLateness;
		private static bool isFirstStart;
		private static List<Task> TasksQueue;
		private static DBHandle dbHandle;
		private static Thread workerThread;

		private TaskScheduler(){
			run = true;
			//running = true;
			isFirstStart = true;
			TasksQueue = new List<Task>();
			maxLateness = int.Parse(ConfigurationManager.AppSettings["BackupSet.RetryTime"]);
			workerThread = new Thread(Schedule);
        		workerThread.Start();
			int availThreads,null1 = 0;
			int maxThreads,null2 = 0;
			ThreadPool.GetAvailableThreads(out availThreads, out null1);
			ThreadPool.GetMaxThreads(out maxThreads, out null2);
			Logger.Append("HUBRN", Severity.INFO, "Started scheduler, thread pool "+availThreads+"/"+maxThreads)	;
		}
		
		public static void Start(){
			if(_instance == null)
				_instance = new TaskScheduler();
		}
		
		public static void Stop(){
			run = false;
			Logger.Append("HUBRN", Severity.INFO, "Stopping scheduler...");
			workerThread.Join();
		}
		
		internal static TaskScheduler Instance(){
			return _instance;
		}
		
		internal static List<Task> Tasks{
			get{return TasksQueue;}	
		}
		
		internal  List<Task> GetTasks(){
			List<Task> gTasks = new List<Task>();
			lock(TasksQueue)
				foreach(Task t in TasksQueue)
					gTasks.Add((Task)t);
			return gTasks;
		}
		
		private void Schedule(){
			dbHandle = new DBHandle();
			while(run){
				if(isFirstStart){
					int interruptedTasks = dbHandle.UpdateCancelledTasks();
					Logger.Append("HUBRN", Severity.INFO, "Updated "+interruptedTasks+" previously interrupted task(s).");
					Logger.Append("HUBRN", Severity.INFO, "Waiting 20s before starting to schedule tasks...");
					Thread.Sleep(20000);
					isFirstStart = false;
				}
				Logger.Append("HUBRN", Severity.DEBUG, "Polling database for next BackupSets to run.");
				List<BackupSet> nextBS = dbHandle.GetTaskSets(DateTime.Now, DateTime.Now.AddMinutes(1), false);
				int added=0;
				foreach(BackupSet bsn in nextBS){
					CreateTask(bsn, TaskStartupType.Scheduled, null);
				}
				Logger.Append("HUBRN", Severity.DEBUG, "Added "+nextBS.Count+" Backup Sets to queue. Total queue size : "+TasksQueue.Count);
					
				for(int i = TasksQueue.Count - 1; i >= 0; i--){
					Task task = TasksQueue[i];
					//Console.WriteLine("schedule : parsed date begin="+bs.BackupTimes[0].Begin+":00");
					//REACTIVER!!!	//if(TimeSpan.Parse(bs.BackupTimes[0].Begin+":00") <= new TimeSpan(DateTime.Now.Hour, DateTime.Now.Minute, 0)){
						
					if(task.RunStatus == TaskRunningStatus.Done || task.RunStatus == TaskRunningStatus.Cancelled || task.RunStatus == TaskRunningStatus.Error){
						Logger.Append("HUBRN", Severity.INFO, "Task "+task.Id+", Backupset "+task.BackupSet.ToString()+" ended with status "+task.RunStatus+", removing from queue.");
						RunTask.Complete(task);
						TasksQueue.RemoveAt(i); // TODO : remove this line, instead keep DEBUG one
#if DEBUG
#else
						TasksQueue.RemoveAt(i);
#endif
					}
					//else if(task.RunStatus == TaskRunningStatus.Error)
					else if(task.RunStatus == TaskRunningStatus.PendingStart){
						//
						//task.Start();
						RunTask.Start(task);
						try{
							StartTask(task);
							task.RunStatus = TaskRunningStatus.Started;
							// wait 0.1 second before starting another task
							Thread.Sleep(100);
						}
						catch(OverQuotaException oqe){
							Logger.Append("HUBRN", Severity.ERROR, "Could not start task "+task.Id+" on client node #"+task.BackupSet.NodeId
						              +" : "+oqe.Message);
								task.Status = TaskStatus.Error;
								task.RunStatus = TaskRunningStatus.Error;
								task.EndDate = DateTime.Now;
								task.CurrentAction = oqe.Message;
								task.AddLogEntry(830, "", "");
						}
						catch(UnreachableNodeException){
							Logger.Append("HUBRN", Severity.ERROR, "Could not send task "+task.Id+" to node #"+task.BackupSet.NodeId+": Node is offline");	
							//TimeSpan lateness = new TimeSpan(DateTime.Now.Hour, DateTime.Now.Minute, 0).Subtract(TimeSpan.Parse(task.BackupSet.BackupTimes[0].Begin+":00"));
							TimeSpan lateness = DateTime.Now.Subtract(task.StartDate);
							if(lateness >= new TimeSpan(0, maxLateness,0) ){
								Logger.Append("HUBRN", Severity.ERROR, "Could not start task "+task.Id+" for client #"+task.BackupSet.NodeId+", retry time expired. (lateness :"+lateness.Minutes+" minutes)");	
								task.Status = TaskStatus.Error;
								task.RunStatus = TaskRunningStatus.Error;
								task.EndDate = DateTime.Now;
								task.CurrentAction = "Retry time expired";
								task.AddLogEntry(901, "", "");
								//task.Complete();
								//RunTask.Complete(task);
							}
							else {
								int remainingRetry = new TimeSpan(0, maxLateness, 0).Subtract(lateness).Minutes;
								Logger.Append("HUBRN", Severity.WARNING, "Could not start task "+task.Id+" for client #"+task.BackupSet.NodeId+", will retry during "+remainingRetry+" mn");	
								task.Status = TaskStatus.Warning;
								//task.RunStatus = TaskRunningStatus.PendingStart;
								//TaskPublisher.Instance().Notify(task);
								//task.AddLogEntry("901", "Could not start Backup Set , will retry during "+remainingRetry+" mn");
								task.AddLogEntry(901, remainingRetry.ToString(), "");
								task.CurrentAction = "Could not start operation, will retry during "+remainingRetry+" mn";
							}
						}
						
					}
					/*else if(task.RunStatus == TaskRunningStatus.Started){ // task is running, ask fresh stats to client node
						Logger.Append("HUBRN", Severity.DEBUG, "Asking stats to node #"+task.BackupSet.ClientId+" for task "+task.Id);
						Node theNode = Hub.NodesList.GetClient(task.BackupSet.ClientId);
						if(theNode != null)
							theNode.AskStats(task.Id);
					}*/
					/*else if(task.RunStatus == TaskRunningStatus.Error){ 
						//Logger.Append("HUBRN", Severity.WARNING, "Task "+task.Id+" ended with status ERROR.");
						//task.RunStatus = TaskRunningStatus.Error;
						//task.Complete();
					RunTask.Complete(task);
					}*/
					/*else if(task.RunStatus == TaskRunningStatus.Cancelled){ // task is running, ask fresh stats to client node
						Logger.Append("HUBRN", Severity.WARNING, "Task "+task.Id+" has been cancelled by manual request.");
						task.Complete();
					}*/
					//}
				} // end for
				Utils.SetProcInfo("Hub ("+TasksQueue.Count+" tasks)");
				for(int i=0; i<60;i++)
					if(run) Thread.Sleep(1000); 
			}
			Logger.Append("HUBRN", Severity.INFO, "Scheduler stopped.");
		}
		
		private bool TasksQueueExists(BackupSet bs){
			var list = from Task t in TasksQueue 
				where t.BackupSet.Id == bs.Id 
				&& t.RunStatus != TaskRunningStatus.Cancelling
				&& t.RunStatus != TaskRunningStatus.Cancelled 
				&& t.RunStatus != TaskRunningStatus.Done
				select t;
			if(list.Count() == 0) return false;
			else return true;
		}
		
		private bool StartTask(Task task){
			bool done = false;
			if(task.Operation == TaskOperation.HouseKeeping)
				return StartHouseKeeping(task);
			//ClientNode taskTargetNode = Hub.NodesList.GetClient(task.BackupSet.NodeId);

			ClientNode taskTargetNode = GetHandlingNode(task.Id);
			/*if(task.BackupSet.HandledBy >0)
				taskTargetNode = Hub.NodesList.GetClient(task.BackupSet.HandledBy);
			else
				taskTargetNode = Hub.NodesList.GetClient(task.BackupSet.NodeId);*/
			if(taskTargetNode == null)
				throw new UnreachableNodeException();
			else if(taskTargetNode.Quota > 0 && taskTargetNode.UsedQuota >= taskTargetNode.Quota)
				throw new OverQuotaException(taskTargetNode.UsedQuota, taskTargetNode.Quota);
			Logger.Append("HUBRN", Severity.INFO, "Starting Task "+task.Id+" : type "+task.Type +" ("+task.BackupSet.ScheduleTimes[0].Level+"), backup Set "+task.BackupSet.Id+" for client #"+task.BackupSet.NodeId+" (handled by node #"+task.BackupSet.HandledBy+")");
			//Console.WriteLine("TaskScheduler : handledby = "+task.BackupSet.HandledBy+", proxying info is null : "+(task.BackupSet.ProxyingInfo == null));
			try{
				// if task is an incremental/differential backup, get the reference backup times
				//DateTime refStartDate;
				//DateTime refEndDate;
				P2PBackup.Common.Task referenceTask = dbHandle.GetLastReferenceTask(task.BackupSet.Id, task.BackupSet.ScheduleTimes[0].Level);
				if(referenceTask != null){
					task.StorageBudget = (int)((referenceTask.OriginalSize/task.BackupSet.MaxChunkSize)+2);
					Console.WriteLine(" ____ ref task="+referenceTask.Id+", oSize="+referenceTask.OriginalSize/1024/1024+"MB, maxchunksize="+task.BackupSet.MaxChunkSize/1024/1024+"MB, %%="+referenceTask.OriginalSize/task.BackupSet.MaxChunkSize+", calculated budget="+task.StorageBudget);
				}
				task.Level = task.BackupSet.ScheduleTimes[0].Level;
				if(task.BackupSet.ScheduleTimes[0].Level != BackupLevel.Full){
					
					if(referenceTask == null || referenceTask.Id <= 0){ // no ref backup found, doing full
						Logger.Append("HUBRN", Severity.INFO, "No reference backup found for task "+task.Id+", performing FULL backup.");
						task.BackupSet.ScheduleTimes[0].Level = BackupLevel.Full;
						task.Level = BackupLevel.Full;
					}
					else{
						task.ParentTrackingId = referenceTask.Id;
						Logger.Append("HUBRN", Severity.INFO, "Task "+task.Id+" is "+task.BackupSet.ScheduleTimes[0].Level+"."
						              +" Using reference task "+referenceTask.Id+" ("+referenceTask.StartDate+" - "+referenceTask.EndDate +")");
						//refStartDate = referenceTask.StartDate;
						//refEndDate = referenceTask.EndDate;
					}
				}
				taskTargetNode.SendMessage("BKS "+task.Id+" "
					+((referenceTask == null)? (0+" "+0+" "+0):referenceTask.Id+" "+referenceTask.IndexSum
				  		+" "+referenceTask.SyntheticIndexSum)
				        //+((referenceTask == null)? (0+" "+0):referenceTask.Id+" "+referenceTask.SyntheticIndexSum)
				        +" "+Utils.GetUnixTimeFromDateTime(referenceTask.StartDate)
				        +" "+Utils.GetUnixTimeFromDateTime(referenceTask.EndDate)
				        +" "+task.BackupSet.DumpToXml());
				task.RunStatus = TaskRunningStatus.Started;
				//n.Status = NodeStatus.Backuping;
				done = true;
			}
			catch (Exception e){
				done = false;
				Logger.Append("HUBRN", Severity.ERROR, "Could not send task "+task.Id+" to node #"+taskTargetNode.Uid+" : "+e.ToString()/*+"---Stacktrace:"+e.StackTrace+" inner msg:"+e.InnerException.Message*/);
				//n.Status = NodeStatus.Error;
			}
			return done;
		}
		
		internal void SetTaskRunningStatus(long taskId, TaskRunningStatus status){
			lock(TasksQueue){
				for(int i = TasksQueue.Count - 1; i >= 0; i--){
					if(TasksQueue[i].Id == taskId)
						TasksQueue[i].RunStatus = status;
				}
			}
		}
		
		internal void SetTaskCurrentActivity(long taskId, int code, string activity){
			lock(TasksQueue){
				for(int i = TasksQueue.Count - 1; i >= 0; i--){
					if(TasksQueue[i].Id == taskId)
						TasksQueue[i].CurrentAction = activity;
				}
			}
			
		}
		
		internal void SetTaskStatus(long taskId, TaskStatus status){
			lock(TasksQueue){
				for(int i = TasksQueue.Count - 1; i >= 0; i--){
					if(TasksQueue[i].Id == taskId){
						TasksQueue[i].Status = status;
						
					}
				}
			}
		}
		
		internal void CancelTask(long taskId, User u){
			Logger.Append("HUBRN", Severity.INFO, "Received cancel request for task "+taskId);
			Task task = GetTask(taskId);
			if(task.RunStatus <= TaskRunningStatus.PendingStart){
				SetTaskRunningStatus(taskId, TaskRunningStatus.Cancelled); 
				return;
			}
			ClientNode taskTargetNode = GetHandlingNode(taskId);
			Logger.Append("HUBRN", Severity.INFO, "Asking  to node #"+task.BackupSet.NodeId+" to cancel task "+task.Id);
			if(taskTargetNode != null){
				taskTargetNode.SendMessage("CANCEL "+task.Id); 
				taskTargetNode.Status = NodeStatus.Idle;
			}
			else
				Logger.Append("HUBRN", Severity.WARNING, "Could not send cancel message to node #"+task.BackupSet.NodeId+": node is offline");
			SetTaskRunningStatus(taskId, TaskRunningStatus.Cancelling); 

		}
		
		internal void PauseTask(long taskId, User u){
			Task task = GetTask(taskId);
			ClientNode taskTargetNode = GetHandlingNode(taskId);
			if(taskTargetNode != null){
				Logger.Append("HUBRN", Severity.INFO, "Asking  to node #"+task.BackupSet.NodeId+" to cancel task "+task.Id);
				taskTargetNode.SendMessage("PAUSE "+task.Id); 
				SetTaskRunningStatus(taskId, TaskRunningStatus.Paused); 
			}
		}
		
		internal void UpdateTaskStats(long taskId, long originalSize, long finalSize, long nbItems, int completionpercent){
			lock(TasksQueue){
				for(int i = TasksQueue.Count - 1; i >= 0; i--){
					if(TasksQueue[i].Id != taskId)
						continue;
					TasksQueue[i].OriginalSize = originalSize;
					TasksQueue[i].FinalSize = finalSize;
					TasksQueue[i].TotalItems = (int)nbItems;
					TasksQueue[i].Percent = completionpercent;
				}
			}	
		}
		
		// To be called when backup is done/error/cancelled
		internal static void UpdateTask(long taskId, long originalSize, long finalSize, long nbItems, string indexName, string indexSum, string synthIndexSum, List<int> indexDestinations, int completionPercent){
			lock(TasksQueue){
				for(int i = TasksQueue.Count - 1; i >= 0; i--){
					if(TasksQueue[i].Id != taskId)
						continue;
					TasksQueue[i].OriginalSize = originalSize;
					TasksQueue[i].FinalSize = finalSize;
					TasksQueue[i].TotalItems = (int)nbItems;
					TasksQueue[i].IndexName = indexName;
					TasksQueue[i].IndexSum = indexSum;
					TasksQueue[i].SyntheticIndexSum = synthIndexSum;
					TasksQueue[i].EndDate = DateTime.Now;
					TasksQueue[i].IndexStorageNodes = indexDestinations;
					TasksQueue[i].Percent = completionPercent;
					if(TasksQueue[i].Status == TaskStatus.Error)
						TasksQueue[i].RunStatus = TaskRunningStatus.Error;
					else if(TasksQueue[i].RunStatus != TaskRunningStatus.Cancelling)
						TasksQueue[i].RunStatus = TaskRunningStatus.Done;
					else
						TasksQueue[i].RunStatus = TaskRunningStatus.Cancelled;
					//((RunTask)TasksQueue[i]).Complete();
				}
			}	
		}
		
		internal static void AddTaskLogEntry(long taskId, int code, string activity, string additionalMessage1, string additionalMessage2){
			lock(TasksQueue){
				for(int i = TasksQueue.Count - 1; i >= 0; i--){
					if(TasksQueue[i].Id == taskId)
						TasksQueue[i].AddLogEntry(code, additionalMessage1, additionalMessage2);
				}
			}	
		}
		
		/*internal void RemoveTaskSession(long taskId, int nodeId){
			lock(TasksQueue){
				for(int i = TasksQueue.Count - 1; i >= 0; i--){
					if(TasksQueue[i].Id == taskId)
						foreach(P2PBackup.Common.Node n in TasksQueue[i].StorageNodes)
							if(n.Uid == nodeId)
								TasksQueue[i].RemoveStorageNode(n);
				}
			}	
		}*/
		/*internal  BackupSet GetRunningBs(int bsId){
			var bs = from task in tasksQueue 
					where task.BackupSet.Id == bsId 
					select task.BackupSet;
			//Console.WriteLine("getrunningbs : got "+bs.Count());
			return (bs.ToList())[0] as BackupSet;
		}*/
		
		internal Task GetTask(long taskId){
			for(int i = TasksQueue.Count - 1; i >= 0; i--){
				if(TasksQueue[i].Id == taskId)
					return TasksQueue[i];
			}
			return null;
		}
		
		/// <summary>
		/// Te be used when a node connects. If it is complete re-connection (with re-login), 
		/// clean previously running tasks.
		/// </summary>
		/// <param name='nodeId'>
		/// Node identifier.
		/// </param>
		internal void Clean(ClientNode node){
			lock(TasksQueue){
				for(int i = TasksQueue.Count - 1; i >= 0; i--){
					if(TasksQueue[i].UserId == node.Uid){
						TasksQueue[i].AddLogEntry(808, "", "");
						TasksQueue[i].RunStatus = TaskRunningStatus.Cancelled;
						//((RunTask)TasksQueue[i]).Complete();
						RunTask.Complete(TasksQueue[i]);
					}
				}
			}	
		}
		
		private bool StartHouseKeeping(Task task){
			var cleanThread = System.Threading.Tasks.Task.Factory.StartNew(() =>{
				List<P2PBackup.Common.Task> expiredBackups = dbHandle.GetExpiredBackups();
				task.OriginalSize = expiredBackups.Sum(o=> o.FinalSize);
				task.TotalItems = expiredBackups.Count;
				Logger.Append("HUBRN", Severity.INFO, "Started cleaning "+expiredBackups.Count+" expired backups");
				//int done = 0;
				try{
				foreach(P2PBackup.Common.Task nodeTask in expiredBackups){
					if(nodeTask == null) continue;
					dbHandle.UpdateTaskStatus(nodeTask.Id, TaskRunningStatus.Expiring);
					ClientNode node = Hub.NodesList.GetNode(nodeTask.BackupSet.NodeId);
					if(node != null){
						Logger.Append("HUBRN", Severity.INFO, "Asking node #"+node.Uid+" ("+node.NodeName+") to expire task "+nodeTask.Id);
						node.SendMessage("EXP "+task.Id+" "+nodeTask.Id+" "+nodeTask.IndexName+" "+nodeTask.IndexSum);
					}
					else
						Logger.Append("HUBRN", Severity.WARNING, "Can't expire task "+nodeTask.Id+" of node #"+nodeTask.BackupSet.NodeId+", node is offline");
					//done++;
					
				}
				}
				catch(Exception e){
					Console.WriteLine("StartHouseKeeping() : "+e.Message+" ---- "+e.StackTrace);	
				}
				
			}, System.Threading.Tasks.TaskCreationOptions.LongRunning);
			/*cleanThread.ContinueWith(o=>{
				UpdateTask(task.Id, task.OriginalSize, task.FinalSize, "", "", new List<int>(), 100);
			}, System.Threading.Tasks.TaskContinuationOptions.OnlyOnRanToCompletion);*/
			return true;
		}

		private Task CreateTask(BackupSet bs, TaskStartupType taskType, User u){
			if(TasksQueueExists(bs))
				return null;
			Task newBackup = new Task();
			newBackup.Type = taskType;
			newBackup.Operation = bs.Operation;
			newBackup.BackupSet = bs;
			newBackup.NodeId = bs.NodeId;
			newBackup.CurrentAction = "Initializing";
			newBackup.RunStatus = TaskRunningStatus.PendingStart;
			if(u != null){
				newBackup.UserId = u.Id;
				Logger.Append("HUBRN", Severity.DEBUG, "User "+u.Id+" ("+u.Name+") started new task for backupset "+bs.Id+" (client "+bs.NodeId+")");
			}
			//((RunTask)newBackup).Create();
			RunTask.Create(newBackup);
			if(newBackup.Id > -1){
				TasksQueue.Add((Task)newBackup);
				//added++;

				TaskPublisher.Instance().Notify(newBackup);
				return newBackup;
			}
			else
				Logger.Append("HUBRN", Severity.ERROR, "Unable to create "+newBackup.Operation+" Task ("+newBackup.Type+")");
			return null;
		}

		internal long StartImmediateTask(int bsId, User u, BackupLevel? level){
			if(!run) return 0;
			BackupSet bs = dbHandle.GetTaskSet(bsId);
			if(level.HasValue)
				bs.ScheduleTimes[0].Level = (BackupLevel)level;
			if(bs != null && bs.Id >=0){
				Task manualTask = CreateTask(bs, TaskStartupType.Manual, u);
				StartTask(manualTask);
				return manualTask.Id;
			}
			else
				throw new Exception("Invalid Taskset id");
			return 0;
		}

		private ClientNode GetHandlingNode(long taskId){
			Task t = GetTask(taskId);
			if(t.BackupSet.HandledBy >0)
				return Hub.NodesList.GetNode(t.BackupSet.HandledBy);
			else 
				return Hub.NodesList.GetNode(t.NodeId);

		}
	}
	
}
	 
