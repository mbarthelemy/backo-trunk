using System;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;

using P2PBackup.Common;
using P2PBackupHub.Utilities;


namespace P2PBackupHub {

	internal partial class DBHandle {

		// add a newly discovered VM as node
		internal P2PBackup.Common.Node AddNode(P2PBackup.Common.Node n){
			DbConnection dbConnection = GetConnection();
       			DbCommand dbCommand = dbConnection.CreateCommand();
			IDbTransaction dbt = dbConnection.BeginTransaction();
		
			int nodeId = GetNewNodeId();
			//string query2 = "INSERT INTO nodescertificates (id, nodeid, serial, certificate) VALUES (@id, @nodeid, @serial, @certificate)";
				//""+nodeId+", '"+nodeHostName+"', '"+nodePubKey+"')";
			try{

				string query = "INSERT INTO Nodes (uid, internalid, name, ip, os, kind, hypervisor) VALUES("
					+"@uid, @internalid, @name, @ip, @os, @kind, @hypervisor)";
					/*
					+" ("+nodeId
					+", '"+n.InternalId
					+"', '" + n.NodeName
					+ "', '" + n.IP
					+"', '"	+n.OS 
					+"', "+(int)n.Kind
					+ ", 'True')";*/
				dbCommand.CommandText = query;
				DbParameter uid = dbCommand.CreateParameter();
				uid.ParameterName = "@uid";
				uid.Value = nodeId;
				dbCommand.Parameters.Add(uid);

				DbParameter iid = dbCommand.CreateParameter();
				iid.ParameterName = "@internalid";
				iid.Value = n.InternalId;
				dbCommand.Parameters.Add(iid);

				DbParameter name = dbCommand.CreateParameter();
				name.ParameterName = "@name";
				name.Value = n.NodeName;
				dbCommand.Parameters.Add(name);

				DbParameter ip = dbCommand.CreateParameter();
				ip.ParameterName = "@ip";
				ip.Value = n.IP;
				dbCommand.Parameters.Add(ip);

				DbParameter os = dbCommand.CreateParameter();
				os.ParameterName = "@os";
				os.Value = n.OS;
				dbCommand.Parameters.Add(os);

				DbParameter kind = dbCommand.CreateParameter();
				kind.ParameterName = "@kind";
				kind.Value = (int)n.Kind;
				dbCommand.Parameters.Add(kind);

				DbParameter hypervisor = dbCommand.CreateParameter();
				hypervisor.ParameterName = "@hypervisor";
				hypervisor.Value = n.Hypervisor;
				dbCommand.Parameters.Add(hypervisor);

				dbCommand.ExecuteNonQuery();
				/*dbCommand.CommandText = query2;
				DbParameter id = dbCommand.CreateParameter();
				id.ParameterName = "@id";
				id.Value = GetNewNodeCertId();
				dbCommand.Parameters.Add(id);
				DbParameter nodeid = dbCommand.CreateParameter();
				nodeid.ParameterName = "@nodeid";
				nodeid.Value = nodeId;
				dbCommand.Parameters.Add(nodeid);
				DbParameter serial = dbCommand.CreateParameter();
				serial.ParameterName = "@serial";
				serial.Value = certificate.SerialNumber;
				dbCommand.Parameters.Add(serial);
				DbParameter cert = dbCommand.CreateParameter();
				cert.DbType = DbType.Binary;
				cert.ParameterName = "@certificate";
				cert.Value = certificate.Export(X509ContentType.Cert);//TODO use type .Pfx when mono implements it
				dbCommand.Parameters.Add(cert);
				dbCommand.ExecuteNonQuery();*/
				dbt.Commit();
				InitNodeConf(nodeId);

			}
			catch(Exception ex){
				Logger.Append("HUBRN", Severity.ERROR, "ERROR adding new node :"+ex.Message+"---"+ex.StackTrace
					+", query was :"+dbCommand.CommandText);
				try{
				dbt.Rollback();
				}catch{}
			}
			finally{
				dbCommand.Dispose();
				dbConnection.Close();
				dbConnection.Dispose();
			}
			return GetNode(nodeId);
		}

		internal void AddNode(string internalId, string nodeHostName , string nodeIP, X509Certificate2 certificate){
			DbConnection dbConnection = GetConnection();
       			DbCommand dbCommand = dbConnection.CreateCommand();
			IDbTransaction dbt = dbConnection.BeginTransaction();
		
			int nodeId = GetNewNodeId();
			string query = "INSERT INTO Nodes (uid, internalid, name, ip) VALUES ("+nodeId+", '"+internalId+"', '" + nodeHostName + "', '" + nodeIP + "')";
			string query2 = "INSERT INTO nodescertificates (id, nodeid, serial, certificate) VALUES (@id, @nodeid, @serial, @certificate)";
				//""+nodeId+", '"+nodeHostName+"', '"+nodePubKey+"')";
			try{
				dbCommand.CommandText = query;
				dbCommand.ExecuteNonQuery();
				dbCommand.CommandText = query2;
				DbParameter id = dbCommand.CreateParameter();
				id.ParameterName = "@id";
				id.Value = GetNewNodeCertId();
				dbCommand.Parameters.Add(id);
				DbParameter nodeid = dbCommand.CreateParameter();
				nodeid.ParameterName = "@nodeid";
				nodeid.Value = nodeId;
				dbCommand.Parameters.Add(nodeid);
				DbParameter serial = dbCommand.CreateParameter();
				serial.ParameterName = "@serial";
				serial.Value = certificate.SerialNumber;
				dbCommand.Parameters.Add(serial);
				DbParameter cert = dbCommand.CreateParameter();
				cert.DbType = DbType.Binary;
				cert.ParameterName = "@certificate";
				cert.Value = certificate.Export(X509ContentType.Cert);//TODO use type .Pfx when mono implements it
				dbCommand.Parameters.Add(cert);
				dbCommand.ExecuteNonQuery();
				dbt.Commit();
				InitNodeConf(nodeId);
			}
			catch(Exception ex){
				Logger.Append("HUBRN", Severity.ERROR, "ERROR adding new node :"+ex.Message+"---"+ex.StackTrace
					+", query was :"+dbCommand.CommandText);
				dbt.Rollback();
			}
			finally{
				dbCommand.Dispose();
				dbConnection.Close();
				dbConnection.Dispose();
			}	
		}

		private void InitNodeConf(int nodeId){
			DbConnection dbConnection = GetConnection();
       			DbCommand dbCommand = dbConnection.CreateCommand();
			DbTransaction dbt = dbConnection.BeginTransaction();
			Dictionary<string, string> baseTemplateConfig = GetNodeConfigTable(0);
			try{
				foreach (KeyValuePair<string, string> de in baseTemplateConfig){
					string query = "INSERT INTO \"NodesConfig\"(nid, key, value) VALUES ("+nodeId+", '"+de.Key+"', '"+de.Value+"')";
					dbCommand.CommandText = query;
					dbCommand.ExecuteNonQuery();
				}
				dbt.Commit();
			}
			catch(Exception ex){
				Logger.Append("HUBRN", Severity.ERROR, "Error initializing configuration for new node #"+nodeId+" :"+ex.Message+"---"+ex.StackTrace
					+", query was :"+dbCommand.CommandText);
				dbt.Rollback();
			}
			finally{
				dbCommand.Dispose();
				dbConnection.Close();	
				dbConnection.Dispose();
			}
		}
		
		private int GetNewNodeId(){
			string query = "SELECT MAX(uid) FROM nodes";
			int id = -1;
			try{
				id = (int)ExecuteScalar(query);
			}
			catch(Exception ex){
				Logger.Append("HUBRN",Severity.ERROR,ex.Message);
			}
			return id +1;
		}

		private int GetNewNodeCertId(){
			string query = "SELECT MAX(id) FROM nodescertificates";
			int id = -1;
			try{
				id = (int)ExecuteScalar(query);
			}
			catch(Exception ex){
				Logger.Append("HUBRN",Severity.ERROR,ex.Message);
				return -1;
			}
			return id +1;
		}

		internal void DeleteNode(string uN){
			string query = "DELETE FROM Nodes WHERE name = '" + uN + "'" ;
			try{
				ExecuteNonQuery(query);
			}	
			catch(Exception ex){
				Logger.Append("HUBRN", Severity.ERROR, "ERROR deleting node :"+ex.Message+"---"+ex.StackTrace
					+", query was :"+query);
			}
		}


		internal P2PBackup.Common.Node GetNode(string internalId){
			if(internalId == null) return null;
			//string query = "SELECT * FROM nodes WHERE internalid = :Internalid";
			//string query = "SELECT * FROM nodes WHERE internalid ='"+internalId+"'";
			string query = "SELECT uid,internalid,name,kind,ip,storagesize,storagefree,storagegroup,nodegroup,version,os,quota,usedquota, status, lastconnection,hypervisor,storagepriority, "
				+"(SELECT value from \"NodesConfig\" where key='Storage.ListenIp' AND nid=uid) listenIp,"
				+"(SELECT value from \"NodesConfig\" where key='Storage.ListenPort' AND nid=uid) listenPort FROM nodes" 
				+" LEFT JOIN nodescertificates ON uid=nodeid"
				+" WHERE internalid=@internalid";
			P2PBackup.Common.Node u = null;
			using (DbCommand cmd = GetCommand()){

				try{
					cmd.CommandText = query;
					System.Data.Common.DbParameter iid = cmd.CreateParameter();
					//iid.DbType = System.Data.DbType.String;
					iid.ParameterName = "@internalid";
					iid.Value = internalId;
					cmd.Parameters.Add(iid);
					using(DbDataReader reader = cmd.ExecuteReader()){
					//using(DbDataReader reader= QueryAndGetReader(query)){
	       					while(reader.Read())
							u = GetNodeFromReader(reader);
					}
				}
				catch(Exception ex){
					Logger.Append("HUBRN", Severity.ERROR, "Could not retrieve Node from DB : "+ex.Message+" (query was \""+query+"\"");
				}
			}
			return u;
		}
		


		internal P2PBackup.Common.Node NodeApproved(string hostName, string certSerial){
			string query = "SELECT uid,name,ip,storagesize,storagefree,storagegroup,nodegroup,version,os,quota,usedquota,lastconnection,status,hypervisor,kind,internalid,storagepriority, "
				+"(SELECT value from \"NodesConfig\" where key='Storage.ListenIp' AND nid=uid) listenIp,"
				+"(SELECT value from \"NodesConfig\" where key='Storage.ListenPort' AND nid=uid) listenPort FROM nodes, nodescertificates WHERE uid=nodeid"
					+" AND serial= '" + certSerial + "'";
				//+" and userName='"+hostName+"'";
			P2PBackup.Common.Node u = null;
			using (DbDataReader reader = QueryAndGetReader(query)){
				try{
	       				while(reader.Read()) u = (P2PBackup.Common.Node)GetNodeFromReader(reader);
				}
				catch(Exception ex){
					Logger.Append("HUBRN", Severity.ERROR, "Could not check Node approval from DB : "+ex.Message+" (query was \""+query+"\") : "+ex.StackTrace);
				}
				//reader.Close();
			}
			return u;
		}

		internal P2PBackup.Common.Node GetNode(int id){

			string query = "SELECT uid,internalid,name,kind,ip,storagesize,storagefree,storagegroup,nodegroup,version,os,quota,usedquota, status, lastconnection,hypervisor,storagepriority, "
				+"(SELECT value from \"NodesConfig\" where key='Storage.ListenIp' AND nid=uid) listenIp,"
				+"(SELECT value from \"NodesConfig\" where key='Storage.ListenPort' AND nid=uid) listenPort FROM nodes" 
				//+" LEFT JOIN nodescertificates ON uid=nodeid"	
				+" WHERE uid=" + id;	
			P2PBackup.Common.Node u = null;
			using (DbDataReader reader = QueryAndGetReader(query)){
				try{
	       				while(reader.Read()) u = (P2PBackup.Common.Node)GetNodeFromReader(reader);
				}
				catch(Exception ex){
					Logger.Append("HUBRN", Severity.ERROR, "Could not retrieve Node from DB : "+ex.Message+" (query was \""+query+"\") : "+ex.StackTrace);
				}
			}
			//Verify perms
			if(sessionUser != null && sessionUser.GetRoleForGroup(u.Group) < RoleEnum.Admin)
				throw new PermisssionException("user "+sessionUser.Name+" has no perm to add backupset");
			
			return u;
		}
		
		internal List<P2PBackup.Common.Node> GetNodes(){
			string groupPermRestriction = string.Empty;
			//Verify perms
			if(sessionUser != null && !sessionUser.IsSuperAdminOrViewer()){
				groupPermRestriction = " WHERE nodes.nodegroup IN(";
				List<int> allowedGroups = sessionUser.GetGroupsForRole(RoleEnum.Viewer);
				//Console.WriteLine("getNodes : getgroupfrroles: got "+allowedGroups.Count);
				if(allowedGroups == null || allowedGroups.Count == 0)
					return null; // user has no rights to view nodes
				groupPermRestriction += string.Join(",", allowedGroups);
				groupPermRestriction +=")";
				
			}

			string query = "SELECT uid,internalid,name,kind,ip,storagesize,storagefree,storagegroup,nodegroup,version,os,quota,usedquota, status, lastconnection,hypervisor,storagepriority, "
				+"(SELECT value from \"NodesConfig\" where key='Storage.ListenIp' AND nid=uid) listenIp,"
				+"(SELECT value from \"NodesConfig\" where key='Storage.ListenPort' AND nid=uid) listenPort FROM nodes" 
				+" LEFT JOIN nodescertificates ON uid=nodeid";			
			query += groupPermRestriction;
			List<P2PBackup.Common.Node> nodes = new List<P2PBackup.Common.Node>();
			//using(DbConnection dbConnection = GetConnection()){
       			//	using(DbCommand dbCommand = dbConnection.CreateCommand()){
			using (DbDataReader reader = QueryAndGetReader(query)){
				try{
	       				while(reader.Read())
						nodes.Add(GetNodeFromReader(reader));
				}
				catch(Exception ex){
					Logger.Append("HUBRN", Severity.ERROR, "Could not retrieve Clients from DB : "+ex.Message+"----"+ex.StackTrace+" (query was \""+query+"\"");
				}
				reader.Close();
				reader.Dispose();
			}
			//	}
			//}
			return nodes;
		}
		
		internal List<NodeGroup> GetNodeGroups(){
			string groupPermRestriction = string.Empty;
			//Verify perms
			if(sessionUser != null && !sessionUser.IsSuperAdminOrViewer()){
				groupPermRestriction = " WHERE id IN(";
				List<int> allowedGroups = sessionUser.GetGroupsForRole(RoleEnum.Viewer);
				//Console.WriteLine("getNodes : getgroupfrroles: got "+allowedGroups.Count);
				if(allowedGroups == null || allowedGroups.Count == 0)
					return null; // user has no rights to view nodes
				groupPermRestriction += string.Join(",", allowedGroups);
				groupPermRestriction +=")";
				
			}

			string query = "SELECT id, name, description FROM \"NodeGroups\"";	
			query += groupPermRestriction;
			List<NodeGroup> groups = new List<NodeGroup>();
			using (DbDataReader reader = QueryAndGetReader(query)){
				try{
					int i=0;
	       				while(reader.Read()){
						NodeGroup ng = new NodeGroup();
						ng.Id = reader.GetInt32(reader.GetOrdinal("id"));
						ng.Name =  reader[reader.GetOrdinal("name")] as string;
						ng.Description = reader[reader.GetOrdinal("description")] as string;
						groups.Add(ng);
					}
				}
				catch(Exception ex){
					Logger.Append("HUBRN",Severity.ERROR, "Could not retrieve storage nodes from DB : "+ex.Message+"---"+ex.StackTrace+" (query was \""+query+"\"");
				}
				reader.Close();
				reader.Dispose();
			}
			return groups;
		}
		
		internal List<P2PBackup.Common.Node> GetStorageNodes(){
			string groupPermRestriction = string.Empty;
			//Verify perms
			if(sessionUser != null && !sessionUser.IsSuperAdminOrViewer()){
				groupPermRestriction = " AND id IN(";
				List<int> allowedGroups = sessionUser.GetGroupsForRole(RoleEnum.Viewer);
				//Console.WriteLine("getNodes : getgroupfrroles: got "+allowedGroups.Count);
				if(allowedGroups == null || allowedGroups.Count == 0)
					return null; // user has no rights to view nodes
				groupPermRestriction += string.Join(",", allowedGroups);
				groupPermRestriction +=")";
				
			}

			string query = "SELECT n.uid,n.name,n.ip,n.storagesize,n.storagefree,n.storagegroup,n.nodegroup,n.version,n.os,n.quota,n.usedquota,n.lastconnection,n.status,n.kind,n.hypervisor,n.internalid,n.storagepriority, "
				+"(SELECT value from \"NodesConfig\" where key='Storage.ListenIp' AND nid=uid) listenIp,"
				+"(SELECT value from \"NodesConfig\" where key='Storage.ListenPort' AND nid=uid) listenPort FROM nodes n , nodescertificates nc, \"StorageGroups\" sg WHERE n.uid=nc.nodeid"
				+" AND sg.id = n.storagegroup";
			List<P2PBackup.Common.Node> storageNodes = new List<P2PBackup.Common.Node>();
			using (DbDataReader reader = QueryAndGetReader(query)){
				try{
					int i=0;
	       				while(reader.Read()){
						P2PBackup.Common.Node n = GetNodeFromReader(reader);
						if(n != null){ 	storageNodes.Add(n);
						//Console.WriteLine("got "+n.NodeName);
						i++;}
					}
				}
				catch(Exception ex){
					Logger.Append("HUBRN",Severity.ERROR, "Could not retrieve storage nodes from DB : "+ex.Message+"---"+ex.StackTrace+" (query was \""+query+"\"");
				}
				reader.Close();
				reader.Dispose();
			}
			return storageNodes;
		}

		internal List<NodeCertificate>GetCertificates(){
			List<NodeCertificate> nodesCerts = new List<NodeCertificate>();
			string query = "SELECT id, nodeid, certificate FROM nodescertificates";
			using (DbDataReader reader = QueryAndGetReader(query)){
				try{
	       				while(reader.Read()){
						X509Certificate2 nc = (X509Certificate2)new X509Certificate2((byte[])reader["certificate"]);
						NodeCertificate nCert = new NodeCertificate(nc);
						nCert.Id = (int)reader["id"];
						nCert.NodeId = (int)reader["nodeid"];
						nodesCerts.Add(nCert);
						//Console.WriteLine("GetCertificates: id="+nCert.Id+", subj="+nCert.CN+", validfrom);
					}
				}
				catch(Exception ex){
					Logger.Append("HUBRN",Severity.ERROR, "Could not retrieve certificates from DB : "+ex.Message+"---"+ex.StackTrace+" (query was \""+query+"\"");
				}
				reader.Close();
				reader.Dispose();
			}
			return nodesCerts;

		}
		
		internal List<P2PBackup.Common.StorageGroup> GetStorageGroups(){
			string query = "SELECT id, name, priority, processingflags FROM \"StorageGroups\" ORDER BY id";			
			List<P2PBackup.Common.StorageGroup> sgs = new List<P2PBackup.Common.StorageGroup>();
			using (DbDataReader reader = QueryAndGetReader(query)){
				try	{
	       				while(reader.Read()) {
						StorageGroup  sg = null;
						int id = (Int32)reader[0];
						string name =  reader.GetString(1);
						int priority = (Int32)reader[2];

						sg = new StorageGroup(id, name, priority);
						sg.ProcessingFlags = DataProcessingFlags.None;
						if(!reader.IsDBNull(reader.GetOrdinal("processingflags")))
							sg.ProcessingFlags = (DataProcessingFlags)(int)reader["processingflags"];
						sgs.Add(sg);
					}
				}
				catch(Exception ex){
					Logger.Append("HUBRN", Severity.ERROR, "Could not retrieve storage groups from DB : "+ex.Message+" (query was \""+query+"\"");
				}
				reader.Close();
				reader.Dispose();
			}
			return sgs;
		}


		private P2PBackup.Common.Node GetNodeFromReader(DbDataReader reader){
			int uid = reader.GetInt32(reader.GetOrdinal("uid"));
			string name =  reader[reader.GetOrdinal("name")] as string;
			string ip = reader[reader.GetOrdinal("ip")] as string;
			//Int32 port = reader.IsDBNull(reader.GetOrdinal("Storage.ListenPort"))? 0 : reader.GetInt32(reader.GetOrdinal("port")) ;
			//string key = reader[reader.GetOrdinal("pubkey")] as string;
			long share = (long)(reader.IsDBNull(reader.GetOrdinal("storagesize"))? 0 : reader.GetInt64(reader.GetOrdinal("storagesize")) );
			//long access = reader.IsDBNull(reader.GetOrdinal("accessible"))? 0 : reader.GetInt64(reader.GetOrdinal("accessible")) ;
			long avail = reader.IsDBNull(reader.GetOrdinal("storagefree"))? 0 : reader.GetInt64(reader.GetOrdinal("storagefree")) ;
			int sg = reader.IsDBNull(reader.GetOrdinal("storagegroup"))? 0 : reader.GetInt32(reader.GetOrdinal("storagegroup")) ;

			int groupId = reader.IsDBNull(reader.GetOrdinal("nodegroup"))? 0 : reader.GetInt32(reader.GetOrdinal("nodegroup")) ;
			DateTime lastconnection = DateTime.MinValue;
			DateTime.TryParse(reader[reader.GetOrdinal("lastconnection")] as string, out lastconnection);
			/*bool locked;
			if(reader.GetString(reader.GetOrdinal("locked")).ToLower() == "true")
				locked = true;
			else
				locked = false;*/
			NodeStatus status = (NodeStatus) (reader.IsDBNull(reader.GetOrdinal("status"))? 0 : reader.GetInt32(reader.GetOrdinal("status")) );
			string version = reader[reader.GetOrdinal("version")] as string;
			string os = reader[reader.GetOrdinal("os")] as string;
			long quota = reader[reader.GetOrdinal("quota")] as long ? ?? default(long);
			//long usedQuota = reader[reader.GetOrdinal("usedquota")] as long ? ?? default(long);
			long usedQuota = GetNodeUsedQuota(uid);
			//Console.WriteLine("dbhandle.getnodefromreader : available="+avail+", osg="+reader[reader.GetOrdinal("storagefree")]);
			int backupsets = GetNbBackupSets(uid);
			string listenIP = reader[reader.GetOrdinal("listenIp")] as string;
			int listenPort = 0;
			int.TryParse(reader.IsDBNull(reader.GetOrdinal("listenPort"))? "0" : reader.GetString(reader.GetOrdinal("listenPort")), out listenPort);
			//reader.Close();
			P2PBackup.Common.Node n = new P2PBackup.Common.Node(name, ip, listenIP, listenPort, share, /*access,*/ avail, status, uid, sg, groupId, version, os, quota, usedQuota, backupsets, lastconnection);
			n.InternalId = reader[reader.GetOrdinal("internalid")] as string;
			n.Kind = (KindEnum) (reader.IsDBNull(reader.GetOrdinal("kind"))? 0 : reader.GetInt32(reader.GetOrdinal("kind")) );
			n.Hypervisor = (reader.IsDBNull(reader.GetOrdinal("hypervisor"))? 0 : reader.GetInt32(reader.GetOrdinal("hypervisor")) );
			n.StoragePriority = (reader.IsDBNull(reader.GetOrdinal("storagepriority"))? 0 :reader.GetInt32(reader.GetOrdinal("storagepriority")) );
			return n;
		}

		private long GetNodeUsedQuota(int nodeId){
			long usedSpace = 0;
			string query = "SELECT (sum(finalsize))"
				+" FROM backups, \"TaskSets\" "
				+" WHERE backups.bsid = \"TaskSets\".id"
				+" AND clientid="+nodeId
				+" GROUP BY \"TaskSets\".clientid";
			try{
				usedSpace = Convert.ToInt64(ExecuteScalar(query));
       				
			}
			catch(Exception ex){
				Logger.Append("HUBRN", Severity.ERROR, "Could not retrieve node storage usage : "+ex.Message+" (query was \""+query+"\"");
			}
			return usedSpace;
		}


	}
}
