using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using P2PBackupHub.Utilities;
using P2PBackup.Common;
using P2PBackupHub.Notifiers;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;

namespace P2PBackupHub {

	internal partial class DBHandle {

		DbProviderFactory dbpf;
		DbConnectionStringBuilder dbcs;
		User sessionUser = null;
		/// <summary>
		/// Constructor for internal operations (no authentication)
		/// </summary>
		public DBHandle(){
			try{
				dbpf = DbProviderFactories.GetFactory(ConfigurationManager.AppSettings["Storage.DBHandle.Provider"]);
				dbcs = dbpf.CreateConnectionStringBuilder();
				dbcs.ConnectionString = ConfigurationManager.AppSettings["Storage.DBHandle."+ConfigurationManager.AppSettings["Storage.DBHandle.Provider"]];
			}
			catch(Exception e){
				Console.WriteLine("FATAL : Unable to instanciate Storage DB provider '"+ConfigurationManager.AppSettings["Storage.DBHandle.Provider"]
					+"'. Check the parameter 'Storage.DBHandle' inside the configuration file, and make sure the provider is installed."+Environment.NewLine);
				
				// Let's give the user some useful hints
				DataTable providersTable =  DbProviderFactories.GetFactoryClasses();
				string providers = String.Empty;
				Console.WriteLine("Currently registered providers are : "+providers+Environment.NewLine);
				foreach(DataRow dr in providersTable.Rows){
					Console.WriteLine("* "+dr[2]+"\t("+dr[3]+")");
					providers += dr[2]+", ";
				}
				Logger.Append("HUBRN", Severity.CRITICAL, "Unable to instanciate Storage DB provider '"
					+ConfigurationManager.AppSettings["Storage.DBHandle.Provider"]+"'. Available providers are "+providers+".");
				throw(e);
			}
		}
		
		/// <summary>
		/// Initializes a new instance of the <see cref="P2PBackupHub.DBHandle"/> class.
		/// Every method call of the created instance will check user permissions while retrieving/touching data
		/// </summary>
		/// <param name='u'>
		/// U. The logged user wishing to use the dbhandle instance
		/// </param>
		public DBHandle(User u){
			if(u != null) sessionUser = u;
			else throw new Exception("Attempted to call DB handle with a null user (!?)");

			try{
				dbpf = DbProviderFactories.GetFactory(ConfigurationManager.AppSettings["Storage.DBHandle.Provider"]);
				dbcs = dbpf.CreateConnectionStringBuilder();
				dbcs.ConnectionString = ConfigurationManager.AppSettings["Storage.DBHandle."+ConfigurationManager.AppSettings["Storage.DBHandle.Provider"]];
			}
			catch(Exception e){
				Console.WriteLine("FATAL : Unable to instanciate Storage DB provider '"+ConfigurationManager.AppSettings["Storage.DBHandle.Provider"]
					+"'. Check the parameter 'Storage.DBHandle' inside the configuration file, and make sure the provider is installed."+Environment.NewLine);
				
				// Let's give the user some useful hints
				DataTable providersTable =  DbProviderFactories.GetFactoryClasses();
				string providers = String.Empty;
				Console.WriteLine("Currently registered providers are : "+providers+Environment.NewLine);
				foreach(DataRow dr in providersTable.Rows){
					Console.WriteLine("* "+dr[2]+"\t("+dr[3]+")");
					providers += dr[2]+", ";
				}
				Logger.Append("HUBRN", Severity.CRITICAL, "Unable to instanciate Storage DB provider '"
					+ConfigurationManager.AppSettings["Storage.DBHandle.Provider"]+"'. Available providers are "+providers+".");
				throw(e);
			}
		}
		
		private DbConnection GetConnection(){
			System.Data.Common.DbConnection dbConnection = dbpf.CreateConnection();
			dbConnection.ConnectionString = dbcs.ConnectionString;
       			dbConnection.Open();
       			return dbConnection;
		}

		private DbCommand GetCommand(){
			return GetConnection().CreateCommand();

		}

		private DbDataReader QueryAndGetReader(string query){
			//using (DbConnection dbConnection = GetConnection()){
			try{
			DbConnection dbConnection = GetConnection();
			      // using(DbCommand dbCommand = dbConnection.CreateCommand()){
				DbCommand dbCommand = dbConnection.CreateCommand();
				dbCommand.CommandText = query;
		       		return dbCommand.ExecuteReader(CommandBehavior.CloseConnection);
			}
			catch(Exception e){
				Logger.Append("HUBRN", Severity.ERROR, "Error executing query '"+query+"' : "+e.Message);	
			}
			return null;
		}

		private DbDataReader QueryAndGetReader(string query, List<IDbDataParameter> paramz){
			//using (DbConnection dbConnection = GetConnection()){
			try{
			DbConnection dbConnection = GetConnection();
			      // using(DbCommand dbCommand = dbConnection.CreateCommand()){
				DbCommand dbCommand = dbConnection.CreateCommand();
				dbCommand.CommandText = query;
				foreach(IDbDataParameter param in paramz)
					dbCommand.Parameters.Add(param);

		       		return dbCommand.ExecuteReader(CommandBehavior.CloseConnection);
			}
			catch(Exception e){
				Logger.Append("HUBRN", Severity.ERROR, "Error executing query '"+query+"' : "+e.Message);	
			}
			return null;
		}

		private int ExecuteNonQuery(string query){
			int rowsAffected = 0;
			try{
				using(DbConnection dbConnection = GetConnection()){
	       				using(DbCommand dbCommand = dbConnection.CreateCommand()){
						dbCommand.CommandText = query;
						rowsAffected = dbCommand.ExecuteNonQuery();
					}
				}
			}
			catch(Exception e){
				Logger.Append("HUBRN", Severity.ERROR, "Error executing query '"+query+"' : "+e.Message);	
			}
			return rowsAffected;
		}
		
		private object ExecuteScalar(string query){
			try{
				using(DbConnection dbConnection = GetConnection()){
	       				using(DbCommand dbCommand = dbConnection.CreateCommand()){
						dbCommand.CommandText = query;
						//dbCommand.ExecuteNonQuery();
						return dbCommand.ExecuteScalar();
					}
				}
			}
			catch(Exception e){
				Logger.Append("HUBRN", Severity.ERROR, "Error executing query '"+query+"' : "+e.Message);	
			}
			return null;
		}

		private int GetNewId(string table){
			string query = "SELECT MAX(id) FROM "+table;
			int id = 0;
			try{
				id = (int)ExecuteScalar(query);
			}
			catch(Exception ex){
				Logger.Append("HUBRN",Severity.ERROR,ex.Message);
			}
			return id +1;
		}
	}
}

