using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using P2PBackupHub.Utilities;
using P2PBackup.Common;
using P2PBackupHub.Notifiers;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;

namespace P2PBackupHub{
	/// <summary>
	/// Handles all communication with the database
	/// TODO : split this to regular DAL components, 1 per class
	/// </summary>
	internal partial class DBHandle{
		

		// needs SuperAdmin perms - handled using CAS
		/*internal void AddNode(string nodeHostName , string nodeIP, string nodePubKey){
			DbConnection dbConnection = GetConnection();
       			DbCommand dbCommand = dbConnection.CreateCommand();
			IDbTransaction dbt = dbConnection.BeginTransaction();
		
			int nodeId = GetNewNodeId();
			string query = "INSERT INTO Nodes (uid, userName, ip, locked) VALUES ("+nodeId+", '" + nodeHostName + "', '" + nodeIP + "', 'True')";
			string query2 = "INSERT INTO nodescertificates (nodeid, hostname, pubkey ) VALUES("+nodeId+", '"+nodeHostName+"', '"+nodePubKey+"')";
			try{
				dbCommand.CommandText = query;
				dbCommand.ExecuteNonQuery();
				dbCommand.CommandText = query2;
				dbCommand.ExecuteNonQuery();
				dbt.Commit();
				InitNodeConf(nodeId);
			}
			catch(Exception ex){
				Logger.Append("HUBRN", Severity.ERROR, "ERROR adding new node :"+ex.Message+"---"+ex.StackTrace
					+", query was :"+dbCommand.CommandText);
				dbt.Rollback();
			}
			finally{
				dbCommand.Dispose();
				dbConnection.Close();
				dbConnection.Dispose();
			}	
		}*/


	


		/// <summary>
		/// Adds the users IP-address to the database
		/// </summary>
		/// <param name="uName">the user to change</param>
		/// <param name="address">the "new" ip-address</param>
		/*public void SaveIP(int nodeId, string address){
			string query = "UPDATE Nodes SET ip = '" + address + "' WHERE uid = " + nodeId + "";
			try{
				ExecuteNonQuery(query);
			}
			catch(Exception ex){
				Logger.Append("HUBRN", Severity.ERROR, "Could not save client  version to DB : "+ex.Message+" -- "+ex.StackTrace+" (query was \""+query+"\"");
			}
		}*/
		
		public bool UpdateInfo(ClientNode n){
			DbConnection dbc = GetConnection();
			DbCommand dbCommand = dbc.CreateCommand();
			string query = String.Empty;
			if(n.Uid <1) // security attack??
				return false;
			try{
				query = "UPDATE nodes SET version = @version, os= @os, ip= @ip, lastconnection= @lastconn WHERE uid="+n.Uid;
				Logger.Append("HUBRN",Severity.DEBUG2, "trying to update node version and OS, query "+query);
				dbCommand.CommandText = query;
				IDbDataParameter pVer = dbCommand.CreateParameter();
				pVer.ParameterName = "@version";
				pVer.Value = n.Version;
				dbCommand.Parameters.Add(pVer);
				IDbDataParameter pOS = dbCommand.CreateParameter();
				pOS.ParameterName = "@os";
				pOS.Value = n.OS;
				dbCommand.Parameters.Add(pOS);
				IDbDataParameter pIp = dbCommand.CreateParameter();
				pIp.ParameterName = "@ip";
				pIp.Value = n.IP;
				dbCommand.Parameters.Add(pIp);
				IDbDataParameter lastconn = dbCommand.CreateParameter();
				lastconn.ParameterName = "@lastconn";
				lastconn.Value = n.LastConnection;
				dbCommand.Parameters.Add(lastconn);
				dbCommand.ExecuteNonQuery();
				
			}
			catch(Exception ex){
				Logger.Append("HUBRN", Severity.ERROR, ex.Message+"---"+ex.StackTrace+" (query was "+query+")");
			}
			finally{
				dbCommand.Dispose();
				dbc.Close();
				dbc.Dispose();
			}
			return true;
			
		}
		
		/// <summary>
		/// Saves the user's share
		/// </summary>
		/// <param name="uN">username</param>
		/// <param name="s">size of the share</param>
		/// <param name="av">new available size</param>
		/// <param name="ac">new accessible space</param>
		public void SaveShare(string uN, long s, long av, long ac)	{
			string query = "UPDATE Nodes SET storagesize = " + s + ", storagefree = " + av + " WHERE Nodes.name = '" + uN + "'";
			try{
				ExecuteNonQuery(query);
			}
			catch(Exception ex){
				Logger.Append("HUBRN", Severity.ERROR, ex.Message+"---"+ex.StackTrace+" (query was "+query+")");
			}
		}
		
		internal void UpdateStorageSpace(ClientNode node){
			string query = "UPDATE nodes SET storagesize="+node.StorageSize+", storagefree ="+node.Available
				+" WHERE uid="+node.Uid;
			try{
				ExecuteNonQuery(query);
			}
			catch(Exception ex){
				Logger.Append("HUBRN", Severity.ERROR, ex.Message+"---"+ex.StackTrace+" (query was "+query+")");
			}
		}
		
		internal void UpdateStorageHistory(ClientNode node, long taskId){
			string query = "INSERT INTO statstorage (utimestamp, taskid, nodeid, storagespace, storagefree)"
				+"VALUES("+Utilities.Utils.GetUnixTimeFromDateTime(DateTime.Now)+","+taskId+","+node.Uid
				+","+node.StorageSize+","+node.Available+")";
			try{
				ExecuteNonQuery(query);
			}
			catch(Exception ex){
				Logger.Append("HUBRN", Severity.ERROR, ex.Message+"---"+ex.StackTrace+" (query was "+query+")");
			}
		}
		
		public bool SaveNodeConfig(int node, Hashtable configTable){
			//as no check has been done before, we only allow keys registered by the "template" node (uid=0) as a pimary safety check
			// TODO : stronger validation (escape/use parameterized query, check if paths exists, check share size < disk size, check listenip belongs to node ...)
			Dictionary<string, string> templateVerifyTable = GetNodeConfigTable(0);
			if(node <1) // security attack??
				return false;
			DbConnection dbc = GetConnection();
			string query = String.Empty;
			
			try{
				foreach(string key in configTable.Keys){
					if(templateVerifyTable.ContainsKey(key)){
						query = "UPDATE \"NodesConfig\" SET value = @value WHERE key='"+key+"' AND nid="+node;
						Logger.Append("HUBRN", Severity.DEBUG2, "trying to save node config, query "+query);
						DbCommand dbCommand = dbc.CreateCommand();
						dbCommand.CommandText = query;
						IDbDataParameter p = dbCommand.CreateParameter();
						p.ParameterName = "@value";
						p.Value = (string)configTable[key];
						dbCommand.Parameters.Add(p);
						dbCommand.ExecuteNonQuery();
						dbCommand.Dispose();
					}
				}
				return true;
			}
			catch(Exception ex){
				Logger.Append("HUBRN", Severity.ERROR, ex.Message+"---"+ex.StackTrace+" (query was "+query+")");
			}
			finally{
				dbc.Close();	
				dbc.Dispose();
			}
			return false;
		}
		
		internal void UpdateNodeGeneralConf(int nodeId, long? storageSize, long? quota, int? storageGroup, int? nodeGroup){
			string query = "UPDATE nodes SET ";
			if(storageSize.HasValue)
				query += "storagesize="+storageSize+",";
			if(quota.HasValue)
				query += "quota="+quota+",";
			if(storageGroup.HasValue)
				query += "storagegroup="+storageGroup+",";
			if(nodeGroup.HasValue)
				query += "nodegroup="+nodeGroup+",";
			query += "uid="+nodeId+" WHERE uid="+nodeId;
			
			try{
				ExecuteNonQuery(query);
			}
			catch(Exception ex){
				Logger.Append("HUBRN", Severity.ERROR, "Could not save configuration to DB : "+ex.Message+" -- "+ex.StackTrace+" (query was \""+query+"\"");
			}
			
		}
			
			
		/// <summary>
		/// Saves the user's public key
		/// </summary>
		/// <param name="uN">username</param>
		/// <param name="key">public key</param>
		/*public void SaveKey(string uN, string key){
			string query = "UPDATE Nodes SET pubkey = '" + key + "' WHERE userName = '" + uN + "'";
			DbConnection dbConnection = GetConnection();
       			DbCommand dbCommand = dbConnection.CreateCommand();
			dbCommand.CommandText = query;
			try	{
				dbCommand.ExecuteNonQuery();
			}
			catch(Exception ex){
				MessageBox.Show(ex.Message, "Hub.DBHandle.SaveKey");
			}
			finally{
				dbCommand.Dispose();
				dbConnection.Close();
				dbConnection.Dispose();
			}
		}*/

		

		/// <summary>
		/// Updates the user's available space
		/// </summary>
		/// <param name="uN">username</param>
		/// <param name="avail">new available space, space left in the user's own share</param>
		public void ChangeAvailabless(string uN, long avail){
			string query = "UPDATE Nodes SET storagefree = " + avail + " WHERE name = '" + uN + "'";
			try{
				ExecuteNonQuery(query);
			}
			catch(Exception ex){
				Logger.Append("HUBRN", Severity.ERROR, ex.Message+"---"+ex.StackTrace+" (query was "+query+")");
			}
		}
		
		/*[Obsolete]
		public void AddBackupTracking(int bsId, BackupType backupType){
			string query = "INSERT INTO backups(id, bsid, backuptype, datebegin) VALUES("
				+GetNewBackupTrackingId()+", "+bsId+", '"+backupType.ToString()+"', '"+Utils.GetUnixTimeFromDateTime(DateTime.Now)+"')";
			try{
				ExecuteNonQuery(query);
			}	
			catch(Exception ex){
				Logger.Append("HUBRN", Severity.ERROR, ex.Message+"---"+ex.StackTrace+" (query was "+query+")");
			}
		}*/
		

		
		public List<int> GetIndexSources(string indexName){
			List<int> sources = new List<int>();
			string query = "SELECT destination FROM backups b, backuptodest bd WHERE b.bsid = bd.bsid AND indexname='"+indexName+"'";
			using (DbDataReader reader = QueryAndGetReader(query)){
				try{
	       				while(reader.Read()) {
						sources.Add(Int32.Parse(reader.GetString(0)));
					}
				}
				catch(Exception ex){
					Logger.Append("HUBRN", Severity.ERROR, "Could get storage sources for index "+indexName+" :"+ex.Message);
				}
				
			}
			return sources;
		}
		
		/// <summary>
		/// marks a file as back upped and transferred
		/// </summary>
		/// <param name="id">the owner's userid</param>
		/// <param name="fName">filename</param>
		/// <param name="destName">destination's username</param>
		
		/// <summary>
		/// Gets the userid for this client
		/// </summary>
		/// <param name="destName">the user's username</param>
		/// <returns>the user's id</returns>
		private int GetUserId (string destName){
			string query = "SELECT uid FROM nodes WHERE username = '" + destName + "'";
			int uid = 0;
			using (DbDataReader reader = QueryAndGetReader(query)){
				try{
	       				while(reader.Read()) 
						uid = Int32.Parse(reader.GetString(0));				
				}
				catch(Exception ex){
					Logger.Append("HUBRN", Severity.ERROR, "Could not retrieve id for node"+destName+" : "+ex.Message+" (query was : "+query+")");
				}
			}
			return uid;
			
		}

		public string GetNodeConfig(int nodeId){
			string sb = String.Empty;
			string query = "SELECT key, value, keytype FROM \"NodesConfig\" WHERE nid="+nodeId;
			using (DbDataReader reader = QueryAndGetReader(query)){
				try{
					while(reader.Read())
						sb += (reader.GetString(reader.GetOrdinal("key"))+"="+reader.GetString(reader.GetOrdinal("value"))+"|");
					sb.Remove(sb.Length -2, 1);
				}
				catch(Exception ex){
					Logger.Append("HUBRN", Severity.ERROR, ex.Message+"---"+ex.StackTrace+" (query was "+query+")");
				}
				reader.Close();
				reader.Dispose();
			}	
			return sb;
		}
		
		public Dictionary<string, string> GetNodeConfigTable(int nodeId){
			Dictionary<string, string> config = new Dictionary<string, string>();
			string query = "SELECT key, value, keytype FROM \"NodesConfig\" WHERE nid="+nodeId;
			using (DbDataReader reader = QueryAndGetReader(query)){
				try{
					while(reader.Read())
						config.Add(reader.GetString(reader.GetOrdinal("key")), reader.GetString(reader.GetOrdinal("value")));
				}
				catch(Exception ex){
					Logger.Append("HUBRN", Severity.ERROR, ex.Message+"---"+ex.StackTrace+" (query was "+query+")");
				}
				reader.Close();
				reader.Dispose();
			}	
			return config;
		}
		
		
		/// <summary>
		/// Deletes a file
		/// </summary>
		/// <param name="uid">the owner's user id</param>
		/// <param name="filename">filename</param>
		/// <param name="duid">destination's user id</param>
		public void DeleteFile(int uid, string filename, int duid){
			string query = "DELETE FROM Files WHERE uid = " + uid + " AND filename = '" + filename + "' AND destination = " + duid + "" ;
			try{
				ExecuteNonQuery(query);
			}	
			catch(Exception ex){
				Logger.Append("HUBRN", Severity.ERROR, "Could not delete entry from db : "+ex.Message+"---"+ex.StackTrace+" (query was : "+query+")");
			}	
		}
		
		internal void ApproveNode(int nodeId, bool lockStatus){
			string version = System.Environment.Version.ToString();
			string query = "UPDATE nodes SET locked='"+lockStatus.ToString()+"' WHERE uid="+nodeId;
			try{
				ExecuteNonQuery(query);
				Logger.Append("HUBRN", Severity.DEBUG, "updated node #"+nodeId+" approval status to "+lockStatus);
			}	
			catch(Exception ex){
				Logger.Append("HUBRN", Severity.ERROR, "Could not save runtime version to sysinfo :"+ex.Message);
			}
		}
		
		internal void SaveRuntimeVersion(){
			string version = System.Environment.Version.ToString();
			string query = "UPDATE systeminfo SET value='"+version+"' WHERE systemkey='runtimeversion'";
			try{
				ExecuteNonQuery(query);
			}	
			catch(Exception ex){
				Logger.Append("HUBRN", Severity.ERROR, "Could not save runtime version to sysinfo :"+ex.Message);
			}
		}
		
		internal string GetRuntimeVersion(){
			string query = "SELECT value FROM \"systeminfo\" WHERE systemkey='runtimeversion'";	
			try{
				return (string)ExecuteScalar(query);
			}	
			catch(Exception ex){
				Logger.Append("HUBRN", Severity.ERROR, "Could not get runtime version from sysinfo :"+ex.Message);
				return null;
			}
		}
		

		


		
		internal List<User> GetUsers(){

			string query = "SELECT * from \"users\"";
			List<User> users = new List<User>();
			if(sessionUser != null	&& !sessionUser.IsSuperAdmin())
				return users;
			using (DbDataReader reader = QueryAndGetReader(query)){
				try{
					while(reader.Read()){
						User tempU = GetUserFromReader(reader);
						tempU.Roles = GetUserRoles(tempU.Id);
						users.Add(tempU);
					}
				}
				catch(Exception ex){
					Logger.Append("HUBRN", Severity.ERROR,ex.Message+"---"+ex.StackTrace+" (query was "+query+")");
				}
				reader.Close();
				reader.Dispose();
			}
			return users;
		}
		
		internal User GetUser(int userId){
			string query = "SELECT * FROM users WHERE id="+userId;
			User user = new User(userId);
			using (DbDataReader reader = QueryAndGetReader(query)){
				try{
					while(reader.Read()){
						user = GetUserFromReader(reader);
						user.Roles = GetUserRoles(userId);
					}
				}
				catch(Exception ex){
					Logger.Append("HUBRN", Severity.ERROR,ex.Message+"---"+ex.StackTrace+" (query was "+query+")");
				}
				reader.Close();
				reader.Dispose();
			}
			return user;
		}
		
		private User GetUserFromReader(DbDataReader reader){
			
			User user = new User(reader.GetInt32(reader.GetOrdinal("id")));
			user.Name = reader.GetString(reader.GetOrdinal("name"));
			user.Password = PasswordManager.Get(reader.GetInt32(reader.GetOrdinal("password")));
			user.Email = reader.GetString(reader.GetOrdinal("email"));
			user.IsEnabled = bool.Parse(reader.GetString(reader.GetOrdinal("isactive")));
			long lld = reader.IsDBNull(reader.GetOrdinal("lastlogindate"))? 0 : reader.GetInt32(reader.GetOrdinal("lastlogindate")); // BUGGY should be int64
			user.LastLoginDate = Utils.GetDateTimeFromUnixTime(lld);
			user.SetCulture(reader.GetString(reader.GetOrdinal("culture")));
			return user;
		}

		private List<UserRole> GetUserRoles(int userId){
			
			List<UserRole> uRoles = new List<UserRole>();
			try{
				string query = "SELECT DISTINCT(role) FROM userrole WHERE id="+userId;
				using (DbDataReader reader = QueryAndGetReader(query)){
					while(reader.Read()){
						string role = reader.GetString(0);
						string ngQuery = "SELECT nodegroupid FROM userrole WHERE id=@id AND role=@role";
						using(DbConnection dbc = GetConnection()){
							using(DbCommand dbCommand = dbc.CreateCommand()){
								dbCommand.CommandText = ngQuery;
								IDbDataParameter pid = dbCommand.CreateParameter();
								pid.ParameterName = "@id";
								pid.Value = userId;
								dbCommand.Parameters.Add(pid);
								IDbDataParameter pr = dbCommand.CreateParameter();
								pr.ParameterName = "@role";
								pr.Value = role;
								dbCommand.Parameters.Add(pr);
								UserRole ur = new UserRole();
								RoleEnum re = RoleEnum.None;
								Enum.TryParse(role, true, out re);
								ur.Role = re;
								using(DbDataReader rreader = dbCommand.ExecuteReader()){
									while(rreader.Read()){
										ur.GroupsInRole.Add(rreader.GetInt32(0));
										Console.WriteLine("GetUserRoles() : user "+userId+" has role '"+role+"' ("+ur.Role+") for group '"+rreader.GetInt32(0)+"'");
									}
								}
								uRoles.Add(ur);
							}

						}
					}
				}
			}
			catch(Exception e){
				Logger.Append("HUBRN", Severity.ERROR, "Unable to retrieve user roles for uid "+userId+": "
					+e.Message+"---"+e.StackTrace);
			}
			return uRoles;

		}

		internal User AuthenticateUser(string login, string password){
			string query = "SELECT * from \"users\" where name= @userName";
			DbConnection dbc = GetConnection();
			DbCommand dbCommand = dbc.CreateCommand();
			if(login == null || password == null || login == String.Empty || password == String.Empty)
				return null; // Don't even care to ask db when null or blank values provided
			User user;
			try{
				
				dbCommand.CommandText = query;
				IDbDataParameter pUserName = dbCommand.CreateParameter();
				pUserName.ParameterName = "@userName";
				pUserName.Value = login;
				dbCommand.Parameters.Add(pUserName);
				/*IDbDataParameter pOS = dbCommand.CreateParameter();
				pOS.ParameterName = "@password";
				pOS.Value = password;
				dbCommand.Parameters.Add(pOS);*/
				using(DbDataReader reader = dbCommand.ExecuteReader(CommandBehavior.SingleRow)){
					if(reader == null || !reader.HasRows) return null; // carefully check if there is no data, in order to signal user or password is invalid
					reader.Read();
					user = new User(reader.GetInt32(reader.GetOrdinal("id")));
					user.Name = reader.GetString(reader.GetOrdinal("name"));
					user.Password = PasswordManager.Get(reader.GetInt32(reader.GetOrdinal("password")));
					// if passwords don't match, reject
					if(user.Password.Id == 0 || user.Password.Value != password)
						return null;
					user.Email = reader.GetString(reader.GetOrdinal("email"));
					user.IsEnabled = bool.Parse(reader.GetString(reader.GetOrdinal("isactive")));
					if(!user.IsEnabled) user = null; // reject disabled account

					user.LastLoginDate = DateTime.Now;
					user.SetCulture(reader.GetString(reader.GetOrdinal("culture")));
					//Console.WriteLine("culture raw="+reader.GetString(reader.GetOrdinal("culture"))
					//   +", name="+user.Culture.TwoLetterISOLanguageName);
				}
				user.Roles = GetUserRoles(user.Id);
				UpdateUserLastLoginDate(user);
				Logger.Append("HUBRN", Severity.INFO, "User "+user.Name+" has logged in.");
				return user;
			}
			catch(Exception ex){
				Logger.Append("HUBRN", Severity.ERROR, ex.Message+"---"+ex.StackTrace+" (query was "+query+")");
			}
			finally{
				dbCommand.Dispose();
				dbc.Close();
				dbc.Dispose();
			}
			return null;
		}
		
		public void UpdateUserLastLoginDate(User u){
			string query = "UPDATE users SET lastlogindate="+Utils.GetUnixTimeFromDateTime(DateTime.Now)+" WHERE id=" + u.Id;
			try{
				ExecuteNonQuery(query);
			}
			catch(Exception ex){
				Logger.Append("HUBRN", Severity.ERROR, "Could not save client  version to DB : "+ex.Message+" -- "+ex.StackTrace+" (query was \""+query+"\"");
			}
		}
		
		public MailParameters GetMailConfiguration(int bsId, TaskRunningStatus status){
			string query = "SELECT * from emailnotifier WHERE bsid="+bsId+" AND status='"+status+"'";
			MailParameters mailParams = new MailParameters();
			using (DbDataReader reader = QueryAndGetReader(query)){
				try{
					while(reader.Read()){
						mailParams.From = "shb@example.com";// hardcoded for now reader.GetString(reader.GetOrdinal("name"));
						if(!reader.IsDBNull(reader.GetOrdinal("userid")))
							mailParams.To = GetUser(reader.GetInt32(reader.GetOrdinal("userid"))).Email;
						else
							mailParams.To = reader.GetString(reader.GetOrdinal("recipients"));
						Tuple<string, string> subjectAndBody = GetMailTemplate(reader.GetInt32(reader.GetOrdinal("msgtplid")));
						mailParams.Subject = subjectAndBody.Item1;
						mailParams.Body = subjectAndBody.Item2;
					}
				}
				catch(Exception ex){
					Logger.Append("HUBRN", Severity.ERROR,ex.Message+"---"+ex.StackTrace+" (query was "+query+")");
				}
				reader.Close();
				reader.Dispose();
			}
			return mailParams;
		}
		
		public Tuple<string, string> GetMailTemplate(int id){
			string query = "SELECT subject, body FROM msgtemplates WHERE id="+id;
			Tuple<string, string> subjectAndBody = new Tuple<string, string>("","");
			using (DbDataReader reader = QueryAndGetReader(query)){
				try{
					while(reader.Read()){
						
						subjectAndBody = new Tuple<string, string>(reader.GetString(0), reader.GetString(1));
					}
				}
				catch(Exception ex){
					Logger.Append("HUBRN", Severity.ERROR,ex.Message+"---"+ex.StackTrace+" (query was "+query+")");
				}
				reader.Close();
				reader.Dispose();
			}
			return subjectAndBody;
		}
		

	}
}
