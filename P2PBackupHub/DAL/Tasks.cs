using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using P2PBackupHub.Utilities;
using P2PBackup.Common;
using P2PBackupHub.Notifiers;
using System.Linq;
//using System.Security.Cryptography;
//using System.Security.Cryptography.X509Certificates;

namespace P2PBackupHub{

	internal partial class DBHandle{

		internal string AddBackupSet(BackupSet bs){
			//Verify perms
			if(sessionUser != null && sessionUser.GetRoleForGroup(GetNode(bs.NodeId).Group) < RoleEnum.Admin)
				throw new PermisssionException("user "+sessionUser.Name+" has no perm to add backupset");
			
			DbConnection dbConnection = GetConnection();
       			DbCommand dbCommand = dbConnection.CreateCommand();
			DbTransaction dbt = dbConnection.BeginTransaction();
			try{
				dbCommand.Connection = dbConnection;
				int bsId = GetNewBackupSetId();
				if(bsId == 0)
					return "ERROR - -- -  -- - - - ";
				// insert backuptimes
				string query = "INSERT INTO backuptime (bsid,";
				foreach(ScheduleTime bTime in bs.ScheduleTimes)
					query += bTime.Day.ToString().ToLower().Substring(0,3)+"begin,";
				query = query.Remove(query.Length - 1);
				query += ") VALUES ("+bsId+", ";
				foreach(ScheduleTime bTime in bs.ScheduleTimes)
					query += " '"+bTime.Level.ToString().Substring(0,1).ToLower()+"@"+bTime.Begin+"',";
				query = query.Remove(query.Length - 1);
				query += ")";
				dbCommand.CommandText = query;
				dbCommand.ExecuteNonQuery();
				// insert basepaths
				foreach (BasePath bPath in bs.BasePaths){
					string pQuery = "INSERT INTO backuppaths (bsid, gen, basepath, includepolicy, excludepolicy)"
						+" VALUES("+bsId+", 1, "
						+ " '"+bPath.Path+"', '"+bPath.IncludePolicy+"', '"+bPath.ExcludePolicy+"')" ;
					dbCommand.CommandText = pQuery;
					dbCommand.ExecuteNonQuery();
				}
				string bsQuery = "INSERT INTO \"TaskSets\" (id, gen, clientid, processingflags, preop, postop, redundancy, retentiondays, isactive, parallelism) VALUES(";
				bsQuery += bsId+", 0"
					+","+bs.NodeId
					//+", '"+bs.Encrypt.ToString()+"', '"+bs.Compress.ToString()+"', '"
					//+bs.ClientDedup+"', '"
					+", "+bs.DataFlags
					+", '"+bs.Preop+"', '"+bs.Postop+"', 1, 0, 'True', '"+bs.Parallelism.Kind.ToString().ToLower().Substring(0,1)+":"+bs.Parallelism.Value+"')";
				dbCommand.CommandText = bsQuery;
				dbCommand.ExecuteNonQuery();
				dbt.Commit();
				
			}	
			catch(Exception ex){
				Logger.Append("HUBRN", Severity.ERROR, "ERROR adding backup set :"+ex.Message+"---"+ex.StackTrace
					+", query was :"+dbCommand.CommandText);
				dbt.Rollback();
			}
			finally{
				dbCommand.Dispose();
				dbConnection.Close();
				dbConnection.Dispose();
			}	
			return null;	
		}
		


		
		internal Hashtable GetBackupHistory(int bsId, DateTime startDate, DateTime endDate){
			string query = "SELECT id, datebegin FROM backups WHERE bsid="+bsId+" ORDER BY datebegin" ;
				//+" AND datebegin BETWEEN '"+startDate.ToFileTime()+"' AND '"+endDate.ToFileTime()+"'";
			Hashtable bks = new Hashtable();
			using (DbDataReader reader = QueryAndGetReader(query)){
				while(reader.Read()) {
					try{
						int id = reader.GetInt32(0);
						DateTime begin =  Utils.GetDateTimeFromUnixTime(reader.GetInt64(1));
						//int priority = (int)reader[2];
						bks.Add(id, begin);
					}
					catch(Exception ex){
						Logger.Append("HUBRN", Severity.ERROR, "Could not retrieve bckup history from DB : "+ex.Message+" ---- "+ex.StackTrace+" (query was \""+query+"\"");
					}
				}
				
				reader.Close();
				reader.Dispose();
			}
			//Console.WriteLine("GetBackupHistory : query = "+query);
			return bks;
		}
		/// <summary>
		///Get the last successfully ran task for a backupset.
		///Used to retrieve the reference task when doing incr or diff backup
		/// </summary>
		/// <param name="bsId">
		/// A <see cref="System.Int32"/>
		/// </param>
		/// <param name="bType">
		/// A <see cref="BackupType"/>
		/// </param>
		/// <returns>
		/// A <see cref="P2PBackup.Common.Task"/>
		/// </returns>
		// TODO! check genid, and return null if no reference with same genid
		internal P2PBackup.Common.Task GetLastReferenceTask(int bsId, BackupLevel bType){
			string query = "SELECT * FROM backups WHERE id=("
				+"SELECT MAX(id) FROM backups WHERE bsid="+bsId
				//if(bType == BackupLevel.Differential)
				+" AND level <> '"+BackupLevel.SnapshotOnly.ToString()+"'";
				query += " AND runningstatus='Done') AND bsid="+bsId;
			Console.WriteLine(query);
			P2PBackup.Common.Task t = new RunTask();
			using (DbDataReader reader = QueryAndGetReader(query)){
				try{
	       				while(reader.Read()) {
						t = GetTaskFromReader(reader, 
						        Utils.GetDateTimeFromUnixTime(reader.GetInt64(reader.GetOrdinal("datebegin"))), 
							Utils.GetDateTimeFromUnixTime(reader.GetInt64(reader.GetOrdinal("dateend")))
						);
					}
				}
				catch(Exception ex){
					Logger.Append("HUBRN", Severity.ERROR, "Could not retrieve reference task from DB : "+ex.Message+" (query was \""+query+"\". Stacktrace : "+ex.StackTrace);
				}
				reader.Close();
				reader.Dispose();
			}
			
			return t;
		}
		
		internal List<P2PBackup.Common.Task> GetTaskHistory(string[] bsId, DateTime startDate, DateTime endDate, List<TaskRunningStatus> status, string sizeOperator, long size, int limit, int offset, out int totalCount){
			//Console.WriteLine("GetTaskHistory :0");
			if(sizeOperator != null && (sizeOperator != "<" && sizeOperator != ">") ){
				totalCount = 0;
				return null;
			}
			else if(sizeOperator == null) sizeOperator = ">=";
			//Console.WriteLine("GetTaskHistory :1");
			string normalBegin = "SELECT * FROM backups";
			string countBegin =  "SELECT COUNT(*) FROM backups";
			string query	= "";
			if(bsId != null && bsId.Length >0)
				query = " WHERE  bsid IN ('"+String.Join("','", bsId)+"')" ;
			else
				query = " WHERE 1=1";
			//Console.WriteLine("GetTaskHistory :2");
			if(status != null)
				query += " AND runningstatus IN ('"+string.Join("','", status)+"')";
			//Console.WriteLine("GetTaskHistory :3");
			//if(size.HasValue())
				query += " AND size "+sizeOperator+" "+size;
			query +=  " AND datebegin BETWEEN '"+Utils.GetUnixTimeFromDateTime(startDate)+"' AND '"+Utils.GetUnixTimeFromDateTime(endDate)+"'";
				//+ " ";
			
			string limitQuery = " ORDER BY id LIMIT "+limit+" OFFSET "+offset;
			List<P2PBackup.Common.Task> bks = new List<P2PBackup.Common.Task>();
			//Console.WriteLine("GetTaskHistory() count="+countBegin+query);
			totalCount = (int)(long)ExecuteScalar(countBegin+query);
			using (DbDataReader reader = QueryAndGetReader(normalBegin+query+limitQuery)){
				try{
	       				while(reader.Read()) {
						bks.Add(GetTaskFromReader(reader, startDate, endDate));
					}
				}
				catch(Exception ex){
					Logger.Append("HUBRN", Severity.ERROR, "Could not retrieve tasks history from DB : "+ex.Message+" (query was \""+query+"\". Stacktrace : "+ex.StackTrace);
					return bks;
				}
				reader.Close();
				reader.Dispose();
			}
			return bks;
		}

		internal Task GetTaskHistory(long taskId){
			string query = "SELECT * from backups WHERE id="+taskId;
			using (DbDataReader reader = QueryAndGetReader(query)){
				try{
	       				while(reader.Read()) {
						Task t = GetTaskFromReader(reader);
						t.LogEntries = GetTaskLogEntries(taskId);
						return t;
					}
				}
				catch(Exception ex){
					Logger.Append("HUBRN", Severity.ERROR, "Could not retrieve task #"+taskId+" from history : "+ex.Message+" (query was \""+query+"\". Stacktrace : "+ex.StackTrace);
				}
			}
			return null;
		}

		private P2PBackup.Common.Task GetTaskFromReader(DbDataReader reader){
			P2PBackup.Common.Task t = new P2PBackup.Common.Task();
			t.Id = reader.GetInt32(reader.GetOrdinal("id"));
			//if(startDate > DateTime.MinValue){
			//string day = startDate.DayOfWeek.ToString().Substring(0,3).ToLower();
			/*string bsQuery = @"SELECT bs.clientid, bs.operation, bs.id, bs.name, bs.processingflags, bs.preop, bs.postop, bs.redundancy, " +
				"bs.retentiondays, bs.storagegroup, bs.maxchunksize, bs.maxpacksize, bs.maxchunkfiles, bs.parallelism, bs.priority, "
				+"bs.datebegin AS begin, bs.dateend AS end"+
				" FROM \"TaskSets\" bs, backuptime bt " +
				" WHERE   bs.id = bt.bsid AND bs.id="+reader.GetInt32(reader.GetOrdinal("bsid"));
			using (DbDataReader bsReader = QueryAndGetReader(bsQuery)){
				try{
	       				while(bsReader.Read()){
						t.BackupSet = GetBSFromBigReader(bsReader, DateTime.Now.DayOfWeek);
						BackupType bt = BackupType.Full;
						Enum.TryParse(reader[reader.GetOrdinal("backuptype")] as string, true, out bt);
						t.BackupSet.ScheduleTimes[0].Type = bt;
						//t.BackupSet.ClientDedup = bool.Parse(reader[reader.GetOrdinal("clientdedup")] as string);
						//t.BackupSet.Compress = bool.Parse(reader[reader.GetOrdinal("compress")] as string);
						//t.BackupSet.Encrypt = bool.Parse(reader[reader.GetOrdinal("encrypt")] as string);
					}
				}
				catch(Exception ex){
					Logger.Append("HUBRN", Severity.ERROR, ex.Message+"---"+ex.StackTrace);
				}
				
			}
			}*/
			t.StartDate = /*startDate;*/Utils.GetDateTimeFromUnixTime(reader.GetInt64(reader.GetOrdinal("datebegin")));
			long end = 0;
			if(!reader.IsDBNull(reader.GetOrdinal("dateend"))){
			end = reader.GetInt64(reader.GetOrdinal("dateend"));
			//end = 	(long)reader[reader.GetOrdinal("dateend")];
				//Console.WriteLine("dateend="+end+", raw="+(string)reader[reader.GetOrdinal("dateend")]);
			t.EndDate =  Utils.GetDateTimeFromUnixTime(end);
			//Console.WriteLine("enddate="+t.EndDate);
			}

			t.Operation = (TaskOperation)Enum.Parse(typeof(TaskOperation), reader.GetString(reader.GetOrdinal("operation")));
			t.Type = (TaskStartupType) Enum.Parse(typeof(TaskStartupType),reader.GetString(reader.GetOrdinal("tasktype")));
			int flags = reader.IsDBNull(reader.GetOrdinal("processingflags"))? 0 : (int)reader["processingflags"];
			//t.BackupSet.DataFlags = (DataProcessingFlags)flags;
			t.OriginalSize = (long)reader["size"]; //originalSize;
			t.FinalSize = (long)reader["finalsize"]; //finalSize;
			t.Percent = (int)reader["completionpercent"];
			t.TotalItems = (int)reader["totalitems"];
			TaskRunningStatus trs = TaskRunningStatus.Unknown;
			Enum.TryParse(reader["runningstatus"] as string, true, out trs);
			t.RunStatus = trs;
			TaskStatus ts = TaskStatus.Null;
			Enum.TryParse(reader["status"] as string, true, out ts);
			BackupLevel bl = BackupLevel.Default;
			Enum.TryParse(reader[reader.GetOrdinal("level")] as string, true, out bl);
			t.Level = bl;
			t.Status = ts;
			t.CurrentAction = "";
			t.UserId = (int)reader["userid"];
			t.ParentTrackingId = (int)reader["parentid"];
			t.IndexName = reader["indexname"] as string;
			// this field can be null if task was interrupted before generating an index
			string rawSNs = reader["indexsn"] as String;
			if(! string.IsNullOrEmpty(rawSNs)){
				List<int> indexSNs = (reader["indexsn"] as string).Split(new char[]{','}).Select(n => int.Parse(n)).ToList();
				if (indexSNs != null) t.IndexStorageNodes = indexSNs;
			}
			t.IndexSum = reader["indexsum"] as string;
			t.SyntheticIndexSum = reader["synthindexsum"] as string;
			return t;
		}


		private P2PBackup.Common.Task GetTaskFromReader(DbDataReader reader, DateTime startDate, DateTime endDate){

			P2PBackup.Common.Task t = new P2PBackup.Common.Task();
			t.Id = reader.GetInt32(reader.GetOrdinal("id"));
			if(startDate > DateTime.MinValue){
			string day = startDate.DayOfWeek.ToString().Substring(0,3).ToLower();
			string bsQuery = @"SELECT bs.clientid, bs.operation, bs.id, bs.gen, bs.handledby, bs.name, bs.processingflags, bs.preop, bs.postop, bs.redundancy, " +
				"bs.retentiondays, bs.snapshotretention, bs.maxchunksize, bs.maxpacksize, bs.maxchunkfiles, bs.parallelism, bs.priority, "
				+"bt."+day+"begin AS begin, bt."+day+"end AS end"+
				" FROM \"TaskSets\" bs, backuptime bt " +
				" WHERE   bs.id = bt.bsid AND bs.id="+reader.GetInt32(reader.GetOrdinal("bsid"))
				+" AND bs.gen=(SELECT MAX(gen) FROM \"TaskSets\" WHERE id=bs.id)";
			using (DbDataReader bsReader = QueryAndGetReader(bsQuery)){
				try{
	       				while(bsReader.Read()){
						t.BackupSet = GetBSFromBigReader(bsReader, DateTime.Now.DayOfWeek);
						BackupLevel bt = BackupLevel.Full;
						Enum.TryParse(reader[reader.GetOrdinal("level")] as string, true, out bt);
						t.BackupSet.ScheduleTimes[0].Level = bt;
							t.Level = bt;
						//t.BackupSet.ClientDedup = bool.Parse(reader[reader.GetOrdinal("clientdedup")] as string);
						//t.BackupSet.Compress = bool.Parse(reader[reader.GetOrdinal("compress")] as string);
						//t.BackupSet.Encrypt = bool.Parse(reader[reader.GetOrdinal("encrypt")] as string);
					}
				}
				catch(Exception ex){
					Logger.Append("HUBRN", Severity.ERROR, ex.Message+"---"+ex.StackTrace);
				}
				
			}
			}
			t.StartDate = Utils.GetDateTimeFromUnixTime(reader.GetInt64(reader.GetOrdinal("datebegin")));
			long end = 0;
			if(!reader.IsDBNull(reader.GetOrdinal("dateend"))){
			end = reader.GetInt64(reader.GetOrdinal("dateend"));
			//end = 	(long)reader[reader.GetOrdinal("dateend")];
				//Console.WriteLine("dateend="+end+", raw="+(string)reader[reader.GetOrdinal("dateend")]);
			t.EndDate =  Utils.GetDateTimeFromUnixTime(end);
			//Console.WriteLine("enddate="+t.EndDate);
			}
			//if(endDate < DateTime.MaxValue)
			//	t.EndDate = endDate;
			//Console.WriteLine("GetTaskFromReader() : enddate="+t.EndDate);
			t.Operation = (TaskOperation)Enum.Parse(typeof(TaskOperation), reader.GetString(reader.GetOrdinal("operation")));
			t.Type = (TaskStartupType) Enum.Parse(typeof(TaskStartupType),reader.GetString(reader.GetOrdinal("tasktype")));

			int flags = reader.IsDBNull(reader.GetOrdinal("processingflags"))? 0 : (int)reader["processingflags"];
			t.BackupSet.DataFlags = (DataProcessingFlags)flags;
			t.OriginalSize = (long)reader[reader.GetOrdinal("size")]; //originalSize;
			//long.TryParse(reader[reader.GetOrdinal("finalsize")] as string, out finalSize);
			t.FinalSize = (long)reader[reader.GetOrdinal("finalsize")]; //finalSize;
			t.Percent = (int)reader[reader.GetOrdinal("completionpercent")];
			t.TotalItems = (int)reader[reader.GetOrdinal("totalitems")];
			t.IndexSum = reader.IsDBNull(reader.GetOrdinal("indexsum"))? null : reader.GetString(reader.GetOrdinal("indexsum"));
			t.SyntheticIndexSum = reader.IsDBNull(reader.GetOrdinal("synthindexsum"))? null : reader.GetString(reader.GetOrdinal("synthindexsum"));
			TaskRunningStatus trs = TaskRunningStatus.Unknown;
			Enum.TryParse(reader[reader.GetOrdinal("runningstatus")] as string, true, out trs);
			t.RunStatus = trs;
			TaskStatus ts = TaskStatus.Null;
			Enum.TryParse(reader[reader.GetOrdinal("status")] as string, true, out ts);
			t.Status = ts;
			t.CurrentAction = "";
			t.UserId = (int)reader[reader.GetOrdinal("userid")];
			t.ParentTrackingId = (int)reader[reader.GetOrdinal("parentid")];
			return t;
		}


		internal List<Tuple<DateTime, int, string, string>> GetTaskLogEntries(long taskId){
			List<Tuple<DateTime, int, string, string>> logEntries = new List<Tuple<DateTime, int, string, string>>();
			string query = "SELECT * FROM bslog WHERE trackingid="+taskId+" ORDER BY utimestamp ASC";
			using (DbDataReader reader = QueryAndGetReader(query)){
				try{
	       				while(reader.Read()){
						long when = reader.GetInt64(reader.GetOrdinal("utimestamp"));
						DateTime entryDate = Utils.GetDateTimeFromUnixTime(when);
						int code = reader.GetInt32(reader.GetOrdinal("errorcode"));
						string message1 = reader[reader.GetOrdinal("data")] as string;
						string message2 = reader[reader.GetOrdinal("data2")] as string;
						Tuple<DateTime, int, string, string> entry = new Tuple<DateTime, int, string, string>(entryDate, code, message1, message2);
						logEntries.Add(entry);
					}
				}
				catch(Exception ex){
					Logger.Append("HUBRN", Severity.ERROR, ex.Message+"---"+ex.StackTrace);
				}
				return logEntries;
			}
			
		}
		

		
		private int GetNbBackupSets(int uid){
			string query = "SELECT count(*) from \"TaskSets\" where clientid="+uid;			
			int nBs = 0;
			try{
				nBs = (int)(long)ExecuteScalar(query);
       				
			}
			catch(Exception ex){
				Logger.Append("HUBRN", Severity.ERROR, "Could not retrieve storage groups from DB : "+ex.Message+" (query was \""+query+"\"");
			}
			return nBs;
		}

		public long AddBackupTracking(Task task){
			long trackingId = GetNewBackupTrackingId();
			string query = "INSERT INTO backups(id, bsid, bsgen, level, operation, tasktype, datebegin, runningstatus, dateend, processingflags, size, finalsize, completionpercent, totalitems, userid, parentid) VALUES("
				+trackingId+", "+task.BackupSet.Id+", "+task.BackupSet.Generation+", '"+task.BackupSet.ScheduleTimes[0].Level+"', '"+task.Operation
				+"', '"+task.Type+"', "+Utils.GetUnixTimeFromDateTime(DateTime.UtcNow)
				+", '"+task.RunStatus
				+"', '"+Utils.GetUnixTimeFromDateTime(DateTime.UtcNow)+"'"
				//+"', '"+task.BackupSet.Compress+"', '"+task.BackupSet.Encrypt+"', '"+task.BackupSet.ClientDedup
				+","+(int)task.BackupSet.DataFlags
				+",0 ,0, 0, 0, "+task.UserId+", "+task.ParentTrackingId+")";
			try{
				ExecuteNonQuery(query);
				return trackingId;
			}	
			catch(Exception ex){
				Logger.Append("HUBRN", Severity.ERROR, ex.Message+"---"+ex.StackTrace+" (query was "+query+")");
			}
			return -1;
		}
		
		public void CompleteBackupTracking(Task task){
			string joinedStorageNodes = String.Join(",", task.IndexStorageNodes);
			string query = "UPDATE backups SET indexname='"+task.IndexName+"', indexsum='"+task.IndexSum+"', synthindexsum='"+task.SyntheticIndexSum+"', dateend="+Utils.GetUnixTimeFromDateTime(task.EndDate)
				+", indexsn='"+joinedStorageNodes+"', size="+task.OriginalSize+", finalsize="+task.FinalSize+", parentid="+task.ParentTrackingId
				+", status='"+task.Status+"', runningstatus='"+task.RunStatus+"', completionpercent="+task.Percent+", totalitems="+task.TotalItems
				+" WHERE id="+task.Id;
			try{
				//Console.WriteLine("completebackuptracking="+query);
				ExecuteNonQuery(query);
			}	
			catch(Exception ex){
				Logger.Append("HUBRN", Severity.ERROR, ex.Message+"---"+ex.StackTrace+" (query was "+query+")");
			}
			using(DbConnection dbc = GetConnection()){
				foreach(Tuple<DateTime, int, string, string> logEntry in task.LogEntries){
					// TODO !!! parameterize input (dangerous since itm3 and item4 are strings)
					string logQuery = "INSERT INTO bslog (trackingid, errorcode, data, data2, utimestamp) VALUES(@trackingid, @errorcode, @data, @data2, @utimestamp)";
						//+task.Id+",'"+logEntry.Item2.Replace("'", @"\'")+"', '"+logEntry.Item3.Replace("'", @"\'")+"', '"+logEntry.Item4.Replace("'", @"\'")+"', "+Utils.GetUnixTimeFromDateTime(logEntry.Item1)+")";
	
					DbCommand dbCommand = dbc.CreateCommand();
					dbCommand.CommandText = logQuery;
					IDbDataParameter tid = dbCommand.CreateParameter();
					tid.ParameterName = "@trackingid";
					tid.Value = task.Id;
					dbCommand.Parameters.Add(tid);

					IDbDataParameter errorcode = dbCommand.CreateParameter();
					errorcode.ParameterName = "@errorcode";
					errorcode.Value = logEntry.Item2;
					dbCommand.Parameters.Add(errorcode);

					IDbDataParameter data = dbCommand.CreateParameter();
					data.ParameterName = "@data";
					data.Value = logEntry.Item3;
					dbCommand.Parameters.Add(data);

					IDbDataParameter data2 = dbCommand.CreateParameter();
					data2.ParameterName = "@data2";
					data2.Value = logEntry.Item4;
					dbCommand.Parameters.Add(data2);

					IDbDataParameter utimestamp = dbCommand.CreateParameter();
					utimestamp.ParameterName = "@utimestamp";
					utimestamp.Value = Utils.GetUnixTimeFromDateTime(logEntry.Item1);
					dbCommand.Parameters.Add(utimestamp);
					try{
						//Console.WriteLine(logQuery);
						dbCommand.ExecuteNonQuery();
					}	
					catch(Exception ex){
						Logger.Append("HUBRN", Severity.ERROR, ex.Message+"---"+ex.StackTrace+" (query was "+logQuery+")");
					}
					
				}
			}
		}
		
		/// <summary>
		/// On hub start, set the recorded tasks that have previously been interrupted by hub stop as cancelled
		/// </summary>
		/// <param name='bsId'>
		public int UpdateCancelledTasks(){
			string query = "UPDATE backups SET runningstatus='"+TaskRunningStatus.Cancelled+"'"
				+" WHERE runningstatus in ('"+TaskRunningStatus.PendingStart+"', '"+TaskRunningStatus.Started+"')";
			try{
				return ExecuteNonQuery(query);
			}	
			catch(Exception ex){
				Logger.Append("HUBRN", Severity.ERROR, ex.Message+"---"+ex.StackTrace+" (query was "+query+")");
			}
			return 0;
		}
		
		internal void UpdateTaskStatus(long taskId, TaskRunningStatus status){
			string query = "UPDATE backups SET runningstatus='"+status+"'"
				+" WHERE id="+taskId;
			try{
				ExecuteNonQuery(query);
			}	
			catch(Exception ex){
				Logger.Append("HUBRN", Severity.ERROR, ex.Message+"---"+ex.StackTrace+" (query was "+query+")");
			}
		}
		
		private long GetLastTrackingId(int bsid){
			string query = "select MAX(id) from \"backups\" where bsid="+bsid;
			long id = 0;
			//try{
				id = (long)ExecuteScalar(query);
			//}
			//catch(Exception ex){
			//	Logger.Append("HUBRN", Severity.ERROR, ex.Message+"---"+ex.StackTrace);
			//}
			return id;
		}
		
		private long GetNewBackupTrackingId(){
			string query = "select MAX(id) from \"backups\"";
			long id = 0;
			try{
				id = 1+(Int32)ExecuteScalar(query); // BUGGY should be int64 (howto with postgres?)
			}
			catch(Exception ex){
				Logger.Append("HUBRN", Severity.ERROR, ex.Message+"---"+ex.StackTrace);
			}
			return id;
		}

		internal void AddBackupSetError(int bsId, string errorCode, string data){
			string query = "INSERT INTO bslog (bsid, trackingid, errorcode, data) VALUES (" + bsId + ", " + GetLastTrackingId(bsId) +", " + errorCode + ", '" + data + "')";
			try{
				ExecuteNonQuery(query);
			}	
			catch(Exception ex){
				Logger.Append("HUBRN", Severity.ERROR, ex.Message+"---"+ex.StackTrace+" (query was "+query+")");
			}
		}

		internal List<P2PBackup.Common.Task> GetExpiredBackups(){
			List<P2PBackup.Common.Task> expired = new List<P2PBackup.Common.Task>();
			string query = "SELECT * FROM backups bk, \"TaskSets\"  tsk WHERE bk.bsid=tsk.id" 
			  +" AND bk.operation in ('Backup', 'Restore')"
			  +" AND bk.runningstatus <> 'Expired'"
			  +" AND bk.id < ("
			  +"  SELECT max(b.id) FROM backups b, \"TaskSets\"  t "
			  +"    WHERE b.bsid=t.id "
			  +"    AND  t.id=bk.bsid "
			  +"    AND b.level='Full'"
			  +"    AND ("
			  +"      b.datebegin IS NULL"
			  +"      OR (b.datebegin+(t.retentiondays*24*60*3600)) < "+Utils.GetUnixTimeFromDateTime(DateTime.Now)
			  +"    )"
			  +") ORDER BY bk.id";
			//Console.WriteLine("getexpiredbackups(): query= "+query);
			//string query = "SELECT * from backups b, \"TaskSets\" t WHERE b.bsid=t.id AND (b.dateend"
			//	+"+(t.retentiondays*24*60*3600)) > "+Utils.GetUnixTimeFromDateTime(DateTime.Now);
			using (DbDataReader reader = QueryAndGetReader(query)){
				
				while(reader.Read()){
					try{
						P2PBackup.Common.Task t = GetTaskFromReader(reader, DateTime.Now, DateTime.Now);
						expired.Add(t);
					}
					catch(Exception ex){
						Logger.Append("HUBRN", Severity.ERROR, "Unable to retrieve expired backups from DB: "+ex.Message+"---"+ex.StackTrace+" (query was "+query+")");
					}
				}
				
				reader.Close();
				reader.Dispose();
			}
			return expired;
		}
		
		internal void DeleteTask(long taskId){
			DbConnection dbConnection = GetConnection();
       			DbCommand dbCommand = dbConnection.CreateCommand();
			IDbTransaction dbt = dbConnection.BeginTransaction();
			string query = "DELETE FROM backups WHERE operation='Backup' AND id="+taskId;
			string query2 = "DELETE FROM bslog WHERE trackingid="+taskId;
			try{
				dbCommand.CommandText = query;
				if(dbCommand.ExecuteNonQuery() > 0){
					dbCommand.CommandText = query2;
					dbCommand.ExecuteNonQuery();
					dbt.Commit();
				}
				else dbt.Rollback();
			}
			catch(Exception ex){
				Logger.Append("HUBRN", Severity.ERROR, "ERROR deleting task "+taskId+" :"+ex.Message+"---"+ex.StackTrace
					+", query was :"+dbCommand.CommandText);
				dbt.Rollback();
			}
			finally{
				dbCommand.Dispose();
				dbConnection.Close();
				dbConnection.Dispose();
			}	
		}

		// gets last full backup info : index name and SHA sum
		internal string GetLastFullBackupInformation(int bsId){
			string lfi = null;
			string query = "SELECT indexname, indexsum FROM backups WHERE bsid="+bsId+" AND level='f' AND datebegin = "
				+"(SELECT MAX(date(datebegin)) FROM backups WHERE bsid="+bsId+" AND level='f')";
			using (DbDataReader reader = QueryAndGetReader(query)){
				try{
					lfi= reader.GetString(0)+" "+reader.GetString(1);
				}
				catch(Exception ex){
					Logger.Append("HUBRN", Severity.ERROR, ex.Message+"---"+ex.StackTrace);
				}
			}
			return lfi;
		}
		
		
		//  returns all sets to be STARTED from now to untilWhen
		public ArrayList GetTaskSets(TimeSpan untilWhen){
			ArrayList bs = new ArrayList();
				
			return bs;
		}

		internal BackupSet GetTaskSet(int bsId){
			string curDay = DateTime.Now.DayOfWeek.ToString().ToLower().Substring(0,3);
			string query = @"SELECT bs.operation, bs.clientid, bs.id, bs.gen, bs.handledby, bs.name, /*n.username,*/  bs.processingflags, bs.maxchunksize, "
				+"bs.maxpacksize, bs.maxchunkfiles, bs.preop, bs.postop, bs.redundancy, bs.parallelism, bs.priority, " 
				+" bs.retentiondays, bs.snapshotretention, bt."+curDay+"begin AS begin, bt."+curDay+"end AS end"+
				" FROM \"TaskSets\" bs LEFT OUTER JOIN \"backuptime\" bt" 
				//" WHERE  bs.id = bt.bsid "
				+" ON  bs.id = bt.bsid "
				+" WHERE bs.id="+bsId
				+" AND bs.gen = (SELECT MAX(gen) FROM \"TaskSets\" WHERE id="+bsId+")";
			DbDataReader reader = QueryAndGetReader(query);
			reader.Read();					
			return GetBSFromBigReader(reader, DateTime.Now.DayOfWeek);


		}

		private int GetNewBackupSetId(){
			string query = "SELECT MAX(id) FROM \"TaskSets\"";
			int id = -1;
			try{
				id = (int)(long)ExecuteScalar(query);
			}
			catch(Exception ex){
				Logger.Append("HUBRN", Severity.ERROR,ex.Message+"---"+ex.StackTrace);
			}
			return id +1;
		}


		
		public List<BackupSet> GetNodeBackupSets(int nodeId){
			List<BackupSet> bsList = new List<BackupSet>();
			string currentDay = DateTime.Now.DayOfWeek.ToString().ToLower().Substring(0,3);;
			string query = @"SELECT bs.operation, bs.clientid, bs.id, bs.gen, bs.handledby, bs.name, n.name,  bs.processingflags, bs.preop, bs.postop, bs.redundancy, " +
					"bs.retentiondays, bs.snapshotretention, bs.maxchunksize, bs.maxpacksize, bs.maxchunkfiles, bs.parallelism, /*bp.basepath, bp.includepolicy, bp.excludepolicy, */ bt."+currentDay+"begin AS begin, bt."+currentDay+"end AS end"+
					" FROM \"TaskSets\" bs, backuptime bt, nodes n" +
					" WHERE /*bs.isactive='True' AND*/ bs.id = bt.bsid AND bs.clientid = n.uid AND n.uid="+nodeId
					+" AND bs.gen=(SELECT MAX(gen) FROM \"TaskSets\" WHERE id=bs.id)";
			try{
			using (DbDataReader reader = QueryAndGetReader(query)){
				try{
	       				while(reader.Read())
						bsList.Add(GetBSFromBigReader(reader, DateTime.Now.DayOfWeek));
				}
				catch(Exception ex){
					Logger.Append("HUBRN", Severity.ERROR, ex.Message+"---"+ex.StackTrace);
				}
			}
			}
			catch(Exception ex2){
				Logger.Append("HUBRN", Severity.ERROR, ex2.Message+", query :"+query+"---"+ex2.StackTrace);	
			}
			return bsList;
		}

		/// <summary>
		///returns all sets to be run between the two dates
		/// for TimeLine display (Web GUI) _AND_for Scheduler, it is MANDATORY that results are sorted by startdate.
		/// </summary>
		/// <returns>
		/// The task sets.
		/// </returns>
		/// <param name='begin'>
		/// Begin.
		/// </param>
		/// <param name='end'>
		/// End.
		/// </param>
		/// <param name='forDisplayOnly'>
		/// return results for display only. For scheduling, set it to false.
		/// </param>
		internal List<BackupSet> GetTaskSets(DateTime begin, DateTime end, bool forDisplayOnly){
			//TimeSpan dateDiff = end.Subtract(begin);
			int nbOfDays = (int)(end.DayOfWeek - begin.DayOfWeek );
			List<BackupSet> bs = new List<BackupSet>();
			using(DbConnection dbConnection = GetConnection()){
       				using(DbCommand dbCommand = dbConnection.CreateCommand()){
					int i=0;
					//for(i=0;i<=nbOfDays;i++){
						DayOfWeek theDay = begin.AddDays(i).DayOfWeek;
						string theEndDay = end.DayOfWeek.ToString().ToLower().Substring(0,3);
						string currentDay = theDay.ToString().ToLower().Substring(0,3);
						
						string query = @"SELECT bs.operation, bs.clientid, bs.id, bs.gen, bs.handledby, bs.name, bs.processingflags, bs.maxchunksize, "
							+"bs.maxpacksize, bs.maxchunkfiles, bs.preop, bs.postop, bs.redundancy, bs.parallelism, bs.priority, "
							+" bs.retentiondays, bs.snapshotretention, bt."+currentDay+"begin AS begin, bt."+currentDay+"end AS end"
							+" FROM \"TaskSets\" bs, \"backuptime\" bt "
							+" WHERE  bs.isactive='True' AND bs.id = bt.bsid "
							+" AND bs.gen = (SELECT MAX(gen) from \"TaskSets\" WHERE id=bs.id)"
							+" AND bt."+currentDay+"begin IS NOT NULL"
							+" AND ( (to_timestamp(substr(bt."+currentDay+"begin,3), 'HH24:MI') > to_timestamp('"+begin.ToString("HH:mm")+"', 'HH24:MI')";
						//if(forDisplayOnly)
						//	query += " OR to_timestamp(bt."+currentDay+"end, 'HH24:MI') < to_timestamp('"+begin.ToString("HH:mm")+"', 'HH24:MI') )";
						//else
							query += ")";
						if(currentDay == theEndDay){
							//query += " AND to_timestamp(substr(bt."+theEndDay+"begin,3), 'HH24:MI') < to_timestamp('"+end.ToString("HH:mm")+"', 'HH24:MI') ";
							query += ")";
						}
						else
							query += " OR to_timestamp(substr(bt."+theEndDay+"begin,3), 'HH24:MI') < to_timestamp('"+end.Hour+":"+end.Minute+"', 'HH24:MI') )";
						query += " ORDER by bt."+currentDay+"begin";
						//Console.WriteLine("GetTaskSets:    "+query);
						dbCommand.CommandText = query;
						//Logger.Append("HUBRN", Severity.DEBUG2, "Retrieving backupsets, day number is "+i+",query is:"+query);
						try{
							using (DbDataReader reader = dbCommand.ExecuteReader(/*CommandBehavior.CloseConnection*/)/*QueryAndGetReader(query)*/){
				       				while(reader.Read()){
				       					BackupSet b = GetBSFromBigReader(reader, theDay);
									if(b != null)
										bs.Add(b);
								}
							
								reader.Close();
								reader.Dispose();
							}
						}
						catch(Exception ex){
							Logger.Append("HUBRN", Severity.ERROR, ex.Message+" (query was :"+query+")---"+ex.StackTrace);
						}
					//}
				}
				dbConnection.Close();
				dbConnection.Dispose();
			}
			return bs;
		}

		internal List<BackupSet> GetTaskSets(){
			List<BackupSet> bs = new List<BackupSet>();
			string query = @"SELECT bs.operation, bs.clientid, bs.id, bs.gen, bs.handledby, bs.name, /*n.username,*/  bs.processingflags, bs.maxchunksize, "
				+"bs.maxpacksize, bs.maxchunkfiles, bs.preop, bs.postop, bs.redundancy, bs.parallelism, bs.priority, "
				+" bs.retentiondays, bs.snapshotretention "
				+" FROM \"TaskSets\" bs" 
				+" WHERE bs.gen = (SELECT MAX(gen) FROM \"TaskSets\" WHERE id=bs.id)";
			using (DbDataReader reader = QueryAndGetReader(query)){
				while(reader.Read())
				try{
					bs.Add(GetBSFromBigReader(reader));
				}
				catch(Exception ex){
					Logger.Append("HUBRN", Severity.ERROR, ex.Message+"---"+ex.StackTrace);
				}
			}
			return bs;

		}



		private BackupSet GetBSFromBigReader(DbDataReader reader, DayOfWeek day){
			string backBegin = reader["begin"] as string;
			string backEnd = "";
			ScheduleTime bTime = new ScheduleTime();
			if(!string.IsNullOrEmpty(backBegin)){
				if (! reader.IsDBNull(reader.GetOrdinal("end")))
					backEnd = ""+reader.GetString(reader.GetOrdinal("end"));
				BackupLevel bt = 0;
				switch(backBegin.Substring(0,1)){
				case "f":
					bt = BackupLevel.Full;
					break;
				/*case "d":
					bt = BackupLevel.Differential;
					break;*/
				case "r":
					bt = BackupLevel.Refresh;
					break;
				case "s":
					bt = BackupLevel.SyntheticFull;
					break;
				}
				bTime = new ScheduleTime(bt, day, backBegin.Substring(2), backEnd);
			}
			BackupSet bs = new BackupSet();
			bs.Id = reader.GetInt32(reader.GetOrdinal("id"));
			bs.Generation = reader.GetInt32(reader.GetOrdinal("gen"));
			bs.Operation = (TaskOperation)Enum.Parse(typeof(TaskOperation), reader.GetString(reader.GetOrdinal("operation")));
			int flags = reader.IsDBNull(reader.GetOrdinal("processingflags"))? 0 : (int)reader["processingflags"];
			bs.DataFlags = (DataProcessingFlags)flags;
			//bs.Parallelism = reader.GetInt32(reader.GetOrdinal("parallelism"));

			string parallelismInfo = reader[reader.GetOrdinal("parallelism")] as string;
			if(string.IsNullOrEmpty(parallelismInfo))
				bs.Parallelism = new Parallelism{Kind=ParallelismLevel.Absolute, Value=2};
			else
				bs.Parallelism = new Parallelism(parallelismInfo);
			if (! reader.IsDBNull(reader.GetOrdinal("preop")))
				bs.Preop = reader.GetString(reader.GetOrdinal("preop"));
			if (! reader.IsDBNull(reader.GetOrdinal("postop")))
				bs.Postop = reader.GetString(reader.GetOrdinal("postop"));
			bs.Name = reader[reader.GetOrdinal("name")] as string;
			bs.NodeId = reader.GetInt32(reader.GetOrdinal("clientid"));
			bs.HandledBy = reader.IsDBNull(reader.GetOrdinal("handledby"))? 0 : (int)reader["handledby"];
			bs.Redundancy = reader.GetInt32(reader.GetOrdinal("redundancy"));
			bs.RetentionDays = reader.GetInt32(reader.GetOrdinal("retentiondays"));
			bs.SnapshotRetention = reader.IsDBNull(reader.GetOrdinal("snapshotretention"))? 0 : (int)reader["snapshotretention"];
			List<ScheduleTime> bTimes = new List<ScheduleTime>();
			bTimes.Add(bTime);
			bs.ScheduleTimes = bTimes;
			bs.BasePaths = GetBSPaths(bs.Id);
			bs.StorageGroups = GetBSStorageGroups(bs.Id);
			bs.Notifications = GetBSNotifications(bs.Id);
			int maxChunkSize = int.Parse(ConfigurationManager.AppSettings["Backup.MaxChunkSize"]);
			int maxPackSize = int.Parse(ConfigurationManager.AppSettings["Backup.PackChunkSize"]);
			int maxChunkFiles = int.Parse(ConfigurationManager.AppSettings["Backup.MaxChunkFiles"]);
			//int.TryParse(reader.GetString(reader.GetOrdinal("maxchunksize")), out maxChunkSize);
			bs.MaxChunkSize = reader.IsDBNull(reader.GetOrdinal("maxchunksize"))? maxChunkSize : reader.GetInt32(reader.GetOrdinal("maxchunksize")) ;
			//int.TryParse(reader.GetString(reader.GetOrdinal("maxpacksize")), out maxPackSize);
			bs.MaxPackSize = reader.IsDBNull(reader.GetOrdinal("maxpacksize"))? maxPackSize : reader.GetInt32(reader.GetOrdinal("maxpacksize")) ;
			bs.MaxChunkFiles = reader.IsDBNull(reader.GetOrdinal("maxchunkfiles"))? maxChunkFiles : reader.GetInt32(reader.GetOrdinal("maxchunkfiles")) ;
			if(bs.HandledBy >0 && bs.HandledBy != bs.NodeId){ //proxied backup : retrieve, if needed, proxy info
				//Console.WriteLine("DBHandle : HANDLEDBY!!!! ");
				P2PBackup.Common.Node realClientNode = GetNode(bs.NodeId);
				//Console.WriteLine("node = "+realClientNode.InternalId);//+", hypervisor="+realClientNode.Hypervisor);
				if(realClientNode.Hypervisor >0){
					bs.ProxyingInfo = new ProxyTaskInfo();
					bs.ProxyingInfo.Hypervisor = GetHypervisor(realClientNode.Hypervisor);
					bs.ProxyingInfo.Node = realClientNode;
				}
			} 
			return bs;
		}

		private BackupSet GetBSFromBigReader(DbDataReader reader){

			BackupSet bs = new BackupSet();
			try{
			bs.Id = reader.GetInt32(reader.GetOrdinal("id"));
			bs.Generation = reader.GetInt32(reader.GetOrdinal("gen"));
			bs.Operation = (TaskOperation)Enum.Parse(typeof(TaskOperation), reader.GetString(reader.GetOrdinal("operation")));
			int flags = reader.IsDBNull(reader.GetOrdinal("processingflags"))? 0 : (int)reader["processingflags"];
			bs.DataFlags = (DataProcessingFlags)flags;
			//bs.Parallelism = reader.GetInt32(reader.GetOrdinal("parallelism"));
			string parallelismInfo = reader[reader.GetOrdinal("parallelism")] as string;
			if(string.IsNullOrEmpty(parallelismInfo))
				bs.Parallelism = new Parallelism{Kind=ParallelismLevel.Absolute, Value=2};
			else
				bs.Parallelism = new Parallelism(parallelismInfo);
			if (! reader.IsDBNull(reader.GetOrdinal("preop")))
				bs.Preop = reader.GetString(reader.GetOrdinal("preop"));
			if (! reader.IsDBNull(reader.GetOrdinal("postop")))
				bs.Postop = reader.GetString(reader.GetOrdinal("postop"));
			bs.Name = reader[reader.GetOrdinal("name")] as string;
			bs.NodeId = reader.GetInt32(reader.GetOrdinal("clientid"));
			bs.HandledBy = reader.IsDBNull(reader.GetOrdinal("handledby"))? 0 : (int)reader["handledby"];
			bs.Redundancy = reader.GetInt32(reader.GetOrdinal("redundancy"));
			bs.RetentionDays = reader.GetInt32(reader.GetOrdinal("retentiondays"));
			bs.SnapshotRetention = reader.IsDBNull(reader.GetOrdinal("snapshotretention"))? 0 : (int)reader["snapshotretention"];
			List<ScheduleTime> bTimes = new List<ScheduleTime>();
			//bTimes.Add(bTime);
			bs.ScheduleTimes = bTimes;
			bs.BasePaths = GetBSPaths(bs.Id);
			bs.StorageGroups = GetBSStorageGroups(bs.Id);
			bs.Notifications = GetBSNotifications(bs.Id);
			int maxChunkSize = int.Parse(ConfigurationManager.AppSettings["Backup.MaxChunkSize"]);
			int maxPackSize = int.Parse(ConfigurationManager.AppSettings["Backup.PackChunkSize"]);
			int maxChunkFiles = int.Parse(ConfigurationManager.AppSettings["Backup.MaxChunkFiles"]);
			//int.TryParse(reader.GetString(reader.GetOrdinal("maxchunksize")), out maxChunkSize);
			bs.MaxChunkSize = reader.IsDBNull(reader.GetOrdinal("maxchunksize"))? maxChunkSize : reader.GetInt32(reader.GetOrdinal("maxchunksize")) ;
			//int.TryParse(reader.GetString(reader.GetOrdinal("maxpacksize")), out maxPackSize);
			bs.MaxPackSize = reader.IsDBNull(reader.GetOrdinal("maxpacksize"))? maxPackSize : reader.GetInt32(reader.GetOrdinal("maxpacksize")) ;
			bs.MaxChunkFiles = reader.IsDBNull(reader.GetOrdinal("maxchunkfiles"))? maxChunkFiles : reader.GetInt32(reader.GetOrdinal("maxchunkfiles")) ;
			if(bs.HandledBy >0 && bs.HandledBy != bs.NodeId){ //proxied backup : retrieve, if needed, proxy info
				//Console.WriteLine("DBHandle : HANDLEDBY!!!! ");
				P2PBackup.Common.Node realClientNode = GetNode(bs.NodeId);
				//Console.WriteLine("node = "+realClientNode.ToString()+", hypervisor="+realClientNode.Hypervisor);
				if(realClientNode.Hypervisor >0){
					bs.ProxyingInfo = new ProxyTaskInfo();
					bs.ProxyingInfo.Hypervisor = GetHypervisor(realClientNode.Hypervisor);
					bs.ProxyingInfo.Node = realClientNode;
				}
			}
			return bs;
			}
			catch(Exception e){
				Logger.Append("HUBRN", Severity.ERROR, "Could not retrieve Taskset : "+e.ToString());
				return null;
			}
		}
		
		private List<int> GetBSStorageGroups(int bsid){
			string query = "SELECT sgid FROM bstosg WHERE bsid="+bsid;
			List<int> sgs = new List<int>();
				using (DbDataReader reader = QueryAndGetReader(query)){
					try{
						while(reader.Read()){
							sgs.Add(reader.GetInt32(0));
						}
					}
					catch(Exception ex){
						Logger.Append("HUBRN", Severity.ERROR, ex.Message+"---"+ex.StackTrace+" (query was "+query+")");
					}
					reader.Close();
					reader.Dispose();
				}
			return sgs;
		}
		
		private List<BasePath> GetBSPaths(int bsid){
			string query = "SELECT basepath, includepolicy, excludepolicy, excludepaths, snapshot, type"
				+" FROM backuppaths WHERE bsid="+bsid
				+" AND bsgen=(SELECT MAX(bsgen) FROM backuppaths where bsid="+bsid+")";
			List<BasePath> bPaths = new List<BasePath>();

			using (DbDataReader reader = QueryAndGetReader(query)){
				try{
					while(reader.Read()){
						BasePath bp = new BasePath();

						bp.Path = reader.GetString(reader.GetOrdinal("basepath"));
						if (! reader.IsDBNull(reader.GetOrdinal("includepolicy")))
							bp.IncludePolicy.AddRange(new List<string>(reader.GetString(reader.GetOrdinal("includepolicy")).Split(new char[]{'|'}, StringSplitOptions.RemoveEmptyEntries)));
						if (! reader.IsDBNull(reader.GetOrdinal("excludepolicy")))
							bp.ExcludePolicy.AddRange(new List<string>(reader.GetString(reader.GetOrdinal("excludepolicy")).Split(new char[]{'|'}, StringSplitOptions.RemoveEmptyEntries)));
						//bp.Type = (BasePath.PathType)Enum.Parse(typeof(BasePath.PathType), reader.GetString(reader.GetOrdinal("type")));
						bp.Type = reader["type"] as String;
						//if(bp.Type == BasePath.PathType.OBJECT)
						//	bp.Config = GetBSPathSpoConfig(bsid, bp.Path);
						if (! reader.IsDBNull(reader.GetOrdinal("snapshot")))
							bp.SnapshotType += ""+reader.GetString(reader.GetOrdinal("snapshot"));
						if (! reader.IsDBNull(reader.GetOrdinal("excludepaths"))){
							bp.ExcludedPaths = new List<string>();
							bp.ExcludedPaths.AddRange(reader.GetString(reader.GetOrdinal("excludepaths")).Split(new char[]{'|'}, StringSplitOptions.RemoveEmptyEntries));
						}
						bPaths.Add(bp);
					}
				}
				catch(Exception ex){
					Logger.Append("HUBRN", Severity.ERROR, ex.Message+"---"+ex.StackTrace+" (query was "+query+")");
				}
				reader.Close();
				reader.Dispose();
			}
			return bPaths;
		}

		/*private BasePathConfig GetBSPathSpoConfig(int bsid, string path){
			string query = "SELECT * FROM specialobjects WHERE bsid="+bsid;
			BasePathConfig config = new BasePathConfig();
			using (DbDataReader reader = QueryAndGetReader(query)){
				try{
					while(reader.Read()){
						string spoPath = reader.GetString(reader.GetOrdinal("spopath"));
						if(spoPath.StartsWith(path)){
							//Dictionary<string,string> kvps = new Dictionary<string, string>();
							List<Tuple<string, string>> kvps = new List<Tuple<string, string>>();
							if (! reader.IsDBNull(reader.GetOrdinal("config"))){
								foreach(string nameValuePair in (reader.GetString(reader.GetOrdinal("config")).Split(new char[]{'|'}, StringSplitOptions.RemoveEmptyEntries))){
									string[] nvp = nameValuePair.Split(new char[]{'='});
									kvps.Add(new Tuple<string, string>(nvp[0], nvp[1]));
								}
								config.ConfigPairs = kvps;

							}
							if (! reader.IsDBNull(reader.GetOrdinal("password")))
								config.Password = PasswordManager.Get((int)reader["password"]);

						}
					}
				}
				catch(Exception ex){
					Logger.Append("HUBRN", Severity.ERROR, ex.Message+"---"+ex.StackTrace+" (query was "+query+")");
				}
				
			}
			return config;
		}*/
		
		private List<TaskNotification> GetBSNotifications(int bsid){
			List<TaskNotification> notifications = new List<TaskNotification>();
			string query = "SELECT status, notifier FROM tasknotifiers WHERE bsid="+bsid;
			using (DbDataReader reader = QueryAndGetReader(query)){
				try{
					while(reader.Read()){
						TaskNotification tn = new TaskNotification((TaskRunningStatus)Enum.Parse(typeof(TaskRunningStatus), reader.GetString(reader.GetOrdinal("status"))),
						                      reader.GetString(reader.GetOrdinal("notifier")));
						notifications.Add(tn);
					}
				}
				catch(Exception ex){
					Logger.Append("HUBRN", Severity.ERROR, ex.Message+"---"+ex.StackTrace+" (query was "+query+")");
				}
				reader.Close();
				reader.Dispose();
			}
			return notifications;
		}	
		
	}
}