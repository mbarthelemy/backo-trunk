using System;
using System.Data.Common;
using P2PBackup.Common;

namespace P2PBackupHub {

	internal partial class DBHandle {

		internal Password GetEncryptedPassword(int id){

			string query = "SELECT id, password FROM  passwords WHERE id="+id;
			Password p = new Password();
			using(DbDataReader reader = QueryAndGetReader(query)){
				while(reader.Read()){
					p.Id = (int)reader.GetInt32(0);
					p.Value = reader.GetString(1);
				}
			}
			return p;
		}

		internal int SavePassword(Password password){
			int id = GetNewId("passwords");
			string query = "INSERT INTO passwords(id, password) VALUES("+id+", '"
				+password.Value+"')";
			ExecuteNonQuery(query);
			return id;
		}
	}
}

