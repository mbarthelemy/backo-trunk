using System;
using System.Data.Common;
using System.Collections.Generic;
using P2PBackup.Common;
using P2PBackupHub.Utilities;
// create table hypervisors(id integer, url varchar, username varchar, password integer, providername varchar);
// alter table hypervisors add constraint pk_hypervisor primary key (id);

namespace P2PBackupHub {

	internal partial class DBHandle {

		internal List<Hypervisor> GetHypervisors(){
			List<Hypervisor> hypervisors = new List<Hypervisor>();

			string query = "SELECT id, name, providername, url, username, password, lastdiscover FROM hypervisors";
			using(DbDataReader reader = QueryAndGetReader(query)){
				while(reader.Read()){
					Hypervisor hv = new Hypervisor();
					hv.Id = (int)reader["id"];
					hv.Name = reader["name"] as String;
					hv.Kind = (string)reader["providername"];
					hv.Url = (string)reader["url"];
					hv.UserName = (string)reader["username"];
					hv.Password = PasswordManager.Get((int)reader["password"]);
					if(! reader.IsDBNull(reader.GetOrdinal("lastdiscover")))
						hv.LastDiscover = Utilities.Utils.GetDateTimeFromUnixTime(reader.GetInt32(reader.GetOrdinal("lastdiscover")));
					hypervisors.Add(hv);
				}
			}
			return hypervisors;
		}

		internal Hypervisor GetHypervisor(int id){
			Hypervisor hv = new Hypervisor();

			string query = "SELECT id, name, providername, url, username, password, lastdiscover FROM hypervisors"
				+" WHERE id="+id;
			using(DbDataReader reader = QueryAndGetReader(query)){
				while(reader.Read()){

					hv.Id = (int)reader["id"];
					hv.Name = reader["name"] as String;
					hv.Kind = (string)reader["providername"];
					hv.Url = (string)reader["url"];
					hv.UserName = (string)reader["username"];
					hv.Password = PasswordManager.Get((int)reader["password"]);
					if(! reader.IsDBNull(reader.GetOrdinal("lastdiscover")))
						hv.LastDiscover = Utilities.Utils.GetDateTimeFromUnixTime(reader.GetInt32(reader.GetOrdinal("lastdiscover")));

				}
			}
			return hv;
		}
	
		internal void AddHypervisor(Hypervisor hv){

			string query = "INSERT INTO hypervisors (id, name, providername, url, username, password, lastdiscover) VALUES("
				+"@id, @name, @prov, @url, @username, @password, 0)";



		}

		internal void UpdateDiscoveryDate(int hypervisorId){
			string query = "UPDATE hypervisors SET lastdiscover="+Utils.GetUnixTimeFromDateTime(DateTime.UtcNow)
				+" WHERE id="+hypervisorId;
			try{
				ExecuteNonQuery(query);
			}
			catch(Exception e){
				Logger.Append("HUBRN", Severity.ERROR, "Could not update Hypervisors discovery : "+e.Message);
			}
		}
	}
}

